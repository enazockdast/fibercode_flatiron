	module A_TIMES_X
	use derivatives 

	implicit none

	contains	

	subroutine ATIMESX(N,X,B,DS)
	IMPLICIT NONE 
	INTEGER, PARAMETER :: dp = KIND(1.0D0)
	INTEGER :: N,I
	real (kind=dp) :: X(:)
	real (kind=dp) :: B(:)
	real (kind=dp) :: DS
	
	!DS=1.0/(N*1.0-1.0)
	!DS=0.10;	
	DO I=2,N-1
	!print *,"I is ...", I
	B(I)=(X(I-1)-2.0*X(I)+X(I+1))/DS**2-5.0*X(I)
	ENDDO
	B(1)=X(1)
	B(N)=X(N)

!	print *,"*********************************"
!	do i=1, N
!	print *, B(i)
!	enddo
!       print *,"*********************************"

	end subroutine ATIMESX

!****************************************************!
!****************************************************!
!****************************************************!
	subroutine ATIMESX_ON(N,N0,n_s,n_half,m,C_T0,X,B)

	IMPLICIT NONE 

	INTEGER, PARAMETER :: dp = KIND(1.0D0)
	INTEGER, intent (IN) :: N,N0,n_s, n_half, m
	integer, parameter :: m0 =2
	real (kind=dp) :: X(:)
	real (kind=dp) :: B(:)
	real (kind=dp) :: C_T0(:,:,:)
	real (kind=dp) :: C_T(m+1,n_s,N0), C_T_B1(m+1,n_s,n_half), C_T_B2(m+1,n_s,n_half)
	real (kind=dp) :: x_ns(m0,N),lf
	C_T=C_T0;
        lf=1.0
	C_T_B1=C_T0(:,:,1:n_half);
	C_T_B2=C_T0(:,:,N0-n_half+1:N0); 
	C_T(:,:,1:n_half)=C_T_B1
	C_T(:,:,N-n_half+1:N)=C_T_B2; 
	call deriv_O_n(X(1:N),N,n_s,C_T(:,:,1:N),m0, x_ns(1:m0,1:N),lf);
	B(1:N)=x_ns(2,1:N)-0.0*X(1:N)
	B(1)=X(1)
	B(N)=X(N)

	end subroutine ATIMESX_ON

!****************************************************!
!****************************************************!
!****************************************************!

	subroutine sparse_build_diffusion (N,DS,ia,ja,a)

	implicit none

	INTEGER, PARAMETER :: dp = KIND(1.0D0)

        integer :: ia(:),ja(:),N,i
	real (kind=dp)  :: DS,a(:)

	ia(1)=1
	do i=2,N-1
	ia(i)=ia(i-1)+3

	ja(3*(i-1)+1)=i-1
	ja(3*(i-1)+2)=i
	ja(3*(i-1)+3)=i+1

	a(3*(i-1)+1)=1.0/DS**2
	a(3*(i-1)+2)=-2.0/DS**2
	a(3*(i-1)+3)=1.0/DS**2
	
	
	enddo

	
	ia(N)=ia(N-1)+3		
	ia(N+1)=ia(N)+3
	ja(1)=1;ja(2)=2;ja(3)=3
	ja(3*(N-1)+1)=N-2;ja(3*(N-1)+2)=N-1;ja(3*N)=N
	a(1:3)=0.0;a(1)=1.0;a(3*(N-1)+1:3*N)=0.0;a(3*N)=1.0;
	end subroutine sparse_build_diffusion
 
!***************************************************************!
!***************************************************************!
!***************************************************************!

	subroutine sparse_build_diffusion_ON(N,N0,n_s,n_half,m,C_T0,ia,ja,a,LF)
	implicit none 

	INTEGER, PARAMETER :: dp = KIND(1.0D0)

        integer, intent (IN) :: N0,N,m, n_s, n_half
	integer :: ia(:),ja(:)
	real (kind=dp)  :: a(:),LF
	real (kind=dp), intent (IN) :: C_T0(:,:,:)
	real (kind=dp) :: C_T(m+1,n_s,N0), C_T_B1(m+1,n_s,n_half), C_T_B2(m+1,n_s,n_half)
	integer :: i,j

	C_T=C_T0;

!	C_T_B1=C_T0(:,:,1:n_half);
!	C_T_B2=C_T0(:,:,N0-n_half+1:N0); 
!	C_T(:,:,1:n_half)=C_T_B1
!	C_T(:,:,N-n_half+1:N)=C_T_B2; 



	do i=1,n_half
	ia(i)=n_s*(i-1)+1
	do j=1,n_s
	ja((i-1)*n_s+j)=j
	a((i-1)*n_s+j)=C_T(3,j,i)/LF**2
	enddo
	enddo

	do i=n_half+1,N-n_half
	ia(i)=n_s+ia(i-1)
	do j=1,n_s
	ja((i-1)*n_s+j)=i-n_half+j-1
	a((i-1)*n_s+j)=C_T(3,j,i)/LF**2
	enddo
	enddo
	
	do i=N-n_half+1,N
	ia(i)=n_s+ia(i-1)
	do j=1,n_s
	ja((i-1)*n_s+j)=N-n_s+j
	a((i-1)*n_s+j)=C_T(3,j,i)/LF**2
	enddo
	enddo	
	a(1:n_s)=0.0;a(1)=1.0
!        a(1:n_s)=C_T(2,1:n_s,1)
        a(n_s*(N-1)+1:n_s*N)=0.0;a(n_s*N)=1.0
			
	ia(N+1)=ia(N)+n_s
	!ja(n_s*(N-1)+1)=N-2;ja(3*(N-1)+2)=N-1;ja(3*N)=N
	!a(1:3)=0.0;a(1)=1.0;a(3*(N-1)+1:3*N)=0.0;a(3*N)=1.0;
	end subroutine sparse_build_diffusion_ON 


!**********************************************************!
!**********************************************************!


        subroutine sparse_build_implicit(N,N0,n_s,n_half,m,C_T0,ia,ja,a,LF,mu,TN,xns,yns,zns,dt0)
        implicit none

        INTEGER, PARAMETER :: dp = KIND(1.0D0)

        integer, intent (IN) :: N0,N,m, n_s, n_half
        real (kind=dp), intent(in) :: dt0
        integer :: ia(:),ja(:)
        real (kind=dp)  :: a(:),LF,mu,TN
        real (kind=dp), intent (IN) :: C_T0(:,:,:),xns(:,:),yns(:,:),zns(:,:)
        real (kind=dp) :: C_T(m+1,n_s,N0)
        integer :: i,j,ii,kk,j0,j1,j2,j3,j4,k2

        C_T=C_T0;

        a(:)=0.0;ja(:)=0;ia(:)=0

        do i=1,n_half
        ia(i)=n_s*(i-1)+1
        do j=1,n_s
        ja((i-1)*n_s+j)=j
        a((i-1)*n_s+j)=C_T(3,j,i)/LF**2!-C_T(1,j,i)*dot1/2.0
        enddo
        enddo

        do i=n_half+1,N-n_half
        ia(i)=n_s+ia(i-1)
        do j=1,n_s
        ja((i-1)*n_s+j)=i-n_half+j-1
        a((i-1)*n_s+j)=C_T(3,j,i)/LF**2!-C_T(1,j,i)*dot1/2.0
        enddo
        enddo
        
        do i=N-n_half+1,N
        ia(i)=n_s+ia(i-1)
        do j=1,n_s
        ja((i-1)*n_s+j)=N-n_s+j
        a((i-1)*n_s+j)=C_T(3,j,i)/LF**2!-C_T(1,j,i)*dot1(i)/2.0
        enddo
        enddo

        
   
        a(1:n_s)=C_T(2,1:n_s,1);a(n_s*(N-1)+1:n_s*N)=0.0;a(n_s*N)=1.0
                        
!        ia(N+1)=ia(N)+n_s

!        goto 200

        C_T=C_T0;

!       C_T(:,:,N-n_half+1:N)=C_T_B2(:,:,1:n_half);
        do kk=1,3*N+1
        ia(kk+N0)=n_s*N0+(kk-1)*3*n_s+1    
        enddo
        do kk=3,N-2;
        
           do k2=1,3;
                
            do ii=1,n_s;
                
               j0=k2+(kk-1)*3;
               if (kk<n_half+1) then;
                   j2=1+(ii-1)*3;
                
               elseif ( (kk>n_half) .and. (kk<N-n_half+1)) then
                   j2=j0+(ii-n_half-1)*3-k2+1;
                
               else
                   j2=3*(N-n_s+ii-1)+1;
                
               endif 
               j3=(kk-1)*3*n_s*3+(k2-1)*3*n_s+1+(ii-1)*3;
                
               if ( k2 .eq. 1) then
        
               ja(j3+n_s*N0)=j2+N0;
               a(j3+n_s*N0)=1.0*C_T(5,ii,kk)*(1.0+xns(1,kk)*xns(1,kk))/LF**4 !-beta(nf)*mu(nf)*C_T(2,i,k)*s(k)/L_F(nf);
               a(j3+1+n_s*N0)=1.0*C_T(5,ii,kk)*(0.0+yns(1,kk)*xns(1,kk))/LF**4;
               a(j3+2+n_s*N0)=1.0*C_T(5,ii,kk)*(0.0+zns(1,kk)*xns(1,kk))/LF**4;
               ja(j3+1+n_s*N0)=j2+1+N0;
               ja(j3+2+n_s*N0)=j2+2+N0;

        

               elseif (k2 .eq. 2) then
               a(j3+0+n_s*N0)=1.0*C_T(5,ii,kk)*(0.0+yns(1,kk)*xns(1,kk))/LF**4;
               a(j3+1+n_s*N0)=1.0*C_T(5,ii,kk)*(1.0+yns(1,kk)*yns(1,kk))/LF**4!-beta(nf)*mu(nf)*C_T(2,i,k)*s(k)/L_FF(nf);
               a(j3+2+n_s*N0)=1.0*C_T(5,ii,kk)*(0.0+yns(1,kk)*zns(1,kk))/LF**4;
               ja(j3+0+n_s*N0)=j2+0+N0;
               ja(j3+1+n_s*N0)=j2+1+N0;
               ja(j3+2+n_s*N0)=j2+2+N0;
        
               else
               a(j3+0+n_s*N0)=1.0*C_T(5,ii,kk)*(0.0+xns(1,kk)*zns(1,kk))/LF**4;
               a(j3+1+n_s*N0)=1.0*C_T(5,ii,kk)*(0.0+yns(1,kk)*zns(1,kk))/LF**4;
               a(j3+2+n_s*N0)=1.0*C_T(5,ii,kk)*(1.0+zns(1,kk)*zns(1,kk))/LF**4!-beta(nf)*mu(nf)*C_T(2,i,k)*s(k)/L_FF(nf);
               ja(j3+1+n_s*N0)=j2+1+N0;
               ja(j3+2+n_s*N0)=j2+2+N0;
               ja(j3+0+n_s*N0)=j2+0+N0;
                
               endif ;
        
            enddo;
          enddo;

        
               if (kk<n_half+1) then;
                
                   j4=(kk-1)*3*3*n_s+(kk-1)*3+1;
               elseif ( (kk>n_half) .and. (kk<N-n_half+1))  then
                   j4=(kk-1)*3*3*n_s+n_half*3+1;
               else             
                   j4=(kk-1)*3*3*n_s+(kk-N+n_s-1)*3+1;                          
               endif

          a(j4+n_s*N0)=1.0*a(j4+n_s*N0)+mu/dt0;
          a(j4+n_s*3+1+n_s*N0)=1.0*a(j4+n_s*3+1+n_s*N0)+mu/dt0;
          a(j4+n_s*3*2+2+n_s*N0)=1.0*a(j4+n_s*3*2+2+n_s*N0)+mu/dt0;
        
         enddo;


           kk=1;
          do ii=1,3*n_s
                ja(ii+n_s*N0)=ii+N0
                ja(ii+3*n_s+n_s*N0)=ii+N0
                ja(ii+3*2*n_s+n_s*N0)=ii+N0
          enddo
                
          a(1+n_s*N0:3*n_s*3+n_s*N0)=0.0;
          a(1+n_s*N0) = 1.0/dt0;
          ja(1+n_s*N0)=1+N0;
          a(3*n_s+2+n_s*N0)=1.0/dt0;
          ja(3*n_s+2+n_s*N0)=2+N0;
          a(3*n_s*2+3+n_s*N0)=1.0/dt0;
          ja(3*n_s*2+3+n_s*N0)=3+N0;


! *********** imposing direction of MT rotates with angular rotation of the Spindle ******* !

          do ii=1,3*n_s
                ja(ii+3*3*n_s+n_s*N0)=ii+N0
                ja(ii+3*3*n_s+3*n_s+n_s*N0)=ii+N0
                ja(ii+3*3*n_s+3*2*n_s+n_s*N0)=ii+N0
          enddo
          a(n_s*N0+3*n_s*3+1:2*3*n_s*3+n_s*N0)=0.0;
          kk=2;
          a(3*n_s*3+3+1+n_s*N0) = 1.0/dt0;
          ja(3*n_s*3+3+1+n_s*N0)=4+N0;
          a(3*n_s*3+3*n_s+3+2+n_s*N0)=1.0/dt0;
          ja(3*n_s*3+3*n_s+3+2+n_s*N0)=5+N0;
          a(3*n_s*3+3*n_s*2+3+3+n_s*N0)=1.0/dt0;
          ja(3*n_s*3+3*n_s*2+3+3+n_s*N0)=6+N0;

!********************BC3**********************!
!*********************************************!
! ******** Torque is zero at the end ******** !

           kk = N-1;
           do ii=1,n_s;
               j1=(kk-1)*3*n_s*3+1+(ii-1)*3;
               a(j1+n_s*N0)=C_T(3,ii,kk)/LF**2;
               ja(j1+0+n_s*N0)=(N-n_s)*3+(ii-1)*3+1+N0;
               ja(j1+1+n_s*N0)=(N-n_s)*3+(ii-1)*3+2+N0;
               ja(j1+2+n_s*N0)=(N-n_s)*3+(ii-1)*3+3+N0;

           j1=(kk-1)*3*n_s*3+2+(ii-1)*3+3*n_s;
           a(j1+n_s*N0)=C_T(3,ii,kk)/LF**2;
           ja(j1-1+n_s*N0)=(N-n_s)*3+(ii-1)*3+1+N0;
           ja(j1+0+n_s*N0)=(N-n_s)*3+(ii-1)*3+2+N0;
           ja(j1+1+n_s*N0)=(N-n_s)*3+(ii-1)*3+3+N0;


           j1=(kk-1)*3*n_s*3+3+(ii-1)*3+3*n_s*2;
           a(j1+n_s*N0)=C_T(3,ii,kk)/LF**2;
           ja(j1-2+n_s*N0)=(N-n_s)*3+(ii-1)*3+1+N0;
           ja(j1-1+n_s*N0)=(N-n_s)*3+(ii-1)*3+2+N0;
           ja(j1+0+n_s*N0)=(N-n_s)*3+(ii-1)*3+3+N0;

           enddo;

            kk=N

            do ii=1,n_s;
              j1=(kk-1)*3*n_s*3+1+(ii-1)*3+0*3*n_s;
              a(j1+n_s*N0)=-C_T(4,ii,kk)/LF**3+TN*C_T(2,ii,kk)/LF;
              ja(j1+0+n_s*N0)=(N-n_s)*3+(ii-1)*3+1+N0;
              ja(j1+1+n_s*N0)=(N-n_s)*3+(ii-1)*3+2+N0;
              ja(j1+2+n_s*N0)=(N-n_s)*3+(ii-1)*3+3+N0;

              j1=(kk-1)*3*n_s*3+2+(ii-1)*3+1*3*n_s;
              a(j1+n_s*N0)=-C_T(4,ii,kk)/LF**3+TN*C_T(2,ii,kk)/LF;
              ja(j1-1+n_s*N0)=(N-n_s)*3+(ii-1)*3+1+N0;
              ja(j1+0+n_s*N0)=(N-n_s)*3+(ii-1)*3+2+N0;
              ja(j1+1+n_s*N0)=(N-n_s)*3+(ii-1)*3+3+N0;

              j1=(kk-1)*3*n_s*3+3+(ii-1)*3+2*3*n_s;
              a(j1+n_s*N0)=-C_T(4,ii,kk)/LF**3+TN*C_T(2,ii,kk)/LF;
              ja(j1-2+n_s*N0)=(N-n_s)*3+(ii-1)*3+1+N0;
              ja(j1-1+n_s*N0)=(N-n_s)*3+(ii-1)*3+2+N0;
              ja(j1+0+n_s*N0)=(N-n_s)*3+(ii-1)*3+3+N0;

            enddo;


           ! do ii=1,10*N0*n_s
           !     if (ja(ii) .eq. 0) then
           !     print *,"STH is wrong in sparse",ii
           !     endif
           ! enddo

!200     continue 

        end subroutine sparse_build_implicit

!*************************************************************!
!*************************************************************!
!*************************************************************!
 subroutine sparse_build_implicit_2(N,N0,n_s,n_half,m,C_T0,ia,ja,a,&
            & fac,stab0,stab,beta0,s,LF,mu0,mu,a_2,xns,yns,zns,dt0)
        implicit none

        INTEGER, PARAMETER :: dp = KIND(1.0D0)

        integer, intent (IN) :: N0,N,m, n_s, n_half
        real (kind=dp), intent(in) :: dt0,a_2,s(:)
        integer :: ia(:),ja(:)
        real (kind=dp)  :: a(:),beta0,LF,mu,fac,stab0,stab,mu0
        real (kind=dp), intent (IN) :: C_T0(:,:,:),xns(:,:),yns(:,:),zns(:,:)
        real (kind=dp) :: C_T(m+1,n_s,N0)
        integer :: i,j,ii,kk,j0,j1,j2,j3,j4,k2
        real (kind=dp) :: dot(N0)
        C_T=C_T0;
        dot(1:N0)=(xns(2,1:N0)**2+yns(2,1:N0)**2+zns(2,1:N0)**2);


        a(:)=0.0;ja(:)=0;ia(:)=0



        do kk=1,4*N+1
        ia(kk)=(kk-1)*4*n_s+1
        enddo

        
        do kk=1,N0

          do k2=1,4

            do ii=1,n_s

                if(k2 .eq. 1) then

!**********************************************!
!*************Tension Equation*****************!
!**********************************************!
                   j1=(kk-1)*16*n_s+(ii-1)*4+1
                   j2=j1+1
                   j3=j1+2
                   j4=j1+3

                   a(j1)=C_T(3,ii,kk)/LF**2-C_T(1,ii,kk)*dot(kk)/2.0
                   a(j2)=(6.0*xns(3,kk)*C_T(4,ii,kk)/LF**3+7.0*xns(2,kk)*C_T(5,ii,kk)/LF**4)/2.0
                   a(j3)=(6.0*yns(3,kk)*C_T(4,ii,kk)/LF**3+7.0*yns(2,kk)*C_T(5,ii,kk)/LF**4)/2.0
                   a(j4)=(6.0*zns(3,kk)*C_T(4,ii,kk)/LF**3+7.0*zns(2,kk)*C_T(5,ii,kk)/LF**4)/2.0
    
                    if (kk<n_half+1) then                      
                      ja(j1)=(ii-1)*4+1
                    elseif (kk<(N0-n_half+1)) then
                      ja(j1)=(kk-n_half-1)*4+(ii-1)*4+1
                    else
                      ja(j1)=(N0-n_s+ii-1)*4+1
                    endif

                      ja(j2)=ja(j1)+1
                      ja(j3)=ja(j1)+2
                      ja(j4)=ja(j1)+3

!***********************************************!
!************** X direction ********************!
!***********************************************!
                elseif (k2 .eq. 2) then

                    j1=(kk-1)*16*n_s+4*n_s+(ii-1)*4+1
                    j2=j1+1
                    j3=j1+2
                    j4=j1+3 
                    
                    a(j1)=-C_T(1,ii,kk)*xns(2,kk)-C_T(2,ii,kk)*xns(1,kk)/LF
                    a(j2)=1.0*C_T(5,ii,kk)*(1.0+stab+xns(1,kk)*xns(1,kk))/LF**4-&
                    & beta0*s(i)*C_T(2,ii,kk)/LF+&
                    & 2.0d0*stab0*fac*s(kk)*C_T(2,ii,kk)*(mu0)/LF+&
                    & C_T(1,ii,kk)*mu*a_2/dt0
                        
                    a(j3)=1.0*C_T(5,ii,kk)*(0.0+yns(1,kk)*xns(1,kk))/LF**4
                    a(j4)=1.0*C_T(5,ii,kk)*(0.0+zns(1,kk)*xns(1,kk))/LF**4

                       if (kk<n_half+1) then
                      ja(j1)=(ii-1)*4+1
                    elseif (kk<(N0-n_half+1)) then
                      ja(j1)=(kk-n_half-1)*4+(ii-1)*4+1
                    else
                      ja(j1)=(N0-n_s+ii-1)*4+1
                    endif

                      ja(j2)=ja(j1)+1
                      ja(j3)=ja(j1)+2
                      ja(j4)=ja(j1)+3 


!************************************************************!
!********************** Y direction *************************!
!************************************************************!
                elseif (k2 .eq. 3) then

                    j1=(kk-1)*16*n_s+2*4*n_s+(ii-1)*4+1
                    j2=j1+1
                    j3=j1+2
                    j4=j1+3    

                    a(j1)=-C_T(1,ii,kk)*yns(2,kk)-C_T(2,ii,kk)*yns(1,kk)/LF
                    a(j2)=1.0*C_T(5,ii,kk)*(0.0+xns(1,kk)*yns(1,kk))/LF**4
                    a(j3)=1.0*C_T(5,ii,kk)*(1.0+stab+yns(1,kk)*yns(1,kk))/LF**4-&
                    & beta0*s(i)*C_T(2,ii,kk)/LF+&
                    & 2.0d0*stab0*fac*s(kk)*C_T(2,ii,kk)*(mu0)/LF+&
                    & C_T(1,ii,kk)*mu*a_2/dt0
                     
                    a(j4)=1.0*C_T(5,ii,kk)*(0.0+zns(1,kk)*yns(1,kk))/LF**4

                    if (kk<n_half+1) then
                      ja(j1)=(ii-1)*4+1
                    elseif (kk<(N0-n_half+1)) then
                      ja(j1)=(kk-n_half-1)*4+(ii-1)*4+1
                    else
                      ja(j1)=(N0-n_s+ii-1)*4+1
                    endif

                      ja(j2)=ja(j1)+1
                      ja(j3)=ja(j1)+2
                      ja(j4)=ja(j1)+3

!************************************************************!
!********************** Z direction *************************!
!************************************************************!
                else

                    j1=(kk-1)*16*n_s+3*4*n_s+(ii-1)*4+1
                    j2=j1+1
                    j3=j1+2
                    j4=j1+3
        
                    a(j1)=-C_T(1,ii,kk)*zns(2,kk)-C_T(2,ii,kk)*zns(1,kk)/LF
                    a(j2)=1.0*C_T(5,ii,kk)*(0.0+xns(1,kk)*zns(1,kk))/LF**4
                    a(j3)=1.0*C_T(5,ii,kk)*(0.0+yns(1,kk)*zns(1,kk))/LF**4
                    a(j4)=1.0*C_T(5,ii,kk)*(1.0+stab+zns(1,kk)*zns(1,kk))/LF**4-&
                    & beta0*s(kk)*C_T(2,ii,kk)/LF+&
                    & 2.0d0*stab0*fac*s(kk)*C_T(2,ii,kk)*(mu0)/LF+&
                    & C_T(1,ii,kk)*mu*a_2/dt0

                    if (kk<n_half+1) then
                      ja(j1)=(ii-1)*4+1
                    elseif (kk<(N0-n_half+1)) then
                      ja(j1)=(kk-n_half-1)*4+(ii-1)*4+1
                    else
                      ja(j1)=(N0-n_s+ii-1)*4+1
                    endif

                      ja(j2)=ja(j1)+1
                      ja(j3)=ja(j1)+2
                      ja(j4)=ja(j1)+3

                endif 


            enddo
          
          enddo

        enddo


!**********************************************************!
!***************** Boundary Conditions*********************!
!**********************************************************!

!************* *********Tension BCs***********************!
!**************** ZERO derivative at attachment point *****!
!********* T_{s}=0.0***************************************!
        a(1:4*n_s)=0.0
        do ii=1,n_s
        a((ii-1)*4+1)=C_T(2,ii,1)/LF
        a((ii-1)*4+2)=3.0*C_T(4,ii,1)*xns(2,1)/LF**3
        a((ii-1)*4+3)=3.0*C_T(4,ii,1)*yns(2,1)/LF**3
        a((ii-1)*4+4)=3.0*C_T(4,ii,1)*zns(2,1)/LF**3
        enddo
        
!**********************************************************!
!************* T(L)* x_{s}(L)-x_{sss}=F
!**** since torque is zero at the endpoint ---> x_{ss}=0 ****!
! ---> T(L)=F.x_{s}*****************************************!

        a((N0-1)*16*n_s+1:(N0-1)*16*n_s+4*n_s)=0.0
        do ii=1,n_s
        a((N0-1)*16*n_s+(ii-1)*4+1)=C_T(1,ii,N0)
        enddo
!*******************position BCs **************************!
!****************** Attachment points *********************!
        a(4*n_s+1:16*n_s)=0.0
        do ii=1,n_s
        a((ii-1)*4+2+4*1*n_s)=-beta0*s(1)*C_T(2,ii,1)/LF
        a((ii-1)*4+3+4*2*n_s)=-beta0*s(1)*C_T(2,ii,1)/LF
        a((ii-1)*4+4+4*3*n_s)=-beta0*s(1)*C_T(2,ii,1)/LF
        enddo

        a(4*1*n_s+2)=a_2/dt0+a(4*1*n_s+2)
        a(4*2*n_s+3)=a_2/dt0+a(4*2*n_s+3)
        a(4*3*n_s+4)=a_2/dt0+a(4*3*n_s+4)

        a(16*n_s+n_s*4*1+1:16*n_s*2)=0.0
        do ii=1,n_s

        a(16*n_s+(ii-1)*4+2+4*1*n_s)=-beta0*s(2)*C_T(2,ii,2)/LF!+&
!!        & 2.0d0*20.0d0*5.0*s(2)*C_T(2,ii,2)*(mu0/mu)/LF

        a(16*n_s+(ii-1)*4+3+4*2*n_s)=-beta0*s(2)*C_T(2,ii,2)/LF!+&
!!        & 2.0d0*20.0d0*5.0d0*s(2)*C_T(2,ii,2)*(mu0/mu)/LF

        a(16*n_s+(ii-1)*4+4+4*3*n_s)=-beta0*s(2)*C_T(2,ii,2)/LF!+&
!!        & 2.0d0*20.0d0*5.0d0*s(2)*C_T(2,ii,2)*(mu0/mu)/LF
        enddo

!        do ii=1,n_s
!        a(16*n_s+(ii-1)*4+2+4*1*n_s)=C_T(2,ii,1)/(LF*dt0)
!        a(16*n_s+(ii-1)*4+3+4*2*n_s)=C_T(2,ii,1)/(LF*dt0)
!        a(16*n_s+(ii-1)*4+4+4*3*n_s)=C_T(2,ii,1)/(LF*dt0)
!        enddo

        a(16*n_s+n_s*4*1+6)=a_2/dt0+a(16*n_s+n_s*4*1+6)
        a(16*n_s+n_s*4*2+7)=a_2/dt0+a(16*n_s+n_s*4*2+7)
        a(16*n_s+n_s*4*3+8)=a_2/dt0+a(16*n_s+n_s*4*3+8)
        

!************ end points *****************************!
!************ zero torque at N-1 point ***************!


        a((N0-2)*16*n_s+4*n_s+1:(N0-2)*16*n_s+4*n_s*4)=0.0
        do ii=1,n_s
        a((N0-2)*16*n_s+4*n_s*1+(ii-1)*4+2)=C_T(3,ii,N0-1)/LF**2
        a((N0-2)*16*n_s+4*n_s*2+(ii-1)*4+3)=C_T(3,ii,N0-1)/LF**2
        a((N0-2)*16*n_s+4*n_s*3+(ii-1)*4+4)=C_T(3,ii,N0-1)/LF**2
        enddo

        
!*****************************************************!
! ************  -x_{sss}+T* x_{s}=F ******************!

        a((N0-1)*16*n_s+4*n_s+1:N0*16*n_s)=0.0
        do ii=1,n_s
        a((N0-1)*16*n_s+4*n_s*1+(ii-1)*4+2)=-C_T(4,ii,N0)/LF**3
        a((N0-1)*16*n_s+4*n_s*1+(ii-1)*4+1)=C_T(1,ii,N0)*xns(1,N0)
        
        a((N0-1)*16*n_s+4*n_s*2+(ii-1)*4+3)=-C_T(4,ii,N0)/LF**3
        a((N0-1)*16*n_s+4*n_s*2+(ii-1)*4+1)=C_T(1,ii,N0)*yns(1,N0)

        a((N0-1)*16*n_s+4*n_s*3+(ii-1)*4+4)=-C_T(4,ii,N0)/LF**3
        a((N0-1)*16*n_s+4*n_s*3+(ii-1)*4+1)=C_T(1,ii,N0)*zns(1,N0)
        enddo


        do ii=1,n_s*16*N0
        if (ja(ii) .eq. 0) then
        print *,"not right ",ii
        endif
        enddo
!*****************************************************!
!*****************************************************!

        end subroutine sparse_build_implicit_2



!*************************************************************!
!*************************************************************!

        subroutine inextensibility(N0,n_s,n_half,a_3,s,mu0,dt2,C_T0,LF,alpha &
        & ,ia,ja,a)

        implicit none
        
        INTEGER, PARAMETER :: dp = KIND(1.0D0)
        real (kind=dp), intent (IN), dimension (:)  :: alpha,s
        real (kind=dp), intent (IN) :: C_T0(:,:,:),dt2,LF,mu0
        integer, intent (IN) :: n_s,n_half,N0
        real (kind=dp) :: a(:),a_3
        integer :: ja(:),ia(:)
        integer :: kk,ii,j1

          a(:)=0.0;

          do kk=1,N0+1
          ia(kk)=(kk-1)*n_s+1
          enddo
        

           do kk=1,N0
                   do ii=1,n_s

                

                    j1=(kk-1)*n_s+ii

                    if (kk<n_half+1) then
                      ja(j1)=ii
                    elseif (kk<(N0-n_half+1)) then
                      ja(j1)=(kk-n_half-1)+ii
                       
                    else
                      ja(j1)=N0-n_s+ii
                    endif

                    a(j1)=a_3*C_T0(1,ii,kk)/(dt2)+(-20.0d0*mu0*s(kk)*alpha(kk)+20.0d0*mu0)*C_T0(2,ii,kk)/LF
                  enddo
            enddo

        a(1:n_s)=0.0;a(1)=1.0;

        end subroutine inextensibility 


	end module A_TIMES_X
