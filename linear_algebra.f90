module linear_algebra
  implicit none
contains
  
  subroutine inv(N,a,ia,info)
    implicit none
    INTEGER, PARAMETER :: dp = KIND(1.0D0)  
    integer :: info
    integer, intent (IN) :: N
    real (kind=dp), intent (IN) :: a(:,:)
    real (kind=dp) :: work(N)
    real (kind=dp), intent (OUT) :: ia(:,:)
    integer :: ipvt(N), lwork
    lwork=max(300,N)
    ia(1:N,1:N)=a(1:N,1:N);


    call dgetrf( N, N, ia, N, ipvt, info )

    ! print *, "info is subroutine chkpt 1 is ", info 
    call dgetri( N, ia, N, ipvt, work, lwork, info )
    ! print *, "info is subroutine chkpt 2 is ", info 

  end subroutine inv

 !********************************************************!
 !********************************************************!
 !********************************************************!

  subroutine cross(a,b,c)

    INTEGER, PARAMETER :: dp = KIND(1.0D0)
    real (kind=dp) :: a(:), b(:), c(:)

    c(1)=a(2)*b(3)-a(3)*b(2)
    c(2)=a(1)*b(3)+a(3)*b(1)  
    c(3)=a(1)*b(2)-a(2)*b(1)

  end subroutine cross

 !********************************************************!
 !********************************************************!
 !********************************************************!
 ! CLENCURT  nodes x (Chebyshev points) and weights w
 !          for Clenshaw-Curtis quadrature

  subroutine  clencurt(N,x,w)
    integer, intent (in) :: N
    real*8 :: theta(N+1),v(N-1),pi
    real*8 :: x(:),w(:)
    integer :: i,k

    pi=atan(1.0d0)*4.0d0      
    do i=1,N+1
       theta(i) = pi*(i*1.0d0-1.0d0)/N; 
    enddo
    x = cos(theta);
    w(:) = 0.0d0;
    v(1:N-1)=1.0d0
    v(1:N-1) =1.0d0;
    if (mod(N,2) .eq. 0) then 
       w(1) = 1.0d0/(N**2*1.0d0-1.0d0); w(N+1) = w(1);

       do k=1,N/2-1;
          v(1:N-1) = v(1:N-1) - 2.0d0*cos(2.0d0*k*theta(2:N))/(4.0d0*k**2-1.0d0); 
       enddo
       v(1:N-1) = v(1:N-1) - cos(N*theta(2:N))/(N**2*1.0d0-1.0d0);
    else
       w(1) = 1.0d0/N**2*1.0d0; w(N+1) = w(1);
       do k=1,(N-1)/2;
          v(1:N-1) = v(1:N-1) - 2.0d0*cos(2.0d0*k*theta(2:N))/(4.0d0*k**2-1.0d0); 
       enddo
    endif
    w(2:N) = 2.0d0*v(1:N-1)/N*1.0d0;

  end subroutine clencurt


end module linear_algebra
