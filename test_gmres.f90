	program test_gmres
	
	use  Tension 
	use  A_TIMES_X
	use  sparse_solve
	implicit none
	INCLUDE "mkl_rci.fi"
	integer, parameter :: N0=200, N_f=3, n_s=5, n_half=nint((n_s*1.0-1.0)/2.0), m=4
	real*8, parameter :: ds=1.0/((N0-1)*1.0)
	real*8 :: RHS(N_f*N0), x(N_f*N0), s(N0)
	real*8 :: a(n_s*N0,N_f)
	real*8 :: C_T0(m+1,n_s,1:N0)
	integer :: N_var(N_f)
	integer :: ja(N0*n_s,N_f)
	integer :: ia(N0+1,N_f)
	integer :: nf,i, N, k, N_T 

	N_var(1)=100;N_var(2)=150;N_var(3)=200;
	RHS(:)=0.0;
	k=0
	do i=1,N0
	s(i)=(i-1)*ds
	enddo

	call weights2(N0,n_s,n_half,m+1,s,C_T0) 

	do nf=1,N_f
	N=N_var(nf)
	RHS(1+k)=1.0;
	RHS(k+N)=1.0
	k=k+N
	call sparse_build_diffusion_ON(N,N0,n_s,n_half,m,&
	& C_T0,ia(1:N+1,nf),ja(1:N*n_s,nf),a(1:N*n_s,nf))
	enddo 
	N_T=k;

	print *, "CHPT  1 "
        do i=1,N_var(1)*n_s
	print *, ja(i,1)
	enddo
	call Tension_GMRES (N_var,N0, N_f, N_T,n_s,n_half, m,C_T0, ds, &
	      RHS, ia, ja, a,  x(1:N_T))

	do i=1, N_T
!
	print *, x(i)
!
	enddo


	end program test_gmres
