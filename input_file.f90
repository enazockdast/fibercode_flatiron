
module input_file
  implicit none
  !***************************************************!
  !*************Simulation parameters*****************!
  !***************************************************!
  INTEGER, PARAMETER :: dp = KIND(1.0D0)
  real (kind=dp)     :: dt0=1.00E-3 !DELTA_T
  real (kind=dp), parameter :: t_final=5.0  !T_FINAL
  !***************************************************!
  !*************** Fiber parameters*******************!
  !***************************************************!
  integer, parameter :: N0=31    !N0 ....
  integer, parameter :: N_f=100  !number of fibers on each processor
  integer, parameter :: nprocs=1  !number of processors
  integer, parameter ::N_f_T=N_f*nprocs ! total number of fibers
  real (kind=dp), parameter :: L0=1.0 !Largest Length
  integer, parameter :: n_s=7 !n_s
  integer, parameter :: n_t=2 !n_t
  real (kind=dp), parameter :: E_MT=1.0d0*10.0e-12! N (micron)^2 elastic modulus
  real (kind=dp), parameter :: F_stall=1.0e-12 !N  Stall force of Dynein
  real (kind=dp), parameter :: eta_cyt=1.0d0 ! cytoplasmic viscosity pa.sec

  !***************************************************!
  !*************Spindle & Cortext parameters**********!
  !***************************************************!
  integer, parameter :: nt_SP=43        !nt_SP
  integer, parameter :: ntt_SP=24       !ntt_SP
  integer, parameter :: nt_cor=63       !nt_cor
  integer, parameter :: ntt_cor=34      !ntt_cor
  real (kind=dp), parameter :: a_SP0=5.0d0 !micron: size of nucleus in  dimensional form
  real (kind=dp), parameter  :: a_SP=1.0d0        !a0_SP
  real (kind=dp), parameter  :: b_SP=1.0d0        !b0_SP
  real (kind=dp), parameter  :: a_cor=5.0d0      !a0_cor 
  real (kind=dp), parameter  :: b_cor=5.0d0      !b0_cor
  integer, parameter :: gauss_n=3       !gauss_n
  real (kind=dp), parameter  :: gr=1.00001  !gr

  !**************************************************!
  real (kind=dp), parameter :: pi = 4.0d0 * datan(1.0d0)
  ! \Delta =1 is tau_s sec in real time
  real (kind=dp), parameter :: tau_s=1.0d0/((E_MT/a_SP0**2)/(6.0d0*pi*eta_cyt*a_SP0**2*1.0e-12))
  !**************************************************!
  !**************************************************!

end module input_file
