

      PROGRAM Diffusion_fgmres
      USE sparse_solve
      USE A_TIMES_X

      IMPLICIT NONE
      INCLUDE "mkl_rci.fi"

      
      INTEGER ::N
      integer  :: Nnz
      INTEGER, PARAMETER :: SIZE0=128

      INTEGER,allocatable :: IPAR(:)
      INTEGER,allocatable :: IA(:),JA(:)
	
      REAL*8, allocatable :: DPAR(:), TMP(:)!TMP(N*(2*N+1)+(N*(N+9))/2+1)
      REAL*8, allocatable :: A(:)
     ! DOUBLE PRECISION EXPECTED_SOLUTION(N)
  
      REAL*8, allocatable :: RHS(:), B(:),B2(:),B3(:)
      REAL*8,allocatable  :: COMPUTED_SOLUTION(:),x(:)
      REAL*8,allocatable ::  RESIDUAL(:)
!---------------------------------------------------------------------------
! Some additional variables to use with the RCI (P)FGMRES solver
!---------------------------------------------------------------------------
      INTEGER :: ITERCOUNT 

      INTEGER :: RCI_REQUEST, I
      DOUBLE PRECISION :: DVAR
      REAL*8 :: DS
!---------------------------------------------------------------------------
! An external BLAS function is taken from MKL BLAS to use
! with the RCI (P)FGMRES solver
!---------------------------------------------------------------------------
      DOUBLE PRECISION :: DNRM2
      EXTERNAL DNRM2
	n=201;Nnz=3*n

      allocate (IPAR(SIZE0), DPAR(SIZE0),ia(n+1),ja(Nnz),a(Nnz))
      allocate(RHS(n),B(n),B2(n),B3(n),x(n),computed_solution(n),residual(n)) 
      allocate(TMP(N*(2*N+1)+(N*(N+9))/2+1))

      
!---------------------------------------------------------------------------
! Produce and save the right-hand side in vector B for future use
!---------------------------------------------------------------------------
	DS=1.0/(N*1.0-1.0)
	RHS(2:N-1)=0.0;RHS(1)=1.0;RHS(N)=1.0;
	B(:)=RHS(:)
	B2(:)=RHS(:)

!----------------------------------------------------------------
!  Produce the sparse matrix $\Delta (T) =0$  for preconditioner  
        call sparse_build_diffusion (n,DS,ia,ja,a)
	call sparse_solve_1(n,ia,ja,a,b,x)
!-------------------------------------------------------------
!---------------------------------------------------------------------------
! Initialize the initial guess
!---------------------------------------------------------------------------
      DO I=1,N
        COMPUTED_SOLUTION(I)=x(I)
      ENDDO


!---------------------------------------------------------------------------
! Initialize the solver
!---------------------------------------------------------------------------
      CALL DFGMRES_INIT(N, COMPUTED_SOLUTION, RHS, RCI_REQUEST, IPAR, &
     & DPAR, TMP)
      IF (RCI_REQUEST.NE.0) GOTO 999
!---------------------------------------------------------------------------
! Set the desired parameters:
! do the restart after 2 iterations
! LOGICAL parameters:
! do not do the stopping test for the maximal number of iterations
! do the Preconditioned iterations of FGMRES method
! DOUBLE PRECISION parameters
! set the relative tolerance to 1.0D-3 instead of default value 1.0D-6
!---------------------------------------------------------------------------

       IPAR(5)=30
       IPAR(15)=5
       IPAR(8)=1
       IPAR(9)=0
       IPAR(10)=0
       IPAR(11)=1
       IPAR(12)=1
       DPAR(1)=1.0D-9

!---------------------------------------------------------------------------
! Check the correctness and consistency of the newly set parameters
!---------------------------------------------------------------------------
      CALL DFGMRES_CHECK(N, COMPUTED_SOLUTION, RHS, RCI_REQUEST, &
     & IPAR, DPAR, TMP)
      IF (RCI_REQUEST.NE.0) GOTO 999

!---------------------------------------------------------------------------
! Compute the solution by RCI (P)FGMRES solver with preconditioning
! Reverse Communication starts here
!---------------------------------------------------------------------------
1     CALL DFGMRES(N, COMPUTED_SOLUTION, RHS, RCI_REQUEST, IPAR, &
     & DPAR, TMP)

!---------------------------------------------------------------------------
! If RCI_REQUEST=0, then the solution was found with the required precision
!---------------------------------------------------------------------------
      IF (RCI_REQUEST.EQ.0) GOTO 3
!---------------------------------------------------------------------------
! If RCI_REQUEST=1, then compute the vector A*TMP(IPAR(22))
! and put the result in vector TMP(IPAR(23))
!---------------------------------------------------------------------------	

      IF (RCI_REQUEST.EQ.1) THEN
      CALL ATIMESX(N, TMP(IPAR(22):IPAR(22)+N-1),TMP(IPAR(23):IPAR(23)+N-1),DS)

      	GOTO 1
      ENDIF

!---------------------------------------------------------------------------
! If RCI_request=2, then do the user-defined stopping test
! The residual stopping test for the computed solution is performed here
!---------------------------------------------------------------------------
! NOTE: from this point vector B(N) is no longer containing the right-hand
! side of the problem! It contains the current FGMRES approximation to the
! solution. If you need to keep the right-hand side, save it in some other
! vector before the call to DFGMRES routine. Here we saved it in vector
! RHS(N). The vector B is used instead of RHS to preserve the original
! right-hand side of the problem and guarantee the proper restart of FGMRES
! method. Vector B will be altered when computing the residual stopping
! criterion!
!---------------------------------------------------------------------------
!      IF (RCI_REQUEST.EQ.2) THEN
! Request to the DFGMRES_GET routine to put the solution into B(N) via IPAR(13)
!	print *," RCI = 2"
!      	IPAR(13)=1
! Get the current FGMRES solution in the vector B(N)
!      	CALL DFGMRES_GET(N, COMPUTED_SOLUTION, B, RCI_REQUEST, IPAR, &
!     & DPAR, TMP, ITERCOUNT)
! Compute the current true residual via MKL (Sparse) BLAS routines
!      	CALL MKL_DCSRGEMV('N', N, A, IA, JA, B, RESIDUAL)
!      	CALL DAXPY(N, -1.0D0, RHS, 1, RESIDUAL, 1)
!      	DVAR=DNRM2(N, RESIDUAL, 1)
!      	IF (DVAR.LT.1.0E-3) THEN
!      	   GOTO 3
!      	ELSE
!      	   GOTO 1
!      	ENDIF
!      ENDIF
!---------------------------------------------------------------------------
! If RCI_REQUEST=3, then apply the preconditioner on the vector
! TMP(IPAR(22)) and put the result in vector TMP(IPAR(23))
!---------------------------------------------------------------------------
      IF (RCI_REQUEST.EQ.3) THEN
	
	B2=TMP(IPAR(22):IPAR(22)+N-1)
	!B3(:)=0.0;
	

	if (IPAR(4) .ge. 0) then

	call sparse_solve_1(N,IA,JA,A,B2,B3)
	TMP(IPAR(23):IPAR(23)+N-1)=B3
	else
	TMP(IPAR(23):IPAR(23)+N-1)=TMP(IPAR(22):IPAR(22)+N-1);
	endif 

      !		DO I=0,N-1
      	!		TMP(IPAR(23)+I)=TMP(IPAR(22)+I)
!/(B2(I+1))
      	!	ENDDO

      	GOTO 1
      ENDIF
!---------------------------------------------------------------------------
! If RCI_REQUEST=4, then check if the norm of the next generated vector is
! not zero up to rounding and computational errors. The norm is contained
! in DPAR(7) parameter
!---------------------------------------------------------------------------
      IF (RCI_REQUEST.EQ.4) THEN
	
      	IF (DPAR(7).LT.1.0D-12) THEN
      	   GOTO 3
      	ELSE
      	   GOTO 1
      	ENDIF
!---------------------------------------------------------------------------
! If RCI_REQUEST=anything else, then DFGMRES subroutine failed
! to compute the solution vector: COMPUTED_SOLUTION(N)
!---------------------------------------------------------------------------
      ELSE
      	GOTO 999
      ENDIF
!---------------------------------------------------------------------------
! Reverse Communication ends here
! Get the current iteration number and the FGMRES solution. (DO NOT FORGET to
! call DFGMRES_GET routine as computed_solution is still containing
! the initial guess!). Request to DFGMRES_GET to put the solution into
! vector COMPUTED_SOLUTION(N) via IPAR(13)
!---------------------------------------------------------------------------
3     IPAR(13)=0
      CALL DFGMRES_GET(N, COMPUTED_SOLUTION, RHS, RCI_REQUEST, IPAR, &
     & DPAR, TMP, ITERCOUNT)
!---------------------------------------------------------------------------
! Print solution vector: COMPUTED_SOLUTION(N) and
! the number of iterations: ITERCOUNT
!---------------------------------------------------------------------------
      PRINT *, ''
      PRINT *,' The system has been solved'
      PRINT *, ''
      PRINT *,' The following solution has been obtained:'
      DO I=1,N

	   write(*,*) computed_solution(I)
      ENDDO
      PRINT *, ''

      PRINT *,' Number of iterations: ',ITERCOUNT

!---------------------------------------------------------------------------
! Release internal MKL memory that might be used for computations
! NOTE: It is important to call the routine below to avoid memory leaks
! unless you disable MKL Memory Manager
!---------------------------------------------------------------------------
      CALL MKL_FREE_BUFFERS


      STOP
!---------------------------------------------------------------------------
! Release internal MKL memory that might be used for computations
! NOTE: It is important to call the routine below to avoid memory leaks
! unless you disable MKL Memory Manager
!---------------------------------------------------------------------------
999   WRITE( *,'(A,A,I5)') 'This example FAILED as the solver has', &
     & ' returned the ERROR code', RCI_REQUEST
      CALL MKL_FREE_BUFFERS
      STOP 1

      END

