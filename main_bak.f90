	program main 
        use, intrinsic :: iso_c_binding
        use input_file
        use initialize
        use derivatives
        use linear_algebra
        use Hydro_inter
        use Tension
        use sparse_solve
        use A_TIMES_X
        use BoundaryIntegral
        use  GAUSS_ARRAY
        use BI_SP_build_array
        use BI_cor_build_array
        use GaussInt_Array
        use biophysics
        use fmm_interface
        implicit none
        
        

        interface 

        function OMP_get_thread_num()

        integer :: OMP_get_thread_num

        end function OMP_get_thread_num

        end interface


        call MPI_INIT(mpi_rank,mpi_size)     
        id=mpi_rank   
        print *, "ID IS ", mpi_rank
        call pvfmm_mpi_barrier()
        include 'init2.f90'
        id=mpi_rank
        !	print *, "number of fibers are", N_f
        m0=m
!        call pvFmmStokes_sl_SetOrder(mult_order);
!        call pvFmmStokes_sldl_SetOrder(mult_order);
           call make_fmm_context(mult_order, max_pts, max_depth, true, false,false,sl_context) !sl, dl, periodic, context 
           call make_fmm_context(mult_order, max_pts, max_depth, true, true ,false ,sldl_context) !sl, dl, periodic, context
           call make_fmm_context(mult_order, max_pts, max_depth, true,true,false ,sldl_bulk_context)
        id=mpi_rank


        !	print *, "n_s is ...", n_s
        open(unit=20,file='positions.txt')
        open(unit=21,file='positions_2.txt')
        open(unit=30,file='Tension.txt')
        open(unit=31,file='Tension_2.txt')
        open(unit=40, file='dots.txt')

        open(unit=50, file='B_deriv.txt')
        open(unit=60, file='u_2.txt')
        open(unit=400, file='u.txt')
        open(unit=80, file='xc.txt')

        open(unit=90, file='vel.txt')
        open(unit=100, file='omega.txt')
        open(unit=110,file='vel2.txt')
        open(unit=120,file='omega2.txt')
        open(unit=15, file='TB.txt')
        open(unit=16,file='F_T.txt')
        open(unit=150,file="polymer.txt",status="replace")
        open(unit=160,file="u_bulk.txt",status="replace")
        open(unit=201,file="U_bath_SP_0.txt",status="replace")
        open(unit=5,file='ia.txt')
        open(unit=6,file='ja.txt')
        open(unit=7,file='a.txt')
        open(unit=202,file='N_u_bulk.txt');


!          do nf=1,N_f
!           do i=1,N0
!              if (id .eq. 0) then
!              write(20,50) x(i,nf), y(i,nf),z(i,nf)
!              elseif(id .eq. 1) then
!              write(21,50) x(i,nf), y(i,nf),z(i,nf)
!              endif
!           enddo
!         enddo



!        do i=1,N0
!        dum(i)=1.0-1.0*exp(-1.0*(s(i)-s(1))*1.0);
!        print *,"----------------------------"
!        print *, s(i),i
!        enddo

!        print *, '****************************'

!        print *, dum(N_var(1))

!        print *, '****************************'

!        if (FLAG_BI .eq. 0) then

!        print *,"***********************NO BOUNDARIES !**********************"
!        else
!        print *, "*********************CORTEX BOUNDARIES*********************"
        call inv_BI()
!        print *, "passed inv"
!        endif 


        !*********************************************************************!
        !*********************************************************************!
        !*********************************************************************!
        !*********************   Simulation time loop starts *****************! 
        !*********************************************************************!
        !*********************************************************************!
        !*********************************************************************!
        

        V_EXT(1:3,1)=0.0;
        Omega_EXT(1,1)=0.0d0
        M1(:,:)=0.0;
        M2(:,:)=0.0;
        M1(1,1)=1.0;M1(2,2)=1.0;M1(3,3)=1.0;
        M2(1,1)=1.0;M2(2,2)=1.0;M2(3,3)=1.0;

        stab=-0.0
        stab0=2.0d0
        fac=10.d0
        t_init=0

        do t_step=1,300000

        call system_clock(t000,rate)

!        if (t_step .le. t_init) then
!           flag_manybody=0
!        else
!           flag_manybody=1
!        endif 
        
        k=0	
        if (t_step > t_init) then
         V_EXT(3,1)=10.0d0*mu1(1);
         OMEGA_EXT(2,1)=0.0d0*mu1(1)
        else
         V_EXT(1:3,1)=0.0d0
         OMEGA_EXT(1:3,1)=0.0d0
        endif

        if (t_step<100) then
        dt0=4.0e-4/mu1(1)
        else
        dt0=4.0e-4/mu1(1)
        endif

        if (mod(t_step ,100000) .eq. 0 .or. (t_step .eq. (t_init+1))) then
        
        n_iter=5

!        FLAG_BI=0
        else
!        FLAG_BI=1
        n_iter=1
        endif

	if (id .eq. 0) then
        print *,"time step is ...", t_step        
        endif 


        if ((mod(t_step,1) .eq. 0) .or. (t_step .eq. 1)) then
        call polymerize()
        endif
!        do nf=1,N_f
!        beta_n(nf)=10.0d0*sin((t_step)*dt0*10.0d0*2.0*pi);
!        if (L_F(nf) .ge. 4.50) then
!        beta_n(nf)=-10.0
!        endif
!        enddo
!        beta_n(:)=0.0;
        beta_t(:,1)=beta_t(:,2)
        beta_t(:,2)=beta_t(:,3)
        beta_t(:,3)=beta_t(:,4)        
        beta_t(:,4)=beta_n(:)

        beta_end(:)=(1.0*beta_t(:,1)+3.0*beta_t(:,2)+3.0*beta_t(:,3)+1.0*beta_t(:,4))/8.0
        do nf=1,N_f
         if (flag_BC(N0,nf) .eq. 2) then
          
          beta_i(nf)=0.0d0
          beta(nf)=(beta_i(nf)-beta_end(nf))*exp(-0.10d0*(t_att(nf)*1.0d0-1.0d0))+beta_end(nf)
         else
          beta(nf)=beta_end(nf);
         endif
         L_FF(nf)=L_FF(nf)+dt0*beta(nf)
        enddo

        call cpu_time (t1)
	

        FLAG_T=0

        
        if ( (t_step .eq. 1).and. iter0 .eq. 1) then
        
        ux_SPCOR(:,:)=0.0;uy_SPCOR(:,:)=0.0;uz_SPCOR(:,:)=0.0;
        V_SPCOR(:,:)=0.0;Omega_SPCOR(:,:)=0.0;
        uxs_SPCOR(:,:,:)=0.0;uys_SPCOR(:,:,:)=0.0;uzs_SPCOR(:,:,:)=0.0;
!        elseif (flag_manybody .eq. 1) then
        ux_SPCOR(:,:)=0.0;uy_SPCOR(:,:)=0.0;uz_SPCOR(:,:)=0.0;
        V_SPCOR(:,:)=0.0;Omega_SPCOR(:,:)=0.0;
        uxs_SPCOR(:,:,:)=0.0;uys_SPCOR(:,:,:)=0.0;uzs_SPCOR(:,:,:)=0.0;        
        endif

!        ux_SPCOR=(ux_SPCOR_P-ux_SPCOR_PP)*t_lag*dt0+ux_SPCOR_P;
!        uy_SPCOR=(uy_SPCOR_P-uy_SPCOR_PP)*t_lag*dt0+uy_SPCOR_P;
!        uz_SPCOR=(uz_SPCOR_P-uz_SPCOR_PP)*t_lag*dt0+uz_SPCOR_P;
        
!        V_SPCOR=(V_SPCOR_P-V_SPCOR_PP)*t_lag*dt0+V_SPCOR_P;
!        Omega_SPCOR=(Omega_SPCOR_P-Omega_SPCOR_PP)*t_lag*dt0+Omega_SPCOR_P;
!        endif

        

        do iter0=1,n_iter

!        ux_SPCOR(:,:)=0.0;uy_SPCOR(:,:)=0.0;uz_SPCOR(:,:)=0.0;
!        V_SPCOR(:,:)=0.0;Omega_SPCOR(:,:)=0.0;
!        uxs_SPCOR(:,:,:)=0.0;uys_SPCOR(:,:,:)=0.0;uzs_SPCOR(:,:,:)=0.0;


!$OMP PARALLEL DEFAULT(SHARED) SHARED(XT) PRIVATE(nf,N,kappa,kappa_e)
!$OMP DO
        do nf=1,N_f,1

        N=N0
        if (iter0 .eq. 1) then


        dum(1:N0,nf)=1.0d0-exp(-2.0d0*L_F(nf)*s(1:N0));
        call sparse_build_diffusion_ON(N,N0,n_s,n_half,m,&
                        & C_T0,ia_T(1:N+1,nf),ja_T(1:N*n_s,nf),a_T(1:N*n_s,nf),L_FF(nf))
        

        call deriv_O_N(x0(1:N,nf),N,n_s,C_T(:,:,1:N),m,x_ns0(1:m,1:N,nf),L_F(nf));
        call deriv_O_N(y0(1:N,nf),N,n_s,C_T(:,:,1:N),m,y_ns0(1:m,1:N,nf),L_F(nf));
        call deriv_O_N(z0(1:N,nf),N,n_s,C_T(:,:,1:N),m,z_ns0(1:m,1:N,nf),L_F(nf));

        dot0(nf,1:N)=(x_ns0(1,1:N,nf)*x_ns0(1,1:N,nf)+ &
        & y_ns0(1,1:N,nf)*y_ns0(1,1:N,nf)+ &
        & z_ns0(1,1:N,nf)*z_ns0(1,1:N,nf));

        dot1(nf,1:N)=x_ns0(2,1:N,nf)**2+y_ns0(2,1:N,nf)**2+z_ns0(2,1:N,nf)**2;

            dot2(nf,1:N)=x_ns0(2,1:N,nf)*x_ns0(4,1:N,nf)+ &
            & y_ns0(2,1:N,nf)*y_ns0(4,1:N,nf)+ &
            & z_ns0(2,1:N,nf)*z_ns0(4,1:N,nf);

            dot3(nf,1:N)=x_ns0(3,1:N,nf)*x_ns0(3,1:N,nf)+ &
            & y_ns0(3,1:N,nf)*y_ns0(3,1:N,nf)+ &
            & z_ns0(3,1:N,nf)*z_ns0(3,1:N,nf);


        call sparse_build_implicit_flag(N,N0,n_s,n_half,m,flag_BC(1:N0,nf),flag_BC_T(1:N0,nf),C_T0,&
        & ia_XT2(1:4*N+1,nf),ja_XT2(1:16*N*n_s,nf),a_XT2(1:16*N*n_s,nf),&
        & fac,stab0,stab,beta(nf),s(1:N0),L_FF(nf),mu0(nf),mu(nf),&
        & a_2,x_ns0(:,1:N,nf),y_ns0(:,1:N,nf),z_ns0(:,1:N,nf),&
        & n_0(:,:,nf),n_1(:,:,nf),n_2(:,:,nf),dt0)


!        call sparse_build_implicit_2(N,N0,n_s,n_half,m,C_T0,&
!        & ia_XT2(1:4*N+1,nf),ja_XT2(1:16*N*n_s,nf),a_XT2(1:16*N*n_s,nf),&
!        & fac,stab0,stab,beta(nf),s(1:N0),L_FF(nf),mu0(nf),mu(nf),&
!        & a_2,x_ns0(:,1:N,nf),y_ns0(:,1:N,nf),z_ns0(:,1:N,nf),dt0)


!        if (nf .eq. 1) then
        !do i=1,N0
        ! write (*,80) (C_T0(1,j,i),j=1,n_s)
        !enddo

!        do i=1,N0*16*n_s
!        write (6, *) ja_XT2(i,nf)
!        write (7, *) a_XT2(i,nf)
!        enddo


!        endif 





!        F_L(1,nf)=0.0d0*L_F(nf)*(mu0(nf)*(-1.0d0*x_ns0(1,N,nf)+0.0*y_ns0(1,N,nf))/(sqrt(x_ns0(1,N,nf)**2+y_ns0(1,N,nf)**2+z_ns0(1,N,nf)**2)));
!        F_L(2,nf)=0.0d0*L_F(nf)*(mu0(nf)*(-1.0d0*y_ns0(1,N,nf)-0.0*x_ns0(1,N,nf))/(sqrt(x_ns0(1,N,nf)**2+y_ns0(1,N,nf)**2+z_ns0(1,N,nf)**2)));
!        F_L(3,nf)=0.0d0*L_F(nf)*(mu0(nf)*(-1.0d0*z_ns0(1,N,nf)-0.0*y_ns0(1,N,nf))/(sqrt(x_ns0(1,N,nf)**2+y_ns0(1,N,nf)**2+z_ns0(1,N,nf)**2)));

        !F_L(3,nf)=0.0*mu0(nf)

        !*********************************************************************!
        !**********External force "densities" along the fiber length *********!
        !*********************************************************************!    

        fx_MTT_ext(1,1:N,nf)=0.0d0*mu1(nf)*x_ns0(1,1:N,nf)/(2.0d0);
        fy_MTT_ext(1,1:N,nf)=0.0d0*mu1(nf)*y_ns0(1,1:N,nf)/(2.0d0);
        fz_MTT_ext(1,1:N,nf)=0.0d0*mu1(nf)*z_ns0(1,1:N,nf)/(2.0d0);

        fx_MTT_ext(1,1:N,nf)=0.0d0*s*(1.0d0-s)*mu1(nf)*cos(t_step*dt0*40.0d0)*y_ns0(1,1:N,nf);
        fy_MTT_ext(1,1:N,nf)=-0.0d0*s*(1.0d0-s)*mu1(nf)*cos(t_step*dt0*40.0d0)*x_ns0(1,1:N,nf);
        fz_MTT_ext(1,1:N,nf)=0.0d0*s*(1.0d0-s)*mu1(nf)*z_ns0(1,1:N,nf)/(8.0d0);

        fx_MT_ext(1:N,nf)=fx_MTT_ext(1,1:N,nf);
        fy_MT_ext(1:N,nf)=fy_MTT_ext(1,1:N,nf);
        fz_MT_ext(1:N,nf)=fz_MTT_ext(1,1:N,nf);


        m0=1
        call deriv_O_N(fx_MT_ext(1:N,nf),N,n_s,C_T(:,:,1:N),m0,fx_s(1:m0,1:N,nf),L_F(nf));
        call deriv_O_N(fy_MT_ext(1:N,nf),N,n_s,C_T(:,:,1:N),m0,fy_s(1:m0,1:N,nf),L_F(nf));
        call deriv_O_N(fz_MT_ext(1:N,nf),N,n_s,C_T(:,:,1:N),m0,fz_s(1:m0,1:N,nf),L_F(nf));

        fsxs(1,1:N,nf)=(fx_s(1,1:N,nf)*x_ns0(1,1:N,nf)+ &
        & fy_s(1,1:N,nf)*y_ns0(1,1:N,nf)+ &
        & fz_s(1,1:N,nf)*z_ns0(1,1:N,nf));

        fxss(1,1:N,nf)=fx_MTT_ext(1,1:N,nf)*x_ns0(2,1:N,nf)+ &
        & fy_MTT_ext(1,1:N,nf)*y_ns0(2,1:N,nf)+ &
        & fz_MTT_ext(1,1:N,nf)*z_ns0(2,1:N,nf);

        fxs(1,1:N,nf)=fx_MTT_ext(1,1:N,nf)*x_ns0(1,1:N,nf)+ &
        & fy_MTT_ext(1,1:N,nf)*y_ns0(1,1:N,nf)+ &
        & fz_MTT_ext(1,1:N,nf)*z_ns0(1,1:N,nf);
        !fxs(1,1:N,nf)=1.0d0*mu0(nf)/10.0d0

        F_active(1,nf)=sum(fx_MT_ext(1:N,nf)*chev_w(1:N))*L_F(nf)
        F_active(2,nf)=sum(fy_MT_ext(1:N,nf)*chev_w(1:N))*L_F(nf)
        F_active(3,nf)=sum(fz_MT_ext(1:N,nf)*chev_w(1:N))*L_F(nf)

        xsus_f(1,1:N,nf)=0.0d0*(2.0*fsxs(1,1:N,nf)+fxss(1,1:N,nf));


        endif 

        C_T=C_T0;

        call sphere_motion(N0, N,x(:,nf),y(:,nf),z(:,nf),V_SPCOR,Omega_SPCOR,&
        & 0.0*S_SP_HD_B(:,:)/(mu0(nf)*20.0/(6.0*3.0)),xc0, ux_SP_HD_T(:,nf),uy_SP_HD_T(:,nf),uz_SP_HD_T(:,nf));

        call sphere_motion(N0,N,x(:,nf),y(:,nf),z(:,nf),V_EXT,Omega_EXT, &
        & 0.0*S_SP_HD_B(:,:)/(mu0(nf)*20.0/(6.0*3.0)),xc0,ux_SP_HD_B(:,nf),uy_SP_HD_B(:,nf),uz_SP_HD_B(:,nf));


           call deriv_O_N(ux_SP_HD_T(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uxs_SP_HD_T(1:m0,1:N,nf),L_F(nf));
           call deriv_O_N(uy_SP_HD_T(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uys_SP_HD_T(1:m0,1:N,nf),L_F(nf));
           call deriv_O_N(uz_SP_HD_T(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uzs_SP_HD_T(1:m0,1:N,nf),L_F(nf));

           call deriv_O_N(ux_SPCOR(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uxs_SPCOR(1:m0,1:N,nf),L_FF(nf));
           call deriv_O_N(uy_SPCOR(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uys_SPCOR(1:m0,1:N,nf),L_FF(nf));
           call deriv_O_N(uz_SPCOR(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uzs_SPCOR(1:m0,1:N,nf),L_FF(nf));

           xsus_2(1,1:N,nf)=uxs_SP_HD_B(1,1:N,nf)*x_ns0(1,1:N,nf)+&
           & uys_SP_HD_B(1,1:N,nf)*y_ns0(1,1:N,nf)+uzs_SP_HD_B(1,1:N,nf)*z_ns0(1,1:N,nf)+&
           & (uxs_SP_HD_T(1,1:N,nf)*x_ns0(1,1:N,nf)+&
           & uys_SP_HD_T(1,1:N,nf)*y_ns0(1,1:N,nf)+uzs_SP_HD_T(1,1:N,nf)*z_ns0(1,1:N,nf))*(1.0-dum(1:N,nf))+&
           & 1.0*dum(1:N,nf)*(uxs_SPCOR(1,1:N,nf)*x_ns0(1,1:N,nf)+&
           & uys_SPCOR(1,1:N,nf)*y_ns0(1,1:N,nf)+&
           & uzs_SPCOR(1,1:N,nf)*z_ns0(1,1:N,nf))

!         if (iter0 .eq. 1) then

!         F_XT(4*(nf-1)*N+1:4*(nf-1)*N+N)=100.0*mu0(nf)*(1.0-dot0(nf,1:N0)) &
!         & -xsus_f(1,1:N0,nf)/2.0-1.0*xsus_2(1,1:N,nf)*mu(nf)/2.0 
        
!         F_XT(4*(nf-1)*N+N)=(x_ns0(1,N,nf)*F_L(1,nf)+y_ns0(1,N,nf)*F_L(2,nf)+z_ns0(1,N,nf)*F_L(3,nf))/(sqrt(x_ns0(1,N,nf)**2+y_ns0(1,N,nf)**2+z_ns0(1,N,nf)**2))

!         F_XT(4*(nf-1)*N+1)=-1.0*fxs(1,1,nf) 
        

          U_EXT(1,1:N,nf)=ux_SP_HD_B(1:N,nf)+dum(1:N,nf)*ux_SPCOR(1:N,nf)+(1.0-dum(1:N,nf))*ux_SP_HD_T(1:N,nf)!+&
         ! & dum(1:N)*(fx_MTT_ext(1,1:N,nf)+fxs(1,1:N,nf)*x_ns0(1,1:N,nf))/mu(nf)
          U_EXT(2,1:N,nf)=uy_SP_HD_B(1:N,nf)+dum(1:N,nf)*uy_SPCOR(1:N,nf)+(1.0-dum(1:N,nf))*uy_SP_HD_T(1:N,nf)!+&
         ! & dum(1:N)*(fy_MTT_ext(1,1:N,nf)+fxs(1,1:N,nf)*y_ns0(1,1:N,nf))/mu(nf)
          U_EXT(3,1:N,nf)=uz_SP_HD_B(1:N,nf)+dum(1:N,nf)*uz_SPCOR(1:N,nf)+(1.0-dum(1:N,nf))*uz_SP_HD_T(1:N,nf)!+&
         ! & dum(1:N)*(fz_MTT_ext(1,1:N,nf)+fxs(1,1:N,nf)*z_ns0(1,1:N,nf))/mu(nf)


         do i=1,N0
         F_XT2(4*(i-1)+1+(nf-1)*4*N)=fac*mu0(nf)*(1.0-dot0(nf,i)) &
         & -xsus_f(1,i,nf)/2.0-1.0*xsus_2(1,i,nf)*mu(nf)/2.0

         F_XT2(4*(i-1)+2+(nf-1)*4*N)=mu(nf)*((a_1*xp(i,nf)+a_0*xpp(i,nf))/dt0+&
         & 1.0*U_EXT(1,i,nf)+0.0*beta(nf)*s(i)*x_ns0(1,i,nf))+&
         stab*x_ns0(4,i,nf)+stab0*mu0(nf)*s(i)*fac*2.0*x_ns0(1,i,nf)+&
         & fx_MTT_ext(1,i,nf)+1.0*fxs(1,i,nf)*x_ns0(1,i,nf);

         F_XT2(4*(i-1)+3+(nf-1)*4*N)=mu(nf)*((a_1*yp(i,nf)+a_0*ypp(i,nf))/dt0+&
         & 1.0*U_EXT(2,i,nf)+0.0*beta(nf)*s(i)*y_ns0(1,i,nf))+&
         stab*y_ns0(4,i,nf)+stab0*mu0(nf)*s(i)*fac*2.0*y_ns0(1,i,nf)+&
         & fy_MTT_ext(1,i,nf)+1.0*fxs(1,i,nf)*y_ns0(1,i,nf);

         F_XT2(4*(i-1)+4+(nf-1)*4*N)=mu(nf)*((a_1*zp(i,nf)+a_0*zpp(i,nf))/dt0+&
         & 1.0*U_EXT(3,i,nf)+0.0*beta(nf)*s(i)*z_ns0(1,i,nf))+&
         & stab*z_ns0(4,i,nf)+stab0*mu0(nf)*s(i)*fac*2.0*z_ns0(1,i,nf)+&
         & fz_MTT_ext(1,i,nf)+1.0*fxs(1,i,nf)*z_ns0(1,i,nf);

        enddo


         F_XT2(4*(N0-1)+1+(nf-1)*4*N)=(x_ns0(1,N,nf)*F_L(1,nf)+y_ns0(1,N,nf)*F_L(2,nf)+z_ns0(1,N,nf)*F_L(3,nf))/(sqrt(x_ns0(1,N,nf)**2+y_ns0(1,N,nf)**2+z_ns0(1,N,nf)**2))

         F_XT2(4*(1 -1)+1+(nf-1)*4*N)=-1.0d0*fxs(1,1,nf)


         X_att0(1,nf)=x0(1,nf);X_att0(2,nf)=y0(1,nf);X_att0(3,nf)=z0(1,nf);
         X_att0_2(1,nf)=x0(2,nf);X_att0_2(2,nf)=y0(2,nf);X_att0_2(3,nf)=z0(2,nf);

         dxc1p(1:3,nf)= X_att0(1:3,nf)-xc0(1:3)

         dxc1p(1:3,nf)=dxc1p(1:3,nf)*1.0/sqrt(dxc1p(1,nf)**2+dxc1p(2,nf)**2+dxc1p(3,nf)**2)

         dxc2p(1:3,nf)= X_att0_2(1:3,nf)-xc0(1:3)



          F_XT2((nf-1)*4*N0+2)=(a_1*xp(1,nf)+a_0*xpp(1,nf))/dt0+(V_SPCOR(1,1)+V_EXT(1,1))+&
          & (Omega_SPCOR(2,1)+Omega_EXT(2,1))*dxc1p(3,nf)-(Omega_SPCOR(3,1)+Omega_EXT(3,1))*dxc1p(2,nf);

          F_XT2((nf-1)*4*N0+3)=(a_1*yp(1,nf)+a_0*ypp(1,nf))/dt0+(V_SPCOR(2,1)+V_EXT(2,1)) &
          & -(Omega_SPCOR(1,1)+Omega_EXT(1,1))*dxc1p(3,nf)+(Omega_SPCOR(3,1)+Omega_EXT(3,1))*dxc1p(1,nf)
          
          F_XT2((nf-1)*4*N0+4)=(a_1*zp(1,nf)+a_0*zpp(1,nf))/dt0+(V_SPCOR(3,1)+V_EXT(3,1))+&
          & (Omega_SPCOR(1,1)+Omega_EXT(1,1))*dxc1p(2,nf)-(Omega_SPCOR(2,1)+Omega_EXT(2,1))*dxc1p(1,nf);

          F_XT2((nf-1)*4*N0+4+2)=(a_1*xp(2,nf)+a_0*xpp(2,nf))/dt0+(V_SPCOR(1,1)+V_EXT(1,1))+&
          & (Omega_SPCOR(2,1)+Omega_EXT(2,1))*dxc2p(3,nf)-(Omega_SPCOR(3,1)+Omega_EXT(3,1))*dxc2p(2,nf)!+&
          !& 20.0d0*mu0(nf)*s(2)*5.0d0*2.0d0*x_ns0(1,2,nf)/mu(nf);

          F_XT2((nf-1)*4*N0+4+3)=(a_1*yp(2,nf)+a_0*ypp(2,nf))/dt0+(V_SPCOR(2,1)+V_EXT(2,1))&
          & -(Omega_SPCOR(1,1)+Omega_EXT(1,1))*dxc2p(3,nf)+(Omega_SPCOR(3,1)+Omega_EXT(3,1))*dxc2p(1,nf)!+&
          !& 20.0d0*mu0(nf)*s(2)*5.0d0*2.0d0*y_ns0(1,2,nf)/mu(nf);

          F_XT2((nf-1)*4*N0+4+4)=(a_1*zp(2,nf)+a_0*zpp(2,nf))/dt0+(V_SPCOR(3,1)+V_EXT(3,1))&
          & +(Omega_SPCOR(1,1)+Omega_EXT(1,1))*dxc2p(2,nf)-&
          & (Omega_SPCOR(2,1)+Omega_EXT(2,1))*dxc2p(1,nf)!+&
          !& 20.0d0*mu0(nf)*s(2)*5.0d0*2.0*z_ns0(1,2,nf)/mu(nf)


          F_XT2(4*(nf-1)*N0+4*(N0-2)+2)=0.0
          F_XT2(4*(nf-1)*N0+4*(N0-2)+3)=0.0
          F_XT2(4*(nf-1)*N0+4*(N0-2)+4)=0.0

         if (flag_BC(N0,nf) .eq. 3) then
          F_XT2(4*(nf-1)*N0+4*(N0-1)+2)=F_L(1,nf)
          F_XT2(4*(nf-1)*N0+4*(N0-1)+3)=F_L(2,nf)
          F_XT2(4*(nf-1)*N0+4*(N0-1)+4)=F_L(3,nf)
         elseif (flag_BC(N0,nf) .eq. 2) then
          F_XT2((nf-1)*4*N0+4*(N0-1)+1)=-1.0d0*fxs(1,N0,nf)-s(N0)*beta(nf)*mu(nf)/2.0
!          F_XT2((nf-1)*4*N0+4*(N0-1)+1)=0.0d0
          F_XT2((nf-1)*4*N0+4*(N0-1)+2)=(a_1*xp(N0,nf)+a_0*xpp(N0,nf))/dt0
          F_XT2((nf-1)*4*N0+4*(N0-1)+3)=(a_1*yp(N0,nf)+a_0*ypp(N0,nf))/dt0
          F_XT2((nf-1)*4*N0+4*(N0-1)+4)=(a_1*zp(N0,nf)+a_0*zpp(N0,nf))/dt0
         elseif (flag_BC(N0,nf) .eq. 5) then
!          F_XT((nf-1)*4*N0+4*(N0-1)+2)=0.0d0
!          F_XT((nf-1)*4*N0+4*(N0-1)+3)=0.0d0
!          F_XT((nf-1)*4*N0+4*(N0-1)+4)=((a_1*xp(N0,nf)+a_0*xpp(N0,nf))*n_0(1,N0,nf)+&
!          & (a_1*yp(N0,nf)+a_0*ypp(N0,nf))*n_0(2,N0,nf)+&
!          & (a_1*zp(N0,nf)+a_0*zpp(N0,nf))*n_0(3,N0,nf))/dt0
!           F_XT((nf-1)*4*N0+4*(N0-1)+1)=0.0d0 
           F_XT2((nf-1)*4*N0+4*(N0-1)+2)=(a_1*xp(N0,nf)+a_0*xpp(N0,nf))/dt0+&
           & 0.50*beta(nf)*n_1(1,N0,nf)+&
           & 1.0d0*((V_EXT(1,1)+V_SPCOR(1,1))*x_ns0(1,N0,nf)+(V_EXT(2,1)+V_SPCOR(2,1))*y_ns0(1,N0,nf)+(V_EXT(3,1)+V_SPCOR(3,1))*z_ns0(1,N0,nf))*n_1(1,N0,nf)
           F_XT2((nf-1)*4*N0+4*(N0-1)+3)=(a_1*yp(N0,nf)+a_0*ypp(N0,nf))/dt0+0.50*beta(nf)*n_1(2,N0,nf)+&
           & 1.0d0*((V_EXT(1,1)+V_SPCOR(1,1))*x_ns0(1,N0,nf)+(V_EXT(2,1)+V_SPCOR(2,1))*y_ns0(1,N0,nf)+(V_EXT(3,1)+V_SPCOR(3,1))*z_ns0(1,N0,nf))*n_1(2,N0,nf)
           F_XT2((nf-1)*4*N0+4*(N0-1)+4)=(a_1*zp(N0,nf)+a_0*zpp(N0,nf))/dt0+0.50*beta(nf)*n_1(3,N0,nf)+&
           & 1.0d0*((V_EXT(1,1)+V_SPCOR(1,1))*x_ns0(1,N0,nf)+(V_EXT(2,1)+V_SPCOR(2,1))*y_ns0(1,N0,nf)+(V_EXT(3,1)+V_SPCOR(3,1))*z_ns0(1,N0,nf))*n_1(3,N0,nf)

          endif
        
!          F_XT2(4*(nf-1)*N0+4*(N0-1)+2)=F_L(1,nf)
!          F_XT2(4*(nf-1)*N0+4*(N0-1)+3)=F_L(2,nf)
!          F_XT2(4*(nf-1)*N0+4*(N0-1)+4)=F_L(3,nf)
        
        enddo


        

!$OMP END DO
!$OMP end parallel

        call  pvfmm_mpi_gather( F_XT2,4*N0*N_f,F_XT,4*N0*N_f,0) !recv_count is per proc
        call  pvfmm_mpi_bcast(F_XT,4*N0*N_f_T,0)

!!$OMP  parallel 

!          N=N0
!!$OMP do
!          do nf=1,N_f
!           k=(nf-1)*N0*4 
!           call sparse_solve_1(4*N,IA_XT2(1:4*N+1,nf),JA_XT2(1:16*n_s*N,nf),A_XT2(1:16*n_s*N,nf),F_XT2(1+k:4*N+k),XT_2(1+k:4*N+k)) 
!          enddo

!!$OMP end do
!!$OMP end parallel

!         call  pvfmm_mpi_gather( XT_2,4*N0*N_f,XT,4*N0*N_f,0) !recv_count isper proc                 
!         call  pvfmm_mpi_bcast(XT,4*N0*N_f_T,0)


!        do nf=1,N_f
!         do i=1,N0
!          T(i,nf)=XT(id*4*N_f*N0+4*(nf-1)*N0+(i-1)*4+1)
!          x(i,nf)=XT(id*4*N_f*N0+4*(nf-1)*N0+(i-1)*4+2)
!          y(i,nf)=XT(id*4*N_f*N0+4*(nf-1)*N0+(i-1)*4+3)
!          z(i,nf)=XT(id*4*N_f*N0+4*(nf-1)*N0+(i-1)*4+4)

!         enddo
!       enddo

        if (t_step .eq. 1) then
        do nf=1,N_f_T
          do i=1,N0

              XT((nf-1)*4*N0+(i-1)*4+2)=x_T(i,nf)
              XT((nf-1)*4*N0+(i-1)*4+3)=y_T(i,nf)
              XT((nf-1)*4*N0+(i-1)*4+4)=z_T(i,nf)
              XT((nf-1)*4*N0+(i-1)*4+1)=0.0d0
           enddo
        enddo
        endif 
              

        iter_GMRES=0
        call pvfmm_mpi_barrier()
        call system_clock(t00)
        call implicit_GMRES()
        call system_clock(t07)
        if (id .eq. 0) then
         print *, "GMRES time", real(t07-t00)/real(rate)
         print *,"-----------------------------"
         print *,"PASSED GMRES"
         print *,"-----------------------------"        
       endif 
        call A_DOT_X_implicit(XT)

!        call fmm_eval_self(src,sl_den,nsrc,dl_den_nor,sldl_pot2,max_pts,FLAG_PVFMM)
!        call pvfmmstokes_sldl(src,sl_den,nsrc,src,dl_den_nor,nsrc,trg,sldl_pot2,ntrg,max_pts,FLAG_PVFMM)

!        print *,"--------------------------------------------------"
!        print *, "sldl vs sl",maxval(abs(sldl_pot-sldl_pot2)),maxval(abs(sldl_pot))
!        print *,"--------------------------------------------------"


       if (flag_pvfmm .eq. 1) then                 
        call pvfmm_mpi_gather(sldl_pot,3*nsrc,sldl_pot_T,nsrc*3,0)
        call pvfmm_mpi_bcast(sldl_pot_T,3*nsrc*nprocs,0)
        
        U_bath_SP_0(:)=0.0d0;U_bath_cor_0(:)=0.0d0
        sldl_pot_T=2.0d0*sldl_pot_T/(8.0d0*mu0(1)/6.0d0)
        do j=0,nprocs-1
                print *,"??????????????????????????"
                print *, "nprocessor  is ", j
                print *, "?????????????????????????"
        do i=1,nparts_sp_l
         
         U_bath_SP_0(3*j*nparts_sp_l+(i-1)*3+1)=sldl_pot_T((j*nsrc+nparts_MT)*3+(i-1)*3+1)
         U_bath_SP_0(3*j*nparts_sp_l+(i-1)*3+2)=sldl_pot_T((j*nsrc+nparts_MT)*3+(i-1)*3+2)
         U_bath_SP_0(3*j*nparts_sp_l+(i-1)*3+3)=sldl_pot_T((j*nsrc+nparts_MT)*3+(i-1)*3+3)
!        if (id .eq. 0) then
!        print *,U_bath_SP_0(j*nparts_sp_l+(k-1)*3+3),&
!        & sldl_pot((i-1)*3+3)*2.0/(8.0d0*mu0(1)/6.0d0),sldl_pot_T(j*nsrc*3+(i-1)*3+3)
!        endif 
        enddo
        enddo
       
        do j=0,nprocs-1
        do i=1,ntargs_cor_l
         U_bath_cor_0(3*j*nparts_cor_l+(i-1)*3+1)=sldl_pot_T((j*nsrc+nparts_MT+nparts_sp_l)*3+(i-1)*3+1)
         U_bath_cor_0(3*j*nparts_cor_l+(i-1)*3+2)=sldl_pot_T((j*nsrc+nparts_MT+nparts_sp_l)*3+(i-1)*3+2)
         U_bath_cor_0(3*j*nparts_cor_l+(i-1)*3+3)=sldl_pot_T((j*nsrc+nparts_MT+nparts_sp_l)*3+(i-1)*3+3)
        enddo
        enddo
       else

        seq_pot=seq_pot*2.0d0/(8.0d0*mu0(1)/6.0d0)
        do i=1,nparts_sp_l
         U_bath_SP_0((i-1)*3+1)=seq_pot(1,nparts_MT+i)
         U_bath_SP_0((i-1)*3+2)=seq_pot(2,nparts_MT+i)
         U_bath_SP_0((i-1)*3+3)=seq_pot(3,nparts_MT+i)
        enddo
        do i=1,nparts_cor_l
         U_bath_cor_0((i-1)*3+1)=seq_pot(1,nparts_MT+nparts_sp_l+i)
         U_bath_cor_0((i-1)*3+2)=seq_pot(2,nparts_MT+nparts_sp_l+i)
         U_bath_cor_0((i-1)*3+3)=seq_pot(3,nparts_MT+nparts_sp_l+i)
        enddo

       endif


         call pvfmm_mpi_barrier()
!        if (flag_manybody .eq. 1) then
         call COR_2SPMT()
!        endif 

        call pvfmm_mpi_barrier()
        print *, "REACHED pvfmm_sldl"


        src2=src;trg2=trg;sl_den2(:)=0.0d0;
        
        do i=1,nsrc
            if (i .le. nparts_MT) then
                dl_den_nor2((i-1)*6+1)=0.0d0;
                dl_den_nor2((i-1)*6+2)=0.0d0;
                dl_den_nor2((i-1)*6+3)=0.0d0;
                dl_den_nor2((i-1)*6+4)=0.0d0;
                dl_den_nor2((i-1)*6+5)=0.0d0;
                dl_den_nor2((i-1)*6+6)=1.0d0;
            else
                dl_den_nor2((i-1)*6+1)=(8.0*pi)*sigma_dl_SPCOR_l(1,i-nparts_MT)/fac2**2
                dl_den_nor2((i-1)*6+2)=(8.0*pi)*sigma_dl_SPCOR_l(2,i-nparts_MT)/fac2**2
                dl_den_nor2((i-1)*6+3)=(8.0*pi)*sigma_dl_SPCOR_l(3,i-nparts_MT)/fac2**2
                dl_den_nor2((i-1)*6+4)=sigma_dv_SPCOR_l(1,i-nparts_MT);
                dl_den_nor2((i-1)*6+5)=sigma_dv_SPCOR_l(2,i-nparts_MT);
                dl_den_nor2((i-1)*6+6)=sigma_dv_SPCOR_l(3,i-nparts_MT);
            endif
        enddo

!       if (flag_manybody .eq. 1) then 
        call stokes_sldl_fmm(nsrc, src2, sl_den2, nsrc, src2, dl_den_nor2, nsrc,trg2, sldl_pot3,true, sldl_context)
!       else
!        sldl_pot3(:)=0.0d0
!       endif  
!        iprec=3;
!        ifsingle=0;
!        ifdouble=1;
!        ifpot=0;
!        ifgrad=0;
!        ifpottarg=1;
!        ifgradtarg=0;
!        pottarg_SPCOR(1:3,:)=0.0;
!        pretarg_SPCOR(:)=0.0;
!        gradtarg_SPCOR(1:3,1:3,:)=0.0;
!        ntargs_SPCOR=nparts_MT



 !       call stfmm3Dparttarg(ier,iprec,nparts_SPCOR,source_SPCOR,ifsingle,sigma_sl_SPCOR,ifdouble,&
 !       & sigma_dl_SPCOR,sigma_dv_SPCOR,ifpot,pot_SPCOR,pre_SPCOR,ifgrad,grad_SPCOR, &
 !       & ntargs_SPCOR,source_MT,ifpottarg,pottarg_SPCOR,pretarg_SPCOR,ifgradtarg,gradtarg_SPCOR)



       do i=1,nparts_MT

         pot_MT(1,i)=sldl_pot3((i-1)*3+1)
         pot_MT(2,i)=sldl_pot3((i-1)*3+2)
         pot_MT(3,i)=sldl_pot3((i-1)*3+3)

       end do


 !       print *, "comparison between pvfmm and fmm", maxval(pot_MT-pottarg_SPCOR),minval(pot_MT-pottarg_SPCOR),maxval(abs(pot_MT))
 !      & maxval(abs(pot_MT-pottarg_SPCOR)),maxval(abs(pot_MT/(1e-3+pottarg_SPCOR)))

        do nf=1,N_f
          k=(nf-1)*N0
           ux_SPCOR(1:N0,nf)=pot_MT(1,k+1:k+N0)
           uy_SPCOR(1:N0,nf)=pot_MT(2,k+1:k+N0)
           uz_SPCOR(1:N0,nf)=pot_MT(3,k+1:k+N0)
!           ux_SPCOR(1:N0,nf)=pottarg_SPCOR(1,k+1:k+N0)
!           uy_SPCOR(1:N0,nf)=pottarg_SPCOR(2,k+1:k+N0)
!           uz_SPCOR(1:N0,nf)=pottarg_SPCOR(3,k+1:k+N0) 
       
        enddo

        call system_clock(t08)
        if (id .eq. 0) then        
           print *,"BI calculation ..", real(t08-t07)/real(rate)
           print *, "total time step calculation ", real(t08-t000)/real(rate)
        endif

        
        W_SP(:,:)=0.0d0
        W_SP(1:3,1)=Omega_Tn(1:3,1)+Omega_EXT(1:3,1)+Omega_SPCOR(1:3,1)
        U_SP=V_EXT+V_SPCOR+V_T
        dt2=dt0/400.0d0

        dx_at(1,:)=x0(1,:);
        dx_at(2,:)=y0(1,:);
        dx_at(3,:)=z0(1,:);
        
        dx_at2(1,:)=x0(2,:);
        dx_at2(2,:)=y0(2,:)
        dx_at2(3,:)=z0(2,:)
        x_ns(1,1,:)=x_ns0(1,1,:);
        x_ns(1,2,:)=x_ns0(1,2,:);

      do i=1,400        
         x_ns0(1,1,:)=x_ns(1,1,:);
         x_ns0(1,2,:)=x_ns(1,2,:);
         xc0(1:3)=xc0(1:3)+U_SP(1:3,1)*dt2

        do nf=1,N_f        
         dxc1p(1,nf)=dx_at(1,nf)-xc0(1)
         dxc1p(2,nf)=dx_at(2,nf)-xc0(2)
         dxc1p(3,nf)=dx_at(3,nf)-xc0(3)

         dxc2p(1,nf)=dx_at2(1,nf)-xc0(1)
         dxc2p(2,nf)=dx_at2(2,nf)-xc0(2)
         dxc2p(3,nf)=dx_at2(3,nf)-xc0(3)

         dx_at(1,nf)=dx_at(1,nf)+dt2*(U_SP(1,1))+&
         & dt2*(W_SP(2,1)*dxc1p(3,nf)-W_SP(3,1)*dxc1p(2,nf))
         dx_at(2,nf)=dx_at(2,nf)+dt2*U_SP(2,1)+&
         & dt2*(-W_SP(1,1)*dxc1p(3,nf)+W_SP(3,1)*dxc1p(1,nf))
         dx_at(3,nf)=dx_at(3,nf)+dt2*U_SP(3,1)+&
         & dt2*(W_SP(1,1)*dxc1p(2,nf)-W_SP(2,1)*dxc1p(1,nf))
         x_ns(1,1,nf)=x_ns(1,1,nf)+&
         & dt2*(W_SP(2,1)*z_ns0(1,1,nf)-W_SP(3,1)*y_ns0(1,1,nf))
         x_ns(1,2,nf)=x_ns(1,2,nf)+&
         & dt2*(W_SP(2,1)*z_ns0(1,2,nf)-W_SP(3,1)*y_ns0(1,2,nf))
         y_ns(1,1,nf)=y_ns(1,1,nf)+&
         & dt2*(-W_SP(1,1)*z_ns0(1,1,nf)+W_SP(3,1)*x_ns0(1,1,nf))
         y_ns(1,2,nf)=y_ns(1,2,nf)+&
         & dt2*(-W_SP(1,1)*z_ns0(1,2,nf)+W_SP(3,1)*x_ns0(1,2,nf))
         z_ns(1,1,nf)=z_ns(1,1,nf)+&
         & dt2*(W_SP(1,1)*y_ns0(1,1,nf)-W_SP(2,1)*x_ns0(1,1,nf))
         z_ns(1,2,nf)=z_ns(1,2,nf)+&
         & dt2*(W_SP(1,1)*y_ns0(1,2,nf)-W_SP(2,1)*x_ns0(1,2,nf))
         dx_at2(1,nf)=dx_at2(1,nf)+dt2*(U_SP(1,1)+beta(nf)*s(2)*x_ns(1,2,nf))+&
         & dt2*(W_SP(2,1)*dxc2p(3,nf)-W_SP(3,1)*dxc2p(2,nf))
         dx_at2(2,nf)=dx_at2(2,nf)+dt2*(U_SP(2,1)+beta(nf)*s(2)*y_ns(1,2,nf))+&
         & dt2*(-W_SP(1,1)*dxc2p(3,nf)+W_SP(3,1)*dxc2p(1,nf))
         dx_at2(3,nf)=dx_at2(3,nf)+dt2*(U_SP(3,1)+beta(nf)*s(2)*z_ns(1,2,nf))+&
         & dt2*(W_SP(1,1)*dxc2p(2,nf)-W_SP(2,1)*dxc2p(1,nf))

        enddo
      enddo

        nf=N_f
        dr_at(nf)=sqrt((dx_at(1,nf)-xc0(1))**2+(dx_at(2,nf)-xc0(2))**2+(dx_at(3,nf)-xc0(3))**2)-1.0d0
        
        do nf=1,N_f
         dot0_2(nf,:)=1.0d0-(x_ns(1,:,nf)**2+y_ns(1,:,nf)**2+z_ns(1,:,nf)**2);
        enddo
       
        dt2=maxval(abs(dot0_2(:,:)))/10.0

        if (id .eq. 0) then
        print *,"velocity is ",U_SP/mu1(1)
        print *,"---------------------"
        print *,"V_T0_T is", V_T0_T/mu1(1)
        print *,"----------------------"

        print *,"V_T0_B is", V_T0_B/mu1(1)
        print *,"----------------------"
        print *,"V_T is", V_T/mu1(1)
        print *,"----------------------"
        print *,"V_HD is", V_HD/mu1(1)
        print *,"----------------------"
        print *,"V_SPCOR is", V_SPCOR/mu1(1)
        print *,"----------------------"
        print *,"OMEGA_T is", Omega_T/mu1(1)
        print *,"----------------------"
        print *,"Omega_HD is ...."
        print *,Omega_HD/mu1(1)
        print *,"----------------------"
        endif 
     enddo
        

        do nf=1,N_f
         dr_at(nf)=dx_at(1,nf)-x(1,nf) 
         x(:,nf)=x(:,nf)+dr_at(nf)

         dr_at(nf)=dx_at(2,nf)-y(1,nf) 
         y(:,nf)=y(:,nf)+dr_at(nf)

         dr_at(nf)=dx_at(3,nf)-z(1,nf) 
         z(:,nf)=z(:,nf)+dr_at(nf)

         dr_at(nf)=sqrt((x(1,nf)-xc0(1))**2+(y(1,nf)-xc0(2))**2+(z(1,nf)-xc0(3))**2)

        enddo
        

        do nf=1,N_f
         dr_at(nf)=dx_at2(1,nf)-x(2,nf)
         x(2:N0,nf)=x(2:N0,nf)+dr_at(nf)

         dr_at(nf)=dx_at2(2,nf)-y(2,nf)
         y(2:N0,nf)=y(2:N0,nf)+dr_at(nf)

         dr_at(nf)=dx_at2(3,nf)-z(2,nf)
         z(2:N0,nf)=z(2:N0,nf)+dr_at(nf)
       enddo

!*****************************************!
!*********** BDF time-stepping************!


        !a_0=-0.5;a_1=2.0;a_2=1.50
        a_0=-0.0d0;a_1=1.0d0;a_2=1.0d0;
        xpp=xp;ypp=yp;zpp=zp;
        xp=x;yp=y;zp=z;
        !x0=2.0*xp-xpp;y0=2.0*yp-ypp;z0=2.0*zp-zpp;
        x0=x;y0=y;z0=z;
        Omega_Tp=Omega_T;
        Omega_T0_Tp=Omega_T0;
        Omega_T0_Bp=Omega_T0_B;
        Omega_HDp=Omega_HD;
!***************************************!
!***************************************! 

       L_F(1:N_f)=L_FF(1:N_f)
        xc0p=xc0
         e_SP(:)=xc0(:)


!***************************************************************************!
!***************************************************************************! 
!***************************************************************************!

!**************Computing the bulk velocity field and a give set of
!points*******!

        if (  mod(t_step,1) .eq. 0) then! .or. t_step .eq. 1) then

        W_bulk(:)=1
        do nf=1,N_f
        do i=1,N0        

        r0_bulk=sqrt((x(i,nf)-xc0(1))**2+(y(i,nf)-xc0(2))**2+(z(i,nf)-xc0(3))**2)-1.0d0;
        i_bulk=nint(r0_bulk*1.0d0*N_r_bulk/(a_cor-1.0d0)+1.0e-6)+1

        teta0_bulk=atan2(y(i,nf),x(i,nf));
        if (teta0_bulk<0) then
        teta0_bulk=2.0d0*pi+teta0_bulk;
        endif 
        j_bulk=nint(teta0_bulk*N_teta_bulk/(2.0d0*pi)+1.0e-6)+1
        k_bulk=nint((z(i,nf)/(r0_bulk+1.0d0)+1.0d0)*N_phi_bulk/2.0d0+1.0e-6)+1
        if (r0_bulk<0) then
!        print *, "r0_bulk is", i_bulk,r0_bulk
        endif 
        if (i_bulk< N_r_bulk) then
        W_bulk(k_bulk+(j_bulk-1)*N_phi_bulk+(i_bulk-1)*N_teta_bulk*N_phi_bulk)=0
        endif 


        enddo
        enddo


        do i=1,ntarg_bulk
        if (sqrt((x_bulk(i)+xc0(1))**2/b_cor**2+(y_bulk(i)+xc0(2))**2/b_cor**2+(z_bulk(i)+xc0(3))**2/a_cor**2)>0.9) then
        W_bulk(i)=0
        endif 

          trg_bulk((i-1)*3+1)=(x_bulk(i)+xc0(1)+a_cor+0.1)/fac2;
          trg_bulk((i-1)*3+2)=(y_bulk(i)+xc0(2)+a_cor+0.1)/fac2;
          trg_bulk((i-1)*3+3)=(z_bulk(i)+xc0(3)+a_cor+0.1)/fac2;
          targ_bulk(1,i)=x_bulk(i)+xc0(1)
          targ_bulk(2,i)=y_bulk(i)+xc0(2)
          targ_bulk(3,i)=z_bulk(i)+xc0(3)
        enddo 
!        call bulk_flow()

        call stokes_sldl_fmm(nsrc, src, 2.0d0*sl_den/(8.0d0*mu0(1)/6.0d0), nsrc,src2, dl_den_nor2, ntrg_bulk,trg_bulk, pot_bulk,true, sldl_bulk_context)
!********************** Flow field generated by external force/torque applied on
!the spindle ************!                
        call sphere_motion(ntarg_bulk, ntarg_bulk, targ_bulk(1,:),targ_bulk(2,:),targ_bulk(3,:),&
        & V_EXT+V_T0_T+V_T0_B,Omega_EXT+Omega_T0_T+Omega_T0_B,0.0*E_T,xc0,&
        & ux_bulk_SP,uy_bulk_SP,uz_bulk_SP);


!**************************************************************************************************!

!********************** Flow field generated by external force/torque &********! 
!******forces from MTs and the point of attachment applied on the spindle******!    

        call sphere_motion(ntarg_bulk, ntarg_bulk, targ_bulk(1,:),targ_bulk(2,:),targ_bulk(3,:), &
        & V_HD,Omega_HD,0.0*S_SP_HD/(mu0(1)*20.0/18.0),xc0,&
        & ux_bulk_SP2,uy_bulk_SP2,uz_bulk_SP2);

!**************************************************************************************************!
        do i=1,ntarg_bulk
         dum_bulk(i)=1.0d0-exp(-2.0d0*abs(sqrt(x_bulk(i)**2+y_bulk(i)**2+z_bulk(i)**2)-1.0d0));
         ux_bulk(i)=1.0d0*W_bulk(i)*(1.0*ux_bulk_SP(i)+(1.0-dum_bulk(i))*ux_bulk_SP2(i)+pot_bulk((i-1)*3+1)*dum_bulk(i))
         uy_bulk(i)=1.0d0*W_bulk(i)*(1.0*uy_bulk_SP(i)+(1.0-dum_bulk(i))*uy_bulk_SP2(i)+pot_bulk((i-1)*3+2)*dum_bulk(i))
         uz_bulk(i)=1.0d0*W_bulk(i)*(1.0*uz_bulk_SP(i)+(1.0-dum_bulk(i))*uz_bulk_SP2(i)+pot_bulk((i-1)*3+3)*dum_bulk(i))
!        ux_bulk(:)=ux_bulk_SP(:)+(1.0-dum_bulk)*ux_bulk_SP2(:)+&
!        & ((2.0)*pottarg_bulk_MT(1,:)+(1.0)*pottarg_bulk_SPCOR(1,:))*dum_bulk(:)
!        uy_bulk(:)=uy_bulk_SP(:)+(1.0-dum_bulk)*uy_bulk_SP2(:)+&
!        & ((2.0)*pottarg_bulk_MT(2,:)+(1.0)*pottarg_bulk_SPCOR(2,:))*dum_bulk(:)
!        uz_bulk(:)=uz_bulk_SP(:)+(1.0-dum_bulk)*uz_bulk_SP2(:)+&
!        & ((2.0)*pottarg_bulk_MT(3,:)+(1.0)*pottarg_bulk_SPCOR(3,:))*dum_bulk(:)
!        k=0
        enddo
        do i=1,ntarg_bulk

        if (W_bulk(i) .ne. 0) then
        k=k+1
        write(160,60) x_bulk(i),y_bulk(i),z_bulk(i),ux_bulk(i),uy_bulk(i),uz_bulk(i)
        endif 
        enddo
        write(202,*) k
        endif

!***************************************************************************!
!***************************************************************************! 
!***************************************************************************! 

          if (mod(t_step,1) .eq. 0 ) then
            if (mod(t_step,1) .eq. 0 .or. t_step .eq. 1) then

!             do nf=1,N_f
!                write(50,50) F_active(1,nf)/mu0(nf),F_active(2,nf)/mu0(nf),F_active(3,nf)/mu0(nf),T(1,nf)
                do i=1,N0*N_f_T
                  if (id .eq. 0) then
!                   write(20,50) x0(i,nf), y0(i,nf), z0(i,nf)
!                   write(30,50) T(i,nf) 
                    write(20,50) XT((i-1)*4+2),XT((i-1)*4+3),XT((i-1)*4+4)
                    write(30,50) XT((i-1)*4+1)
                  elseif(id .eq. 1) then
                    ! write(21,50) x0(i,nf), y0(i,nf), z0(i,nf)
                    write(21,50) XT((i-1)*4+2),XT((i-1)*4+3),XT((i-1)*4+4)
                    write(31,50) XT((i-1)*4+1)                
                  endif 
                enddo
                do i=1,N0
                 do nf=1,N_f
                     write(400,50)x(i,nf),y(i,nf),z(i,nf),ux2_ext(i,nf)+u_ext(1,i,nf),uy2_ext(i,nf)+u_ext(2,i,nf),uz2_ext(i,nf)+u_ext(3,i,nf)
                 enddo
                enddo
                do i=1,ntargs_SP

                if(id .eq. 0) then
                  write (201,50) targ_SP(1,i),targ_SP(2,i),targ_SP(3,i), &
                  & U_bath_SP_0((i-1)*3+1),U_bath_SP_0((i-1)*3+2),U_bath_SP_0((i-1)*3+3)
                endif 
                enddo
             endif 
                if (id .eq. 0) then
                   write(80,50) xc0(1),xc0(2),xc0(3)
                   write(90,50) U_SP(1,1)/mu1(1),U_SP(2,1)/mu1(1),U_SP(3,1)/mu1(1),V_SP(1)/mu1(1),V_SP(2)/mu1(1),V_SP(3)/mu1(1)
                   write (100,50) W_SP(1,1)/mu1(1),W_SP(2,1)/mu1(1),W_SP(3,1)/mu1(1),Omega_SPCOR(1,1)/mu1(1),Omega_SPCOR(2,1)/mu1(1),Omega_SPCOR(3,1)/mu1(1)
                elseif (id .eq. 1) then
                    write(110,50) U_SP(1,1)/mu1(1),U_SP(2,1)/mu1(1),U_SP(3,1)/mu1(1),W_SP(1,1)/mu1(1),W_SP(2,1)/mu1(1),W_SP(3,1)/mu1(1)
                endif 
            endif
!        print *,"########################"
!        enddo

         call pvfmm_mpi_barrier()

        enddo
        call MPI_finalize

	close(150);close(20);close(30);close(40);close(50);close(60);close(80)

3000      format(9F12.2)
30      format(10F10.3)
40      format(10F15.3)
50	format(6F25.12)
60      format(6F25.12)
70      format(6F25.12,2X,1I14)
80      format(12F20.12)
	end program main  
