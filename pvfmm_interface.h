#ifndef _PVFMMINTERFACE_H_
#define _PVFMMINTERFACE_H_

#include <cstddef>  //for size_t

#define DIM 3
typedef double real_t;

extern "C"
{
  // MPI interface
  void mpi_init_pvfmm(int &rank,int &size);
  void mpi_finalize_pvfmm();
  
  // PVFMM interface
  void make_pvfmm_context(const int &mult_order, const int &max_pts,
			  const int &max_depth, const int  &sl,  const int &dl,
			  const int &periodic, void **context);
  
  void clear_pvfmm_context(void **context);
  
  void stokes_sl_pvfmm(
		       const size_t &nsrc, const real_t *src, const real_t *den,
		       const size_t &ntrg, const real_t *trg, real_t *pot,
		       const int &rebuild_tree, void **context);

  void stokes_sldl_pvfmm(
			 const size_t &sl_nsrc, const real_t *sl_src, const real_t *sl_den,
			 const size_t &dl_nsrc, const real_t *dl_src, const real_t *dl_den_nor,
			 const size_t &ntrg, const real_t *trg, real_t *pot,
			 const int &rebuild_tree, void **context);

  void pvfmm_mpi_gather_(
        void* send_data, int &send_count,
        void* recv_data, int &recv_count,
        int &root);

  void pvfmm_mpi_scatter_(
      void* send_data, int &send_count,
      void* recv_data, int &recv_count,
      int &root);


  void pvfmm_mpi_reduce_(
      void* send_data, void* recv_data,
      int &send_count, int &root);


  void pvfmm_mpi_barrier_();

  void pvfmm_mpi_bcast_(
       void* send_data,
       int &send_count,int &root);
}

#endif //_PVFMMINTERFACE_H_
