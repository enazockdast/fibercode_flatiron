module Tension
USE sparse_solve
USE A_TIMES_X 
USE derivatives 
USE Hydro_Inter 
USE linear_algebra
implicit none 

contains



      subroutine  Tension_GMRES (N_var,N0, N_f, N_T,n_s, n_half, m, C_T0, ds, &
      RHS, ia, ja, a,  computed_solution) 

      IMPLICIT NONE
      INCLUDE "mkl_rci.fi"

      
      INTEGER, intent (IN) ::N_var(:), N0, N_f, N_T, n_s, n_half, m
      
      INTEGER, PARAMETER :: SIZE0=128

      INTEGER :: IPAR(SIZE0)
      INTEGER :: nf, N, k, k2
      INTEGER, intent (IN), dimension (:,:) :: ia,ja
 
      real*8, intent (IN), dimension (:,:) :: a
      real*8, intent (IN) :: C_T0(:,:,:)
      real*8 :: RHS(:),B(N0),B2(N0),B3(N0)
       
 
	
      REAL*8 :: DPAR(SIZE0), TMP(N_T*(2*N_T+1)+(N_T*(N_T+9))/2+1)
      
     ! DOUBLE PRECISION EXPECTED_SOLUTION(N)
  
      
      REAL*8  :: COMPUTED_SOLUTION(:)
      REAL*8 ::  RESIDUAL(N0)

!---------------------------------------------------------------------------
! Some additional variables to use with the RCI (P)FGMRES solver
!---------------------------------------------------------------------------
      INTEGER :: ITERCOUNT 

      INTEGER :: RCI_REQUEST, I
      DOUBLE PRECISION :: DVAR
      REAL*8, intent (IN)  :: DS
!---------------------------------------------------------------------------
! An external BLAS function is taken from MKL BLAS to use
! with the RCI (P)FGMRES solver
!---------------------------------------------------------------------------
      DOUBLE PRECISION :: DNRM2
      EXTERNAL DNRM2

!---------------------------------------------------------------------------
! Initialize the initial guess
!---------------------------------------------------------------------------
      
      COMPUTED_SOLUTION(:)=1.0
      
	print *, " N_T is ...  ", N_T
	print *, "ds is ...  ", ds

!---------------------------------------------------------------------------
! Initialize the solver
!---------------------------------------------------------------------------
      CALL DFGMRES_INIT(N_T, COMPUTED_SOLUTION, RHS, RCI_REQUEST, IPAR, &
     & DPAR, TMP)
      IF (RCI_REQUEST.NE.0) GOTO 999
!---------------------------------------------------------------------------
! Set the desired parameters:
! do the restart after 2 iterations
! LOGICAL parameters:
! do not do the stopping test for the maximal number of iterations
! do the Preconditioned iterations of FGMRES method
! DOUBLE PRECISION parameters
! set the relative tolerance to 1.0D-3 instead of default value 1.0D-6
!---------------------------------------------------------------------------

       IPAR(5)=30
       IPAR(15)=5
       IPAR(8)=1
       IPAR(9)=0
       IPAR(10)=0
       IPAR(11)=1
       IPAR(12)=1
       DPAR(1)=1.0D-9

!---------------------------------------------------------------------------
! Check the correctness and consistency of the newly set parameters
!---------------------------------------------------------------------------
      CALL DFGMRES_CHECK(N_T, COMPUTED_SOLUTION, RHS, RCI_REQUEST, &
     & IPAR, DPAR, TMP)
      IF (RCI_REQUEST.NE.0) GOTO 999

!---------------------------------------------------------------------------
! Compute the solution by RCI (P)FGMRES solver with preconditioning
! Reverse Communication starts here
!---------------------------------------------------------------------------
1     CALL DFGMRES(N_T, COMPUTED_SOLUTION, RHS, RCI_REQUEST, IPAR, &
     & DPAR, TMP)

!---------------------------------------------------------------------------
! If RCI_REQUEST=0, then the solution was found with the required precision
!---------------------------------------------------------------------------
      IF (RCI_REQUEST.EQ.0) GOTO 3
!---------------------------------------------------------------------------
! If RCI_REQUEST=1, then compute the vector A*TMP(IPAR(22))
! and put the result in vector TMP(IPAR(23))
!---------------------------------------------------------------------------	

      IF (RCI_REQUEST.EQ.1) THEN
      k=0
      do nf=1, N_f       
      N=N_var(nf)
      
!     CALL ATIMESX(N, TMP(IPAR(22)+k:IPAR(22)+N+k-1),TMP(IPAR(23)+k:IPAR(23)+k+N-1),DS)
     CALL ATIMESX_ON(N,N0,n_s,n_half,m,C_T0,&
     & TMP(IPAR(22)+k:IPAR(22)+N+k-1),TMP(IPAR(23)+k:IPAR(23)+k+N-1))
      k=k+N
      enddo

      	GOTO 1
      ENDIF
	
!	if (IPAR(4) .eq. 1) then
!	do i=1,N_T
!	print *, TMP(IPAR(22)+i)
!	enddo
!	endif 

!---------------------------------------------------------------------------
! If RCI_request=2, then do the user-defined stopping test
! The residual stopping test for the computed solution is performed here
!---------------------------------------------------------------------------
! NOTE: from this point vector B(N) is no longer containing the right-hand
! side of the problem! It contains the current FGMRES approximation to the
! solution. If you need to keep the right-hand side, save it in some other
! vector before the call to DFGMRES routine. Here we saved it in vector
! RHS(N). The vector B is used instead of RHS to preserve the original
! right-hand side of the problem and guarantee the proper restart of FGMRES
! method. Vector B will be altered when computing the residual stopping
! criterion!
!---------------------------------------------------------------------------
!      IF (RCI_REQUEST.EQ.2) THEN
! Request to the DFGMRES_GET routine to put the solution into B(N) via IPAR(13)
!	print *," RCI = 2"
!      	IPAR(13)=1
! Get the current FGMRES solution in the vector B(N)
!      	CALL DFGMRES_GET(N, COMPUTED_SOLUTION, B, RCI_REQUEST, IPAR, &
!     & DPAR, TMP, ITERCOUNT)
! Compute the current true residual via MKL (Sparse) BLAS routines
!      	CALL MKL_DCSRGEMV('N', N, A, IA, JA, B, RESIDUAL)
!      	CALL DAXPY(N, -1.0D0, RHS, 1, RESIDUAL, 1)
!      	DVAR=DNRM2(N, RESIDUAL, 1)
!      	IF (DVAR.LT.1.0E-3) THEN
!      	   GOTO 3
!      	ELSE
!      	   GOTO 1
!      	ENDIF
!      ENDIF
!---------------------------------------------------------------------------
! If RCI_REQUEST=3, then apply the preconditioner on the vector
! TMP(IPAR(22)) and put the result in vector TMP(IPAR(23))
!---------------------------------------------------------------------------
      IF (RCI_REQUEST.EQ.3) THEN

	
	k2=0
	do nf=1,N_f
	N=N_var(nf)
	B2(1:N)=TMP(IPAR(22)+k2:IPAR(22)+N+k2-1)

	call sparse_solve_1(N,IA(1:N+1,nf),JA(1:n_s*N,nf),A(1:n_s*N,nf),B2(1:N),B3(1:N))
	TMP(IPAR(23)+k2:IPAR(23)+k2+N-1)=B3(1:N)
	k2=k2+N
	enddo
	

      	GOTO 1
      ENDIF
!---------------------------------------------------------------------------
! If RCI_REQUEST=4, then check if the norm of the next generated vector is
! not zero up to rounding and computational errors. The norm is contained
! in DPAR(7) parameter
!---------------------------------------------------------------------------
      IF (RCI_REQUEST.EQ.4) THEN
	
      	IF (DPAR(7).LT.1.0D-12) THEN
      	   GOTO 3
      	ELSE
      	   GOTO 1
      	ENDIF
!---------------------------------------------------------------------------
! If RCI_REQUEST=anything else, then DFGMRES subroutine failed
! to compute the solution vector: COMPUTED_SOLUTION(N)
!---------------------------------------------------------------------------
      ELSE
      	GOTO 999
      ENDIF
!---------------------------------------------------------------------------
! Reverse Communication ends here
! Get the current iteration number and the FGMRES solution. (DO NOT FORGET to
! call DFGMRES_GET routine as computed_solution is still containing
! the initial guess!). Request to DFGMRES_GET to put the solution into
! vector COMPUTED_SOLUTION(N) via IPAR(13)
!---------------------------------------------------------------------------
3     IPAR(13)=0
      CALL DFGMRES_GET(N, COMPUTED_SOLUTION, RHS, RCI_REQUEST, IPAR, &
     & DPAR, TMP, ITERCOUNT)
!---------------------------------------------------------------------------
! Print solution vector: COMPUTED_SOLUTION(N) and
! the number of iterations: ITERCOUNT
!---------------------------------------------------------------------------
      PRINT *, ''
      PRINT *,' The system has been solved'
      PRINT *, ''
      PRINT *,' The following solution has been obtained:'
      DO I=1,N_T

	   write(*,*) computed_solution(I)
      ENDDO
      PRINT *, ''

      PRINT *,' Number of iterations: ',ITERCOUNT

!---------------------------------------------------------------------------
! Release internal MKL memory that might be used for computations
! NOTE: It is important to call the routine below to avoid memory leaks
! unless you disable MKL Memory Manager
!---------------------------------------------------------------------------
      CALL MKL_FREE_BUFFERS


      STOP
!---------------------------------------------------------------------------
! Release internal MKL memory that might be used for computations
! NOTE: It is important to call the routine below to avoid memory leaks
! unless you disable MKL Memory Manager
!---------------------------------------------------------------------------
999   WRITE( *,'(A,A,I5)') 'This example FAILED as the solver has', &
     & ' returned the ERROR code', RCI_REQUEST
      CALL MKL_FREE_BUFFERS
      STOP 1

      END subroutine Tension_GMRES

!**************************************************************************!
!**************************************************************************!
!**************************************************************************!
!**************************************************************************!
 
	subroutine Tension_A_DOT_X(T_T, T, T_tr, T_ns, Tt, &
	& x, y, z, x_ns0, y_ns0, z_ns0, r0, &
	& ux_ext_MT_B, uy_ext_MT_B, uz_ext_MT_B, &
	& C_T0, C_T_B2, dxc1, xc0, mu, mu1, C, C0, ds, N_var, N0, N_F, N_T, n_s, n_half, &
	& source, targ, sigma_sl, sigma_dl, sigma_dv, pot, pre, grad, &
	& pottarg, pretarg, gradtarg, RHS)

	implicit none 

	real*8 :: ds
	real*8 :: T_T(:,:), T(:,:), Tt(:,:)
	real*8, intent (IN), dimension (:,:) :: x, y, z, r0, &
	& ux_ext_MT_B, uy_ext_MT_B, uz_ext_MT_B 
	real*8, intent (IN), dimension (:,:,:) :: x_ns0, y_ns0, z_ns0
	real*8, dimension (:,:,:) :: T_tr, T_ns
	real*8, intent (IN), dimension (:,:,:) :: C_T0, C_T_B2
	real*8, intent (IN), dimension (:) :: mu, mu1, C, C0
	real*8, intent (IN)  :: xc0(:), dxc1(:,:)
	integer, intent (IN) :: N_var(:)
	integer, intent (IN) :: N0, N_F, N_T, n_s, n_half
	real*8, intent (OUT) :: RHS (:,:)
	real*8, dimension (:,:) :: source, targ, pot, pottarg, &
	& sigma_sl, sigma_dl, sigma_dv
	real*8 ::pre(:), pretarg(:)
	real*8 :: grad(:,:,:), gradtarg(:,:,:)

	integer :: ier, iprec, nsource, ntarg, ifsingle, ifdouble, & 
	& ifpot, ifgrad, ifpottarg, ifgradtarg, nparts

	integer ::k, nf,m0, N,i 
	real*8 :: M1(3,3), M2(3,3), E_T(3,3)
	real*8 :: C_T(5,n_s,N0)
	real*8 :: F_MT(3,1), T_MT(3,1), V_T0(3,1), Omega_T0(3,1) 
	real*8, dimension (N0, N_f) :: f2x_MT, f2y_MT, f2z_MT
	real*8, dimension (1,N0, N_f) :: f2x_MTT, f2y_MTT, f2z_MTT, &
	& ux_s1, uy_s1, uz_s1, ux_s2, uy_s2, uz_s2, ux_s, uy_s, uz_s, &
	& xsus
	real*8, dimension (N0, N_f) :: ux_ext_SP, uy_ext_SP, uz_ext_SP, &
	& ux_ext_MT, uy_ext_MT, uz_ext_MT, &
	& ux_ext, uy_ext, uz_ext,&
	& ux_self, uy_self, uz_self, &
	& ux_ext_2, uy_ext_2, uz_ext_2 
	real*8 :: dum(1,N0), dot1(1,N0)
	!*******************************!
	iprec=3;
	nparts=N_T;
	ifsingle=1;
	ifdouble=0;
	ifpot=1;
	ifgrad=0;
	ntarg=0;
	ifpottarg=0;
	ifgradtarg=0;
	

	M1(:,:)=0.0;
	M2(:,:)=0.0;
	M1(1,1)=1.0;M1(2,2)=1.0;M1(3,3)=1.0;
	M2(1,1)=1.0;M2(2,2)=1.0;M2(3,3)=1.0;
	  k=0;
	  V_T0(1:3,1)=0.0;
	  F_MT(:,1)=0.0;T_MT(:,1)=0.0;
	do nf=1,N_f;

	    N=N_var(nf);
	    k=k+N;
	    C_T=C_T0;
	    C_T(:,:,N-n_half+1:N)=C_T_B2;
	    T(1:N,nf)=T_T(k-N+1:k,1);
	    m0=2
	    call deriv_O_n(T(1:N,nf),N,n_s,&
	    & C_T(2:m0+1,:,1:N),m0, T_ns(1:m0,1:N,nf))

	    source(1,k-N+1:k)=x(1:N,nf);
	    source(2,k-N+1:k)=y(1:N,nf);
	    source(3,k-N+1:k)=z(1:N,nf);	      
	      
	    dot1(1,1:N)=x_ns0(2,1:N,nf)**2+y_ns0(2,1:N,nf)**2+y_ns0(2,1:N,nf)**2;
	    
	    F_MT(1,1)=F_MT(1,1)+C(nf)*(T(1,nf)*x_ns0(1,1,nf)-x_ns0(3,1,nf));
	    F_MT(2,1)=F_MT(2,1)+C(nf)*(T(1,nf)*y_ns0(1,1,nf)-y_ns0(3,1,nf));
	    F_MT(3,1)=F_MT(3,1)+C(nf)*(T(1,nf)*z_ns0(1,1,nf)-z_ns0(3,1,nf));
	    
	    T_MT(1,1)=T_MT(1,1)-C(nf)*( y_ns0(2,1,nf)*z_ns0(1,1,nf)-z_ns0(2,1,nf)*y_ns0(1,1,nf))+&
	    & (dxc1(2,nf)*F_MT(3,nf)-dxc1(3,nf)*F_MT(2,nf));
	      
	    T_MT(2,1)=T_MT(2,1)-C(nf)*(-x_ns0(2,1,nf)*z_ns0(1,1,nf)+z_ns0(2,1,nf)*x_ns0(1,1,nf))+ &
	    & (-dxc1(1,nf)*F_MT(3,nf)+dxc1(3,nf)*F_MT(1,nf));
	      
	    T_MT(3,1)=T_MT(3,1)-C(nf)*( x_ns0(2,1,nf)*y_ns0(1,1,nf)-y_ns0(2,1,nf)*x_ns0(1,1,nf))+ &
	    & (dxc1(1,nf)*F_MT(2,nf)-dxc1(2,nf)*F_MT(1,nf));
	 	      	      
	enddo;
	    
	V_T0=matmul(M1,F_MT)
	V_T0=V_T0/mu1(1)
	Omega_T0=matmul(M2, T_MT) 
	Omega_T0=Omega_T0/mu1(1)    
	E_T(1:3,1:3)=0.0;
	      
	    
	k=0;
	do nf=1,N_f;
	      
	N=N_var(nf);
	k=k+N;  
	C_T=C_T0;
	C_T(:,:,N-n_half+1:N)=C_T_B2;
	T_tr(1,1:N,nf)=T(1:N,nf);

	call sphere_motion(N, N_f, x(1:N,nf),y(1:N,nf),z(1:N,nf),V_T0,Omega_T0,E_T,r0(1:N,nf),xc0, &
	& ux_ext_SP(1:N,nf),uy_ext_SP(1:N,nf),uz_ext_SP(1:N,nf));
		       
	f2x_MTT(1,1:N,nf)=1.0*C(nf)*(T_ns(1,1:N,nf)*x_ns0(1,1:N,nf)+ &
	& T_tr(1,1:N,nf)*x_ns0(2,1:N,nf));
	      
	f2y_MTT(1,1:N,nf)=1.0*C(nf)*(T_ns(1,1:N,nf)*y_ns0(1,1:N,nf)+ &
	& T_tr(1,1:N,nf)*y_ns0(2,1:N,nf));
	      
	f2z_MTT(1,1:N,nf)=1.0*C(nf)*(T_ns(1,1:N,nf)*z_ns0(1,1:N,nf)+ &
	& T_tr(1,1:N,nf)*z_ns0(2,1:N,nf));
	     
	      
	f2x_MT(1:N,nf)=f2x_MTT(1,1:N,nf);
	f2y_MT(1:N,nf)=f2y_MTT(1,1:N,nf);
	f2z_MT(1:N,nf)=f2z_MTT(1,1:N,nf);
	      
	       
	sigma_sl(1,k-N+1:k)=f2x_MT(1:N,nf)*ds;
	sigma_sl(2,k-N+1:k)=f2y_MT(1:N,nf)*ds;
	sigma_sl(3,k-N+1:k)=f2z_MT(1:N,nf)*ds;
	 
	enddo;
	       	    	    	  
	call stfmm3Dpartself(ier,iprec,nparts,source, ifsingle,sigma_sl,ifdouble,& 
	& sigma_dl,sigma_dv, ifpot,pot,pre,ifgrad,grad)
	 
	do nf=1,N_f;
	 
	N=N_var(nf);

	call MT_uinf_self(f2x_MT(1:N,nf),f2y_MT(1:N,nf),f2z_MT(1:N,nf),&
	& x(1:N,nf),y(1:N,nf),z(1:N,nf),N,ds, &
	& ux_self(1:N,nf),uy_self(1:N,nf),uz_self(1:N,nf))

	enddo;
	 
	do i=1,N
	dum(1,i)=1.0-1.0*exp(-(10.*ds*(i-1)*1.0))
	enddo
	 
	k=0;m0=1
	do nf=1,N_f;    
	   N=N_var(nf);
	   k=k+N;
	   ux_ext_MT(1:N,nf)=(2.0*pot(1,k-N+1:k)-ux_self(1:N,nf))/mu(nf);
	   uy_ext_MT(1:N,nf)=(2.0*pot(2,k-N+1:k)-uy_self(1:N,nf))/mu(nf);
	   uz_ext_MT(1:N,nf)=(2.0*pot(3,k-N+1:k)-uz_self(1:N,nf))/mu(nf);
	      
	   C_T=C_T0;
	   C_T(:,:,N-n_half+1:N)=C_T_B2;

	   call deriv_O_n(ux_ext_MT(1:N,nf),N,n_s,C_T(:,:,1:N),m0,ux_s1(1:m0,1:N,nf));
	   call deriv_O_n(uy_ext_MT(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uy_s1(1:m0,1:N,nf));
	   call deriv_O_n(uz_ext_MT(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uz_s1(1:m0,1:N,nf));
	    
	   call deriv_O_n(ux_ext_SP(1:N,nf),N,n_s,C_T(:,:,1:N),m0,ux_s2(1:m0,1:N,nf));
	   call deriv_O_n(uy_ext_SP(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uy_s2(1:m0,1:N,nf));
	   call deriv_O_n(uz_ext_SP(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uz_s2(1:m0,1:N,nf));
	       
	   ux_ext(1:N,nf)=1.0*ux_ext_MT(1:N,nf)*dum(1,1:N)+ &
	   & 1.0*ux_ext_SP(1:N,nf);
	   uy_ext(1:N,nf)=1.0*uy_ext_MT(1:N,nf)*dum(1,1:N)+ &
	   & 1.0*uy_ext_SP(1:N,nf);
	   uz_ext(1:N,nf)=1.0*uz_ext_MT(1:N,nf)*dum(1,1:N)+ &
	   & 1.0*uz_ext_SP(1:N,nf);
	   
	   ux_s(1,1:N,nf)=1.0*ux_s1(1,1:N,nf)*dum(1,1:N)+ &
	   & 1.0*ux_s2(1,1:N,nf);
	    
	   uy_s(1,1:N,nf)=1.0*uy_s1(1,1:N,nf)*dum(1,1:N)+ &
	   & 1.0*uy_s2(1,1:N,nf);

	   uz_s(1,1:N,nf)=1.0*uz_s1(1,1:N,nf)*dum(1,1:N)+ &
	   & 1.0*uz_s2(1,1:N,nf);
	    
	   xsus(1,1:N,nf)=ux_s(1,1:N,nf)*x_ns0(1,1:N,nf)+ &
	   &  uy_s(1,1:N,nf)*y_ns0(1,1:N,nf)+uz_s(1,1:N,nf)*z_ns0(1,1:N,nf);
	    
	enddo;
	 
	    k=0;
	do nf=1,N_f;
		
	  N=N_var(nf);
	  Tt(1,1:N)=T(1:N,nf);
	       
	  k=k+N;

	  RHS(1+k-N:k,1)=T_ns(2,1:N,nf)-dot1(1,1:N)*Tt(1,1:N)+xsus(1,1:N,nf)/2.0;


	enddo;
	    
	k=0;
	do nf=1,N_f;      
	   N=N_var(nf);           
	   k=k+N;
	   RHS(1+k-N,1)=T_ns(1,1,nf);
	   RHS(k,1)=1.0*T(N,nf);
		    
		    
	enddo;

	end subroutine Tension_A_DOT_X


end module Tension
     

  
    
