	program main
	use input_file
	use initialize_2 
	use derivatives 
	use linear_algebra 
	use Hydro_inter
	implicit none
	include 'init2.f90'

	m0=m
	do t_step=1,10
    k=0

	do nf=1,N_f

	N=N_var(nf)

	if ( FLAG_P(nf) .eq. 0) then

	elseif (FLAG_P(nf) .eq. (-1) ) then

	N_var(nf)=N-1

	else


	!mu1(nf)=log(((s(N+1)-s(1))/a_MT)**2*exp(1.0));
	!mu(nf)=mu0(nf)*mu2(nf);

	call deriv_O_N_1point(x(N-n_s:N,nf),n_s,C_T_B2(1:m0+1,:,n_half),m0,xns_end(1:m0));
	call deriv_O_N_1point(y(N-n_s:N,nf),n_s,C_T_B2(1:m0+1,:,n_half),m0,yns_end(1:m0));
	call deriv_O_N_1point(y(N-n_s:N,nf),n_s,C_T_B2(1:m0+1,:,n_half),m0,zns_end(1:m0));

	rns_end=sqrt(xns_end(1)**2+yns_end(1)**2+zns_end(1)**2);

	x(N+1,nf)=x(N,1)+xns_end(1)*ds/rns_end;
	y(N+1,nf)=y(N,1)+yns_end(1)*ds/rns_end;
	z(N+1,nf)=z(N,1)+zns_end(1)*ds/rns_end;

	xt(N+1,:,nf)=xt(N,:,nf)+xst(N,:,nf)*ds;
	yt(N+1,:,nf)=yt(N,:,nf)+yst(N,:,nf)*ds;
	zt(N+1,:,nf)=zt(N,:,nf)+zst(N,:,nf)*ds;


	fx_MT(N+1,nf)=fx_MT(N,nf);
	fy_MT(N+1,nf)=fy_MT(N,nf);
	fz_MT(N+1,nf)=fz_MT(N,nf);

	fx_s(1,N+1,nf)=fx_s(1,N,nf);
	fy_s(1,N+1,nf)=fy_s(1,N,nf);
	fz_s(1,N+1,nf)=fz_s(1,N,nf);

	ux_ext(N+1,nf)=ux_ext(N,nf);
	uy_ext(N+1,nf)=uy_ext(N,nf);
	uz_ext(N+1,nf)=uz_ext(N,nf);

	ux_s(1,N+1,nf)=ux_s(1,N,nf);
	uy_s(1,N+1,nf)=uy_s(1,N,nf);
	uz_s(1,N+1,nf)=uz_s(1,N,nf);


	xsus(1,N+1,nf)=xsus(1,N,nf);
	xsuTs(1,N+1,nf)=xsuTs(1,N,nf);

	T(N+1,nf)=T(N,nf);


	T_ns(:,N+1,nf)=T_ns(:,N,nf);

	Ts(1,N+1,nf)=0.0;


	Tt(:,N+1,nf)=Tt(:,N,nf);

	Tst(:,N+1,nf)=Tst(:,N,nf);

	QX0(1,N+1,nf)=QX0(1,N,nf);
	QY0(1,N+1,nf)=QY0(1,N,nf);
	QZ0(1,N+1,nf)=QZ0(1,N,nf);

	QX(1,N+1,nf)=QX(1,N,nf);
	QY(1,N+1,nf)=QY(1,N,nf);
	QZ(1,N+1,nf)=QZ(1,N,nf);

	Qxt(:,N+1,nf)=Qxt(:,N,nf);
	Qyt(:,N+1,nf)=Qyt(:,N,nf);
	Qzt(:,N+1,nf)=Qzt(:,N,nf);

	xst(:,N+1,nf)=xst(:,N,nf);
	yst(:,N+1,nf)=yst(:,N,nf);
	zst(:,N+1,nf)=zst(:,N,nf);

	U_EXT(:,N+1,nf)=U_EXT(:,N,nf);

	C_T(:,:,N-n_half+1+1:N+1)=C_T_B2;
	C_T(:,:,N-n_half+1)=C_T(:,:,N-n_half);

	N_var(nf)=N+1

	endif;
	N=N_var(nf)
	k=k+N
        kappa=sqrt(x_ns0(2,1,nf)**2+y_ns0(2,1,nf)**2+z_ns0(2,1,nf)**2);
        call BC1(kappa, x_ns0(1:m,1,nf), y_ns0(1:m,1,nf),z_ns0(1:m,1,nf), &
	& x(1:n_s,nf), y(1:n_s,nf), z(1:n_s,nf), n_s, m, C_T(2:m+1,1:n_s,1), s(1:n_s), BC_T(nf))

    !    [x_ns0,y_ns0,z_ns0,BCT]=BC1(kappa,x_ns0,y_ns0,z_ns0,nf);
      
    !    BC_T(nf)=BCT;   
    
	print *, BC_T(nf), N
        F_T(k,1)=(xs0(1,N,nf)*F_L(1,nf)+ys0(1,N,nf)*F_L(2,nf)+zs0(1,N,nf)*F_L(3,nf));

30	format(10F7.3)
40	format(10F15.3)

!*********************************************************************!
!**********External forces applid on the end of fiber*****************!
!*********************************************************************!

    F_L(1,nf)=(-mu0(nf)*(xs0(1,N,nf)-1.0e-1*ys0(1,N,nf))/(1.0*N_f));
    F_L(2,nf)=(-mu0(nf)*(ys0(1,N,nf)+1.0e-1*xs0(1,N,nf))/(1.0*N_f));
    F_L(3,nf)=0.0*(-mu0(nf)*zs0(1,N,nf)/N_f);
!*********************************************************************!
!**********External force "densities" along the fiber length *********!
!*********************************************************************!    
    
     fx_MTT_ext(1,1:N,nf)=0.0*mu0(nf)*ys0(1,1:N,nf)*(s(1:N)+L/2.0)**0.50;
     fy_MTT_ext(1,1:N,nf)=-0.0*mu0(nf)*xs0(1,1:N,nf)*(s(1:N)+L/2.0);
     fz_MTT_ext(1,1:N,nf)=0.0*s(1:N);
     fx_MT_ext(1:N,nf)=fx_MTT(1,1:N,nf);
     fy_MT_ext(1:N,nf)=fy_MTT(1,1:N,nf);
     fz_MT_ext(1:N,nf)=fz_MTT(1,1:N,nf);


     fx_MTT_B(1,1:N,nf)=1.0*C(nf)*(-1.0*x_ns0(4,1:N,nf))+fx_MTT(1,1:N,nf);      
     fy_MTT_B(1,1:N,nf)=1.0*C(nf)*(-1.0*y_ns0(4,1:N,nf))+fy_MTT(1,1:N,nf);      
     fz_MTT_B(1,1:N,nf)=1.0*C(nf)*(-1.0*z_ns0(4,1:N,nf))+fz_MTT(1,1:N,nf);
      
     fx_MT_B(1:N,nf)=fx_MTT_B(1,1:N,nf);
     fy_MT_B(1:N,nf)=fy_MTT_B(1,1:N,nf);
     fz_MT_B(1:N,nf)=fz_MTT_B(1,1:N,nf);

     fx_2(1:N,nf)=fx_MT_B(1:N,nf)+fx_MT_ext(1:N,nf)
     fy_2(1:N,nf)=fy_MT_B(1:N,nf)+fy_MT_ext(1:N,nf)
     fz_2(1:N,nf)=fz_MT_B(1:N,nf)+fz_MT_ext(1:N,nf)
	
    m0=1   
    call deriv_O_N(fx_MT_ext(1:N,nf),N,n_s,C_T(:,:,1:N),m0,fx_s(1:m0,1:N,nf));
    call deriv_O_N(fy_MT_ext(1:N,nf),N,n_s,C_T(:,:,1:N),m0,fy_s(1:m0,1:N,nf));
    call deriv_O_N(fz_MT_ext(1:N,nf),N,n_s,C_T(:,:,1:N),m0,fz_s(1:m0,1:N,nf));
       
    fsxs(1,1:N,nf)=(fx_s(1,1:N,nf)*xs0(1,1:N,nf)+ &
    & fy_s(1,1:N,nf)*ys0(1,1:N,nf)+ &
    & fz_s(1,1:N,nf)*zs0(1,1:N,nf));
        
    fxss(1,1:N,nf)=fx_MTT(1,1:N,nf)*x_ns0(2,1:N,nf)+ &
    & fy_MTT(1,1:N,nf)*y_ns0(2,1:N,nf)+ &
    & fz_MTT(1,1:N,nf)*z_ns0(2,1:N,nf);
        
    xsus_f(1,1:N,nf)=2.0*fsxs(1,1:N,nf)+fxss(1,1:N,nf);
!     ux_ext_MT_B(1:N,nf)=0.0;
!     uy_ext_MT_B(1:N,nf)=0.0;
!     uz_ext_MT_B(1:N,nf)=0.0;

	
!        do nf2=1,N_f;
!            if (nf .ne. nf2) then ;
!                N2=N_var(nf2);
		
                
         
!            call MT_uinf(fx_MT_B(1:N2,nf2),fy_MT_B(1:N2,nf2),fz_MT_B(1:N2,nf2),x(1:N,nf), &
!            & y(1:N,nf),z(1:N,nf),x(1:N2,nf2),y(1:N2,nf2),z(1:N2,nf2),N,N2,ds, &
!	     & ux_ext_2(1:N,nf2),uy_ext_2(1:N,nf2),uz_ext_2(1:N,nf2));
!         
!           ux_ext_MT_B(1:N,nf)=ux_ext_MT_B(1:N,nf)+1.0*ux_ext_2(1:N,nf2)*(mu(nf2))**(-1); 
!           uy_ext_MT_B(1:N,nf)=uy_ext_MT_B(1:N,nf)+1.0*uy_ext_2(1:N,nf2)*(mu(nf2))**(-1);  
!           uz_ext_MT_B(1:N,nf)=uz_ext_MT_B(1:N,nf)+1.0*uz_ext_2(1:N,nf2)*(mu(nf2))**(-1);
!           	
!           
!           
!            endif;
!        enddo;
     
       dot0(1,1:N)=xs0(1,1:N,nf)*xs0(1,1:N,nf)+ &
       & ys0(1,1:N,nf)*ys0(1,1:N,nf)+ &
       & zs0(1,1:N,nf)*zs0(1,1:N,nf);
    
       dot2(1,1:N)=x_ns0(2,1:N,nf)*x_ns0(4,1:N,nf)+ &
       & y_ns0(2,1:N,nf)*y_ns0(4,1:N,nf)+ &
       & z_ns0(2,1:N,nf)*z_ns0(4,1:N,nf);
    
       dot3(1,1:N)=x_ns0(3,1:N,nf)**2+ &
       & y_ns0(3,1:N,nf)**2+ &
       & z_ns0(3,1:N,nf)**2;
    
       
       F_T(k-N+1:k,1)= mu(nf)*(1-dot0(1,1:N))-&
       & (7.0*dot2(1,1:N)+6.0*dot3(1,1:N))/2.0-xsus_f(1,1:N,nf)/2.0;
          
        r0(1:N,nf)=sqrt((x(1:N,nf)-xc0(1))**2+(y(1:N,nf)-xc0(2))**2+ &
        & (z(1:N,nf)-xc0(3))**2);
        dxc1(1:3,nf)=X_att(1:3,nf)-xc0(1:3);
       
   ! enddo;  
!	N_T=k;
!	e_SP=xc0;
!	e1=e1+cross(Omega_T0,e1)*dt0;
!	e2=e2+cross(Omega_T0,e2)*dt0;
!	e3=e3+cross(Omega_T0,e3)*dt0;
!	Rot=[e1;e2;e3];
	enddo
	call manybody_stokeslet (fx_2,fy_2,fz_2,x,y,z,ds,N_T,N_f, N_var, mu &
	ux_self, uy_self, uz_self, & 
	& ier, iprec,nparts,source, ifsingle,sigma_sl,ifdouble,& 
	& sigma_dl,sigma_dv, ifpot,pot,pre,ifgrad,grad,&
	ux_2,uy_2,uz_2)
	
	k=0;m0=1;
	do nf=1,N_f;    
	   N=N_var(nf);
	   k=k+N;	   
	      
	   C_T=C_T0;
	   C_T(:,:,N-n_half+1:N)=C_T_B2;

	   call deriv_O_n(ux_2(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uxs_2(1:m0,1:N,nf));
	   call deriv_O_n(uy_2(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uys_2(1:m0,1:N,nf));
	   call deriv_O_n(uz_2(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uzs_2(1:m0,1:N,nf));

	   xsus_2(1,1:N,nf)=uxs_2(1,1:N,nf)*x_ns0(1,1:N,nf)+ &
	   &  uys_2(1,1:N,nf)*y_ns0(1,1:N,nf)+uzs_2(1,1:N,nf)*z_ns0(1,1:N,nf);

	   F_T(k-N+1:k,1)=F_T(k-N+1:k,1)-xsus_2(1,1:N,nf)/2.0
	    	       	   	    
	enddo;

	print *, "**************************************"


	enddo

	


	end program main  
