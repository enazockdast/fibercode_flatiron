	module biophysics
	use input_file
	use  initialize
        use derivatives
	implicit none
	contains 

	subroutine polymerize()
        implicit none

        real*8 :: alpha, fac(N_f+1),r_min
        integer :: seed(N_f+1)
        integer :: m00,clock

        real*8, parameter :: V_g0=0.12d0, V_s0=-0.288d0 ! micron / sec
        real*8, parameter :: f_cat0=0.014d0, f_res0=0.044d0 ! frequency 1/sec
        real*8 :: V_g, V_s, f_cat, f_res


        V_g=(V_g0/a_SP0)*tau_s
        V_s=(V_s0/a_SP0)*tau_s

        f_cat=f_cat0*tau_s*dt0
        f_res=f_res0*tau_s*dt0

        print *,"###############################"
        print *, "V_G is  ", V_g
        print *,"###############################"

        print *, "V_S is  ", V_s
        print *,"###############################"
        print *, "f_cat is  ", f_cat
        print *,"###############################"
        print *, "f_res is  ", f_res
        print *,"###############################"


                m00=2

        do nf=1,N_f+1
        call system_clock(count=clock)
        seed(nf)=(t_step-1)*N_f+2*nf+1+clock
        enddo
        call random_seed(put=seed(1:N_f+1))
        call random_number(fac)
        
        
                

!                seed(1)=3


        print *, "number of fibers is", N_f 

!        call random_seed(put=seed)

        do nf=1,N_f

            if (flagp_p(nf) .eq.  1) then 

                if (fac(nf+1) < f_cat) then
                flag_p(nf)=-1
                beta_n(nf)=V_s
                else
                flag_p(nf)=1
                beta_n(nf)=V_g;
                endif

            elseif (flagp_p(nf) .eq. (-1) ) then

               if (fac(nf+1) <f_res) then

                flag_p(nf)=1
                beta_n(nf)=V_g
                else
                flag_p(nf)=-1
                beta_n(nf)=V_s
                endif 
           
           endif

                call ellips_min (x(N0,nf),y(N0,nf),z(N0,nf),a_cor,b_cor,r_min)
                
          
             if ((r_min .le. 0.50) .or. (L_F(nf) .ge. 16.0)) then

                flag_p(nf)=-1
                beta_n(nf)=V_s

                print *, "REACHED THE BOUNDARY!"
                print *, "SHOULD DEPOLYMERIZE"
             endif
    
    
                if ((L_F(nf) .le. 0.50))  then
        

                flag_p(nf)=1
                beta_n(nf)=V_g
!                        N_var(nf)=N_var_p(nf)

                endif



        flagp_p(nf)=flag_p(nf);
!        N_var_p(nf)=N_var(nf)
        enddo
        

        end subroutine polymerize

!************************************************************************!
!************************************************************************!
!************************************************************************!

        subroutine ellips_min(x_end,y_end,z_end,a_cor,b_cor,r_min)

        implicit none

        real*8,intent(IN) :: x_end, y_end, z_end
        real*8, intent (in)  :: a_cor,b_cor
        real*8 :: r_min
        real*8 :: r_end,teta_end,f_end,fp_end
        integer :: i

        r_end=sqrt(x_end**2+y_end**2);
        teta_end=atan2(a_cor*r_end,b_cor*z_end);

        do i=1,4;
          f_end=(a_cor**2-b_cor**2)*sin(2.0*teta_end)/2.0-z_end*a_cor*sin(teta_end)+r_end*b_cor*cos(teta_end);
          fp_end=(a_cor**2-b_cor**2)*cos(2.0*teta_end)-z_end*a_cor*cos(teta_end)-r_end*b_cor*sin(teta_end);    
          teta_end=teta_end-f_end/fp_end;    
        enddo;
              
        r_min=sqrt((r_end-b_cor*sin(teta_end))**2+(z_end-a_cor*cos(teta_end))**2)



        end subroutine ellips_min

!*******************************************************************!
!*******************************************************************!
!*******************************************************************!

        end module biophysics 
