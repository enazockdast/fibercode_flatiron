        module BoundaryIntegral



        use input_file
        use initialize
        use  GAUSS_ARRAY
        use BI_SP_build_array
        use BI_cor_build_array
        use GaussInt_Array
        use linear_algebra



        implicit none 

        contains

        subroutine inv_BI() 
        implicit none

            call  gauss_descrete(nt_SP,ntt_SP,a_SP,b_SP,gauss_n,gr,&
            & A_S_SP,I1_SP,I2_SP,I3_SP,x_g_SP,y_g_SP,z_g_SP, &
            & n_N_SP,nx_SP,ny_SP,nz_SP,h_SP,h_SP_node,coor_SP,con_SP, &
            & xm_SP,ym_SP,zm_SP,F)

            call GaussQuad(gauss_p,gauss_w,F,gauss_n)
            gauss_p0(:)=gauss_p(:,1);
            gauss_w0(:)=gauss_w(:,1);


            call  gauss_descrete(nt_cor,ntt_cor,a_cor,b_cor,gauss_n, &
            & gr, A_S_cor,I1_cor,I2_cor,I3_cor,x_g_cor,y_g_cor,z_g_cor,&
            & n_N_cor,nx_cor,ny_cor,nz_cor,h_cor,h_cor_node,coor_cor,con_cor,&
            & xm_cor,ym_cor,zm_cor,F)
                
                
            call SP_T(T_SP,nt_SP,ntt_SP,gauss_n,xm_SP,ym_SP,zm_SP,&
            & x_g_SP,y_g_SP,z_g_SP,h_SP,n_N_SP, con_SP,&
            &  I1_SP,I2_SP,I3_SP,A_S_SP,gauss_w0,F)
                
                
                
            call cor_T(T_cor,nt_cor,ntt_cor,gauss_n,xm_cor,ym_cor,zm_cor,&
            & x_g_cor,y_g_cor,z_g_cor,h_cor,n_N_cor, con_cor, &
            I1_cor,I2_cor,I3_cor,A_S_cor,gauss_w0,F)
                
            call inv(3*n_node_cor,T_cor,iT_cor,info_cor)
            call inv(3*n_node_SP,T_SP,iT_SP,info_SP)
        
                 coor_SP(:,1)=coor_SP(:,1)+e_SP(1)
                 coor_SP(:,1)=coor_SP(:,2)+e_SP(2)
                 coor_SP(:,3)=coor_SP(:,3)+e_SP(3)

                 Qx_cor(:)=0.0;Qy_cor(:)=0.0;Qz_cor(:)=0.0;
                 Qx_SP(:)=0.0;Qy_SP(:)=0.0;Qz_SP(:)=0.0
                 Q_SP(:)=0.0
                 Q_cor(:)=0.0
                 QxSP_g(:)=0.0;QySP_g(:)=0.0;QzSP_g(:)=0.0;
                 Qxcor_g(:)=0.0;Qycor_g(:)=0.0;Qzcor_g(:)=0.0;





                 U_bath_cor(:)=0.0
                 U_bath_SP_2(:)=0.0



           targ_SP(1,:)=xm_SP(:)+e_SP(1);
           targ_SP(2,:)=ym_SP(:)+e_SP(2);
           targ_SP(3,:)=zm_SP(:)+e_SP(3);

           source_SP=targ_SP

!           source_SP(1,:)=x_g_SP(:)+e_SP(1)
!           source_SP(2,:)=y_g_SP(:)+e_SP(2)
!           source_SP(3,:)=z_g_SP(:)+e_SP(3)

           targ_cor(1,:)=xm_cor(:)
           targ_cor(2,:)=ym_cor(:)
           targ_cor(3,:)=zm_cor(:)
                
            source_cor=targ_cor
!           source_cor(1,:)=x_g_cor(:)
!           source_cor(2,:)=y_g_cor(:)
!           source_cor(3,:)=z_g_cor(:)

            source_SPCOR(1:3,1:nparts_SP)=source_SP
            source_SPCOR(1:3,nparts_SP+1:nparts_SPCOR)=source_cor
        
            source_sp_l=source_SP(1:3,nparts_sp_l*id+1:nparts_sp_l*(id+1))
            source_cor_l=source_cor(1:3,nparts_cor_l*id+1:nparts_cor_l*(id+1))

            source_spcor_l(:,1:nparts_sp_l)=source_sp_l
            source_spcor_l(:,nparts_sp_l+1:nparts_spcor_l)=source_cor_l
            targ_sp_l=source_sp_l;targ_cor_l=source_cor_l;

      end subroutine inv_BI

!***********************************************************!
!***********************************************************!
!***********************************************************!

        subroutine BI_compute()
        implicit none
        integer :: iter
        real*8 :: err1, err2,alpha0,beta0

        alpha0=1.0;beta0=0.0

        Qxcor_g(:)=0.0;Qycor_g(:)=0.0;Qzcor_g(:)=0.0;
        QxSP_g(:)=0.0;QySP_g(:)=0.0;QzSP_g(:)=0.0;


           targ_SP(1,:)=xm_SP(:)+e_SP(1);
           targ_SP(2,:)=ym_SP(:)+e_SP(2);
           targ_SP(3,:)=zm_SP(:)+e_SP(3);

!           source_SP(1,:)=x_g_SP(:)+e_SP(1)
!           source_SP(2,:)=y_g_SP(:)+e_SP(2)
!           source_SP(3,:)=z_g_SP(:)+e_SP(3)

            source_SP=targ_SP
!            source_cor=targ_cor;

            source_sp_l=source_SP(1:3,nparts_sp_l*id+1:nparts_sp_l*(id+1))
            source_cor_l=source_cor(1:3,nparts_cor_l*id+1:nparts_cor_l*(id+1))

            targ_sp_l=source_sp_l;
            targ_cor_l=source_cor_l;
        
            source_spcor_l(:,1:nparts_sp_l)=source_sp_l
            source_spcor_l(:,nparts_sp_l+1:nparts_spcor_l)=source_cor_l

        
        do iter=1,6
        
!        sigma_dl_cor(1,:)=Qxcor_g(:)
!        sigma_dl_cor(2,:)=Qycor_g(:)
!        sigma_dl_cor(3,:)=Qzcor_g(:)
!        sigma_dv_cor(:,:)=n_N_cor(:,:)

        sigma_dl_cor(1,:)=Qx_cor(:)*h_cor_node(:)
        sigma_dl_cor(2,:)=Qy_cor(:)*h_cor_node(:)
        sigma_dl_cor(3,:)=Qz_cor(:)*h_cor_node(:)
        sigma_dv_cor(1,:)=nx_cor(:)
        sigma_dv_cor(2,:)=ny_cor(:)
        sigma_dv_cor(3,:)=nz_cor(:)

!        print *, "maximum Q_cor is", maxval(Qx_cor),minval(Qx_cor)
!        print *, "h_cor_node is", maxval(h_cor_node), minval(h_cor_node)

        iprec=2;ifsingle=0;ifdouble=1;ifpot=0;ifgrad=0;ifpottarg=1;ifgradtarg=0;

!        call stfmm3Dparttarg(ier,iprec,nparts_cor,source_cor,ifsingle,sigma_sl_cor,ifdouble,&
!        & sigma_dl_cor,sigma_dv_cor,ifpot,pot_cor,pre_cor,ifgrad,grad_cor,&
!        & ntargs_SP,targ_SP,ifpottarg,pottarg_SP,pretarg_SP,ifgradtarg,gradtarg_SP)


        call stfmm3Dparttarg(ier,iprec,nparts_cor,source_cor,ifsingle,sigma_sl_cor,ifdouble,&
        & sigma_dl_cor,sigma_dv_cor,ifpot,pot_cor,pre_cor,ifgrad,grad_cor,&
        & ntargs_SP_l,targ_SP_l,ifpottarg,pottarg_SP_l,pretarg_SP_l,ifgradtarg,gradtarg_SP_l)

        do i=1,ntargs_sp_l
         pottarg_sp_1D_l ((i-1)*3+1)=pottarg_sp_l(1,i)
         pottarg_sp_1D_l ((i-1)*3+2)=pottarg_sp_l(2,i)
         pottarg_sp_1D_l ((i-1)*3+3)=pottarg_sp_l(3,i)
        enddo

        call pvfmm_mpi_gather(pottarg_sp_1D_l,ntargs_sp_l*3,pottarg_sp_1D,3*ntargs_sp_l,0)
        call pvfmm_mpi_bcast(pottarg_sp_1D,3*ntargs_sp,0)

        U_bath_SP_1=(-1.0d0/(4.0d0*pi))*pottarg_sp_1D

!        do i=1,ntargs_SP
!        U_bath_SP_1((i-1)*3+1)=(-1.0/(4.0*pi))*(pottarg_SP(1,i))
!        U_bath_SP_1((i-1)*3+2)=(-1.0/(4.0*pi))*(pottarg_SP(2,i))
!        U_bath_SP_1((i-1)*3+3)=(-1.0/(4.0*pi))*(pottarg_SP(3,i))
!        enddo
        
!        print *, "size of U_bath is", size(U_bath_SP_1),size(pottarg_SP(1,:)) 

!        print *, "maximum of U_bath_SP is",maxval(U_bath_SP_1),minval(U_bath_SP_1)
!        print *, "maximum of U_bath_sing is",maxval(U_bath_SP_2),minval(U_bath_SP_2)
        
        call USP_sing(U_bath_SP_2,F_SP,Tq_SP,xm_SP,ym_SP,zm_SP,0.0*e_SP,n_node_SP)                
!        Q_SP=-(U_bath_SP_0+U_bath_SP_2+U_bath_SP_1)
         Q_SP(:)=0.0
        CALL DGEMM('N','N',3*n_node_SP,1,3*n_node_SP,alpha0,iT_SP,&
        & 3*n_node_SP,-(U_bath_SP_0+U_bath_SP_1+U_bath_SP_2),3*n_node_SP,beta0,Q_SP,3*n_node_SP)
!         Q_SP=matmul(iT_SP,Q_SP)                 
        do i=1,n_node_SP
        Qx_SP(i)=Q_SP((i-1)*3+1)
        Qy_SP(i)=Q_SP((i-1)*3+2)
        Qz_SP(i)=Q_SP((i-1)*3+3)
        enddo
                        
        call GaussQ_FMM(QxSP_g,QySP_g,QzSP_g, &
        & Qx_SP,Qy_SP,Qz_SP,gauss_n,gauss_w0,h_SP,nt_SP,ntt_SP,F,con_SP);


        call BI_VOmega_FMM(V_SP,Omega_SP,QxSP_g,QySP_g,QzSP_g,&
        & x_g_SP,y_g_SP,z_g_SP,A_S_SP,&
        & I1_SP,I2_SP,I3_SP,gauss_n,n_element_SP,0.0*e_SP)
        if (id .eq. 0) then
         print *,"----------------------"
         print *,  "V_SP  is ", V_SP(1),V_SP(2),V_SP(3)
         print *,"----------------------"
        
         print *, "Omega_SP  is ", Omega_SP(1),Omega_SP(2),Omega_SP(3)
         print *,"----------------------"
        endif 

            if (iter >2) then
!
            err1=maxval(abs((V_SP-V_prev)/V_SP))
            err2=maxval(abs(V_prev-V_pp))
!            print *, "errors are   ", err1, err2
            if (err1<1.0e-3 .or. err2 < 1.0e-3) then
            goto 10
            endif
            endif

           if (iter .eq. 1) then

            V_prev=V_SP

            else
            V_pp=V_prev
            V_prev=V_SP
            endif

!        sigma_dl_SP(1,:)=QxSP_g(:)
!        sigma_dl_SP(2,:)=QySP_g(:)
!        sigma_dl_SP(3,:)=QzSP_g(:)
!        sigma_dv_SP(:,:)=n_N_SP(:,:)

        sigma_dl_SP(1,:)=Qx_SP(:)*h_SP_node(:)
        sigma_dl_SP(2,:)=Qy_SP(:)*h_SP_node(:)
        sigma_dl_SP(3,:)=Qz_SP(:)*h_SP_node(:)
        sigma_dv_SP(1,:)=nx_SP(:)
        sigma_dv_SP(2,:)=ny_SP(:)
        sigma_dv_SP(3,:)=nz_SP(:)

        iprec=2;ifsingle=0;ifdouble=1;ifpot=0;ifgrad=0;ifpottarg=1;ifgradtarg=0;

!        call stfmm3Dparttarg(ier,iprec,nparts_SP,source_SP,ifsingle,sigma_sl_SP,ifdouble,&
!        & sigma_dl_SP,sigma_dv_SP,ifpot,pot_SP,pre_SP,ifgrad,grad_SP,&
!        & ntargs_cor,targ_cor,ifpottarg,pottarg_cor,pretarg_cor,ifgradtarg,gradtarg_cor)
        
        call stfmm3Dparttarg(ier,iprec,nparts_SP,source_SP,ifsingle,sigma_sl_SP,ifdouble,&
        & sigma_dl_SP,sigma_dv_SP,ifpot,pot_SP,pre_SP,ifgrad,grad_SP,&
        & ntargs_cor_l,targ_cor_l,ifpottarg,pottarg_cor_l,pretarg_cor_l,ifgradtarg,gradtarg_cor_l)



        do i=1,ntargs_cor_l
         pottarg_cor_1D_l ((i-1)*3+1)=pottarg_cor_l(1,i)
         pottarg_cor_1D_l ((i-1)*3+2)=pottarg_cor_l(2,i)
         pottarg_cor_1D_l ((i-1)*3+3)=pottarg_cor_l(3,i)
        enddo

        call pvfmm_mpi_gather(pottarg_cor_1D_l,ntargs_cor_l*3,pottarg_cor_1D,3*ntargs_cor_l,0)
        call pvfmm_mpi_bcast(pottarg_cor_1D,3*ntargs_cor,0)

        U_bath_cor=(-2.0d0)*pottarg_cor_1D


!        do i=1,ntargs_cor
!        U_bath_cor((i-1)*3+1)=(-2.0)*(pottarg_cor(1,i))
!        U_bath_cor((i-1)*3+2)=(-2.0)*(pottarg_cor(2,i))
!        U_bath_cor((i-1)*3+3)=(-2.0)*(pottarg_cor(3,i))
!        enddo


       call USP_sing(U_bath_cor_2,F_SP,Tq_SP,xm_cor,ym_cor,zm_cor,e_SP,n_node_cor)


!       print *, "size of U_bath_cor is", size(U_bath_cor),size(pottarg_cor(1,:)) 

!        print *, "maximum of U_bath_cor is",maxval(U_bath_cor),minval(U_bath_cor)
!        print *, "maximum of U_bath_sing cor is",maxval(U_bath_cor_2),minval(U_bath_cor_2)

!        Q_cor=-(U_bath_cor_0+U_bath_cor+U_bath_cor_2)
        Q_cor(:)=0.0
        CALL DGEMM('N','N',3*n_node_cor,1,3*n_node_cor,alpha0,iT_cor,&
        & 3*n_node_cor,-(U_bath_cor_0+U_bath_cor+U_bath_cor_2),3*n_node_cor,beta0,Q_cor,3*n_node_cor)
!        Q_cor=matmul(iT_cor,Q_cor)
                
        do i=1,n_node_cor
        Qx_cor(i)=Q_cor((i-1)*3+1)
        Qy_cor(i)=Q_cor((i-1)*3+2)
        Qz_cor(i)=Q_cor((i-1)*3+3)
        enddo
!        print *, "maximum Q_cor is", maxval(Qx_cor),minval(Qx_cor)

        call GaussQ_FMM(Qxcor_g,Qycor_g,Qzcor_g, Qx_cor,Qy_cor,Qz_cor,& 
        & gauss_n,gauss_w0,h_cor,nt_cor,ntt_cor,F,con_cor);
        
        call BI_FT(F_cor,Tq_cor,Qxcor_g,Qycor_g,Qzcor_g,h_cor,&
        & x_g_cor,y_g_cor,z_g_cor,gauss_n,gauss_w0,n_element_cor,e_cor)
        enddo

10      sigma_sl_SPCOR(1:3,:)=0.0;
        sigma_dl_SPCOR(1:3,1:nparts_SP)=sigma_dl_SP(1:3,:)*(-2.0)
        sigma_dl_SPCOR(1:3,nparts_SP+1:nparts_SPCOR)=sigma_dl_cor(1:3,:)*(-1.0/(4.0*pi))
        source_SPCOR(1:3,1:nparts_SP)=source_SP(1:3,:)
        source_SPCOR(1:3,nparts_SP+1:nparts_SPCOR)=source_cor(1:3,:)
        sigma_dv_SPCOR(1:3,1:nparts_SP)=sigma_dv_SP(1:3,1:nparts_SP)
        sigma_dv_SPCOR(1:3,nparts_SP+1:nparts_SPCOR)=sigma_dv_cor(1:3,:)

        
        end subroutine BI_compute
!***********************************************!
!***********************************************!
!***********************************************!

        subroutine BI_mobility()
        implicit none

        integer :: i2
        U_bath_SP_0(:)=0.0;
        U_bath_cor_0(:)=0.0;
        do i2=1,6

        print *, " I is ...", i2
        F_SP(:)=0.0;Tq_SP=0.0;
        if (i2 .le. 3) then
        F_SP(i2)=6.0*pi
        else
        Tq_SP(i2-3)=8.0*pi
        endif 
        
        call BI_compute ()

        V_MOB(1:3,i2)=V_SP(1:3);
        O_MOB(1:3,i2)=Omega_SP(1:3)
        sigma_dl_SPCOR_MOB(:,:,i2)=sigma_dl_SPCOR(:,:)

        enddo;
        
        end subroutine BI_mobility
        
!***********************************************!
!***********************************************!
!***********************************************!

        subroutine BI_MTSPCOR_MOB
        implicit none

        if (mod(t_step,100) .eq. 0 .or. t_step<2) then
        call BI_mobility()
        endif 

        sigma_sl_SPCOR(1:3,:)=0.0;
        source_SPCOR(1:3,1:n_node_SP)=source_SP(1:3,:)
        source_SPCOR(1:3,n_node_SP+1:n_node_SP+n_node_cor)=source_cor(1:3,:)
        sigma_dv_SPCOR(1:3,1:n_node_SP)=sigma_dv_SP(1:3,1:n_node_SP)
        sigma_dv_SPCOR(1:3,n_node_SP+1:n_node_SP+n_node_cor)=sigma_dv_cor(1:3,:)
        do i=1,6

        sigma_dl_SPCOR(:,:)=sigma_dl_SPCOR_MOB(:,:,i)

        iprec=3;
        ifsingle=0;
        ifdouble=1;
        ifpot=0;
        ifgrad=0;
        ifpottarg=1;
        ifgradtarg=0;
        pottarg_SPCOR(1:3,:)=0.0;
        pretarg_SPCOR(:)=0.0;
        gradtarg_SPCOR(1:3,1:3,:)=0.0;


!        call stfmm3Dparttarg(ier,iprec,nparts_SPCOR,source_SPCOR,ifsingle,sigma_sl_SPCOR,ifdouble,&
!        & sigma_dl_SPCOR,sigma_dv_SPCOR,ifpot,pot_SPCOR,pre_SPCOR,ifgrad,grad_SPCOR, &
!        & nparts_MT,source_MT,ifpottarg,pottarg_SPCOR,pretarg_SPCOR,ifgradtarg,gradtarg_SPCOR)
 
        pottarg_SPCOR_MOB(:,:,i)=pottarg_SPCOR(:,:)

        enddo

        end subroutine BI_MTSPCOR_MOB

!***********************************************************!
        subroutine BI_SPCOR2MT
        implicit none
        iprec=3;
        ifsingle=0;
        ifdouble=1;
        ifpot=0;
        ifgrad=0;
        ifpottarg=1;
        ifgradtarg=0;
        pottarg_SPCOR(1:3,:)=0.0;
        pretarg_SPCOR(:)=0.0;
        gradtarg_SPCOR(1:3,1:3,:)=0.0;
        ntargs_SPCOR=nparts_MT



!        call stfmm3Dparttarg(ier,iprec,nparts_SPCOR,source_SPCOR,ifsingle,sigma_sl_SPCOR,ifdouble,&
!        & sigma_dl_SPCOR,sigma_dv_SPCOR,ifpot,pot_SPCOR,pre_SPCOR,ifgrad,grad_SPCOR, &
!        & ntargs_SPCOR,source_MT,ifpottarg,pottarg_SPCOR,pretarg_SPCOR,ifgradtarg,gradtarg_SPCOR)

        call USP_sing(U_bath_MT_0,F_SP,Tq_SP,source_MT(1,:),source_MT(2,:),source_MT(3,:),e_SP,nparts_MT)

        do i=1,nparts_MT

        pottarg_SPCOR(1,i)=pottarg_SPCOR(1,i)+U_bath_MT_0((i-1)*3+1)
        pottarg_SPCOR(2,i)=pottarg_SPCOR(2,i)+U_bath_MT_0((i-1)*3+2)
        pottarg_SPCOR(3,i)=pottarg_SPCOR(3,i)+U_bath_MT_0((i-1)*3+3)        
      
        enddo
        
        end subroutine BI_SPCOR2MT
!*********************************************************8

        subroutine COR_2SPMT()
        implicit none
        real*8 :: alpha0,beta0
        alpha0=1.0;beta0=0.0
         k=0
!        sigma_sl_MT=1.0d0*sigma_sl_MT/(8.0d0*mu0(1)/6.0d0)
!        do nf=1,N_f

!        N=N_var(nf)
!        sigma_sl_MT(1:3,k+1:k+N)=(1.0*sigma_sl_MT_T(1:3,k+1:k+N)+1.0*sigma_sl_MT_B(1:3,k+1:k+N))/(8.0*mu0(nf)/6.0)
!        k=k+N
!        enddo
!        N_total=N0*N_f;
!        iprec=3;
!!       nparts=N_total+1;
!        nparts_MT=N0*N_f
!        ifsingle=1;
!        ifdouble=0;
!        ifpot=0;
!        ifgrad=0;
!        ntarg_MT=n_node_SP+n_node_cor;
!        ifpottarg=1;
!        ifgradtarg=0;
!        sigma_dl_MT(1:3,:)=0.0;
!        sigma_dv_MT(1:3,:)=0.0;
!        sigma_dv_MT(1,:)=1.0;
!        pot_MT(1:3,:)=0.0;
!        pre_MT(:)=0.0;
!        grad_MT(1:3,1:3,:)=0.0;
!        pottarg_MT(:,:)=0.0
!        pretarg_MT(:)=0.0
!        gradtarg_MT(:,:,:)=0.0

!        targ_SP(1,:)=xm_SP(:)+e_SP(1);
!        targ_SP(2,:)=ym_SP(:)+e_SP(2);
!        targ_SP(3,:)=zm_SP(:)+e_SP(3);

!! !       source_SP(1,:)=x_g_SP(:)+e_SP(1)
!! !       source_SP(2,:)=y_g_SP(:)+e_SP(2)
 !!!       source_SP(3,:)=z_g_SP(:)+e_SP(3)

!        source_SP=targ_SP;


!        targ_MT(1:3,1:n_node_SP)=targ_SP(1:3,:)
!        targ_MT(1:3,n_node_SP+1:n_node_SP+n_node_cor)=targ_cor(1:3,:)

!        call stfmm3Dparttarg(ier,iprec,nparts_MT,source_MT,ifsingle,sigma_sl_MT,ifdouble,&
!        & sigma_dl_MT,sigma_dv_MT, ifpot,pot_MT,pre_MT,ifgrad,grad_MT, &
!        & ntarg_MT,targ_MT,ifpottarg,pottarg_MT,pretarg_MT,ifgradtarg,gradtarg_MT)

!        do i=1,ntargs_SP
!        U_bath_SP_0((i-1)*3+1)=(1.50d0)*(pottarg_MT(1,i))/(8.0d0*mu0(1)/6.0d0)
!        U_bath_SP_0((i-1)*3+2)=(1.50d0)*(pottarg_MT(2,i))/(8.0d0*mu0(1)/6.0d0)
!        U_bath_SP_0((i-1)*3+3)=(1.50d0)*(pottarg_MT(3,i))/(8.0d0*mu0(1)/6.0d0)
!        enddo
!
!        do i=1,ntargs_cor
!        U_bath_cor_0((i-1)*3+1)=(2.0d0)*(pottarg_MT(1,i+ntargs_SP))
!        U_bath_cor_0((i-1)*3+2)=(2.0d0)*(pottarg_MT(2,i+ntargs_SP))
!        U_bath_cor_0((i-1)*3+3)=(2.0d0)*(pottarg_MT(3,i+ntargs_SP))
!        enddo


!        print *, "Tension force is ", F_MT_T
!        print *, "Bending force is ...", F_MT_B

        F_SP(:)=1.0*8.0*pi*(1.0*F_MT_T(:,1)+1.0*F_MT_B(:,1)+1.0*F_EXT(:,1));
        Tq_SP(:)=1.0*8.0*pi*(T_MT_T(:,1)+T_MT_B(:,1)+1.0*Tq_EXT(:,1));
!        F_SP(:)=1.0*6.0*pi*(1.0*V_T0_T(:,1)+1.0*V_T0_B(:,1)+1.0*V_EXT(:,1));
!        Tq_SP(:)=1.0*8.0*pi*(T_MT_T(:,1)+T_MT_B(:,1)+1.0*Tq_EXT(:,1));

        call BI_compute ()
        
        print *, "PASSED BI_COMPUTE"

        print *, "*******************************"
        print *, "computed SP velocity from BI is "
        print *, V_SP(:)
        print *, Omega_SP(:)
!        print *, F_MT_T(:,1)
!        print *, F_MT_B(:,1)
        print *, "*******************************"
        dV(:,1)=V_SP(:)
        dW(:,1)=Omega_SP(:)
!        Q_SP=-(U_bath_SP_1)
!         Q_SP=matmul(iT_SP,Q_SP)
         Q_SP(:)=0.0
        CALL DGEMM('N','N',3*n_node_SP,1,3*n_node_SP,alpha0,iT_SP,&
        & 3*n_node_SP,-U_bath_SP_1,3*n_node_SP,beta0,Q_SP,3*n_node_SP)

        print *, "MAXIMUM U_BATH_SP_1", maxval(abs(U_bath_SP_1))

         do i=1,n_node_SP
        Qx_SP(i)=Q_SP((i-1)*3+1)
        Qy_SP(i)=Q_SP((i-1)*3+2)
        Qz_SP(i)=Q_SP((i-1)*3+3)
        enddo
!        print *, "MAXVAL Q_SP backward is ", maxval(Q_SP),minval(Q_SP)

        call GaussQ_FMM(QxSP_g,QySP_g,QzSP_g, &
        & Qx_SP,Qy_SP,Qz_SP,gauss_n,gauss_w0,h_SP,nt_SP,ntt_SP,F,con_SP);


        call BI_VOmega_FMM(V_SP,Omega_SP,QxSP_g,QySP_g,QzSP_g,&
        & x_g_SP,y_g_SP,z_g_SP,A_S_SP,&
        & I1_SP,I2_SP,I3_SP,gauss_n,n_element_SP,0.0*e_SP)
!        if (iter0 .eq. 1) then
        V_SPCOR(:,1)=V_SP(:);
        Omega_SPCOR(:,1)=Omega_SP(:)
!        endif 

        print *, "############################"
        print *, "backward velocity is "
        print *, V_SPCOR(:,1)
        print *, "############################"
        print *, "backward rotation is "
        print *, Omega_SPCOR(:,1)
        print *, "############################"


!        Q_SP=-(U_bath_SP_1+U_bath_SP_0)
         Q_SP(:)=0.0
        CALL DGEMM('N','N',3*n_node_SP,1,3*n_node_SP,alpha0,iT_SP,&
        & 3*n_node_SP,-(U_bath_SP_1+U_bath_SP_0+U_bath_SP_2),3*n_node_SP,beta0,Q_SP,3*n_node_SP)
!         Q_SP=matmul(iT_SP,Q_SP)

         do i=1,n_node_SP
        Qx_SP(i)=Q_SP((i-1)*3+1)
        Qy_SP(i)=Q_SP((i-1)*3+2)
        Qz_SP(i)=Q_SP((i-1)*3+3)
        enddo

        call GaussQ_FMM(QxSP_g,QySP_g,QzSP_g, &
        & Qx_SP,Qy_SP,Qz_SP,gauss_n,gauss_w0,h_SP,nt_SP,ntt_SP,F,con_SP);


!        sigma_dl_SP(1,:)=QxSP_g(:)
!        sigma_dl_SP(2,:)=QySP_g(:)
!        sigma_dl_SP(3,:)=QzSP_g(:)
!        sigma_dl_SPCOR(1:3,1:nparts_SP)=sigma_dl_SP(1:3,:)*(-2.0)

        sigma_dl_SP(1,:)=Qx_SP(:)*h_SP_node(:)
        sigma_dl_SP(2,:)=Qy_SP(:)*h_SP_node(:)
        sigma_dl_SP(3,:)=Qz_SP(:)*h_SP_node(:)
        sigma_dl_SPCOR(1:3,1:nparts_SP)=sigma_dl_SP(1:3,:)*(-2.0)

        sigma_dl_sp_l=sigma_dl_SP(1:3,nparts_sp_l*id+1:nparts_sp_l*(id+1))
        sigma_dv_sp_l=sigma_dv_SP(1:3,nparts_sp_l*id+1:nparts_sp_l*(id+1))

        sigma_dl_cor_l=sigma_dl_cor(1:3,nparts_cor_l*id+1:nparts_cor_l*(id+1))
        sigma_dv_cor_l=sigma_dv_cor(1:3,nparts_cor_l*id+1:nparts_cor_l*(id+1))
        
        sigma_dl_spcor_l(:,1:nparts_sp_l)=sigma_dl_sp_l*(-2.0d0)
        sigma_dl_spcor_l(:,nparts_sp_l+1:nparts_spcor_l)=sigma_dl_cor_l*(-1.0/(4.0*pi))

        sigma_dv_spcor_l(:,1:nparts_sp_l)=sigma_dv_sp_l
        sigma_dv_spcor_l(:,nparts_sp_l+1:nparts_spcor_l)=sigma_dv_cor_l

!        sl_den(:)=0.0;sldl_pot(:)=0.0;


!        PRINT *, "ID IS ", ID
!        print *, "maxval sigma_dl_l is", sigma_dl_cor_l(3,33),sigma_dl_cor(3,33),size(dl_den_nor),nsrc*6
!        call random_number(dl_den_nor)
!          dl_den_nor(:)=-0.0d0
!        do i=1,nsrc

!          if (i .le. nparts_MT) then


!             dl_den_nor((i-1)*6+1)=(8.0d0*PI)*0.0d0/fac2**2
!             dl_den_nor((i-1)*6+2)=(8.0d0*PI)*0.0d0/fac2**2
!             dl_den_nor((i-1)*6+3)=(8.0d0*PI)*0.0d0/fac2**2

!             dl_den_nor((i-1)*6+4)=0.0d0
!             dl_den_nor((i-1)*6+5)=0.0d0
!             dl_den_nor((i-1)*6+6)=1.0d0

!           else

!                dl_den_nor((i-1)*6+1)=sigma_dl_SPCOR_l(1,i-nparts_MT)/fac2**2
!                dl_den_nor((i-1)*6+2)=sigma_dl_SPCOR_l(2,i-nparts_MT)/fac2**2
!                dl_den_nor((i-1)*6+3)=sigma_dl_SPCOR_l(3,i-nparts_MT)/fac2**2
!                dl_den_nor((i-1)*6+4)=sigma_dv_SPCOR_l(1,i-nparts_MT)
!                dl_den_nor((i-1)*6+5)=sigma_dv_SPCOR_l(2,i-nparts_MT)
!                dl_den_nor((i-1)*6+6)=sigma_dv_SPCOR_l(3,i-nparts_MT)

!           endif

!             dl_den_nor((i-1)*6+1)=0.0
!             dl_den_nor((i-1)*6+2)=0.0
!             dl_den_nor((i-1)*6+3)=-0.0e-3
!             dl_den_nor((i-1)*6+4)=0.0
!             dl_den_nor((i-1)*6+5)=0.0
!             dl_den_nor((i-1)*6+6)=1.0
!        enddo

!        FLAG_PVFMM=1

!        call pvfmm_mpi_barrier()
!        print *, "REACHED pvfmm_sldl"
!        call pvfmmstokes_sldl(src,sl_den,nsrc,src,dl_den_nor,nsrc,trg,sldl_pot,ntrg,max_pts,FLAG_PVFMM)
!        call pvFmmStokes_sl(src,sl_den,nsrc,trg,sldl_pot,ntrg,max_pts,FLAG_PVFMM)
!        print *, "PASSED SLDL CONSTRUCTION"

!      do i=1,nparts_MT

!        pot_MT(1,i)=sldl_pot((i-1)*3+1)
!         pot_MT(2,i)=sldl_pot((i-1)*3+2)
!         pot_MT(3,i)=sldl_pot((i-1)*3+3)
!         
!       end do
               
!        do nf=1,N_f
!          k=(nf-1)*N0
!           ux_SPCOR(1:N0,nf)=pot_MT(1,k+1:k+N0)
!           uy_SPCOR(1:N0,nf)=pot_MT(2,k+1:k+N0)
!           uz_SPCOR(1:N0,nf)=pot_MT(3,k+1:k+N0)
!        
!        enddo

!        iprec=3;
!        ifsingle=0;
!        ifdouble=1;
!        ifpot=0;
!        ifgrad=0;
!        ifpottarg=1;
!        ifgradtarg=0;
!        pottarg_SPCOR(1:3,:)=0.0;
!        pretarg_SPCOR(:)=0.0;
!        gradtarg_SPCOR(1:3,1:3,:)=0.0;
!        ntargs_SPCOR=nparts_MT

!        call stfmm3Dparttarg(ier,iprec,nparts_SPCOR,source_SPCOR,ifsingle,sigma_sl_SPCOR,ifdouble,&
!        & sigma_dl_SPCOR,sigma_dv_SPCOR,ifpot,pot_SPCOR,pre_SPCOR,ifgrad,grad_SPCOR, &
!        & ntargs_SPCOR,source_MT,ifpottarg,pottarg_SPCOR,pretarg_SPCOR,ifgradtarg,gradtarg_SPCOR)



!        call BI_SPCOR2MT()



!        k=0
!        if (iter0 .eq. 1) then
!        do nf=1,N_f
!        N=N_var(nf)
!           ux_SPCOR(1:N,nf)=pottarg_SPCOR(1,k+1:k+N)
!           uy_SPCOR(1:N,nf)=pottarg_SPCOR(2,k+1:k+N)
!           uz_SPCOR(1:N,nf)=pottarg_SPCOR(3,k+1:k+N)
!        k=k+N
!        enddo
!        V_SP(:)=0.0
!        ux_SPCOR(:,:)=0.0;uy_SPCOR(:,:)=0.0;uz_SPCOR(:,:)=0.0;
        end subroutine COR_2SPMT
!*************************************************************************88

        subroutine bulk_flow()

!************* MT induced flow field *******************!
        k=0
        do nf=1,N_f

        N=N_var(nf)

        sigma_sl_MT(1,(nf-1)*N0+1:nf*N0)=(fx_MT_T(1:N0,nf)+fx_MT_B(1:N0,nf))*chev_w(1:N0)*L_FF(nf)
        sigma_sl_MT(2,(nf-1)*N0+1:nf*N0)=(fy_MT_T(1:N0,nf)+fy_MT_B(1:N0,nf))*chev_w(1:N0)*L_FF(nf)
        sigma_sl_MT(3,(nf-1)*N0+1:nf*N0)=(fz_MT_T(1:N0,nf)+fz_MT_B(1:N0,nf))*chev_w(1:N0)*L_FF(nf)
        k=k+N
        enddo

        
        N_total=N0*N_f;
        iprec=3;
!       nparts=N_total+1;
        nparts_MT=N_total
        ifsingle=1;
        ifdouble=0;
        ifpot=0;
        ifgrad=0;
        ifpottarg=1;
        ifgradtarg=0;
        sigma_dl_MT(1:3,:)=0.0;
        sigma_dv_MT(1:3,:)=0.0;
        sigma_dv_MT(1,:)=1.0;
        pot_MT(1:3,:)=0.0;
        pre_MT(:)=0.0;
        grad_MT(1:3,1:3,:)=0.0;

        targ_bulk(1,:)=x_bulk+xc0(1)
        targ_bulk(2,:)=y_bulk+xc0(2)
        targ_bulk(3,:)=z_bulk+xc0(3)

        pottarg_bulk_MT(:,:)=0.0
        pretarg_bulk(:)=0.0
        gradtarg_bulk(:,:,:)=0.0

        

        call stfmm3Dparttarg(ier,iprec,nparts_MT,source_MT,ifsingle,sigma_sl_MT,ifdouble,&
        & sigma_dl_MT,sigma_dv_MT, ifpot,pot_MT,pre_MT,ifgrad,grad_MT, &
        & ntarg_bulk,targ_bulk,ifpottarg,pottarg_bulk_MT,pretarg_bulk,ifgradtarg,gradtarg_bulk)

!********************************************************************************************************!
!************ Flow field induced by hydrodynamic interactions of Spindle and Cortex with MTs ************!

        iprec=3;
        ifsingle=0;
        ifdouble=1;
        ifpot=0;
        ifgrad=0;
        ifpottarg=1;
        ifgradtarg=0;
        pottarg_bulk_SPCOR(1:3,:)=0.0;
        

        call stfmm3Dparttarg(ier,iprec,nparts_SPCOR,source_SPCOR,ifsingle,sigma_sl_SPCOR,ifdouble,&
        & sigma_dl_SPCOR,sigma_dv_SPCOR,ifpot,pot_SPCOR,pre_SPCOR,ifgrad,grad_SPCOR, &
        & ntarg_bulk,targ_bulk,ifpottarg,pottarg_bulk_SPCOR,pretarg_bulk,ifgradtarg,gradtarg_bulk)


        end subroutine bulk_flow

!****************************************************************************************************!

      end module BoundaryIntegral   
