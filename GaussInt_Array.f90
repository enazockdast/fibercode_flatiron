	module GaussInt_Array
	
	implicit none
	
	contains 

	subroutine USP(U_bath,x2,y2,z2,x_g,y_g,z_g, &
	& n_N,Qx_g,Qy_g,Qz_g,h,F_EXT,T_EXT,e, &
	&  gauss_n,gauss_w,n_node,n_element)
	
	implicit none
	
	
	real*8, dimension(:) :: x2,y2,z2,U_bath,gauss_w
	real*8 :: xm,ym,zm,nx,ny,nz,B0,Qx,Qy,Qz,dot,dot3
	real*8 :: Kxx,Kxy,Kxz,Kyy,Kyz,Kzz,UX,UY,UZ,rp,rp3
	real*8 :: ux2,uy2,uz2
	real*8, dimension(:) :: x_g,y_g,z_g,Qx_g,Qy_g,Qz_g,h
	real*8, dimension (:,:) :: n_N
	real*8 :: F_EXT(:),T_EXT(:),e(:),one_8pi,r,r5
	integer :: gauss_n,i,j,k,m,l,n_node,n_element,k2

	
	
	
	one_8pi=1.0/(32.0*atan(1.0))
	
	U_bath(:)=0.0;
	
	


	do i=1,n_node;
	
	
	ux2=0.0;uy2=0.0;uz2=0.0;
	
	rp=sqrt((x2(i)-e(1))**2+(y2(i)-e(2))**2+(z2(i)-e(3))**2);
			
      do m=1,n_element
      
      
		do j=1,gauss_n;
			
			do k=1,gauss_n;
				k2=(m-1)*gauss_n**2+(j-1)*gauss_n+k
				xm=x_g(k2);
				ym=y_g(k2);
				zm=z_g(k2);
				nx=n_N(1,k2);
				ny=n_N(2,k2);
				nz=n_N(3,k2);
				B0=h(k2);
				Qx=Qx_g(k2);
				Qy=Qy_g(k2);
				Qz=Qz_g(k2);
		
		
		dot=(x2(i)-xm)*nx+(y2(i)-ym)*ny+(z2(i)-zm)*nz;
		r=((x2(i)-xm)**2+(y2(i)-ym)**2+(z2(i)-zm)**2)**0.5;
		r5=r**5;

		
			   Kxx=-6.0*(xm-x2(i))*(xm-x2(i))*dot/r5;
			   Kxy=-6.0*(xm-x2(i))*(ym-y2(i))*dot/r5;
			   Kxz=-6.0*(xm-x2(i))*(zm-z2(i))*dot/r5;
			   Kyy=-6.0*(ym-y2(i))*(ym-y2(i))*dot/r5;
			   Kyz=-6.0*(ym-y2(i))*(zm-z2(i))*dot/r5;
			   Kzz=-6.0*(zm-z2(i))*(zm-z2(i))*dot/r5;			  
			   UX=Kxx*Qx+Kxy*Qy+Kxz*Qz;
			   UY=Kyy*Qy+Kxy*Qx+Kyz*Qz;
			   UZ=Kxz*Qx+Kyz*Qy+Kzz*Qz;
			   
			   
			   

			   ux2=ux2+ gauss_w(j)*gauss_w(k)*(UX*B0);
		   
		   
			   uy2=uy2+ &
				 &  gauss_w(j)*gauss_w(k)*(UY*B0);
		   
		   
			   uz2=uz2+ &
				 &  gauss_w(j)*gauss_w(k)*(UZ*B0);
			enddo;
			
		enddo;
		
		
		
	  enddo
	  
	  

		
		dot3=(x2(i)-e(1))*F_EXT(1)+(y2(i)-e(2))*F_EXT(2)+(z2(i)-e(3))*F_EXT(3);
	     rp3=rp**3
		U_bath((i-1)*3+1)=ux2+ &
		&   one_8pi*(F_EXT(1)/rp+dot3*(x2(i)-e(1))/rp**3)+ &
		+one_8pi*(T_EXT(2)*(z2(i)-e(3))-T_EXT(3)*(y2(i)-e(2)))/rp3;
		   
			   
		U_bath((i-1)*3+2)=uy2+ &
		&  one_8pi*(F_EXT(2)/rp+dot3*(y2(i)-e(2))/rp**3)+&
		& one_8pi*(T_EXT(3)*(x2(i)-e(1))-T_EXT(1)*(z2(i)-e(3)))/rp3;
		   
		U_bath((i-1)*3+3)=uz2+ &
		& one_8pi*(F_EXT(3)/rp+dot3*(z2(i)-e(3))/rp3) +&
		& one_8pi*(T_EXT(1)*(y2(i)-e(2))-T_EXT(2)*(x2(i)-e(1)))/rp3;
			
	!			  if ((i < 5) ) then
	!	print *,"------------ subroutine -----"
	!	print *,U_bath((i-1)*3+1),e(1),e(2),e(3)
	!	print *,"------------------------------"
	!	endif   
			   
	enddo;
	
	
	end subroutine USP
	
	
	
	
	subroutine Ucor(U_bath,x2,y2,z2,x_g,y_g,z_g, &
	& n_N,Qx_g,Qy_g,Qz_g,h, gauss_n,gauss_w,n_node_1,n_node_2)
	
	implicit none
	
	
	real*8, dimension(:) :: x2,y2,z2,U_bath,gauss_w
	real*8 :: xm,ym,zm,nx,ny,nz,B0,Qx,Qy,Qz,dot
	real*8 :: Kxx,Kxy,Kxz,Kyy,Kyz,Kzz,UX,UY,UZ
	real*8 :: ux2,uy2,uz2
	real*8, dimension(:) :: x_g,y_g,z_g,Qx_g,Qy_g,Qz_g,h
	real*8, dimension (:,:) :: n_N
	real*8 :: three_4pi,r,r5
	integer :: gauss_n,i,j,k,m,l,n_node_1,n_node_2,k2

	
	
	
	three_4pi=3.0/(16.0*atan(1.0))

	print *,"gauss weights are", gauss_w
	print *, "the constant is " , three_4pi
	open(unit=10,file="U_cor_2.txt");

	do i=1,n_node_1;
	
	ux2=0.0;uy2=0.0;uz2=0.0;
			
      do m=1,n_node_2
      
      
		do j=1,gauss_n;
			
			do k=1,gauss_n;
				k2=(m-1)*gauss_n**2+(j-1)*gauss_n+k
				xm=x_g(k2);
				ym=y_g(k2);
				zm=z_g(k2);
				nx=n_N(1,k2);
				ny=n_N(2,k2);
				nz=n_N(3,k2);
				B0=h(k2);
				Qx=Qx_g(k2);
				Qy=Qy_g(k2);
				Qz=Qz_g(k2);
		
		
		dot=(x2(i)-xm)*nx+(y2(i)-ym)*ny+(z2(i)-zm)*nz;
		r=((x2(i)-xm)**2+(y2(i)-ym)**2+(z2(i)-zm)**2)**0.5;
		r5=r**5;

		
			   Kxx=(xm-x2(i))*(xm-x2(i))*dot/r5;
			   Kxy=(xm-x2(i))*(ym-y2(i))*dot/r5;
			   Kxz=(xm-x2(i))*(zm-z2(i))*dot/r5;
			   Kyy=(ym-y2(i))*(ym-y2(i))*dot/r5;
			   Kyz=(ym-y2(i))*(zm-z2(i))*dot/r5;
			   Kzz=(zm-z2(i))*(zm-z2(i))*dot/r5;
			  
			   
			   UX=Kxx*Qx+Kxy*Qy+Kxz*Qz;
			   UY=Kyy*Qy+Kxy*Qx+Kyz*Qz;
			   UZ=Kxz*Qx+Kyz*Qy+Kzz*Qz;
			   		   

			   ux2=ux2+ gauss_w(j)*gauss_w(k)*(UX*B0);	   
			   uy2=uy2+ gauss_w(j)*gauss_w(k)*(UY*B0) 		   
			   uz2=uz2+ gauss_w(j)*gauss_w(k)*(UZ*B0);
			   
			enddo;
			
		enddo;
		
	  enddo

		U_bath((i-1)*3+1)=ux2*(-three_4pi)		   
		U_bath((i-1)*3+2)=uy2*(-three_4pi)
		U_bath((i-1)*3+3)=uz2*(-three_4pi)
		
		
	enddo;
	do i=1,3*n_node_1
	write (10,30) U_bath(i)
	enddo
30		format(1X,1F10.6)	
	
	
	end subroutine Ucor 
	
	!------------------------------------------------------!
	!------------------------------------------------------!
	
	subroutine USP_sing(U_bath,F_EXT,T_EXT,xm0,ym0,zm0,e,n_node)
	
	implicit none
	
	real*8, intent(IN), dimension (:) :: xm0,ym0,zm0,e,F_EXT,T_EXT 
	real*8, intent (out) :: U_bath(:)
	integer :: i,n_node
	real*8, dimension(n_node) :: xm,ym,zm
	real*8 :: dot3,rp,rp3,one_8pi
	
	
	
	U_bath(:)=0.0
	
	
	xm(:)=xm0(:)-e(1)
	ym(:)=ym0(:)-e(2)
	zm(:)=zm0(:)-e(3)
	
	one_8pi=1.0/(32.0*atan(1.0))
!	print *,"------------------------"
!	print *,F_EXT(1),F_EXT(2),F_EXT(3)
!	print *,"------------------------"
!	print *,T_EXT(1),T_EXT(2),T_EXT(3)
	
	do i=1,n_node
	
	dot3=xm(i)*F_EXT(1)+ym(i)*F_EXT(2)+zm(i)*F_EXT(3);
	rp=sqrt(xm(i)**2+ym(i)**2+zm(i)**2)
	     rp3=rp**3
		U_bath((i-1)*3+1)=one_8pi*((F_EXT(1)/rp+dot3*xm(i)/rp3)+ &
		+(T_EXT(2)*zm(i)-T_EXT(3)*ym(i))/rp3);
		   
			   
		U_bath((i-1)*3+2)=one_8pi*((F_EXT(2)/rp+dot3*ym(i)/rp3)+&
		& +(T_EXT(3)*xm(i)-T_EXT(1)*zm(i))/rp3);
		   
		U_bath((i-1)*3+3)=one_8pi*((F_EXT(3)/rp+dot3*zm(i)/rp3) +&
		&+(T_EXT(1)*ym(i)-T_EXT(2)*xm(i))/rp3);
		
	enddo
	
	end subroutine USP_sing
	
	
	
	
	
	
	
	subroutine BI_VOmega(V,Omega,Qx_g,Qy_g,Qz_g,h,&
    & x_g,y_g,z_g,A_S,I1,I2,I3,gauss_n,gauss_w,n_element,e)
    
    
    implicit none 
    
    
    real*8, dimension(:) :: x_g,y_g,z_g,h,Qx_g,Qy_g,Qz_g
    real*8 :: V(:),Omega(:),gauss_w(:),e(:)
    real*8 :: dum,I1,I2,I3,A_S,pi
    integer :: i,j,k,gauss_n,n_element,k2
      
	Omega(:)=0.0;V(:)=0.0;pi=4.0*atan(1.0)
	do i=1,n_element;
		do j=1,gauss_n;
			do k=1,gauss_n;
				
				k2=(i-1)*gauss_n**2+(j-1)*gauss_n+k
				dum=h(k2)*gauss_w(j)*gauss_w(k);
		
		V(1)=V(1)+Qx_g(k2)*dum;
		V(2)=V(2)+Qy_g(k2)*dum;
		V(3)=V(3)+Qz_g(k2)*dum;
		Omega(1)=Omega(1)-&
		& 4.0*pi*dum*(-Qy_g(k2)*(z_g(k2)-e(3))+&
		& Qz_g(k2)*(y_g(k2)-e(2)))/I1;
		
		Omega(2)=Omega(2)-&
		& 4.0*pi*dum*(-Qz_g(k2)*(x_g(k2)-e(1))+&
		& Qx_g(k2)*(z_g(k2)-e(3)))/I2;
		
		Omega(3)=Omega(3)-&
		& 4.0*pi*dum*(-Qx_g(k2)*(y_g(k2)-e(2))+&
		Qy_g(k2)*(x_g(k2)-e(1)))/I3;
		
			enddo;
		enddo;
		
	enddo;

	V=V*(-4.0*pi/A_S);
	
	end subroutine BI_VOmega
	
	
	
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	       subroutine BI_VOmega_FMM(V,Omega,Qx_g,Qy_g,Qz_g,&
    & x_g,y_g,z_g,A_S,I1,I2,I3,gauss_n,n_element,e)


    implicit none


    real*8, dimension(:) :: x_g,y_g,z_g,Qx_g,Qy_g,Qz_g
    real*8 :: V(:),Omega(:),e(:)
    real*8 :: I1,I2,I3,A_S,pi
    integer :: i,j,k,gauss_n,n_element,k2

        Omega(:)=0.0;V(:)=0.0;pi=4.0*atan(1.0)
        do i=1,n_element;
                do j=1,gauss_n;
                        do k=1,gauss_n;
                                
                                k2=(i-1)*gauss_n**2+(j-1)*gauss_n+k
                
                V(1)=V(1)+Qx_g(k2);
                V(2)=V(2)+Qy_g(k2);
                V(3)=V(3)+Qz_g(k2);
                Omega(1)=Omega(1)-&
                & 2.0*pi*(-Qy_g(k2)*(z_g(k2)-e(3))+&
                & Qz_g(k2)*(y_g(k2)-e(2)))/I1;
                
                Omega(2)=Omega(2)-&
                & 2.0*pi*(-Qz_g(k2)*(x_g(k2)-e(1))+&
                & Qx_g(k2)*(z_g(k2)-e(3)))/I2;
                
                Omega(3)=Omega(3)-&
                & 2.0*pi*(-Qx_g(k2)*(y_g(k2)-e(2))+&
                Qy_g(k2)*(x_g(k2)-e(1)))/I3;
                
                        enddo;
                enddo;
                
        enddo;

        V=V*(-4.0*pi/A_S);
        
        end subroutine BI_VOmega_FMM
        
!**********************************************************!
!**********************************************************!
!**********************************************************!
		subroutine BI_FT(F_EXT,T_EXT,Qx_g,Qy_g,Qz_g,h,&
    & x_g,y_g,z_g,gauss_n,gauss_w,n_element,e)
    
    
    implicit none 
    
    
    real*8, dimension(:) :: x_g,y_g,z_g,h,Qx_g,Qy_g,Qz_g
    real*8 :: F_EXT(:),T_EXT(:),gauss_w(:),e(:)
    real*8 :: dum,I1,I2,I3,A_S,pi
    integer :: i,j,k,gauss_n,n_element,k2

	T_EXT(:)=0.0;F_EXT(:)=0.0;pi=4.0*atan(1.0)
	do i=1,n_element;
		do j=1,gauss_n;
			do k=1,gauss_n;
				
				k2=(i-1)*gauss_n**2+(j-1)*gauss_n+k
				dum=h(k2)*gauss_w(j)*gauss_w(k);
		
		F_EXT(1)=F_EXT(1)+Qx_g(k2)*dum;
		F_EXT(2)=F_EXT(2)+Qy_g(k2)*dum;
		F_EXT(3)=F_EXT(3)+Qz_g(k2)*dum;
		T_EXT(1)=T_EXT(1)+&
		& dum*(Qy_g(k2)*(z_g(k2)-e(3))-&
		& Qz_g(k2)*(y_g(k2)-e(2)));
		
		T_EXT(2)=T_EXT(2)-&
		& dum*(Qz_g(k2)*(x_g(k2)-e(1))-&
		& Qx_g(k2)*(z_g(k2)-e(3)));
		
		T_EXT(3)=T_EXT(3)-&
		& dum*(Qx_g(k2)*(y_g(k2)-e(2))-&
		Qy_g(k2)*(x_g(k2)-e(1)));
		
			enddo;
		enddo;
		
	enddo;

	
	end subroutine BI_FT
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
	
	
	
	end module GaussInt_Array







			   
			   
		
		
