	program test_pvFmm_dl
        use, intrinsic :: iso_c_binding
        use fmm_interface
        implicit none

        !! variable
        integer :: i, j, DIM=3, mpi_rank, mpi_size
	integer :: t, t1, t2, t3, t4, t5,rate,flag
	integer (C_SIZE_T), parameter :: nsrc=1000,ntrg=nsrc,nprocs=1,nsrc_seq=nsrc*nprocs,ntrg_seq=ntrg*nprocs
        real (C_DOUBLE), allocatable, target, dimension(:) :: src,sl_den,dl_den_nor,trg,dl_pot1,dl_pot2,sl_pot
        integer (C_INT), parameter ::mult_order=4,max_pts=400,max_depth=20 !mult_order should be even
        integer (C_SIZE_T) :: FLAG_PVFMM

	integer, parameter :: iprec=3, ifsingle=1, ifdouble=1, ifpot=0,ifgrad=0,ifpottarg=1,ifgradtarg=0
	real*8 , parameter :: PI=atan(1.0d0)*4.0d0
	integer :: ier
	real (C_DOUBLE), allocatable :: seq_src_1D(:),seq_trg_1D(:),seq_src(:,:),seq_trg(:,:), &
        & seq_den(:,:), sigma_dl(:,:), sigma_dv(:,:), pre(:), pretarg(:),grad(:,:,:), gradtarg(:,:,:), &
        & seq_pot(:,:),seq_pottarg(:,:),seq_pot_flat(:), dir_pot(:),dir_pot_dl(:)
        real (C_DOUBLE) :: fac
        type (C_PTR) :: sl_context, sldl_context        
        logical (C_INT), parameter :: true=1, false=0

        real (C_DOUBLE) :: dr(3), dot,dr0,ndot,qdot

        fac=1.0d0
        !! allocate/init
        allocate(src(DIM*nsrc), sl_den(DIM*nsrc), dl_den_nor(2*DIM*nsrc),&
        & trg(DIM*ntrg), dl_pot1(DIM*ntrg), dl_pot2(DIM*ntrg), sl_pot(DIM*ntrg))

        allocate(seq_src(DIM,nsrc_seq),seq_src_1D(DIM*nsrc_seq),seq_trg_1D(DIM*ntrg_seq), &
        & seq_den(DIM,nsrc_seq), sigma_dl(DIM,nsrc_seq),sigma_dv(DIM,nsrc_seq),&
        & pre(nsrc_seq), grad(DIM,DIM,nsrc_seq),seq_pot(DIM,nsrc_seq),&
        & seq_pot_flat(DIM*ntrg), dir_pot(DIM*ntrg),dir_pot_dl(DIM*ntrg))

        allocate(seq_pottarg(DIM,ntrg),pretarg(ntrg),gradtarg(DIM,DIM,ntrg),seq_trg(DIM,ntrg_seq))



        call random_number(seq_src_1D)
        call random_number(seq_trg_1D)
        call random_number(seq_den)
        call random_number(sigma_dl)
!        call random_number(sigma_dv)
!        sigma_dv(3,:)=sqrt(1.0d0-sigma_dv(2,:)**2-sigma_dv(3,:)**2);
!        call random_number(src)
!        call random_number(sl_den)
!        call random_number(dl_den_nor)
        
!        seq_trg_1D=seq_src_1D

        sigma_dv(1,:)=1.0d0;sigma_dv(2:3,:)=0.0d0
        !sigma_dl(1,:)=1.0d0;sigma_dl(2,:)=2.0d0;sigma_dl(3,:)=1.0d0;
        !seq_den(1,:)=-1.0d0;seq_den(2,:)=-1.0d0;seq_den(3,:)=2.0d0;

        do i=1,nsrc_seq
          seq_src(1,i)=seq_src_1D((i-1)*3+1)*fac
          seq_src(2,i)=seq_src_1D((i-1)*3+2)*fac
          seq_src(3,i)=seq_src_1D((i-1)*3+3)*fac
        enddo

        do i=1,ntrg_seq
          seq_trg(1,i)=seq_trg_1D((i-1)*3+1)*fac
          seq_trg(2,i)=seq_trg_1D((i-1)*3+2)*fac
          seq_trg(3,i)=seq_trg_1D((i-1)*3+3)*fac
        enddo

         call MPI_INIT(mpi_rank,mpi_size)

           call make_fmm_context(mult_order, max_pts, max_depth, true, false, false,sl_context) !sl, dl, periodic, context 
           call make_fmm_context(mult_order, max_pts, max_depth, true, true , false ,sldl_context) !sl, dl, periodic, context 
!**********************************************************************!

        print *,"MPI rank is ", mpi_rank

        do i=1,nsrc


           src((i-1)*3+1)=seq_src(1,mpi_rank*nsrc+i)
           src((i-1)*3+2)=seq_src(2,mpi_rank*nsrc+i)
           src((i-1)*3+3)=seq_src(3,mpi_rank*nsrc+i)


           sl_den((i-1)*3+1)=(8.0d0*PI)*seq_den(1,i+mpi_rank*nsrc)
           sl_den((i-1)*3+2)=(8.0d0*PI)*seq_den(2,i+mpi_rank*nsrc)
           sl_den((i-1)*3+3)=(8.0d0*PI)*seq_den(3,i+mpi_rank*nsrc)

           dl_den_nor((i-1)*6+1)=(8.0d0*PI)*sigma_dl(1,i+mpi_rank*nsrc)
           dl_den_nor((i-1)*6+2)=(8.0d0*PI)*sigma_dl(2,i+mpi_rank*nsrc)
           dl_den_nor((i-1)*6+3)=(8.0d0*PI)*sigma_dl(3,i+mpi_rank*nsrc)

           dl_den_nor((i-1)*6+4)=sigma_dv(1,i+mpi_rank*nsrc)
           dl_den_nor((i-1)*6+5)=sigma_dv(2,i+mpi_rank*nsrc)
           dl_den_nor((i-1)*6+6)=sigma_dv(3,i+mpi_rank*nsrc)

        end do
        
        do i=1,ntrg
           trg((i-1)*3+1)=seq_trg(1,mpi_rank*nsrc+i)
           trg((i-1)*3+2)=seq_trg(2,mpi_rank*nsrc+i)
           trg((i-1)*3+3)=seq_trg(3,mpi_rank*nsrc+i)
        enddo

        call system_clock(t1, rate) !to get the rate

        print *, "NSRC is ", nsrc_seq,nsrc
!        trg=src
        FLAG_PVFMM=1
      do t=1,1
        flag=1
        call system_clock(t1)
         call stokes_sl_fmm(nsrc, src, sl_den, ntrg, trg,sl_pot,true,sl_context)
!         call stokes_sl_fmm(nsrc, src, sl_den, nsrc, src, sl_pot, false, sl_context)
!        call system_clock(t2)
!        FLAG_PVFMM=0        
!        call system_clock(t3)
!         call stfmm3Dpartself(ier,iprec,nsrc_seq,seq_src,ifsingle,seq_den,ifdouble,sigma_dl,sigma_dv,ifpot,seq_pot,pre,ifgrad,grad)
!        call stfmm3Dparttarg(ier,iprec,nsrc_seq,seq_src,ifsingle,seq_den,ifdouble,&
!        & sigma_dl,sigma_dv,ifpot,seq_pot,pre,ifgrad,grad,&
!        &  ntrg_seq,seq_trg,ifpottarg,seq_pottarg,pretarg,ifgradtarg,gradtarg)

        call system_clock(t2)
        FLAG_PVFMM=1
        
         call stokes_sldl_fmm(nsrc, src, 0.0d0*sl_den, nsrc, src, dl_den_nor, ntrg, trg, dl_pot1,true, sldl_context)        
!         call stokes_sl_fmm(nsrc, src, sl_den, ntrg, trg,dl_pot1,true,sl_context)
        do i=1,ntrg
           seq_pot_flat((i-1)*3+1)=seq_pottarg(1,i+mpi_rank*nsrc)
           seq_pot_flat((i-1)*3+2)=seq_pottarg(2,i+mpi_rank*nsrc)
           seq_pot_flat((i-1)*3+3)=seq_pottarg(3,i+mpi_rank*nsrc)
        end do

        print *,"***********************************"
        print *,'sldl error (vs. seq):',maxval(abs(dl_pot1-seq_pot_flat)),maxval(dl_pot1/seq_pot_flat),minval(dl_pot1/seq_pot_flat)
!        print *,'sldl error (no rebuild):',maxval(abs(dl_pot2-dl_pot1))
        print *, "sl dl vs sl", maxval(abs(dl_pot1/sl_pot)),minval(abs(dl_pot1/sl_pot))
        print *,"***********************************"

        print *,"construction and eval", real(t2-t1)/real(rate)
        print *,"just eval", real(t3-t2)/real(rate)
        print *,"sequential", real(t4-t3)/real(rate)
        dir_pot(:)=0.0d0
        dir_pot_dl(:)=0.0d0
        do i=1,ntrg
           do j=1,nsrc
                dr(1)=trg((i-1)*3+1)-src((j-1)*3+1)
                dr(2)=trg((i-1)*3+2)-src((j-1)*3+2)
                dr(3)=trg((i-1)*3+3)-src((j-1)*3+3)

                dot=seq_den(1,j)*dr(1)+seq_den(2,j)*dr(2)+seq_den(3,j)*dr(3)
                dr0=sqrt(dr(1)**2+dr(2)**2+dr(3)**2)+1.0d-12
                
                dir_pot((i-1)*3+1)=dir_pot((i-1)*3+1)+seq_den(1,j)/dr0+dot*dr(1)/dr0**3
                dir_pot((i-1)*3+2)=dir_pot((i-1)*3+2)+seq_den(2,j)/dr0+dot*dr(2)/dr0**3
                dir_pot((i-1)*3+3)=dir_pot((i-1)*3+3)+seq_den(3,j)/dr0+dot*dr(3)/dr0**3


                qdot=dr(1)*sigma_dl(1,j)+dr(2)*sigma_dl(2,j)+dr(3)*sigma_dl(3,j)
                ndot=dr(1)*sigma_dV(1,j)+dr(2)*sigma_dV(2,j)+dr(3)*sigma_dV(3,j)

                dir_pot_dl((i-1)*3+1)=dir_pot_dl((i-1)*3+1)+3.0d0*qdot*ndot*dr(1)/dr0**5
                dir_pot_dl((i-1)*3+2)=dir_pot_dl((i-1)*3+2)+3.0d0*qdot*ndot*dr(2)/dr0**5
                dir_pot_dl((i-1)*3+3)=dir_pot_dl((i-1)*3+3)+3.0d0*qdot*ndot*dr(3)/dr0**5

           enddo
        enddo
        print *, "direct vs PVFMM", & 
        & maxval((sl_pot)/dir_pot), &
        & minval((sl_pot)/dir_pot)
        print *, "Double Layer direct vs PVFMM", &
        & maxval((dl_pot1)/dir_pot_dl), &
        & minval((dl_pot1)/dir_pot_dl)

        print *, "PI is ...", pi
       enddo

        call clear_fmm_context(sl_context);
        call clear_fmm_context(sldl_context);
        call MPI_FINALIZE
	end program test_pvFmm_dl

