	module  initialize
	
	implicit none

	!*************************************************************!
	!************** Fiber Parameters *****************************!
	!*************************************************************! 
	integer :: N_f,N0,n_s,n_half,n_t
	integer :: N_teta,N_phi
	real*8  :: L, ds , a_MT
	integer, parameter :: m=4
	!*************************************************************!
	!************** Simulation Parameters ************************!
	!*************************************************************! 
	integer :: nt_final
	real*8  :: dt0, t_final
	!*************************************************************!
	!************** Spinle & Cortex Parameters *******************!
	!*************************************************************! 
	integer :: nt_SP, ntt_SP, nt_cor, ntt_cor, gauss_n
	real*8  :: a0_SP,b0_SP,a0_cor,b0_cor, L0, gr, xc0(3)
	real*8, parameter :: pi = 4.0*atan(1.0)

	!*************************************************************!
	!*****************Other variables*****************************!
	!*************************************************************!
	integer :: i,j,k,nf
	real*8, dimension (3) :: e1, e2, e3
	real*8, dimension (3,1) :: V, omega, F_L
	real*8, dimension (3,1) :: V_T0, V_Tp, Omega_T0, Omega_Tp
	real*8 :: teta0, phi0, teta_i, teta_f, phi_i, phi_f,dteta,dphi
	real*8 :: xi
	character (len=30) :: txt

	integer, allocatable :: N_var(:)
	real*8, allocatable, dimension (:) :: L_F,mu0,mu,mu1,mu2,C,C0, imu2, s(:), &
	& dum1D(:)
	real*8, allocatable, dimension (:,:) :: x,y,z,x0,y0,z0, r0, dum2D, & 
	& dxdt, dydt, dzdt, &
	& ux_ext,uy_ext,uz_ext, ux_ext_2, uy_ext_2, uz_ext_2, &
	& ux_ext_SP,uy_ext_SP,uz_ext_SP,&
	& ux_ext_MT,uy_ext_MT,uz_ext_MT,&
	& ux_ext_MT_T,uy_ext_MT_T,uz_ext_MT_T,ux_ext_MT_B,uy_ext_MT_B,uz_ext_MT_B, &
	& fx_MT,fy_MT,fz_MT,&
	& f2x_MT_B,f2y_MT_B,f2z_MT_B,f2x_MT_T,f2y_MT_T,f2z_MT_T, &
	& T	


	real*8, allocatable, dimension (:,:,:) :: xs0,ys0,zs0,xs,ys,zs, &
	& x_ns0,y_ns0,z_ns0,ux_s,uy_s,uz_s,xsus,xsuTs,xsus_f, &
	& fx_MTT,fy_MTT,fz_MTT, &
	& fx_s, fy_s, fz_s, &
	& f2x_MTT_B,f2y_MTT_B,f2z_MTT_B,f2x_MTT_T,f2y_MTT_T,f2z_MTT_T, &
	& QX,QY,QZ,QX0,QY0,QZ0, U_EXT,&
	& Ts,T_ns    

	real*8, allocatable, dimension (:,:,:) :: Tt,Tst,&
	& xt, yt, zt, &
	& xst, yst, zst, &
	& QXt, QYt, QZt, &
	& ux_t, uy_t, uz_t

	real*8, allocatable, dimension (:,:,:) :: Vatt_t, Vatt_t_2
	real*8, allocatable, dimension (:,:) :: Vt,xct
	real*8, allocatable, dimension (:,:) :: TNt

	real*8, allocatable, dimension (:,:) :: F_T,T_T,T_Tp
	real*8, allocatable, dimension (:,:,:) ::ES

	real*8, allocatable :: C_T0(:,:,:),C_T_B1(:,:,:),C_T_B2(:,:,:)
	real*8, allocatable :: c_1T(:,:), c_2T(:,:), c_1(:), c_2(:), c_3(:),time_1(:),time_2(:)

!	open (unit=1,file='input_file.txt')
!	read (1, *, end=10) txt, txt, txt, txt
!	read (1, *, end=10) dt0,txt
!	read (1, *, end=10) t_final,txt
!	read (1, *, end=10) txt, txt, txt, txt
!	read (1, *, end=10) N0,txt
!	read (1, *, end=10) N_f,txt
!	read (1, *, end=10) L0,txt,txt
!	read (1, *, end=10) n_s,txt
!	read (1, *, end=10) n_t,txt
!	read (1, *, end=10) txt, txt, txt, txt, txt, txt
!	read (1, *, end=10) nt_SP,txt
!	read (1, *, end=10) ntt_SP,txt
!	read (1, *, end=10) nt_cor,txt
!	read (1, *, end=10) ntt_cor,txt
!	read (1, *, end=10) a0_SP,txt
!	read (1, *, end=10) b0_SP,txt
!	read (1, *, end=10) a0_cor,txt
!	read (1, *, end=10) b0_cor,txt
!	read (1, *, end=10) gauss_n,txt
!	read (1, *, end=10) gr,txt
!10	close(1)


!******************************************************************************************!
!******************************************************************************************!
!******************************************************************************************!
	allocate(N_var(N_f))
	allocate(mu0(N0), mu(N0), mu1(N0), mu2(N0),  imu2(N0), &
	& L_F(N0), C(N0), C0(N0), s(N0), dum1D(N0), dum2D(m,N0))

	allocate(x(N0,N_f),y(N0,N_f),z(N0,N_f),&
	& x0(N0,N_f),y0(N0,N_f),z0(N0,N_f), r0(N0,N_f), &
	& dxdt(N0,N_f), dydt(N0,N_f), dzdt(N0,N_f), &
	& ux_ext(N0,N_f), uy_ext(N0,N_f), uz_ext(N0,N_f), &
	& ux_ext_MT(N0,N_f), uy_ext_MT(N0,N_f), uz_ext_MT(N0,N_f), &
	& ux_ext_SP(N0,N_f), uy_ext_SP(N0,N_f), uz_ext_SP(N0,N_f), &
	& ux_ext_MT_T(N0,N_f), uy_ext_MT_T(N0,N_f), uz_ext_MT_T(N0,N_f), &
	& ux_ext_MT_B(N0,N_f), uy_ext_MT_B(N0,N_f), uz_ext_MT_B(N0,N_f), &
	& fx_MT(N0,N_f), fy_MT(N0,N_f), fz_MT(N0,N_f), &
	& f2x_MT_B(N0,N_f), f2y_MT_B(N0,N_f), f2z_MT_B(N0,N_f), &
	& f2x_MT_T(N0,N_f), f2y_MT_T(N0,N_f), f2z_MT_T(N0,N_f), &
	& T(N0,N_f))

	allocate(xs0(1,N0,N_f),ys0(1,N0,N_f),zs0(1,N0,N_f), &
	& xs(1,N0,N_f),ys(1,N0,N_f),zs(1,N0,N_f), &
	& ux_s(1,N0,N_f), uy_s(1,N0,N_f), uz_s(1,N0,N_f), &
	& xsus(1,N0,N_f), xsuTs(1,N0,N_f), xsus_f(1,N0,N_f), & 
	& fx_s(1,N0,N_f), fy_s(1,N0,N_f), fz_s(1,N0,N_f), &
	& fx_MTT(1,N0,N_f), fy_MTT(1,N0,N_f), fz_MTT(1,N0,N_f), & 
	& f2x_MTT_T(1,N0,N_f), f2y_MTT_T(1,N0,N_f), f2z_MTT_T(1,N0,N_f), & 
	& f2x_MTT_B(1,N0,N_f), f2y_MTT_B(1,N0,N_f), f2z_MTT_B(1,N0,N_f), & 
	& QX0(1,N0,N_f), QY0(1,N0,N_f), QZ0(1,N0,N_f), & 
	& QX(1,N0,N_f), QY(1,N0,N_f), QZ(1,N0,N_f), & 
	& x_ns0(m,N0,N_f), y_ns0(m,N0,N_f), z_ns0(m,N0,N_f), &
	& U_EXT(3,N0,N_f),&
	& Ts(1,N0,N_f), T_ns(m,N0,N_f))   

	allocate(xt(n_t,N0,N_f),yt(n_t,N0,N_f),zt(n_t,N0,N_f), &
	& xst(n_t,N0,N_f),yst(n_t,N0,N_f),zst(n_t,N0,N_f), &
	& QXt(n_t,N0,N_f),QYt(n_t,N0,N_f),QZt(n_t,N0,N_f), &
	& ux_t(n_t,N0,N_f),uy_t(n_t,N0,N_f),uz_t(n_t,N0,N_f))

	allocate(T_T(N0*N_f,1),T_Tp(N0*N_f,1),F_T(N0*N_f,1))

	allocate(Vt(n_t,3),xct(n_t,3))
	allocate(TNt(n_t,N_f),Vatt_t(n_t,3,N_f),Vatt_t_2(n_t,3,N_f)) 
	allocate(ES(3,3,N_f))

	allocate(C_T0(m+1,n_s,N0),C_T_B1(m+1,n_s,n_half), C_T_B2(m+1,n_s,n_half))

	allocate(c_1(n_t),c_1T(n_t,1), c_2T(n_t+1,2),c_2(n_t+1), c_3(n_t), time_1(n_t), time_2(n_t+1))

!******************************************************************************************!
!******************************************************************************************!
!******************************************************************************************!

	x(:,:)=0.0; y(:,:)=0.0; z(:,:)=0.0;
	x0(:,:)=0.0; y0(:,:)=0.0; z0(:,:)=0.0; r0(:,:)=0.0;

	xs0(:,:,:)= 0.0; ys0(:,:,:)= 0.0; zs0(:,:,:)= 0.0;
	xs(:,:,:) = 0.0; ys(:,:,:) = 0.0; zs(:,:,:) = 0.0;
	x_ns0(:,:,:)=0.0;y_ns0(:,:,:)=0.0;z_ns0(:,:,:)=0.0;

	ux_ext(:,:)=0.0; uy_ext(:,:)=0.0; uz_ext(:,:)=0.0;
	ux_s(:,:,:)=0.0; uy_s(:,:,:)=0.0; uz_s(:,:,:)=0.0;   
	ux_ext_2(:,:)=0.0; uy_ext_2(:,:)=0.0; uz_ext_2(:,:)=0.0;  
	ux_ext_SP(:,:)=0.0; uy_ext_SP(:,:)=0.0; uz_ext_SP(:,:)=0.0;  
	ux_ext_MT(:,:)=0.0; uy_ext_MT(:,:)=0.0; uz_ext_MT(:,:)=0.0;  
	ux_ext_MT_T(:,:)=0.0; uy_ext_MT_T(:,:)=0.0; uz_ext_MT_T(:,:)=0.0;  
	ux_ext_MT_B(:,:)=0.0; uy_ext_MT_B(:,:)=0.0; uz_ext_MT_B(:,:)=0.0; 

	fx_MT(:,:)=0.0; fy_MT(:,:)=0.0; fz_MT(:,:)=0.0; 
	fx_MTT(:,:,:)=0.0; fy_MTT(:,:,:)=0.0; fz_MTT(:,:,:)=0.0; 
	fx_s(1,:,:)=0.0; fy_s(1,:,:)=0.0; fz_s(1,:,:)=0.0; 


	f2x_MT_B(:,:)=0.0; f2y_MT_B(:,:)=0.0; f2z_MT_B(:,:)=0.0; 
	f2x_MT_T(:,:)=0.0; f2y_MT_T(:,:)=0.0; f2z_MT_T(:,:)=0.0; 
	f2x_MTT_B(:,:,:)=0.0; f2y_MTT_B(:,:,:)=0.0; f2z_MTT_B(:,:,:)=0.0; 
	f2x_MTT_T(:,:,:)=0.0; f2y_MTT_T(:,:,:)=0.0; f2z_MTT_T(:,:,:)=0.0; 

	QX(:,:,:)=0.0; QY(:,:,:)=0.0; QZ(:,:,:)=0.0; U_EXT(:,:,:)=0.0;
	QX0(:,:,:)=0.0; QY0(:,:,:)=0.0; QZ0(:,:,:)=0.0; 


	T(:,:)=0.0;T_ns(:,:,:)=0.0; Ts(:,:,:)=0.0;
	Tt(:,:,:)=0.0; Tst(:,:,:)=0.0; TNt(:,:)=0.0;

	T_T(:,1)=0.0;T_Tp(:,1)=0.0;F_T(:,1)=0.0;

	xt(:,:,:)=0.0; yt(:,:,:)=0.0; zt(:,:,:)=0.0; 
	xst(:,:,:)=0.0; yst(:,:,:)=0.0; zst(:,:,:)=0.0;
	dxdt(:,:)=0.0; dydt(:,:)=0.0; dzdt(:,:)=0.0;
	QXt(:,:,:)=0.0; QYt(:,:,:)=0.0; QZt(:,:,:)=0.0; 
	ux_t(:,:,:)=0.0; uy_t(:,:,:)=0.0; uz_t(:,:,:)=0.0; 

	xsus(:,:,:)=0.0; xsuTs(:,:,:)=0.0;xsus_f(:,:,:)=0.0;

	Vt(:,:)=0.0; xct(:,:)=0.0; 
	Vatt_t(:,:,:)=0.0; Vatt_t_2(:,:,:)=0.0;

	ES(1:3,1:3,N_f)=0.0;

	time_1(:)=0.0; time_2(:)=0.0;
	
	L=a0_SP
	N_teta=nint(1.0*N_f/2.0);
	N_phi=nint(1.0*N_f/(1.0*N_teta));
	print *, "N_teta is .. ", N_teta
	print *, " N_Phi is ", N_phi
	do i=1, N_f
	L_F(i)=L+(i-1)*(1.50*L)/N_f
	enddo

	ds=L0/(1.0*N0-1.0);
	N_var(1:N_f)=nint(L_F(1:N_f)/ds);
	a_MT=0.002;

	 C(1:N_f)=1.0;
	 C0(1:N_f)=log((L_F/a_MT)**2*exp(1.0));
	mu0(1:N_f)=100.0*C;
	mu1=mu0;
	mu2(1:N_f)=8.0/(6.0*C0);
	imu2=1.0/mu2;
	mu(1:N_f)=mu0(1:N_f)*mu2(1:N_f);
	!%mu2(1:N_f)=6.0*(1.0*L).*C0./(8.0*L_F);

	do i=1,N0
	s(i)=L+0.2+(i*1.0-1.0)*ds
	enddo

	teta_i=-pi/6.0; teta_f=pi/6.0
	phi_i=-pi/6.0;phi_f=pi/6.0
	dteta=(teta_f-teta_i)/(N_teta*1.0-1.0)
	dphi=(phi_f-teta_i)/(N_phi*1.0-1.0)
	do j=1,N_phi
	    do i=1,N_teta;
	    
	teta0=teta_i+dteta*(i*1.0-1.0)
	phi0=phi_i+ +dphi*(j*1.0-1.0)

	x(1:N0,(j-1)*N_teta+i)=s(1:N0)*cos(teta0)*cos(phi0);
	y(1:N0,(j-1)*N_teta+i)=s(1:N0)*sin(teta0)*cos(phi0);
	z(1:N0,(j-1)*N_teta+i)=s(1:N0)*sin(phi0);
	    enddo;
	enddo;

	print *, "ckpt 1"

	 n_half=nint((n_s*1.0-1.0)/2.0);
	 call weights2(N0,n_s,n_half,m+1,s,C_T0)

	print *, "ckpt 2", n_half

	 C_T_B1=C_T0(:,:,1:n_half);
	 C_T_B2=C_T0(:,:,N0-n_half+1:N0); 
 
	 
	 do i=1,n_t
	  time_1(i)=(i-1)*1.0-n_t*1.0
	   time_2(i)=time_1(i);
	 enddo
	 time_2(n_t+1)=0.0
	 if (n_t .eq. 2) then
	 c_3=(/1.5, -0.5/)
	elseif (n_t .eq. 3) then
	c_3=(/2.25, -2.0, 0.75/)
	else
	print *, "Please check the input file. n_t should only take values of 1 or 2"
	stop
	endif 
	 
	call weights1(xi,time_1,n_t,1,c_1T)
	c_1(:)=c_1T(:,1)
	call weights1(xi,time_2,n_t+1,2,c_2T)
	c_2(:)=c_2T(:,2)

	print *, "ckpt 3"

	do nf=1,N_f;
					    
	    call deriv_O_n(x(1:N0,nf),N0, n_s,C_T0,m, x_ns0(1:m,1:N0,nf));
	    call deriv_O_n(y(1:N0,nf),N0, n_s,C_T0,m, y_ns0(1:m,1:N0,nf));
	    call deriv_O_n(z(1:N0,nf),N0, n_s,C_T0,m, z_ns0(1:m,1:N0,nf));
	    	    
	    xs0(1,:,nf)=x_ns0(1,:,nf);
	    ys0(1,:,nf)=y_ns0(1,:,nf);
	    zs0(1,:,nf)=z_ns0(1,:,nf);

		do i=1,n_t;
		    xt(i,1:N0,nf)=x(1:N0,nf);
		    yt(i,1:N0,nf)=y(1:N0,nf);
		    zt(i,1:N0,nf)=z(1:N0,nf);
		    
		    xst(i,1:N0,nf)=xs0(1,1:N0,nf);
		    yst(i,1:N0,nf)=ys0(1,1:N0,nf);
		    zst(i,1:N0,nf)=zs0(1,1:N0,nf);
	       
		enddo ;
	enddo;

	print *, "ckpt 4"
20	format(4F10.3)


	e1=(/1.0, 0.0, 0.0/);
	e2=(/0.0, 1.0, 0.0/);
	e3=(/0.0, 0.0, 1.0/);
	xc0=(/-0.0, 0.0, 0.0/);

 
	 end module initialize


