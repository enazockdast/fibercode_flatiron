    Module GAUSS_ARRAY
    implicit none 
    contains
    
    
    subroutine gauss_descrete(nt,ntt,a,b,gauss_n,gr,&
    & AS,II1,II2,II3,x_g,y_g,z_g,&
    & n_N,nx,ny,nz,h,h2,coor,con,xm,ym,zm,F)
    
    implicit none
    
    integer, intent(IN) :: nt,ntt,gauss_n
    real*8, dimension (gauss_n,gauss_n) :: df1de,df2de,df3de,df4de,&
    & df1dn,df2dn,df3dn,df4dn,f1,f2,f3,f4
    real*8, dimension (:,:,:) :: F 
    real*8 :: gauss_p(gauss_n,1),gauss_w(gauss_n,1)
    real*8 :: gauss_p_t(1,gauss_n)
    real*8, intent (IN) :: a,b,gr
    real*8, intent (OUT) :: AS,II1,II2,II3
    	
    integer :: i, j, k,k2
    integer :: nt2,ntt2
    integer :: n_node,n_element

    !real*8, dimension (:,:,:) :: x_g,y_g,z_g,h !! (n_node,gauss_n,gauss_n) ::
    !real*8, allocatable, dimension (:,:,:,:) :: n_N  !(n_node,gauss_n,gauss_n,3) 

    !real*8, allocatable, dimension (:)  :: xm, ym, zm, nx, ny, nz,dum2 !(n_node,1)
    !integer, allocatable, dimension (:,:)  :: con !(n_node,4), (n_element,4)
    !real*8, allocatable, dimension (:,:) :: coor
    
    real*8, dimension (:) :: x_g,y_g,z_g,h !! (n_node,gauss_n,gauss_n) ::
    real*8, dimension (:,:) :: n_N  !(n_node,gauss_n,gauss_n,3) 

    real*8, dimension (:)  :: xm, ym, zm, nx, ny, nz,h2 !(n_node,1)
    integer, dimension (:,:)  :: con !(n_node,4), (n_element,4)
    real*8, dimension (:,:) :: coor
    
    real*8 :: x_l(4),y_l(4),z_l(4)

    real*8 :: teta(nt), phi(ntt)
    
	real*8, allocatable, dimension(:) :: tet,tet_i,tet_T,phet,&
	&  drad_t,drad_tt,dum2
!	& tet_T(3*nt2,1), phet(ntt2),d_rad_t(nt2),d_rad_tt(ntt2)


    real*8 :: ones(gauss_n,1),ones_t(1,gauss_n),pi,x,y,z,teta0,phi0,DTETA,DPHI
	real*8 :: dxde,dyde,dzde,dxdn,dydn,dzdn,dum(3)
	
	

	nt2=nint((1.0*nt-1.0)/2.0)+1;
	ntt2=nint((1.0*ntt-1.0)/2.0)+1
	n_node=(nt-1)*ntt;n_element=(nt-1)*(ntt-1)
    	ones(:,1)=1.0;
    	ones_t(1,:)=1.0;
    	pi=atan(1.0)*4.0;

	allocate(tet(nt2),tet_i(nt2),dum2(nt2), &
	& drad_t(nt2),drad_tt(ntt2),phet(ntt2),tet_T(3*nt2))
	
	
	dum2=1.0*(/(i,i=1,nt2)/)
        dum2=dum2-1.0;

	



	II1=0.0;
	II2=0.0;
	II3=0.0;
	AS=0.0;

	
    	call GaussQuad(gauss_p,gauss_w,F,gauss_n)
    


	 gauss_p_t=transpose(gauss_p)

	 df1de(:,:)=-matmul(ones,(1.0-gauss_p_t))/4.0;
	 df1dn(:,:)=-matmul((1.0-gauss_p),ones_t)/4.0;

	 df2de(:,:)=matmul(ones,(1.0-gauss_p_t))/4.0;
	 df2dn(:,:)=-matmul((1.0+gauss_p),ones_t)/4.0;
	 
	 df3de(:,:)=matmul(ones,(1.0+gauss_p_t))/4.0;
	 df3dn(:,:)=matmul((1.0+gauss_p),ones_t)/4.0;


	 df4de(:,:)=-matmul(ones,(1.0+gauss_p_t))/4.0;
	 df4dn(:,:)= matmul((1.0-gauss_p),ones_t)/4.0;



	f1=F(:,:,1)
	f2=F(:,:,2)
	f3=F(:,:,3)
	f4=F(:,:,4)


	DTETA=2.0*pi/(nt-1);	

        
!	DPHI=(1.0*pi-pi/(4.0d0*(nt-1)))/(ntt-1)
	
        do i=1,ntt
        phi(i)=acos(2.0d0/(ntt*2.0d0)+(i-1)*2.0d0/(ntt*1.0d0)-1.0d0)
        enddo
	do i=1,nt
	teta(i)=(i-1)*DTETA
	enddo

!        do i=1,ntt
!	phi(i)=pi/(8.0d0*(nt-1))+(i-1)*DPHI
!	enddo

	call condition(con,nt,ntt)
	
	   		
	do j=1,nt-1;
	    do k=1,ntt;
	    
		x=b*cos(teta(j))*sin(phi(k));
		y=b*sin(teta(j))*sin(phi(k));
		z=a*cos(phi(k));
		
		coor((k-1)*(nt-1)+j,1)=x
		coor((k-1)*(nt-1)+j,2)=y;
		coor((k-1)*(nt-1)+j,3)=z;
	

		    
		    xm(j+(k-1)*(nt-1))=x;
		    ym(j+(k-1)*(nt-1))=y;
		    zm(j+(k-1)*(nt-1))=z;
		    nx((k-1)*(nt-1)+j)=x/b**2/sqrt((x/b**2)**2+(y/b**2)**2+(z/a**2)**2);
		    ny((k-1)*(nt-1)+j)=y/b**2/sqrt((x/b**2)**2+(y/b**2)**2+(z/a**2)**2);
		    nz((k-1)*(nt-1)+j)=z/a**2/sqrt((x/b**2)**2+(y/b**2)**2+(z/a**2)**2);
                    if ( j .ne. 1) then
                      if (k .ne. 1 .and. k .ne. ntt) then
                        h2((k-1)*(nt-1)+j)=a*b*DTETA*2.0d0*sqrt(1.0d0+(b-a)*(b+a)*z**2/a**4)/(ntt*1.0d0) 
                      else
                        h2((k-1)*(nt-1)+j)=a*b*DTETA*2.0d0*sqrt(1.0d0+(b-a)*(b+a)*z**2/a**4)/(ntt*1.0d0)
                      endif 
                    elseif (k .ne. 1 .or. k .ne. ntt) then
                      h2((k-1)*(nt-1)+j)=a*b*DTETA*2.0d0*sqrt(1.0d0+(b-a)*(b+a)*z**2/a**4)/(ntt*1.0d0)
                    else
                      h2((k-1)*(nt-1)+j)=a*b*DTETA*2.0d0*sqrt(1.0d0+(b-a)*(b+a)*z**2/a**4)/(ntt*1.0d0)
		    endif
	    enddo;
	enddo;





   
   	k2=0;

	do i=1,n_element;

    
    
	    x_l(1:4)=coor(con(i,1:4),1);
	    y_l(1:4)=coor(con(i,1:4),2);
	    z_l(1:4)=coor(con(i,1:4),3);
    
	    
	    
	    do j=1,gauss_n;
		do k=1,gauss_n;

		k2=(i-1)*gauss_n**2+(j-1)*gauss_n+k
		    x_g(k2)=x_l(1)*f1(j,k)+x_l(2)*f2(j,k)+ &
		    &    x_l(3)*f3(j,k)+x_l(4)*f4(j,k);
		    
		    y_g(k2)=y_l(1)*f1(j,k)+y_l(2)*f2(j,k)+ &
		      &  y_l(3)*f3(j,k)+y_l(4)*f4(j,k);
		    
		    z_g(k2)=z_l(1)*f1(j,k)+z_l(2)*f2(j,k)+ &
		      &  z_l(3)*f3(j,k)+z_l(4)*f4(j,k);
		    
		    dxde=x_l(1)*df1de(j,k)+x_l(2)*df2de(j,k)+ &
		     &   x_l(3)*df3de(j,k)+x_l(4)*df4de(j,k);
		    
		    dyde=y_l(1)*df1de(j,k)+y_l(2)*df2de(j,k)+ &
		      &  y_l(3)*df3de(j,k)+y_l(4)*df4de(j,k);
		    
		    dzde=z_l(1)*df1de(j,k)+z_l(2)*df2de(j,k)+ &
		     &   z_l(3)*df3de(j,k)+z_l(4)*df4de(j,k);
		    
		    dxdn=x_l(1)*df1dn(j,k)+x_l(2)*df2dn(j,k)+ &
		     &   x_l(3)*df3dn(j,k)+x_l(4)*df4dn(j,k);
		    
		    dydn=y_l(1)*df1dn(j,k)+y_l(2)*df2dn(j,k)+ &
		     &   y_l(3)*df3dn(j,k)+y_l(4)*df4dn(j,k);
		    
		    dzdn=z_l(1)*df1dn(j,k)+z_l(2)*df2dn(j,k)+ &
		     &   z_l(3)*df3dn(j,k)+z_l(4)*df4dn(j,k);
		    
		!    dum=cross( /dxde dyde dzde/,/dxdn dydn dzdn/);
			dum(1)=dyde*dzdn-dzde*dydn
			dum(2)=-dxde*dzdn+dzde*dxdn
			dum(3)=dxde*dydn-dyde*dxdn

			
		    
		    h(k2)=sqrt(dum(1)**2+dum(2)**2+dum(3)**2);
		    n_N(1,k2)=x_g(k2)/b**2/sqrt((x_g(k2)/b**2)**2+(y_g(k2)/b**2)**2+(z_g(k2)/a**2)**2);
		    n_N(2,k2)=y_g(k2)/b**2/sqrt((x_g(k2)/b**2)**2+(y_g(k2)/b**2)**2+(z_g(k2)/a**2)**2);
		    n_N(3,k2)=z_g(k2)/a**2/sqrt((x_g(k2)/b**2)**2+(y_g(k2)/b**2)**2+(z_g(k2)/a**2)**2);
		    
		
		    
		    II1=II1+(y_g(k2)**2+z_g(k2)**2)*h(k2)*gauss_w(k,1)*gauss_w(j,1);
		    II2=II2+(x_g(k2)**2+z_g(k2)**2)*h(k2)*gauss_w(k,1)*gauss_w(j,1);
		    II3=II3+(x_g(k2)**2+y_g(k2)**2)*h(k2)*gauss_w(k,1)*gauss_w(j,1);
		    
		    AS=AS+h(k2)*gauss_w(k,1)*gauss_w(j,1);
		    
		    
		enddo;
	    enddo;
	enddo;
	II1=4.0*II1;II2=4.0*II2;II3=4.0*II3;
	end subroutine gauss_descrete
	
	!--------------------------------------------------!
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!----------------------------------------------------! 

     subroutine GaussQuad(gauss_p,gauss_w,F,gauss_n)
     
     implicit none
     integer :: gauss_n
     real*8, dimension (gauss_n,1) :: gauss_p,gauss_w
	 real*8, dimension (1,gauss_n) :: gauss_p_t
     real*8, dimension(:,:,:) :: F
      


	if (gauss_n .eq. 3) then
	! gauss_n=3;% number of gauss points in each direction 
	 gauss_p(:,1)=(/-sqrt(3.0/5.0),0.0, sqrt(3.0/5.0)/);!%gauss points
	 gauss_w(:,1)=(/5.0/9.0, 8.0/9.0, 5.0/9.0/);!% weights for each gauss points

      elseif (gauss_n .eq. 6) then
!Six point guass 

     gauss_p(:,1)=(/0.6612093864662645, &
                 & -0.6612093864662645, &
                 &  0.2386191860831969, &
                 & -0.2386191860831969, &
                 & -0.9324695142031521, &
                 & -0.9324695142031521/)


	 gauss_w(:,1)=(/0.3607615730481386, &
				 & 0.3607615730481386,  &
				 & 0.4679139345726910,  &
				 & 0.4679139345726910,  &
				 & 0.1713244923791704,  &
				 & 0.1713244923791704/)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11


		elseif (gauss_n  .eq. 10) then
			
		gauss_p(:,1)=(/-0.1488743389816312, &
		&  0.1488743389816312, &
		& -0.4333953941292472, &
		&  0.4333953941292472, &
		& -0.6794095682990244, &
		&  0.6794095682990244, &
		& -0.8650633666889845, &
		&  0.8650633666889845, &
		& -0.9739065285171717, &
		&  0.9739065285171717/)

			
		 gauss_w(:,1)=(/0.2955242247147529, &  
		 &  0.2955242247147529, &  
		 &  0.2692667193099963, &  
		 &  0.2692667193099963, &  
		 &  0.2190863625159820, &  
		 &  0.2190863625159820, &  
		 &  0.1494513491505806, &  
		 &  0.1494513491505806, &  
		 &  0.0666713443086881, &  
		 &  0.0666713443086881/)
		
		endif


        gauss_p_t=transpose(gauss_p);
	F(:,:,1)=matmul((1.0-gauss_p),(1.0-gauss_p_t))/4.0;
	F(:,:,2)=matmul((1.0+gauss_p),(1.0-gauss_p_t))/4.0;
	F(:,:,3)=matmul((1.0+gauss_p),(1.0+gauss_p_t))/4.0;
	F(:,:,4)=matmul((1.0-gauss_p),(1.0+gauss_p_t))/4.0;
	
	end subroutine GAUSSQUAD
	
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1	
	
    subroutine condition(con,nt,ntt)
	
	implicit none
	
	!integer, allocatable, dimension(:,:) :: con
	integer, dimension (:,:) :: con
	integer :: i,j,k, nt, ntt,n_element
	
	n_element=(nt-1)*(ntt-1)
	
	!allocate (con(n_element,4))
	

	  do k=1,ntt-1;
	  

	 
		do j=1,nt-2;
		
	!	print *,"got this far", j,k,nt,ntt
		   
	 
		con(j+(k-1)*(nt-1),1)=j+(k-1)*(nt-1);
		con(j+(k-1)*(nt-1),2)=j+(k)*(nt-1);
		con(j+(k-1)*(nt-1),3)=j+(k)*(nt-1)+1;
		con(j+(k-1)*(nt-1),4)=j+(k-1)*(nt-1)+1;
		
		

		
		
		enddo;
	  enddo;
	  
	  
	  do k=1,ntt-1;
		
		con((k)*(nt-1),1)=(k)*(nt-1);
		con((k)*(nt-1),2)=(k+1)*(nt-1);
		con((k)*(nt-1),3)=(k)*(nt-1)+1;
		con((k)*(nt-1),4)=(k-1)*(nt-1)+1;
	  enddo;

	  
	end subroutine condition 
	
	
	subroutine GaussQ(Qx_g,Qy_g,Qz_g,Qx,Qy,Qz,gauss_n,nt,ntt,F,con)
	
	
	implicit none
	
	real*8, dimension (:)  :: Qx_g, Qy_g,Qz_g
        real*8 :: F(:,:,:) 
	
	real*8, dimension (:)  :: Qx, Qy,Qz
	
	integer, dimension (:,:) :: con 
	integer :: i,j,k,l,jj,node,n_node,n_element,nt,ntt,gauss_n,k2
	
	n_node=(nt-1)*ntt;n_element=(nt-1)*(ntt-1)
	

	Qx_g(:)=0.0;
	Qy_g(:)=0.0;
	Qz_g(:)=0.0;
	
	!print *,"F is ", F(1,1,2),F(2,1,2),F(3,1,2)

	do i=1,n_element;
		do node=1,4;
			jj=con(i,node);
			
			do j=1,gauss_n;
				do k=1,gauss_n

			k2=(i-1)*gauss_n**2+(j-1)*gauss_n+k
			
			!print *,"just a test", jj,Qx(jj)
					Qx_g(k2)=Qx_g(k2)+Qx(jj)*F(j,k,node);
					Qy_g(k2)=Qy_g(k2)+Qy(jj)*F(j,k,node);
					Qz_g(k2)=Qz_g(k2)+Qz(jj)*F(j,k,node);
					
				enddo;
			enddo;
			
		enddo;
	enddo;
            
	
	end subroutine GaussQ

	       
        subroutine GaussQ_FMM(Qx_g,Qy_g,Qz_g,Qx,Qy,Qz,gauss_n,gauss_w0,h,nt,ntt,F,con)
        
        
        implicit none
        
        real*8, dimension (:)  :: Qx_g, Qy_g,Qz_g
        real*8 :: F(:,:,:)
        
        real*8, dimension (:)  :: Qx, Qy,Qz
        real*8 :: h(:), gauss_w0(:)
        integer, dimension (:,:) :: con
        integer :: i,j,k,l,jj,node,n_node,n_element,nt,ntt,gauss_n,k2
        
        n_node=(nt-1)*ntt;n_element=(nt-1)*(ntt-1)
        

        Qx_g(:)=0.0;
        Qy_g(:)=0.0;
        Qz_g(:)=0.0;
        
        !print *,"F is ", F(1,1,2),F(2,1,2),F(3,1,2)

        do i=1,n_element;
                do node=1,4;
                        jj=con(i,node);
                        
                        do j=1,gauss_n;
                                do k=1,gauss_n

                        k2=(i-1)*gauss_n**2+(j-1)*gauss_n+k

                                        Qx_g(k2)=Qx_g(k2)+Qx(jj)*F(j,k,node)*gauss_w0(k)*gauss_w0(j)*h(k2);
                                        Qy_g(k2)=Qy_g(k2)+Qy(jj)*F(j,k,node)*gauss_w0(k)*gauss_w0(j)*h(k2);
                                        Qz_g(k2)=Qz_g(k2)+Qz(jj)*F(j,k,node)*gauss_w0(k)*gauss_w0(j)*h(k2);
                                        
                                enddo;
                        enddo;
                        
                enddo;
        enddo;

        
        end subroutine GaussQ_FMM
                                                
	end module GAUSS_ARRAY


     
       
            
