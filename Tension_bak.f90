module Tension
!USE input_file
USE sparse_solve
USE A_TIMES_X 
USE derivatives 
USE Hydro_Inter 
USE linear_algebra
use initialize
use BoundaryIntegral
use  GAUSS_ARRAY
use BI_SP_build_array
use BI_cor_build_array
use GaussInt_Array
implicit none 

contains


       subroutine Tension_GMRES()
!	USE input_file
       use initialize


      IMPLICIT NONE
      INCLUDE "mkl_rci.fi"
     
      
      INTEGER, PARAMETER :: SIZE0=128
      real (kind=dp), allocatable ::  TMP(:), DPAR(:), B(:), B2(:), B3(:), RESIDUAL(:)
	integer, allocatable :: IPAR(:)
	
      INTEGER :: k2
 
 



!---------------------------------------------------------------------------
! Some additional variables to use with the RCI (P)FGMRES solver
!---------------------------------------------------------------------------
      INTEGER :: ITERCOUNT 

      INTEGER :: RCI_REQUEST, I
      DOUBLE PRECISION :: DVAR
 !     real (kind=dp), intent (IN)  :: DS
!---------------------------------------------------------------------------
! An external BLAS function is taken from MKL BLAS to use
! with the RCI (P)FGMRES solver
!---------------------------------------------------------------------------
      DOUBLE PRECISION :: DNRM2
      EXTERNAL DNRM2

!---------------------------------------------------------------------------
! Initialize the initial guess
!---------------------------------------------------------------------------
        N_total=N0*N_f

        allocate (TMP(N_total*(2*(30)+1)+(30*(30+9))/2+1))


	allocate(DPAR(SIZE0), IPAR(SIZE0))
	allocate(B(N0), B2(N0), B3(N0), RESIDUAL(N_total))
      COMPUTED_SOLUTION(1:N_total)=T_T(1:N_total)
      RHS(1:N_total)=F_T(1:N_total)
      
!---------------------------------------------------------------------------
! Initialize the solver
!---------------------------------------------------------------------------
      CALL DFGMRES_INIT(N_total, COMPUTED_SOLUTION, RHS, RCI_REQUEST, IPAR, &
     & DPAR, TMP)
      IF (RCI_REQUEST.NE.0) GOTO 999
!---------------------------------------------------------------------------
! Set the desired parameters:
! do the restart after 2 iterations
! LOGICAL parameters:
! do not do the stopping test for the maximal number of iterations
! do the Preconditioned iterations of FGMRES method
! DOUBLE PRECISION parameters
! set the relative tolerance to 1.0D-3 instead of default value 1.0D-6
!---------------------------------------------------------------------------

       IPAR(5)=30
       IPAR(15)=6
       IPAR(8)=1
       IPAR(9)=0
       IPAR(10)=0
       IPAR(11)=1
       IPAR(12)=1
       DPAR(1)=1.0D-9

!---------------------------------------------------------------------------
! Check the correctness and consistency of the newly set parameters
!---------------------------------------------------------------------------
      CALL DFGMRES_CHECK(N_total, COMPUTED_SOLUTION, RHS, RCI_REQUEST, &
     & IPAR, DPAR, TMP)
      IF (RCI_REQUEST.NE.0) GOTO 999

!---------------------------------------------------------------------------
! Compute the solution by RCI (P)FGMRES solver with preconditioning
! Reverse Communication starts here
!---------------------------------------------------------------------------
1     CALL DFGMRES(N_total, COMPUTED_SOLUTION, RHS, RCI_REQUEST, IPAR, &
     & DPAR, TMP)

!---------------------------------------------------------------------------
! If RCI_REQUEST=0, then the solution was found with the required precision
!---------------------------------------------------------------------------
      IF (RCI_REQUEST.EQ.0) GOTO 3
!---------------------------------------------------------------------------
! If RCI_REQUEST=1, then compute the vector A*TMP(IPAR(22))
! and put the result in vector TMP(IPAR(23))
!---------------------------------------------------------------------------	

      IF (RCI_REQUEST.EQ.1) THEN
!      T_T(1:N_total)=TMP(IPAR(22):IPAR(22)+N_total-1)
!      k=0
!      do nf=1, N_f     
!     N=N_var(nf)
      
!     CALL ATIMESX(N, TMP(IPAR(22)+k:IPAR(22)+N+k-1),TMP(IPAR(23)+k:IPAR(23)+k+N-1),DS)
!     CALL ATIMESX_ON(N,N0,n_s,n_half,m,C_T0,&
!     & TMP(IPAR(22)+k:IPAR(22)+N+k-1),TMP(IPAR(23)+k:IPAR(23)+k+N-1))

!	CALL ATIMESX_ON(N,N0,n_s,n_half,m0,C_T0,&
!     & T_T(1+k:N+k),A_DOT_T(k+1:k+N))
!	
     CALL Tension_A_DOT_X(TMP(IPAR(22):IPAR(22)+N_total-1),N_total)
     TMP(IPAR(23):IPAR(23)+N_total-1)=A_DOT_T(1:N_total)
!     

!	print *, " RC1 1 is passed"
!     & TMP(IPAR(22)+k:IPAR(22)+N+k-1),TMP(IPAR(23)+k:IPAR(23)+k+N-1))
!      k=k+N
!      enddo
!	TMP(IPAR(23):IPAR(23)+N_total-1)=A_DOT_T(1:N_total)
!     do i=1,N_total
!     if (abs(TMP(IPAR(23)+i-1))>0.001) then
!     print *, TMP(IPAR(23)+i-1)
!     endif 
!     enddo

      	GOTO 1
      ENDIF
	
!	if (IPAR(4) .eq. 1) then
!	do i=1,N_total
!	print *, TMP(IPAR(22)+i)
!	enddo
!	endif 

!---------------------------------------------------------------------------
! If RCI_request=2, then do the user-defined stopping test
! The residual stopping test for the computed solution is performed here
!---------------------------------------------------------------------------
! NOTE: from this point vector B(N) is no longer containing the right-hand
! side of the problem! It contains the current FGMRES approximation to the
! solution. If you need to keep the right-hand side, save it in some other
! vector before the call to DFGMRES routine. Here we saved it in vector
! RHS(N). The vector B is used instead of RHS to preserve the original
! right-hand side of the problem and guarantee the proper restart of FGMRES
! method. Vector B will be altered when computing the residual stopping
! criterion!
!---------------------------------------------------------------------------
!      IF (RCI_REQUEST.EQ.2) THEN
! Request to the DFGMRES_GET routine to put the solution into B(N) via IPAR(13)
!	print *," RCI = 2"
!      	IPAR(13)=1
! Get the current FGMRES solution in the vector B(N)
!      	CALL DFGMRES_GET(N, COMPUTED_SOLUTION, B, RCI_REQUEST, IPAR, &
!     & DPAR, TMP, ITERCOUNT)
! Compute the current true residual via MKL (Sparse) BLAS routines
!      	CALL MKL_DCSRGEMV('N', N, A, IA, JA, B, RESIDUAL)
!      	CALL DAXPY(N, -1.0D0, RHS, 1, RESIDUAL, 1)
!      	DVAR=DNRM2(N, RESIDUAL, 1)
!      	IF (DVAR.LT.1.0E-3) THEN
!      	   GOTO 3
!      	ELSE
!      	   GOTO 1
!      	ENDIF
!      ENDIF
!---------------------------------------------------------------------------
! If RCI_REQUEST=3, then apply the preconditioner on the vector
! TMP(IPAR(22)) and put the result in vector TMP(IPAR(23))
!---------------------------------------------------------------------------
      IF (RCI_REQUEST.EQ.3) THEN

	
	k2=0
	do nf=1,N_f

	N=N_var(nf)
	B2(1:N)=TMP(IPAR(22)+k2:IPAR(22)+N+k2-1)

	call sparse_solve_1(N,IA_T(1:N+1,nf),JA_T(1:n_s*N,nf),A_T(1:n_s*N,nf),B2(1:N),B3(1:N))
	TMP(IPAR(23)+k2:IPAR(23)+k2+N-1)=B3(1:N)
	k2=k2+N
!	print *, "nf is ....", nf, N_f, N_total, k2
	enddo
	
!	print *, "passed RCI 3"
      	GOTO 1
      ENDIF
!---------------------------------------------------------------------------
! If RCI_REQUEST=4, then check if the norm of the next generated vector is
! not zero up to rounding and computational errors. The norm is contained
! in DPAR(7) parameter
!---------------------------------------------------------------------------
      IF (RCI_REQUEST.EQ.4) THEN
	
      	IF (DPAR(7).LT.1.0D-12) THEN
      	   GOTO 3
      	ELSE
      	   GOTO 1
      	ENDIF
!---------------------------------------------------------------------------
! If RCI_REQUEST=anything else, then DFGMRES subroutine failed
! to compute the solution vector: COMPUTED_SOLUTION(N)
!---------------------------------------------------------------------------
      ELSE
      	GOTO 999
      ENDIF
!---------------------------------------------------------------------------
! Reverse Communication ends here
! Get the current iteration number and the FGMRES solution. (DO NOT FORGET to
! call DFGMRES_GET routine as computed_solution is still containing
! the initial guess!). Request to DFGMRES_GET to put the solution into
! vector COMPUTED_SOLUTION(N) via IPAR(13)
!---------------------------------------------------------------------------
3     IPAR(13)=0
      CALL DFGMRES_GET(N, COMPUTED_SOLUTION, RHS, RCI_REQUEST, IPAR, &
     & DPAR, TMP, ITERCOUNT)
!---------------------------------------------------------------------------
! Print solution vector: COMPUTED_SOLUTION(N) and
! the number of iterations: ITERCOUNT
!---------------------------------------------------------------------------
!      PRINT *, ''
!      PRINT *,' The system has been solved'
!      PRINT *, ''
!      PRINT *,' The following solution has been obtained:'
!      DO I=1,N_total

!	   write(*,*) computed_solution(I)
!      ENDDO
	k=0;
	do nf=1,N_f
	N=N_var(nf)
	T_T(k+1:k+N)=computed_solution(k+1:k+N)
	T(1:N,nf)=computed_solution(k+1:k+N)
	k=k+N
	enddo
!      PRINT *, ''

!      PRINT *,' Number of iterations: ',ITERCOUNT

!---------------------------------------------------------------------------
! Release internal MKL memory that might be used for computations
! NOTE: It is important to call the routine below to avoid memory leaks
! unless you disable MKL Memory Manager
!---------------------------------------------------------------------------
      CALL MKL_FREE_BUFFERS
	goto 1000


!      STOP
!---------------------------------------------------------------------------
! Release internal MKL memory that might be used for computations
! NOTE: It is important to call the routine below to avoid memory leaks
! unless you disable MKL Memory Manager
!---------------------------------------------------------------------------
999   WRITE( *,'(A,A,I5)') 'This example FAILED as the solver has', &
     & ' returned the ERROR code', RCI_REQUEST
      CALL MKL_FREE_BUFFERS
      STOP 1

1000  continue 
      END subroutine Tension_GMRES

!**************************************************************************!
!**************************************************************************!

       subroutine implicit_GMRES()
!       USE input_file
       use initialize


      IMPLICIT NONE
      INCLUDE "mkl_rci.fi"


      INTEGER, PARAMETER :: SIZE0=128
      real (kind=dp), allocatable ::  TMP(:), DPAR(:), B(:), B2(:), B3(:), RESIDUAL(:)
        integer, allocatable :: IPAR(:)
        
      INTEGER :: k2

!---------------------------------------------------------------------------
! Some additional variables to use with the RCI (P)FGMRES solver
!---------------------------------------------------------------------------
      INTEGER :: ITERCOUNT

      INTEGER :: RCI_REQUEST, I
      DOUBLE PRECISION :: DVAR
 !     real (kind=dp), intent (IN)  :: DS
!---------------------------------------------------------------------------
! An external BLAS function is taken from MKL BLAS to use
! with the RCI (P)FGMRES solver
!---------------------------------------------------------------------------
      DOUBLE PRECISION :: DNRM2
      EXTERNAL DNRM2

!---------------------------------------------------------------------------
! Initialize the initial guess
!---------------------------------------------------------------------------
        N_total=4*N0*N_f
        N=N0

        allocate (TMP(N_total*(2*(16)+1)+(16*(16+9))/2+1))


        allocate(DPAR(SIZE0), IPAR(SIZE0))
        allocate(B(4*N0), B2(4*N0), B3(4*N0), RESIDUAL(N_total))
      COMPUTED_SOLUTION_IM(1:N_total)=XT(1:N_total)
      RHS_im(1:N_total)=F_XT(1:N_total)

        print *, computed_solution_im(N0+1:N0+3)

!---------------------------------------------------------------------------
! Initialize the solver
!---------------------------------------------------------------------------
      CALL DFGMRES_INIT(N_total, COMPUTED_SOLUTION_IM, RHS_im, RCI_REQUEST, IPAR, &
     & DPAR, TMP)
      IF (RCI_REQUEST.NE.0) GOTO 999
!---------------------------------------------------------------------------
! Set the desired parameters:
! do the restart after 2 iterations
! LOGICAL parameters:
! do not do the stopping test for the maximal number of iterations
! do the Preconditioned iterations of FGMRES method
! DOUBLE PRECISION parameters
! set the relative tolerance to 1.0D-3 instead of default value 1.0D-6
!---------------------------------------------------------------------------

       IPAR(5)=16
       IPAR(15)=16
       IPAR(8)=1
       IPAR(9)=0
       IPAR(10)=0
       IPAR(11)=1
       IPAR(12)=1
       DPAR(1)=1.0D-9

!---------------------------------------------------------------------------
! Check the correctness and consistency of the newly set parameters
!---------------------------------------------------------------------------
      CALL DFGMRES_CHECK(N_total, COMPUTED_SOLUTION_IM, RHS_im, RCI_REQUEST, &
     & IPAR, DPAR, TMP)
      IF (RCI_REQUEST.NE.0) GOTO 999

!---------------------------------------------------------------------------
! Compute the solution by RCI (P)FGMRES solver with preconditioning
! Reverse Communication starts here
!---------------------------------------------------------------------------
1     CALL DFGMRES(N_total, COMPUTED_SOLUTION_IM, RHS_im, RCI_REQUEST, IPAR, &
     & DPAR, TMP)

!---------------------------------------------------------------------------
! If RCI_REQUEST=0, then the solution was found with the required precision
!---------------------------------------------------------------------------
      IF (RCI_REQUEST.EQ.0) GOTO 3
!---------------------------------------------------------------------------
! If RCI_REQUEST=1, then compute the vector A*TMP(IPAR(22))
! and put the result in vector TMP(IPAR(23))
!---------------------------------------------------------------------------    

     IF (RCI_REQUEST.EQ.1) THEN
!       
     CALL A_DOT_X_implicit(TMP(IPAR(22):IPAR(22)+N_total-1))
     TMP(IPAR(23):IPAR(23)+N_total-1)=A_DOT_XT(1:N_total)


        GOTO 1
      ENDIF

        
!---------------------------------------------------------------------------
! If RCI_REQUEST=3, then apply the preconditioner on the vector
! TMP(IPAR(22)) and put the result in vector TMP(IPAR(23))
!---------------------------------------------------------------------------
      IF (RCI_REQUEST.EQ.3) THEN

        N=N0
        k2=0
        do nf=1,N_f
        k2=(nf-1)*4*N
        !N=N_var(nf)
        B2(1:4*N)=TMP(IPAR(22)+k2:IPAR(22)+4*N+k2-1)

        call sparse_solve_1(4*N,IA_XT2(1:4*N+1,nf),JA_XT2(1:16*n_s*N,nf),A_XT2(1:16*n_s*N,nf),B2(1:4*N),B3(1:4*N))

        TMP(IPAR(23)+k2:IPAR(23)+k2+4*N-1)=B3(1:4*N)
!        k2=k2+N*4

        enddo
        

        GOTO 1
      ENDIF

!---------------------------------------------------------------------------
! If RCI_REQUEST=4, then check if the norm of the next generated vector is
! not zero up to rounding and computational errors. The norm is contained
! in DPAR(7) parameter
!---------------------------------------------------------------------------
      IF (RCI_REQUEST.EQ.4) THEN
        
        IF (DPAR(7).LT.1.0D-12) THEN
           GOTO 3
        ELSE
           GOTO 1
        ENDIF
!---------------------------------------------------------------------------
! If RCI_REQUEST=anything else, then DFGMRES subroutine failed
! to compute the solution vector: COMPUTED_SOLUTION(N)
!---------------------------------------------------------------------------
      ELSE
        GOTO 999
      ENDIF
!---------------------------------------------------------------------------
! Reverse Communication ends here
! Get the current iteration number and the FGMRES solution. (DO NOT FORGET to
! call DFGMRES_GET routine as computed_solution is still containing
! the initial guess!). Request to DFGMRES_GET to put the solution into
! vector COMPUTED_SOLUTION(N) via IPAR(13)
!---------------------------------------------------------------------------
3     IPAR(13)=0
      CALL DFGMRES_GET(4*N0, COMPUTED_SOLUTION_IM, RHS_im, RCI_REQUEST, IPAR, &
     & DPAR, TMP, ITERCOUNT)
!---------------------------------------------------------------------------
! Print solution vector: COMPUTED_SOLUTION(N) and
! the number of iterations: ITERCOUNT
!---------------------------------------------------------------------------
!      PRINT *, ''
!      PRINT *,' The system has been solved'
!      PRINT *, ''
!      PRINT *,' The following solution has been obtained:'
!      DO I=1,N_total

!          write(*,*) computed_solution(I)
!      ENDDO
!        k=0;
        XT=computed_solution_im
!      PRINT *, ''


!---------------------------------------------------------------------------
! Release internal MKL memory that might be used for computations
! NOTE: It is important to call the routine below to avoid memory leaks
! unless you disable MKL Memory Manager
!---------------------------------------------------------------------------
      CALL MKL_FREE_BUFFERS
        goto 1000




999   WRITE( *,'(A,A,I5)') 'This example FAILED as the solver has', &
     & ' returned the ERROR code', RCI_REQUEST
      CALL MKL_FREE_BUFFERS
      STOP 1

1000  continue
      END subroutine implicit_GMRES

                                    
!**************************************************************************!
!**************************************************************************!
!**************************************************************************!
 
	subroutine Tension_A_DOT_X(X0X,NN)
!	USE input_file
	use initialize
        use BoundaryIntegral
	implicit none 

	integer ::k, nf,m0, N,i 
	real (kind=dp) :: C_T(5,n_s,N0)
	real (kind=dp), dimension (:) :: X0X
	integer, intent (IN) :: NN
	!*******************************!

        open (unit=120,file='xsus.txt')
        open (unit=130,file='u_ext.txt')
        open (unit=140, file='F_MT.txt') 
        open (unit=150,file='T2.txt')



        N_total=N_f*N0
	iprec=3;
	nparts=N_total;
	ifsingle=1;
	ifdouble=0;
	ifpot=1;
	ifgrad=0;
	ntarg=ntargs_SP+ntargs_cor;
	ifpottarg=0;
	ifgradtarg=0;

	if (FLAG_T .eq. 0) then

	sigma_sl_MT_T(:,:)=0.0;
	ux_self(:,:)=0.0;
	uy_self(:,:)=0.0;
	uz_self(:,:)=0.0;

	endif 
	


!****************************************
! ATTENSION!!!!
!	M1=M1*0.0
!	M2=M2*0.0;
!****************************************

          k=0;
          V_T0_T(1:3,1)=0.0;Omega_T0_T(1:3,1)=0.0;
          F_MT_T(:,:)=0.0;T_MT_T(:,1)=0.0;S_SP_T(:,:)=0.0;


!$OMP parallel default(shared) private(nf,N,C_T)
!$OMP do 
	do nf=1,N_f;

!	    N=N_var(nf);
            N=N0
!	    k=k+N;
	    C_T=C_T0;
!	    C_T(:,:,N-n_half+1:N)=C_T_B2;
!	    T(1:N,nf)=X0X(k-N+1:k);
            T(1:N,nf)=X0X((nf-1)*N+1:nf*N)
	    m0=2
	    call deriv_O_N(T(1:N,nf),N,n_s,C_T(:,:,1:N),m0,T_ns(1:m0,1:N,nf),L_F(nf))

	      
	    dot1(nf,1:N)=x_ns0(2,1:N,nf)**2+y_ns0(2,1:N,nf)**2+z_ns0(2,1:N,nf)**2;
	    

           F_MT_TN(1,nf)=T(1,nf)*x_ns0(1,1,nf)
           F_MT_TN(2,nf)=T(1,nf)*y_ns0(1,1,nf)
           F_MT_TN(3,nf)=T(1,nf)*z_ns0(1,1,nf)
!           FDOTR=F_MT_TN(1,nf)*dxc1(1,nf)+F_MT_TN(2,nf)*dxc1(2,nf)+F_MT_TN(3,nf)*dxc1(3,nf)           


!            S_=S_SP_T(1,2)+(F_MT_TN(1,nf)*dxc1(2,nf)+F_MT_TN(2,nf)*dxc1(1,nf))/2.0
!            S_SP_T(1,3)=S_SP_T(1,3)+(F_MT_TN(1,nf)*dxc1(3,nf)+F_MT_TN(3,nf)*dxc1(1,nf))/2.0
!            S_SP_T(3,2)=S_SP_T(3,2)+(F_MT_TN(3,nf)*dxc1(2,nf)+F_MT_TN(2,nf)*dxc1(3,nf))/2.0
!            S_SP_T(1,1)=S_SP_T(1,1)+(F_MT_TN(1,nf)*dxc1(1,nf)-FDOTR/3.0)
!            S_SP_T(2,2)=S_SP_T(2,2)+(F_MT_TN(2,nf)*dxc1(2,nf)-FDOTR/3.0)
!            S_SP_T(3,3)=S_SP_T(3,3)+(F_MT_TN(3,nf)*dxc1(3,nf)-FDOTR/3.0)
	    
	    Ten_T(1,nf)=(dxc1(2,nf)*F_MT_TN(3,nf)-dxc1(3,nf)*F_MT_TN(2,nf));
	      
	    Ten_T(2,nf)=(-dxc1(1,nf)*F_MT_TN(3,nf)+dxc1(3,nf)*F_MT_TN(1,nf));
	      
	    Ten_T(3,nf)=(dxc1(1,nf)*F_MT_TN(2,nf)-dxc1(2,nf)*F_MT_TN(1,nf));
	 	      	      
	enddo;
!$OMP   enddo	
        T_MT_T(1,1)=sum(Ten_T(1,1:N_f))
        T_MT_T(2,1)=sum(Ten_T(2,1:N_f))
        T_MT_T(3,1)=sum(Ten_T(3,1:N_f))

        F_MT_T(1,1)=sum(F_MT_TN(1,1:N_f))
        F_MT_T(2,1)=sum(F_MT_TN(2,1:N_f))
        F_MT_T(3,1)=sum(F_MT_TN(3,1:N_f))




!$OMP   single        

	V_T0_T=matmul(M1,F_MT_T)

	V_T0_T=V_T0_T/(mu0(1))

	Omega_T0_T=matmul(M2, T_MT_T) 
	Omega_T0_T=Omega_T0_T/(mu0(1)*8.0/6.0) 
	E_T(1:3,1:3)=0.0;
!	Omega_T0_T(:,1)=0.0;


	    
	k=0;V_HD_T(:,1)=0.0;V2(:,:)=0.0;
        V_HD_T2(:,:)=0.0;V2_2(:,:)=0.0;
        Omega_HD_T(:,:)=0.0;O2(:,:)=0.0;
        S_SP_HD_T(:,:)=0.0;
!$OMP   end single

!$OMP   do      
	do nf=1,N_f;
!	N=N_var(nf);
        N=N0
	C_T=C_T0;
!	C_T(:,:,N-n_half+1:N)=C_T_B2;
!	T_tr(1,1:N,nf)=T(1:N,nf);

	call sphere_motion(N0, N, x(:,nf),y(:,nf),z(:,nf),V_T0_T,Omega_T0_T,E_T,xc0, &
	& ux_SP_T(:,nf),uy_SP_T(:,nf),uz_SP_T(:,nf));
		       
	fx_MTT_T(1,1:N,nf)=1.0*(T_ns(1,1:N,nf)*x_ns0(1,1:N,nf)+ &
	& T_tr(1,1:N,nf)*x_ns0(2,1:N,nf));
	      
	fy_MTT_T(1,1:N,nf)=1.0*(T_ns(1,1:N,nf)*y_ns0(1,1:N,nf)+ &
	& T_tr(1,1:N,nf)*y_ns0(2,1:N,nf));
	      
	fz_MTT_T(1,1:N,nf)=1.0*(T_ns(1,1:N,nf)*z_ns0(1,1:N,nf)+ &
	& T_tr(1,1:N,nf)*z_ns0(2,1:N,nf));
	     
        fx_MT_T(1:N,nf)=fx_MTT_T(1,1:N,nf);
        fy_MT_T(1:N,nf)=fy_MTT_T(1,1:N,nf);
        fz_MT_T(1:N,nf)=fz_MTT_T(1,1:N,nf);

	if (FLAG_T .eq. 0) then

        sigma_sl_MT_T(1,(nf-1)*N+1:nf*N)=fx_MT_T(1:N,nf)*chev_w(1:N)*L_F(nf)
        sigma_sl_MT_T(2,(nf-1)*N+1:nf*N)=fy_MT_T(1:N,nf)*chev_w(1:N)*L_F(nf)
        sigma_sl_MT_T(3,(nf-1)*N+1:nf*N)=fz_MT_T(1:N,nf)*chev_w(1:N)*L_F(nf)
        source_MT(1,(nf-1)*N+1:nf*N)=x(1:N,nf)
        source_MT(2,(nf-1)*N+1:nf*N)=y(1:N,nf)
        source_MT(3,(nf-1)*N+1:nf*N)=z(1:N,nf)

	   call MT_uinf_self(fx_MT_T(:,nf),fy_MT_T(:,nf),fz_MT_T(:,nf),&
	   & x(:,nf),y(:,nf),z(:,nf),N,N0,chev_w(:)*L_F(nf), &
	   & ux_self(:,nf),uy_self(:,nf),uz_self(:,nf))
!        k=k+N
	endif 	
	      

	call MT_uinf_single(fx_MT_T(:,nf), fy_MT_T(:,nf), fz_MT_T(:,nf), &
	& x(:,nf), y(:,nf), z(:,nf), xc0(1), xc0(2), xc0(3), N0, N, chev_w(:)*L_F(nf), V2(1:3,nf), O2(1:3,nf),V2_2(1:3,nf),S_1(1:3,1:3,nf))

	V2(1:3,nf)=V2(1:3,nf)/(1.0*mu0(1)*8.0/6.0)
        O2(1:3,nf)=O2(1:3,nf)/(1.0*mu0(1)*8.0/6.0)
        V2_2(1:3,nf)=V2_2(1:3,nf)/(1.0*mu0(nf)*8.0/6.0)
        
	 
	enddo;

!$OMP   end do 
!$OMP end parallel 

        Omega_HD_T(1,1)=sum(O2(1,1:N_f))
        Omega_HD_T(2,1)=sum(O2(2,1:N_f))
        Omega_HD_T(3,1)=sum(O2(3,1:N_f))

        V_HD_T(1,1)=sum(V2(1,1:N_f))
        V_HD_T(2,1)=sum(V2(2,1:N_f))
        V_HD_T(3,1)=sum(V2(3,1:N_f))

        V_HD_T2(1,1)=sum(V2_2(1,1:N_f))
        V_HD_T2(2,1)=sum(V2_2(2,1:N_f))
        V_HD_T2(3,1)=sum(V2_2(3,1:N_f))

        S_SP_HD_T(1,1)=sum(S_1(1,1,1:N_f));
        S_SP_HD_T(1,2)=sum(S_1(1,2,1:N_f));
        S_SP_HD_T(1,3)=sum(S_1(1,3,1:N_f));
        S_SP_HD_T(2,2)=sum(S_1(2,2,1:N_f));
        S_SP_HD_T(2,3)=sum(S_1(2,3,1:N_f));
        S_SP_HD_T(3,3)=sum(S_1(3,3,1:N_f));
        S_SP_HD_T(2,1)=S_SP_HD_T(1,2);
        S_SP_HD_T(3,2)=S_SP_HD_T(2,3);
        S_SP_HD_T(3,1)=S_SP_HD_T(1,3);

       
	if (FLAG_T .eq. 0) then
           if (FLAG_MANYBODY .eq. 0) then
           N_total=N0*N_f
           iprec=3;
!          nparts=N_total+1;
           nparts=N_total
           ifsingle=1;
           ifdouble=0;
           ifpot=1;
           ifgrad=0;
           ifpottarg=0;
           ifgradtarg=0;
           sigma_dl_MT_T(1:3,:)=0.0;
           sigma_dv_MT(1:3,:)=0.0;
           sigma_dv_MT(1,:)=1.0;
           pot_MT_T(1:3,:)=0.0;
           pre_MT_T(:)=0.0;
           grad_MT_T(1:3,1:3,:)=0.0;

	   call stfmm3Dpartself(ier,iprec,nparts,source_MT, ifsingle,sigma_sl_MT_T,ifdouble,& 
	   & sigma_dl_MT_T,sigma_dv_MT, ifpot,pot_MT_T,pre_MT_T,ifgrad,grad_MT_T)	
                
           pot_MT_T_PP=pot_MT_T_P
           pot_MT_T_P=pot_MT_T
           else
           pot_MT_T=(pot_MT_T_P-pot_MT_T_PP)*dt0*t_lag+pot_MT_T_P
!           pot_MT_T=pot_MT_T_P
           endif 
	endif



	k=0;m0=1


!$OMP   parallel default(shared) private(nf,C_T,N)
!$OMP   do
	do nf=1,N_f;    
!	   N=N_var(nf);
           N=N0
	   C_T=C_T0;
!	   C_T(:,:,N-n_half+1:N)=C_T_B2;

	call sphere_motion(N0, N, x(:,nf),y(:,nf),z(:,nf),V_HD_T,Omega_HD_T,S_SP_HD_T/(20.0*mu(nf)/18.0),xc0, &
	& ux_SP_HD_T(:,nf),uy_SP_HD_T(:,nf),uz_SP_HD_T(:,nf)); 

        call sphere_motion(N0, N, x(:,nf),y(:,nf),z(:,nf),V_HD_T2,0.0*Omega_HD_T,E_T,xc0, &
        & ux_SP_HD_T2(:,nf),uy_SP_HD_T2(:,nf),uz_SP_HD_T2(:,nf));


        call stresslet(N0,N,x(:,nf),y(:,nf),z(:,nf),xc0,chev_w(:),(S_SP_HD_T)/(8.0*mu0(nf)/6.0),&
        & ux_SP_HD_T2(:,nf),uy_SP_HD_T2(:,nf),uz_SP_HD_T2(:,nf))


	  if (FLAG_T .eq. 0) then
           if (FLAG_MANYBODY .eq. 0) then
	   ux_MT_T(1:N,nf)=(2.0*pot_MT_T(1,(nf-1)*N+1:nf*N)-1.0*ux_self(1:N,nf))/(1.0*mu0(nf)*8.0/6.0);
	   uy_MT_T(1:N,nf)=(2.0*pot_MT_T(2,(nf-1)*N+1:nf*N)-1.0*uy_self(1:N,nf))/(1.0*mu0(nf)*8.0/6.0);
	   uz_MT_T(1:N,nf)=(2.0*pot_MT_T(3,(nf-1)*N+1:nf*N)-1.0*uz_self(1:N,nf))/(1.0*mu0(nf)*8.0/6.0);

           ux_MT_T_PP=ux_MT_T_P;uy_MT_T_PP=uy_MT_T_P;uz_MT_T_PP=uz_MT_T_P; 
	   ux_MT_T_P=ux_MT_T;uy_MT_T_P=uy_MT_T;uz_MT_T_P=uz_MT_T;
           else
            ux_MT_T=(ux_MT_T_P-ux_MT_T_PP)*t_lag*dt0+ux_MT_T_P
            uy_MT_T=(uy_MT_T_P-uy_MT_T_PP)*t_lag*dt0+uy_MT_T_P
            uz_MT_T=(uz_MT_T_P-uz_MT_T_PP)*t_lag*dt0+uz_MT_T_P
!           ux_MT_T=ux_MT_T_P;uy_MT_T=uy_MT_T_P;uz_MT_T=uz_MT_T_P;
           endif 
                
	   call deriv_O_N(ux_MT_T(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uxs_MT_T(1:m0,1:N,nf),L_F(nf));
	   call deriv_O_N(uy_MT_T(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uys_MT_T(1:m0,1:N,nf),L_F(nf));
	   call deriv_O_N(uz_MT_T(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uzs_MT_T(1:m0,1:N,nf),L_F(nf));

	  endif 
	    ux_self(:,nf)=0.0;uy_self(:,nf)=0.0;uz_self(:,nf)=0.0;

	   call deriv_O_N(ux_SP_T(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uxs_SP_T(1:m0,1:N,nf),L_F(nf));
	   call deriv_O_N(uy_SP_T(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uys_SP_T(1:m0,1:N,nf),L_F(nf));
	   call deriv_O_N(uz_SP_T(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uzs_SP_T(1:m0,1:N,nf),L_F(nf));

	   call deriv_O_N(ux_SP_HD_T(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uxs_SP_HD_T(1:m0,1:N,nf),L_F(nf));
	   call deriv_O_N(uy_SP_HD_T(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uys_SP_HD_T(1:m0,1:N,nf),L_F(nf));
	   call deriv_O_N(uz_SP_HD_T(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uzs_SP_HD_T(1:m0,1:N,nf),L_F(nf));

           call deriv_O_N(ux_SP_HD_T2(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uxs_SP_HD_T2(1:m0,1:N,nf),L_F(nf));
           call deriv_O_N(uy_SP_HD_T2(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uys_SP_HD_T2(1:m0,1:N,nf),L_F(nf));
           call deriv_O_N(uz_SP_HD_T2(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uzs_SP_HD_T2(1:m0,1:N,nf),L_F(nf));

	       
	   ux_ext(1:N,nf)=1.0*ux_MT_T(1:N,nf)*dum(1:N)+ &
	   & 1.0*ux_SP_T(1:N,nf)+1.0*ux_SP_HD_T(1:N,nf)*(1.0-dum(1:N))+0.0*ux_SP_HD_T2(1:N,nf)*dum(1:N);

	   uy_ext(1:N,nf)=1.0*uy_MT_T(1:N,nf)*dum(1:N)+ &
	   & 1.0*uy_SP_T(1:N,nf)+1.0*uy_SP_HD_T(1:N,nf)*(1.0-dum(1:N))+0.0*uy_SP_HD_T2(1:N,nf)*dum(1:N);

	   uz_ext(1:N,nf)=1.0*uz_MT_T(1:N,nf)*dum(1:N)+ &
	   & 1.0*uz_SP_T(1:N,nf)+1.0*uz_SP_HD_T(1:N,nf)*(1.0-dum(1:N))+0.0*uz_SP_HD_T2(1:N,nf)*dum(1:N);
   
	   ux_s(1,1:N,nf)=1.0*(1.0-1.0*FLAG_T)*uxs_MT_T(1,1:N,nf)*dum(1:N)+ &
	   & 1.0*uxs_SP_T(1,1:N,nf)+1.0*uxs_SP_HD_T(1,1:N,nf)*(1.0-dum(1:N))+0.0*uxs_SP_HD_T2(1,1:N,nf)*dum(1:N);
	    
	   uy_s(1,1:N,nf)=1.0*(1.0-1.0*FLAG_T)*uys_MT_T(1,1:N,nf)*dum(1:N)+ &
	   & 1.0*uys_SP_T(1,1:N,nf)+1.0*uys_SP_HD_T(1,1:N,nf)*(1.0-dum(1:N))+0.0*uys_SP_HD_T2(1,1:N,nf)*dum(1:N);

	   uz_s(1,1:N,nf)=1.0*(1.0-1.0*FLAG_T)*uzs_MT_T(1,1:N,nf)*dum(1:N)+ &
	   & 1.0*uzs_SP_T(1,1:N,nf)+1.0*uzs_SP_HD_T(1,1:N,nf)*(1.0-dum(1:N))+0.0*uzs_SP_HD_T2(1,1:N,nf)*dum(1:N);
    
	   xsus(1,1:N,nf)=ux_s(1,1:N,nf)*x_ns0(1,1:N,nf)+ &
	   &  uy_s(1,1:N,nf)*y_ns0(1,1:N,nf)+uz_s(1,1:N,nf)*z_ns0(1,1:N,nf);
        
!        k=k+N
	    
	enddo;
!$OMP   end do 




!$OMP  do 
	do nf=1,N_f;
		
!	  N=N_var(nf);
        N=N0	       
!	  k=k+N;

	  A_DOT_T(1+(nf-1)*N:nf*N)=T_ns(2,1:N,nf)-dot1(nf,1:N)*T_tr(1,1:N,nf)/2.0+1.0*mu(nf)*xsus(1,1:N,nf)/2.0;


	enddo;
!$OMP   end do 

!$OMP do 
	do nf=1,N_f;      
!	   N=N_var(nf); 
           N=N0          
!	   k=k+N;
	   A_DOT_T(1+(nf-1)*N)=T_ns(1,1,nf)-& 
        & mu(nf)*0.50*((V2(1,nf))*x_ns0(1,1,nf)+(V2(2,nf))*y_ns0(1,1,nf)+(V2(3,nf))*z_ns0(1,1,nf))/(2.0*sqrt(x_ns0(1,1,nf)**2+y_ns0(1,1,nf)**2+z_ns0(1,1,nf)**2));
	   A_DOT_T(nf*N)=1.0*T(N,nf);
		    
		    
	enddo;
!$OMP  end do
!$OMP  end parallel

	end subroutine Tension_A_DOT_X


!****************************************************************!
!****************************************************************!
!****** IMPLICIT FORMULATION FOR TENSION*************************!
!****************************************************************!
!****************************************************************!


	subroutine A_DOT_X_implicit(XT2)
!	USE input_file
	use initialize

	implicit none 
        real(kind=dp) :: XT2(:),dum_2
        !*******************************!
         
         N=N0
          m0=2
        do nf=1,N_f
        do i=1,N0
          T(i,nf)=XT2(4*(nf-1)*N0+(i-1)*4+1)
          x(i,nf)=XT2(4*(nf-1)*N0+(i-1)*4+2)
          y(i,nf)=XT2(4*(nf-1)*N0+(i-1)*4+3)
          z(i,nf)=XT2(4*(nf-1)*N0+(i-1)*4+4)


        enddo
        source_MT(1,(nf-1)*N+1:nf*N)=x0(1:N,nf)
        source_MT(2,(nf-1)*N+1:nf*N)=y0(1:N,nf)
        source_MT(3,(nf-1)*N+1:nf*N)=z0(1:N,nf)

        call deriv_O_N(x(1:N,nf),N,n_s,C_T(:,:,1:N),m,x_ns(1:m,1:N,nf),L_FF(nf));
        call deriv_O_N(y(1:N,nf),N,n_s,C_T(:,:,1:N),m,y_ns(1:m,1:N,nf),L_FF(nf));
        call deriv_O_N(z(1:N,nf),N,n_s,C_T(:,:,1:N),m,z_ns(1:m,1:N,nf),L_FF(nf));
        call deriv_O_N(T(1:N,nf),N,n_s,C_T(:,:,1:N),m0,T_ns(1:m0,1:N,nf),L_FF(nf))
        enddo


        do nf=1,N_f

        fx_MTT_T(1,1:N,nf)=1.0d0*(T_ns(1,1:N,nf)*x_ns0(1,1:N,nf)+ &
        & T_tr(1,1:N,nf)*x_ns0(2,1:N,nf));

        fy_MTT_T(1,1:N,nf)=1.0d0*(T_ns(1,1:N,nf)*y_ns0(1,1:N,nf)+ &
        & T_tr(1,1:N,nf)*y_ns0(2,1:N,nf));

        fz_MTT_T(1,1:N,nf)=1.0d0*(T_ns(1,1:N,nf)*z_ns0(1,1:N,nf)+ &
        & T_tr(1,1:N,nf)*z_ns0(2,1:N,nf));

        fx_MT_T(1:N,nf)=fx_MTT_T(1,1:N,nf);
        fy_MT_T(1:N,nf)=fy_MTT_T(1,1:N,nf);
        fz_MT_T(1:N,nf)=fz_MTT_T(1,1:N,nf);

        fx_MTT_B(1,1:N,nf)=1.0d0*(-1.0d0*x_ns(4,1:N,nf));
        fy_MTT_B(1,1:N,nf)=1.0d0*(-1.0d0*y_ns(4,1:N,nf));
        fz_MTT_B(1,1:N,nf)=1.0d0*(-1.0d0*z_ns(4,1:N,nf));

        fx_MT_B(1:N,nf)=fx_MTT_B(1,1:N,nf);
        fy_MT_B(1:N,nf)=fy_MTT_B(1,1:N,nf);
        fz_MT_B(1:N,nf)=fz_MTT_B(1,1:N,nf);
        


        sigma_sl_MT(1,(nf-1)*N0+1:nf*N0)=(fx_MT_T(1:N0,nf)+fx_MT_B(1:N0,nf))*chev_w(1:N0)*L_FF(nf)
        sigma_sl_MT(2,(nf-1)*N0+1:nf*N0)=(fy_MT_T(1:N0,nf)+fy_MT_B(1:N0,nf))*chev_w(1:N0)*L_FF(nf)
        sigma_sl_MT(3,(nf-1)*N0+1:nf*N0)=(fz_MT_T(1:N0,nf)+fz_MT_B(1:N0,nf))*chev_w(1:N0)*L_FF(nf)
        

        enddo


!        iprec=3;
!        nparts_MT=N0*N_f
!        ifsingle=1;
!        ifdouble=0;
!        ifpot=1;
!        ifgrad=0;
!        ntarg_MT=n_node_SP+n_node_cor;
!        ifpottarg=1;
!        ifgradtarg=0;
!        sigma_dl_MT(1:3,:)=0.0;
!        sigma_dv_MT(1:3,:)=0.0;
!        sigma_dv_MT(1,:)=1.0;
!        pot_MT(1:3,:)=0.0;
!        pre_MT(:)=0.0;
!        grad_MT(1:3,1:3,:)=0.0;
!        pottarg_MT(:,:)=0.0
!        pretarg_MT(:)=0.0
!        gradtarg_MT(:,:,:)=0.0

!        targ_SP(1,:)=xm_SP(:)+e_SP(1);
!        targ_SP(2,:)=ym_SP(:)+e_SP(2);
!        targ_SP(3,:)=zm_SP(:)+e_SP(3);

!        source_SP(1,:)=x_g_SP(:)+e_SP(1)
!        source_SP(2,:)=y_g_SP(:)+e_SP(2)
!        source_SP(3,:)=z_g_SP(:)+e_SP(3)

!        targ_MT(1:3,1:n_node_SP)=targ_SP(1:3,:)
!        targ_MT(1:3,n_node_SP+1:n_node_SP+n_node_cor)=targ_cor(1:3,:)

!        call stfmm3Dparttarg(ier,iprec,nparts_MT,source_MT,ifsingle,sigma_sl_MT,ifdouble,&
!        & sigma_dl_MT,sigma_dv_MT, ifpot,pot_MT,pre_MT,ifgrad,grad_MT, &
!        & ntarg_MT,targ_MT,ifpottarg,pottarg_MT,pretarg_MT,ifgradtarg,gradtarg_MT)

        iprec=3;     
        nparts=N_f*N0
        ifsingle=1;
        ifdouble=0;
        ifpot=1;
        ifgrad=0;
        ntarg=0;
        ifpottarg=0;
        ifgradtarg=0;
        sigma_dl_MT(1:3,:)=0.0;
        sigma_dv_MT(1:3,:)=0.0;
        sigma_dv_MT(1,:)=1.0;        
        pot_MT(1:3,:)=0.0;
        pre_MT(:)=0.0;
        grad_MT(1:3,1:3,:)=0.0;

        call stfmm3Dpartself(ier,iprec,nparts,source_MT,ifsingle,sigma_sl_MT,ifdouble,&
        & sigma_dl_MT,sigma_dv_MT,ifpot,pot_MT,pre_MT,ifgrad,grad_MT)

        !call COR_2SPMT()

          V_T0_T(1:3,1)=0.0;Omega_T0_T(1:3,1)=0.0;
          F_MT_T(:,:)=0.0;T_MT_T(:,1)=0.0;
          V_T0_B(1:3,1)=0.0;Omega_T0_B(1:3,1)=0.0;
          F_MT_B(:,:)=0.0;T_MT_B(:,1)=0.0;
          k=0;V_HD(:,1)=0.0;V(:,:)=0.0;
          V_2(:,:)=0.0;
          Omega_HD(:,:)=0.0;O(:,:)=0.0;
          S_SP_HD(:,:)=0.0;

!$OMP parallel default(shared) private(nf,N,C_T)
!$OMP do 
	do nf=1,N_f;
            N=N0
!	   
	    C_T=C_T0;
	    m0=2
	      
	    dot1(nf,1:N)=x_ns0(2,1:N,nf)**2+y_ns0(2,1:N,nf)**2+z_ns0(2,1:N,nf)**2;

            dot2(nf,1:N)=x_ns0(2,1:N,nf)*x_ns(4,1:N,nf)+ &
            & y_ns0(2,1:N,nf)*y_ns(4,1:N,nf)+ &
            & z_ns0(2,1:N,nf)*z_ns(4,1:N,nf);

            dot3(nf,1:N)=x_ns0(3,1:N,nf)*x_ns(3,1:N,nf)+ &
            & y_ns0(3,1:N,nf)*y_ns(3,1:N,nf)+ &
            & z_ns0(3,1:N,nf)*z_ns(3,1:N,nf);   
	    
            F_MT_TN(1,nf)=T(1,nf)*x_ns0(1,1,nf)/mu0(nf)
            F_MT_TN(2,nf)=T(1,nf)*y_ns0(1,1,nf)/mu0(nf)
            F_MT_TN(3,nf)=T(1,nf)*z_ns0(1,1,nf)/mu0(nf)
	    
	    Ten_T(1,nf)=(dxc1p(2,nf)*F_MT_TN(3,nf)-dxc1p(3,nf)*F_MT_TN(2,nf))/mu0(nf);	      
	    Ten_T(2,nf)=(-dxc1p(1,nf)*F_MT_TN(3,nf)+dxc1p(3,nf)*F_MT_TN(1,nf))/mu0(nf);	      
	    Ten_T(3,nf)=(dxc1p(1,nf)*F_MT_TN(2,nf)-dxc1p(2,nf)*F_MT_TN(1,nf))/mu0(nf);


            F_MT_BN(1,nf)=-x_ns(3,1,nf)/mu0(nf)
            F_MT_BN(2,nf)=-y_ns(3,1,nf)/mu0(nf)
            F_MT_BN(3,nf)=-z_ns(3,1,nf)/mu0(nf)

            Ten_B(1,nf)=-1.0d0*(y_ns(2,1,nf)*z_ns0(1,1,nf)-z_ns(2,1,nf)*y_ns0(1,1,nf))/mu0(nf)+&
            & 1.0d0*(1.0*dxc1p(2,nf)*(-z_ns(3,1,nf))-dxc1p(3,nf)*(-y_ns(3,1,nf)))/mu0(nf);

            Ten_B(2,nf)=-1.0d0*(-x_ns(2,1,nf)*z_ns0(1,1,nf)+z_ns(2,1,nf)*x_ns0(1,1,nf))/mu0(nf)+&
            & 1.0d0*(-dxc1p(1,nf)*(-z_ns(3,1,nf))+dxc1p(3,nf)*(-x_ns(3,1,nf)))/mu0(nf);

            Ten_B(3,nf)=-1.0d0*(x_ns(2,1,nf)*y_ns0(1,1,nf)-y_ns(2,1,nf)*x_ns0(1,1,nf))/mu0(nf)+ &
            & 1.0d0*(dxc1p(1,nf)*(-y_ns(3,1,nf))-dxc1p(2,nf)*(-x_ns(3,1,nf)))/mu0(nf) ;

            call MT_uinf_single(fx_MT_T(:,nf)+1.0d0*fx_MT_B(:,nf),&
            & fy_MT_T(:,nf)+1.0d0*fy_MT_B(:,nf), fz_MT_T(:,nf)+1.0d0*fz_MT_B(:,nf), &
            & x0(:,nf), y0(:,nf), z0(:,nf), xc0(1), xc0(2), xc0(3), N0, N0, chev_w(1:N0)*L_FF(nf),&
            & V(1:3,nf), O(1:3,nf),V_2(1:3,nf),S_1(1:3,1:3,nf))

           V(1:3,nf)=V(1:3,nf)/(1.0*mu0(nf)*8.0/6.0)
           O(1:3,nf)=O(1:3,nf)/(1.0*mu0(nf)*8.0/6.0)

 !          V_2(1:3,nf)=V_2(1:3,nf)/(1.0*mu0(nf)*8.0/6.0)

	 	      	      
	enddo;
!$OMP   end do
!$OMP   single

        T_MT_T(1,1)=sum(Ten_T(1,1:N_f))
        T_MT_T(2,1)=sum(Ten_T(2,1:N_f))
        T_MT_T(3,1)=sum(Ten_T(3,1:N_f))

        F_MT_T(1,1)=sum(F_MT_TN(1,1:N_f))
        F_MT_T(2,1)=sum(F_MT_TN(2,1:N_f))
        F_MT_T(3,1)=sum(F_MT_TN(3,1:N_f))

        T_MT_B(1,1)=sum(Ten_B(1,1:N_f))
        T_MT_B(2,1)=sum(Ten_B(2,1:N_f))
        T_MT_B(3,1)=sum(Ten_B(3,1:N_f))

        F_MT_B(1,1)=sum(F_MT_BN(1,1:N_f))
        F_MT_B(2,1)=sum(F_MT_BN(2,1:N_f))
        F_MT_B(3,1)=sum(F_MT_BN(3,1:N_f))

        V_T0_B=matmul(M1,F_MT_B)
        Omega_T0_B=1.0d0*matmul(M2,T_MT_B)/(8.0/6.0)
        V_T0_T=matmul(M1,F_MT_T)
        Omega_T0_T=1.0d0*matmul(M2,T_MT_T)/(8.0/6.0)

        Omega_HD(1,1)=1.0d0*sum(O(1,1:N_f))
        Omega_HD(2,1)=1.0d0*sum(O(2,1:N_f))
        Omega_HD(3,1)=1.0d0*sum(O(3,1:N_f))

        V_HD(1,1)=1.0d0*sum(V(1,1:N_f))
        V_HD(2,1)=1.0d0*sum(V(2,1:N_f))
        V_HD(3,1)=1.0d0*sum(V(3,1:N_f))


        S_SP_HD(1,1)=sum(S_1(1,1,1:N_f));
        S_SP_HD(1,2)=sum(S_1(1,2,1:N_f));
        S_SP_HD(1,3)=sum(S_1(1,3,1:N_f));
        S_SP_HD(2,2)=sum(S_1(2,2,1:N_f));
        S_SP_HD(2,3)=sum(S_1(2,3,1:N_f));
        S_SP_HD(3,3)=sum(S_1(3,3,1:N_f));
        S_SP_HD=0.0*S_SP_HD
        S_SP_HD(2,1)=S_SP_HD(1,2);
        S_SP_HD(3,2)=S_SP_HD(2,3);
        S_SP_HD(3,1)=S_SP_HD(1,3);


!        call COR_2SPMT()

        V_T=V_T0_T+V_T0_B+V_HD!+V_SPCOR
        Omega_T=Omega_T0_T+Omega_T0_B+Omega_HD!+Omega_SPCOR
        Omega_Tn=Omega_T
        Omega_T0_Tn=Omega_T0_T
        Omega_T0_Bn=Omega_T0_B
        Omega_HDn=Omega_HD

        !xc0=xc0p!+dt0*V_T(1:3,1)

!$OMP end single 

!$OMP do
       do nf=1,N_f

        Vatt(1:3,nf)=0.0;      
        Vatt(1,nf)=(Omega_Tn(2,1)*dxc1p(3,nf)-Omega_Tn(3,1)*dxc1p(2,nf))
        Vatt(2,nf)=-Omega_Tn(1,1)*dxc1p(3,nf)+1.0*Omega_Tn(3,1)*dxc1p(1,nf)
        Vatt(3,nf)=Omega_Tn(1,1)*dxc1p(2,nf)-1.0*Omega_Tn(2,1)*dxc1p(1,nf)
        Vatt(1:3,nf)=Vatt(1:3,nf)+V_T(1:3,1)
        
        Vatt_2(1:3,nf)=0.0;
        Vatt_2(1 ,nf)=Omega_Tn(2,1)*dxc2p(3,nf)-Omega_Tn(3,1)*dxc2p(2,nf)
        Vatt_2(2 ,nf)=-Omega_Tn(1,1)*dxc2p(3,nf)+1.0*Omega_Tn(3,1)*dxc2p(1,nf)
        Vatt_2(3 ,nf)=Omega_Tn(1,1)*dxc2p(2,nf)-1.0*Omega_Tn(2,1)*dxc2p(1,nf)
        Vatt_2(1:3,nf)=Vatt_2(1:3,nf)+V_T(1:3,1)

        
        enddo
!$OMP end do



!$OMP   do      
	do nf=1,N_f;
        N=N0
	C_T=C_T0;

	call sphere_motion(N0, N, x0(:,nf),y0(:,nf),z0(:,nf),V_T0_T+V_T0_B,Omega_T0_Tn+Omega_T0_Bn,0.0*E_T,xc0, &
	& ux_SP(:,nf),uy_SP(:,nf),uz_SP(:,nf));
		       
	call sphere_motion(N0, N, x0(:,nf),y0(:,nf),z0(:,nf),V_HD,Omega_HDn,0.0*(S_SP_HD)/(20.0*mu0(nf)/18.0),xc0, &
	& ux_SP_HD(:,nf),uy_SP_HD(:,nf),uz_SP_HD(:,nf)); 

           call MT_uinf_self(fx_MT_T(:,nf)+fx_MT_B(:,nf),&
           & fy_MT_T(:,nf)+fy_MT_B(:,nf),fz_MT_T(:,nf)+fz_MT_B(:,nf),&
           & x0(:,nf),y0(:,nf),z0(:,nf),N,N0,chev_w(1:N0)*L_FF(nf), &
           & ux_self(:,nf),uy_self(:,nf),uz_self(:,nf))

!        enddo

           ux_MT(1:N,nf)=(2.0*pot_MT(1,(nf-1)*N+1:nf*N)-1.0*ux_self(1:N,nf))/(1.0*mu0(nf)*8.0/6.0)!+ux_SPCOR(1:N,nf);
           uy_MT(1:N,nf)=(2.0*pot_MT(2,(nf-1)*N+1:nf*N)-1.0*uy_self(1:N,nf))/(1.0*mu0(nf)*8.0/6.0)!+uy_SPCOR(1:N,nf);
           uz_MT(1:N,nf)=(2.0*pot_MT(3,(nf-1)*N+1:nf*N)-1.0*uz_self(1:N,nf))/(1.0*mu0(nf)*8.0/6.0)!+uz_SPCOR(1:N,nf);
!                ux_MT(:,nf)=0.0;uy_MT(:,nf)=0.0;uz_MT(:,nf)=0.0;
	   call deriv_O_N(ux_MT(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uxs_MT(1:m0,1:N,nf),L_FF(nf));
	   call deriv_O_N(uy_MT(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uys_MT(1:m0,1:N,nf),L_FF(nf));
	   call deriv_O_N(uz_MT(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uzs_MT(1:m0,1:N,nf),L_FF(nf));

	   call deriv_O_N(ux_SP(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uxs_SP(1:m0,1:N,nf),L_FF(nf));
	   call deriv_O_N(uy_SP(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uys_SP(1:m0,1:N,nf),L_FF(nf));
	   call deriv_O_N(uz_SP(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uzs_SP(1:m0,1:N,nf),L_FF(nf));

	   call deriv_O_N(ux_SP_HD(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uxs_SP_HD(1:m0,1:N,nf),L_FF(nf));
	   call deriv_O_N(uy_SP_HD(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uys_SP_HD(1:m0,1:N,nf),L_FF(nf));
	   call deriv_O_N(uz_SP_HD(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uzs_SP_HD(1:m0,1:N,nf),L_FF(nf));

	       

	   ux2_ext(1:N,nf)=1.0*ux_MT(1:N,nf)*dum(1:N)+ &
	   & 1.0*ux_SP(1:N,nf)+1.0*ux_SP_HD(1:N,nf)*(1.0-dum(1:N))
           ux_ext(1:N,nf)=ux2_ext(1:N,nf)+&
           & beta(nf)*x_ns(1,1:N,nf)*s(1:N)+&
           & 2.0d0*(-stab0+1.0d0-dot0(nf,1:N))*mu0(nf)*fac*s(1:N)*x_ns(1,1:N,nf)/mu(nf)

	   uy2_ext(1:N,nf)=1.0*uy_MT(1:N,nf)*dum(1:N)+ &
	   & 1.0*uy_SP(1:N,nf)+1.0*uy_SP_HD(1:N,nf)*(1.0-dum(1:N))
           uy_ext(1:N,nf)=uy2_ext(1:N,nf)+&
           & beta(nf)*y_ns(1,1:N,nf)*s(1:N)+&
           & 2.0d0*(-stab0+1.0d0-dot0(nf,1:N))*mu0(nf)*fac*s(1:N)*y_ns(1,1:N,nf)/mu(nf)

	   uz2_ext(1:N,nf)=1.0*uz_MT(1:N,nf)*dum(1:N)+ &
	   & 1.0*uz_SP(1:N,nf)+1.0*uz_SP_HD(1:N,nf)*(1.0-dum(1:N))
           uz_ext(1:N,nf)=uz2_ext(1:N,nf)+&
           & beta(nf)*z_ns(1,1:N,nf)*s(1:N)+&
           & 2.0d0*(-stab0+1.0d0-dot0(nf,1:N))*mu0(nf)*fac*s(1:N)*z_ns(1,1:N,nf)/mu(nf)
   
	   ux_s(1,1:N,nf)=(1.0d0)*uxs_MT(1,1:N,nf)*dum(1:N)+ &
	   & 1.0*uxs_SP(1,1:N,nf)+1.0*uxs_SP_HD(1,1:N,nf)*(1.0-dum(1:N))
	    
	   uy_s(1,1:N,nf)=(1.0d0)*uys_MT(1,1:N,nf)*dum(1:N)+ &
	   & 1.0*uys_SP(1,1:N,nf)+1.0*uys_SP_HD(1,1:N,nf)*(1.0-dum(1:N))

	   uz_s(1,1:N,nf)=(1.0d0)*uzs_MT(1,1:N,nf)*dum(1:N)+ &
	   & 1.0*uzs_SP(1,1:N,nf)+1.0*uzs_SP_HD(1,1:N,nf)*(1.0-dum(1:N))
    
	   xsus(1,1:N,nf)=ux_s(1,1:N,nf)*x_ns0(1,1:N,nf)+ &
	   &  uy_s(1,1:N,nf)*y_ns0(1,1:N,nf)+uz_s(1,1:N,nf)*z_ns0(1,1:N,nf);
        
!        k=k+N
	    
	enddo;
!$OMP   end do 



!$OMP  do
        do nf=1,N_f
        do i=1,N0

        A_DOT_XT(4*(nf-1)*N0+(i-1)*4+1)=T_ns(2,i,nf)-dot1(nf,i)*T(i,nf)/2.0+&
        & +(6.0*dot3(nf,i)+7.0*dot2(nf,i))/2.0+1.0*mu(nf)*xsus(1,i,nf)/2.0;


        dum_2=x_ns(4,i,nf)*x_ns0(1,i,nf)+y_ns(4,i,nf)*y_ns0(1,i,nf)+z_ns(4,i,nf)*z_ns0(1,i,nf);
        A_DOT_XT(4*(nf-1)*N0+(i-1)*4+2)=mu(nf)*x(i,nf)/dt0+x_ns(4,i,nf)+dum_2*x_ns0(1,i,nf)-&
        & T(i,nf)*x_ns0(2,i,nf)-T_ns(1,i,nf)*x_ns0(1,i,nf)-mu(nf)*ux_ext(i,nf)

        A_DOT_XT(4*(nf-1)*N0+(i-1)*4+3)=mu(nf)*y(i,nf)/dt0+y_ns(4,i,nf)+dum_2*y_ns0(1,i,nf)-&
        & T(i,nf)*y_ns0(2,i,nf)-T_ns(1,i,nf)*y_ns0(1,i,nf)-mu(nf)*uy_ext(i,nf)

        A_DOT_XT(4*(nf-1)*N0+(i-1)*4+4)=mu(nf)*z(i,nf)/dt0+z_ns(4,i,nf)+dum_2*z_ns0(1,i,nf)-&
        & T(i,nf)*z_ns0(2,i,nf)-T_ns(1,i,nf)*z_ns0(1,i,nf)-mu(nf)*uz_ext(i,nf)

        enddo

        A_DOT_XT(1+4*(nf-1)*N0)=T_ns(1,1,nf)+3.0*(x_ns0(2,1,nf)*x_ns(3,1,nf)+y_ns0(2,1,nf)*y_ns(3,1,nf)+z_ns0(2,1,nf)*z_ns(3,1,nf))-&
        & 1.0d0*mu(nf)*(V(1,nf)*x_ns0(1,1,nf)+V(2,nf)*y_ns0(1,1,nf)+V(3,nf)*z_ns0(1,1,nf))&
        & /(2.0*sqrt(x_ns0(1,1,nf)**2+y_ns0(1,1,nf)**2+z_ns0(1,1,nf)**2))

        if (flag_BC(N0,nf) .eq. 3) then          
         A_DOT_XT(4*(nf-1)*N0+4*(N0-1)+1)=T(N0,nf)
        elseif(flag_BC(N0,nf) .eq. 2) then
         A_DOT_XT(4*(nf-1)*N0+4*(N0-1)+1)=T_ns(1,N0,nf)
!        elseif(flag_BC(N0,nf) .eq. 5) then
!          A_DOT_XT(4*(nf-1)*N0+4*(N0-1)+1)=(a_2*x(N0,nf)*x_ns0(1,N0,nf)+a_2*y(N0,nf)*y_ns0(1,N0,nf)+a_2*z(N0,nf)*z_ns0(1,N0,nf))/dt0 &
!          & -3.0*(x_ns0(2,N0,nf)*x_ns(3,N0,nf)+y_ns0(2,N0,nf)*y_ns(3,N0,nf)+z_ns0(2,N0,nf)*z_ns(3,N0,nf))-T_ns(1,N0,nf)
        endif 
        A_DOT_XT(4*(nf-1)*N0+2)=x(1,nf)/dt0-Vatt(1,nf)!-beta(nf)*x_ns(1,1,nf)*s(1)
        A_DOT_XT(4*(nf-1)*N0+3)=y(1,nf)/dt0-Vatt(2,nf)!-beta(nf)*y_ns(1,1,nf)*s(1)
        A_DOT_XT(4*(nf-1)*N0+4)=z(1,nf)/dt0-Vatt(3,nf)!-beta(nf)*z_ns(1,1,nf)*s(1)
        
        A_DOT_XT(4*(nf-1)*N0+6)=x(2,nf)/dt0-Vatt_2(1,nf)-beta(nf)*x_ns(1,2,nf)*s(2)!-&
       ! & 2.0d0*(-20.0d0+1.0d0-dot0(nf,2))*mu0(nf)*5.0d0*s(2)*x_ns(1,2,nf)/mu(nf)
        A_DOT_XT(4*(nf-1)*N0+7)=y(2,nf)/dt0-Vatt_2(2,nf)-beta(nf)*y_ns(1,2,nf)*s(2)!-&
       ! & 2.0d0*(-20.0d0+1.0d0-dot0(nf,2))*mu0(nf)*5.0d0*s(2)*y_ns(1,2,nf)/mu(nf)
        A_DOT_XT(4*(nf-1)*N0+8)=z(2,nf)/dt0-Vatt_2(3,nf)-beta(nf)*z_ns(1,2,nf)*s(2)!-&
       ! & 2.0d0*(-20.0d0+1.0d0-dot0(nf,2))*mu0(nf)*5.0d0*s(2)*z_ns(1,2,nf)/mu(nf)


!        A_DOT_XT(4*(nf-1)*N0+2+4)=x_ns(1,1,nf)/dt0-(Omega_Tn(2,1)*z_ns0(1,1,nf)-Omega_Tn(3,1)*y_ns0(1,1,nf))
!        A_DOT_XT(4*(nf-1)*N0+3+4)=y_ns(1,1,nf)/dt0-(-Omega_Tn(1,1)*z_ns0(1,1,nf)+Omega_Tn(3,1)*x_ns0(1,1,nf))
!        A_DOT_XT(4*(nf-1)*N0+4+4)=z_ns(1,1,nf)/dt0-(Omega_Tn(1,1)*y_ns0(1,1,nf)-Omega_Tn(2,1)*x_ns0(1,1,nf))

        A_DOT_XT(4*(N0-1-1)+2+(nf-1)*4*N0)=x_ns(2,N0-1,nf)
        A_DOT_XT(4*(N0-1-1)+3+(nf-1)*4*N0)=y_ns(2,N0-1,nf)
        A_DOT_XT(4*(N0-1-1)+4+(nf-1)*4*N0)=z_ns(2,N0-1,nf)
        if (flag_BC(N0,nf) .eq. 3) then
         A_DOT_XT(4*(N0-1)+2+(nf-1)*4*N0)=-x_ns(3,N0,nf)+T(N0,nf)*x_ns0(1, N0,nf)
         A_DOT_XT(4*(N0-1)+3+(nf-1)*4*N0)=-y_ns(3,N0,nf)+T(N0,nf)*y_ns0(1, N0,nf)
         A_DOT_XT(4*(N0-1)+4+(nf-1)*4*N0)=-z_ns(3,N0,nf)+T(N0,nf)*z_ns0(1, N0,nf)
        elseif(flag_BC(N0,nf) .eq. 2) then
         A_DOT_XT(4*(N0-1)+2+(nf-1)*4*N0)=x(N0,nf)/dt0
         A_DOT_XT(4*(N0-1)+3+(nf-1)*4*N0)=y(N0,nf)/dt0
         A_DOT_XT(4*(N0-1)+4+(nf-1)*4*N0)=z(N0,nf)/dt0

        elseif (flag_BC(N0,nf) .eq. 5) then

!        A_DOT_XT(4*(N0-1)+1+(nf-1)*4*N0)=0.0*a_2*mu(nf)*(x(N0,nf)*x_ns0(1,N0,nf)+y(N0,nf)*y_ns0(2,N0,nf)+z(N0,nf)*z_ns0(3,N0,nf))/dt0 &
!        & -2.0d0*T_ns(1,N0,nf)-0.0d0*(x_ns(3,N0,nf)*x_ns0(2,N0,nf)+x_ns(3,N0,nf)*x_ns0(2,N0,nf)+x_ns(3,N0,nf)*x_ns0(2,N0,nf)) &
!        & -0.0*(ux_ext(N0,nf)*x_ns0(1,N0,nf)+uy_ext(N0,nf)*y_ns0(1,N0,nf)+uz_ext(N0,nf)*z_ns0(1,N0,nf))*mu(nf)

!         A_DOT_XT(4*(N0-1)+2+(nf-1)*4*N0)=(T(N0,nf)*x_ns0(1,N0,nf)-x_ns(3,N0,nf))*n_1(1,N0,nf)+&
!         & (T(N0,nf)*y_ns0(1,N0,nf)-y_ns(3,N0,nf))*n_1(2,N0,nf)+&
!         & (T(N0,nf)*z_ns0(1,N0,nf)-z_ns(3,N0,nf))*n_1(3,N0,nf)

!         A_DOT_XT(4*(N0-1)+3+(nf-1)*4*N0)=(T(N0,nf)*x_ns0(1,N0,nf)-x_ns(3,N0,nf))*n_2(1,N0,nf)+&
!         & (T(N0,nf)*y_ns0(1,N0,nf)-y_ns(3,N0,nf))*n_2(2,N0,nf)+&
!         & (T(N0,nf)*z_ns0(1,N0,nf)-z_ns(3,N0,nf))*n_2(3,N0,nf)
         
!         A_DOT_XT(4*(N0-1)+4+(nf-1)*4*N0)=a_2*(x(N0,nf)*n_0(1,N0,nf)+y(N0,nf)*n_0(2,N0,nf)+z(N0,nf)*n_0(3,N0,nf))/dt0
         A_DOT_XT(4*(nf-1)*N0+4*(N0-1)+1)=T_ns(1,N0,nf)
         A_DOT_XT(4*(N0-1)+2+(nf-1)*4*N0)=x(N0,nf)/dt0-1.0d0*(V_T(1,1)*n_1(1,N0,nf)*V_T(2,1)*n_1(2,N0,nf)+V_T(1,1)*n_1(3,N0,nf))*n_1(1,N0,nf)
         A_DOT_XT(4*(N0-1)+3+(nf-1)*4*N0)=y(N0,nf)/dt0-1.0d0*(V_T(1,1)*n_1(1,N0,nf)*V_T(2,1)*n_1(2,N0,nf)+V_T(1,1)*n_1(3,N0,nf))*n_1(2,N0,nf)
         A_DOT_XT(4*(N0-1)+4+(nf-1)*4*N0)=z(N0,nf)/dt0-1.0d0*(V_T(1,1)*n_1(1,N0,nf)*V_T(2,1)*n_1(2,N0,nf)+V_T(1,1)*n_1(3,N0,nf))*n_1(3,N0,nf)


        endif  
        enddo

!$OMP enddo
!$OMP end parallel 
	end subroutine A_DOT_X_implicit

!************************************************************************************!
!************************************************************************************!
!************************************************************************************!

end module Tension
     

  
    
