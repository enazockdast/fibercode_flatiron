#include <pvFmmInterface.h>
#include <mpi.h>
#include <omp.h>
#include <vector>
#include "pvfmm.hpp"

typedef std::vector<double> vec;

//global FMM trees to keep state between calls
pvfmm::PtFMM_Tree *g_sl_tree;
pvfmm::PtFMM *g_sl_matrices;

pvfmm::PtFMM_Tree *g_sldl_tree;
pvfmm::PtFMM *g_sldl_matrices;

//correction terms for the symmetric part of stokes doublet to get the
//stokes stresslet (stokes double layer kernel)
pvfmm::PtFMM_Tree *g_lap_sl_tree;
pvfmm::PtFMM *g_lap_sl_matrices;


void pvfmm_mpi_init_(int &rank, int &size){
    int argc(0);
    char **argv;
    MPI_Init(&argc, &argv);
    MPI_Comm comm=MPI_COMM_WORLD;

    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);
    std::cout<<" [pvFMM] Initialized MPI (rank/size "<<rank<<"/"<<size<<")\n";
}

void pvfmm_mpi_finalize_(){
    int rank,size;
    MPI_Comm comm=MPI_COMM_WORLD;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);
    std::cout<<" [pvFMM] Finalizing MPI (rank/size "<<rank<<"/"<<size<<")\n";

    //clearing data
    if (g_sl_tree!=NULL)
        g_sl_tree->ClearFMMData();

    if (g_sldl_tree!=NULL)
        g_sldl_tree->ClearFMMData();

    if (g_lap_sl_tree!=NULL)
        g_lap_sl_tree->ClearFMMData();

    delete g_sl_tree;
    delete g_sldl_tree;
    delete g_lap_sl_tree;

    delete g_sl_matrices;
    delete g_sldl_matrices;
    delete g_lap_sl_matrices;

    //finalizing
    MPI_Finalize();
}

void pvfmm_mpi_gather_(
    void* send_data, int &send_count,
    void* recv_data, int &recv_count,
    int &root){

    MPI_Comm comm=MPI_COMM_WORLD;
    std::cout<<MPI_DOUBLE<<"\t"<<MPI_INT<<"\t"<<root<<send_count<<"\t"<<recv_count<<std::endl;
    MPI_Gather(
        send_data, send_count, MPI_DOUBLE,
        recv_data, recv_count, MPI_DOUBLE, root, comm);
}

void pvfmm_mpi_scatter_(
    void* send_data, int &send_count,
    void* recv_data, int &recv_count,
    int &root){

    MPI_Comm comm=MPI_COMM_WORLD;
    MPI_Scatter(
        send_data, send_count, MPI_DOUBLE,
        recv_data, recv_count, MPI_DOUBLE, root, comm);
}


void pvfmm_mpi_reduce_(
      void* send_data, void* recv_data,
      int &send_count, int &root){
      MPI_Comm comm=MPI_COMM_WORLD;
      MPI_Reduce(send_data,recv_data,send_count,MPI_DOUBLE,
      MPI_SUM,root,comm);
}

void pvfmm_mpi_barrier_(){
	MPI_Comm comm=MPI_COMM_WORLD;
	MPI_Barrier(comm);
}


void pvfmm_mpi_bcast_(
      void* send_data, int &send_count,int &root){
      MPI_Comm comm=MPI_COMM_WORLD;	
      MPI_Bcast(send_data,send_count, MPI_DOUBLE,root,comm);
}

void pvfmmstokes_sl_setorder_(int &mult_order){
    std::cout<<" [pvFMM] set sl matrices order:"<<mult_order<<std::endl;
    if(g_sl_matrices)
        delete g_sl_matrices;
    MPI_Comm comm=MPI_COMM_WORLD;
    g_sl_matrices = new pvfmm::PtFMM;
    g_sl_matrices->Initialize(mult_order, comm, &pvfmm::ker_stokes_vel);
}

void pvfmmstokes_sldl_setorder_(int &mult_order){
    std::cout<<" [pvFMM] set sldl matrices order:"<<mult_order<<std::endl;
    MPI_Comm comm=MPI_COMM_WORLD;

    if(g_sldl_matrices)
        delete g_sldl_matrices;
    g_sldl_matrices = new pvfmm::PtFMM;
    g_sldl_matrices->Initialize(mult_order, comm, &pvfmm::ker_stokes_vel,
        &pvfmm::ker_stokes_vel);

    if(g_lap_sl_matrices)
        delete g_lap_sl_matrices;
    g_lap_sl_matrices = new pvfmm::PtFMM;
    g_lap_sl_matrices->Initialize(mult_order, comm, &pvfmm::laplace_grad_d,
        &pvfmm::laplace_potn_d);
}

void pvfmmstokes_sl_(
    double *src, double *den, size_t &nsrc,
    double *trg, double *pot, size_t &ntrg,
    int &max_pts, int &rebuild_tree){

    std::cout<<" [pvFMM-SL] nsrc:"<<nsrc<<",ntrg:"<<ntrg<<",pts:"<<max_pts<<
        ",rebuild:"<<rebuild_tree<<std::endl;

    // copy data
    vec srcv(src,src+COORD_DIM*nsrc);
    vec denv(den,den+COORD_DIM*nsrc);
    vec trgv(trg,trg+COORD_DIM*ntrg);
    vec potv(COORD_DIM*ntrg);

    MPI_Comm comm=MPI_COMM_WORLD;
    // build tree
    if(rebuild_tree || g_sl_tree==NULL){
        assert(comm!=NULL);        //should call pvfmm_mpi_init first
        assert(g_sl_matrices!=NULL); //should also call pvfmmstokes_sl_init
        std::cout<<" [pvFMM-SL] (Re)building pvFMM tree"<<std::endl;

        if(g_sl_tree!=NULL){
            g_sl_tree->ClearFMMData();
            delete g_sl_tree;
        }

        g_sl_tree=PtFMM_CreateTree(srcv, denv, trgv, comm,
            max_pts, pvfmm::FreeSpace);

        // FMM Setup
        g_sl_tree->SetupFMM(g_sl_matrices);

        // Run FMM
        std::cout<<" [pvFMM-SL] Evaluating point FMM"<<std::endl;
        PtFMM_Evaluate(g_sl_tree,potv,ntrg);
    } else {
        // Run FMM
        g_sl_tree->ClearFMMData();
        std::cout<<" [pvFMM-SL] Evaluating point FMM"<<std::endl;
        PtFMM_Evaluate(g_sl_tree,potv,ntrg,&denv);
    }

    // copy to pot
    for(size_t i=0;i<COORD_DIM*ntrg;++i)
        pot[i]=potv[i];
}

void pvfmmstokes_sldl_(
    double *sl_src, double *sl_den, size_t &sl_nsrc,
    double *dl_src, double *dl_den_nor, size_t &dl_nsrc,
    double *trg, double *pot, size_t &ntrg,
    int &max_pts, int &rebuild_tree){

    std::cout<<" [pvFMM-DL] sl_nsrc:"<<sl_nsrc<<",dl_nsrc:"<<dl_nsrc<<
        ",ntrg:"<<ntrg<<",pts:"<<max_pts<<",rebuild:"<<rebuild_tree<<std::endl;

    // copy data
    vec sl_srcv(sl_src,sl_src+COORD_DIM*sl_nsrc);
    vec sl_denv(sl_den,sl_den+COORD_DIM*sl_nsrc);

    vec dl_srcv(dl_src    ,dl_src    +  COORD_DIM*dl_nsrc);
    vec dl_denv(dl_den_nor,dl_den_nor+2*COORD_DIM*dl_nsrc);
    vec ndotf(dl_nsrc);  // density dot normal (Laplace single-layer)

    vec trgv(trg,trg+COORD_DIM*ntrg);
    vec potv(COORD_DIM*ntrg);
    vec pot_corr(COORD_DIM*ntrg);

    for(size_t iS=0;iS<ndotf.size();++iS){
        ndotf[iS] =dl_den_nor[iS*6+0]*dl_den_nor[iS*6+3];
        ndotf[iS]+=dl_den_nor[iS*6+1]*dl_den_nor[iS*6+4];
        ndotf[iS]+=dl_den_nor[iS*6+2]*dl_den_nor[iS*6+5];
    }

    MPI_Comm comm=MPI_COMM_WORLD;
    // build tree
    if(rebuild_tree || g_sldl_tree==NULL){
        assert(comm!=NULL);          //should call pvfmm_mpi_init first
        assert(g_sldl_matrices!=NULL); //should also call pvfmmstokes_sl_init
        std::cout<<" [pvFMM-DL] (Re)building pvFMM tree"<<std::endl;

        if(g_sldl_tree!=NULL){
            g_sldl_tree->ClearFMMData();
            delete g_sldl_tree;
        }

        if(g_lap_sl_tree!=NULL){
            g_lap_sl_tree->ClearFMMData();
            delete g_lap_sl_tree;
        }

        g_sldl_tree=PtFMM_CreateTree(sl_srcv,sl_denv,
            dl_srcv,dl_denv,trgv,comm,max_pts,pvfmm::FreeSpace);

        g_lap_sl_tree=PtFMM_CreateTree(dl_srcv,ndotf,trgv,
            comm,max_pts,pvfmm::FreeSpace);

        // FMM Setup
        g_sldl_tree->SetupFMM(g_sldl_matrices);
        g_lap_sl_tree->SetupFMM(g_lap_sl_matrices);

        // Run FMM
        std::cout<<" [pvFMM-DL] Evaluating point FMM"<<std::endl;
        PtFMM_Evaluate(g_sldl_tree  , potv    , ntrg);
        PtFMM_Evaluate(g_lap_sl_tree, pot_corr, ntrg);
    } else {
        // Run FMM
        g_sldl_tree->ClearFMMData();
        g_lap_sl_tree->ClearFMMData();

        std::cout<<" [pvFMM-DL] Evaluating point FMM"<<std::endl;
        PtFMM_Evaluate(g_sldl_tree  ,potv    ,ntrg,&sl_denv,&dl_denv);
        PtFMM_Evaluate(g_lap_sl_tree,pot_corr,ntrg,&ndotf  );
    }

    //Apply correction, copy
    assert(potv.size()==pot_corr.size());
    for(size_t iT=0;iT<potv.size();++iT)
        pot[iT]=potv[iT]-0.5*pot_corr[iT];
}
