	program test_FMM
	use Hydro_inter 
	implicit none 

	real*8, parameter :: pi=4.0*atan(1.0)
	integer :: i, j, k, dum, t 
	integer, parameter :: iprec=3, ifsingle=1, ifdouble=0, ifpot=1, ifgrad=0
	integer :: ier
	integer, parameter :: ifpottarg=1, ifgradtarg=0
	integer :: nparts, ntargs, n_t, nf, N
	real*8 :: t1, t2

	real*8, allocatable, dimension (:,:) :: source, sigma_sl, sigma_dl, sigma_dv
	real*8, allocatable :: pot(:,:), pre(:), grad(:,:,:), pot_2(:,:)
	real*8, allocatable :: pot_d(:,:), pre_d(:), grad_d(:,:,:)
	real*8, allocatable ::  targ(:,:), pottarg(:,:), pretarg(:), gradtarg(:,:,:)
	real*8, allocatable :: pottarg_d(:,:), pretarg_d(:), gradtarg_d(:,:,:)
	real*8, allocatable, dimension (:,:) :: x, y, z, ux, uy, uz, fx, fy, fz
	integer, allocatable :: N_var(:)
	integer, parameter :: N_F=10, N0=200
	real*8, dimension (N0) :: ux_self, uy_self, uz_self
	real*8 :: ds=0.01





	n_t=N0*N_F

	allocate(source(3,n_t), sigma_sl(3,n_t), &
	& sigma_dl(3,n_t), sigma_dv(3,n_t))

	allocate(pot(3, n_t), pre(n_t), grad(3,3,n_t))
	allocate(pot_d(3, n_t), pot_2(3,n_t), pre_d(n_t), grad_d(3,3,n_t))
	allocate(targ(3,ntargs), pottarg(3, ntargs), pretarg(ntargs), gradtarg(3,3,ntargs))
	allocate(pottarg_d(3, ntargs), pretarg_d(ntargs), gradtarg_d(3,3,ntargs))
	allocate(x(N0,N_F), y(N0,N_F), z(N0,N_F), & 
	& fx(N0,N_F), fy(N0,N_F), fz(N0,N_F), & 
	& ux(N0,N_F), uy(N0,N_F), uz(N0,N_F), N_var(N_F)) 


	pot(:,:)=0.0;sigma_sl(:,:)=0.0;sigma_dl(:,:)=0.0;sigma_dv(1,:)=1.0;sigma_dv(2:3,:)=0.0;
	pre(:)=0.0;grad(:,:,:)=0.0;source(:,:)=0.0;

	x(:,:)=0.0;y(:,:)=0.0;z(:,:)=0.0;fx(:,:)=0.0;fy(:,:)=0.0;fz(:,:)=0.0;
	ux(:,:)=0.0;uy(:,:)=0.0;uz(:,:)=0.0;ux_self(:)=0.0;uy_self(:)=0.0;uz_self(:)=0.0;


	N_var(1:N_f)=40;

	n_t=sum(N_var)
	k=0;
	do nf=1,N_F
	N=N_var(nf);
	do i=1,N
	x(i,nf)=ds*(i-1)+(nf-1)*0.5

	y(i,nf)=ds*(nf-1)*(i-1)*0.5

	z(i,nf)=ds*(nf-1)*(i-1)*0.25

	
	enddo

	source(1,k+1:k+N)=x(1:N,nf)
	source(2,k+1:k+N)=y(1:N,nf)
	source(3,k+1:k+N)=z(1:N,nf)

	
	fx(1:N,nf)=x(1:N,nf)*sqrt(x(1:N,nf)**2+y(1:N,nf)**2)
	fy(1:N,nf)=y(1:N,nf)*sqrt(x(1:N,nf)**2+y(1:N,nf)**2)
	fz(1:N,nf)=z(1:N,nf)*sqrt(x(1:N,nf)**2+y(1:N,nf)**2)
	
	sigma_sl(1,k+1:k+N)=fx(1:N,nf)*ds
	sigma_sl(2,k+1:k+N)=fy(1:N,nf)*ds
	sigma_sl(3,k+1:k+N)=fz(1:N,nf)*ds
	k=k+N
	enddo

	nparts=k


	print *, "nparts is ...", nparts, k 

	do t=1,10


	call direct_manybody(fx,fy,fz,x,y,z,N_var,N_f,N0,ds,ux,uy,uz)

	
	
	call cpu_time(t1)
	call stfmm3Dpartself(ier,iprec,nparts,source, ifsingle,sigma_sl,ifdouble,& 
	& sigma_dl,sigma_dv, ifpot,pot,pre,ifgrad,grad)
	call cpu_time(t2)

	k=0;
	do nf=1,N_F
	N=N_var(nf)
	call MT_uinf_self(fx(:,nf),fy(:,nf),fz(:,nf),x(:,nf),y(:,nf),z(:,nf),N,N0,ds, ux_self,uy_self,uz_self)

	pot(1,k+1:k+N)=2.0*pot(1,k+1:k+N)-ux_self(1:N)
	pot(2,k+1:k+N)=2.0*pot(1,k+1:k+N)-uy_self(1:N)
	pot(3,k+1:k+N)=2.0*pot(1,k+1:k+N)-uz_self(1:N)

	k=k+N

	enddo






	k=0;

	do nf=1,N_f

	print *,"N_f is ...", N_f, nf	
	N=N_var(nf)
	do i=1,N
	k=k+1

	print *, ux(i,nf)/(pot(1,k))-1.0,pot(1,k), nf

	enddo

	enddo 



	call cpu_time(t2)
	print *, "elapsed time  FMM ....", t2-t1

	print *,"*********************************"

	enddo

	end program test_FMM


!	call cpu_time(t1)
!	call stfmm3Dparttarg(ier,iprec,nparts,source, ifsingle,sigma_sl,ifdouble,& 
!	& sigma_dl,sigma_dv, 0,pot,pre,ifgrad,grad,&
!	& ntargs,targ,ifpottarg,pottarg,pretarg,ifgradtarg,gradtarg)
!	call cpu_time(t2)
!	print *, "elapsed time  FMM ....", t2-t1

!	call cpu_time(t1)
!	call st3Dpartdirect(nparts,source, ifsingle,sigma_sl,ifdouble,& 
!	& sigma_dl,sigma_dv, ifpot,pot_d,pre_d,ifgrad,grad_d,&
!	& ntargs,targ,ifpottarg,pottarg_d,pretarg_d,ifgradtarg,gradtarg_d)
!	call cpu_time(t2)	
!	print *, "elapsed time  direct ....", t2-t1

!	print *, "the error message is ....", ier


!	print *, "maximum absolute error is ...",  maxval(abs(pot_d-pot)), maxval(abs(pottarg_d-pottarg)) 



!	targ(1,dum)=6.50*cos(teta(i)+pi/10.0)*cos(phi(j)-pi/10.0)
!	targ(2,dum)=6.50*sin(teta(i)+pi/10.0)*cos(phi(j)-pi/10.0)
!	targ(3,dum)=6.50*sin(phi(j)-pi/10.0)
!
!	sigma_sl(1,dum)=(cos(teta(i))**2)*(sin(teta(i))**2)
!	sigma_sl(2,dum)=-0.1
!	sigma_sl(3,dum)=1.1

!	sigma_dv(1,dum)=1.0
!	sigma_dv(2,dum)=0.0
!	sigma_dv(3,dum)=0.0
!	enddo
!	enddo
!	enddo
!	nparts=k
