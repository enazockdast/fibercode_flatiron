module  initialize

  use input_file
  use fmm_interface 
  use, intrinsic :: iso_c_binding
  implicit none

  !*************************************************************!
  !************** Simulation Parameters ************************!
  !*************************************************************! 
  integer :: nt_final,restart,TID
  integer :: t000,t00,t01,t02,t03,t04,t05,t06,t07,t08,rate
  !*************************************************************!
  !************** Fiber Parameters *****************************!
  !*************************************************************! 

  integer, parameter :: n_half=nint((n_s*1.0-1.0)/2.0), m=4,N_f_h=N_f/2
  real (kind=dp),  parameter ::  a_MT=0.02, ds=L0/(1.0*N0-1.0),L=1.0d0,r_MTOC=0.20

  integer :: N_teta,N_phi, N_total

  real (kind=dp) :: teta0, phi0, teta_i, teta_f, phi_i, phi_f,dteta,dphi
  real (kind=dp) :: xi,err

  integer, allocatable, dimension (:) :: N_var, FLAG_P,N_VAR_P,FLAGP_P,counter,tp,t_att,f_pin
  integer, allocatable, dimension (:,:) :: flag_BC,flag_BC_T
  real (kind=dp), allocatable, dimension (:) :: chev_w,x_pin,y_pin,z_pin
  real (kind=dp), allocatable, dimension (:) :: L_F_T,L_F,L_FF,mu0,mu,mu1,mu2,C,C0, imu2,BC_T,L_max,beta, & 
       beta_n,beta_i,beta_end
  real (kind=dp), allocatable, dimension (:,:) :: dum,dot0, dot0_2,dot1,dot2,dot3,beta_t
  real (kind=dp), allocatable, dimension (:) :: s, teta, phi
  real (kind=dp), allocatable, dimension (:) :: s_bulk, teta_bulk, phi_bulk
  real (kind=dp), allocatable, dimension (:,:,:) :: n_1,n_2,n_0
  real (kind=dp), allocatable, dimension (:) :: x_bulk, y_bulk,z_bulk,dum_bulk, &
       & ux_bulk,uy_bulk,uz_bulk
!       & ux_bulk_SP,uy_bulk_SP,uz_bulk_SP,ux_bulk_SP2,uy_bulk_SP2,uz_bulk_SP2
  real (kind=dp) :: teta0_bulk,phi0_bulk,r0_bulk
  integer :: i_bulk,j_bulk,k_bulk
  integer  :: N_teta_bulk, N_phi_bulk, N_r_bulk
  integer, allocatable, dimension (:) :: W_bulk
  real (kind=dp), allocatable, dimension (:,:) :: F_L
  real (kind=dp), allocatable, dimension (:,:) :: FT,x,y,z,x_T,y_T,z_T, &
       & x0,y0,z0,r0,xp,yp,zp,xpp,ypp,zpp,  & 
       & dxdt, dydt, dzdt, &
       & ux_ext,uy_ext,uz_ext, &
       & ux2_ext,uy2_ext,uz2_ext, &
       & ux_SP,uy_SP,uz_SP,&
       & ux_SP_T, uy_SP_T, uz_SP_T, &
       & ux_SP_B, uy_SP_B, uz_SP_B, &
       & ux_SP_HD, uy_SP_HD, uz_SP_HD, &
       & ux_SP_HD_B, uy_SP_HD_B, uz_SP_HD_B, &
       & ux_SP_HD_T, uy_SP_HD_T, uz_SP_HD_T, &
       & ux_SP_HD_B2, uy_SP_HD_B2, uz_SP_HD_B2, &
       & ux_SP_HD_T2, uy_SP_HD_T2, uz_SP_HD_T2, &
       & ux_MT,uy_MT,uz_MT, &
       & ux_MT_T,uy_MT_T,uz_MT_T, &
       & ux_MT_T_P,uy_MT_T_P,uz_MT_T_P, &
       & ux_MT_T_PP,uy_MT_T_PP,uz_MT_T_PP, &
!       & ux_SPCOR,uy_SPCOR,uz_SPCOR, &
!       & ux_SPCOR_P,uy_SPCOR_P,uz_SPCOR_P, &
!       & ux_SPCOR_PP,uy_SPCOR_PP,uz_SPCOR_PP, &
       & ux_self, uy_self, uz_self, &
       & fx_MT_B,fy_MT_B,fz_MT_B,fx_MT_T,fy_MT_T,fz_MT_T, &
       & fx_MT_ext, fy_MT_ext, fz_MT_ext, &
       & fx_2, fy_2, fz_2, &
       & ux_2, uy_2, uz_2, ux_3,uy_3,uz_3, &
       & ux_2_p, uy_2_p, uz_2_p, &
       & ux_2_pp, uy_2_pp, uz_2_pp, &
       & T,T0


  real (kind=dp), allocatable, dimension (:,:,:) :: xs0,ys0,zs0,xs,ys,zs, &
       & x_ns0,y_ns0,z_ns0, &
       & x_ns, y_ns, z_ns, & 
       & ux_s,uy_s,uz_s, &
       & ux_s0,uy_s0,uz_s0,&
       & xsus,xsuTs,xsus_f, xsus_T, &
       & fx_s, fy_s, fz_s, &
       & fx_MTT_B,fy_MTT_B,fz_MTT_B,fx_MTT_T,fy_MTT_T,fz_MTT_T, &
       & fx_MTT_ext, fy_MTT_ext, fz_MTT_ext, &
       & uxs_MT,uys_MT,uzs_MT, &
       & uxs_MT_T,uys_MT_T,uzs_MT_T, &
       & uxs_SP,uys_SP,uzs_SP, &
       & uxs_SP_B,uys_SP_B,uzs_SP_B, &
       & uxs_SP_T,uys_SP_T,uzs_SP_T, &
       & uxs_SP_HD,uys_SP_HD,uzs_SP_HD, &
       & uxs_SP_HD_B,uys_SP_HD_B,uzs_SP_HD_B, &
       & uxs_SP_HD_T,uys_SP_HD_T,uzs_SP_HD_T, &
       & uxs_SP_HD_B2,uys_SP_HD_B2,uzs_SP_HD_B2, &
       & uxs_SP_HD_T2,uys_SP_HD_T2,uzs_SP_HD_T2, &
!       & uxs_SPCOR,uys_SPCOR,uzs_SPCOR, &
!       & uxs_SPCOR_P,uys_SPCOR_P,uzs_SPCOR_P, &
       & ux_ext_tr, uy_ext_tr, uz_ext_tr, &
       & uxs_2, uys_2, uzs_2, xsus_2, &  
       & fxs,fxss, fsxs, &
       & QX,QY,QZ,QX0,QY0,QZ0, U_EXT,&
       & T_tr, Ts,T_ns    

  ! real (kind=dp), allocatable, dimension (:,:,:) :: Tt,Tst,&
  ! & xt, yt, zt, &
  ! & xst, yst, zst, &
  ! & QXt, QYt, QZt, &
  ! & ux_t, uy_t, uz_t

  real (kind=dp), allocatable, dimension (:,:,:) :: Vatt_t, Vatt_t_2
  real (kind=dp), allocatable, dimension (:,:) :: dxc1, dxc1_2,dxc1p,dxc2p,X_rot,X_att_T, &
       & X_att0,X_att, dxc2, DX_at,DX_at2,dxc2_2,X_att0_2,X_att_2, & 
       & Vatt0,Vatt,Vatt0_2,Vatt_2, Ten_B, Ten_T, F_MT_TN, F_MT_BN
  real (kind=dp), allocatable, dimension (:,:) :: Vt,xct
  real (kind=dp), allocatable, dimension (:,:) :: TNt

  real (kind=dp), allocatable, dimension (:) :: F_T,F_T0,T_T,XT_2, XT,RHS, RHS_im, &
       & computed_solution,computed_solution_im, B3_T,F_XT2,F_XT
  real (kind=dp), allocatable, dimension (:)   :: A_DOT_T,A_DOT_X,A_DOT_XT
  real (kind=dp), allocatable, dimension (:,:,:) ::ES

  real (kind=dp), allocatable :: C_T0(:,:,:), C_T(:,:,:), C_T_B1(:,:,:),C_T_B2(:,:,:)
  real (kind=dp), allocatable :: c_1T(:,:), c_2T(:,:), c_1(:), c_2(:), c_3(:),time_1(:),time_2(:)


  real (kind=dp), allocatable, dimension (:) :: xns_end, yns_end, zns_end,DR_at
  real (kind=dp) :: rns_end

  real (kind=dp), allocatable, dimension (:,:) :: sigma_sl_MT_T, sigma_dl_MT_T, sigma_dv_MT, &
       & source_MT, targ_MT, pot_MT_T, pot_MT_T_P, pot_MT_T_PP,pottarg_MT

  real (kind=dp), allocatable, dimension(:,:) :: sigma_dv,sigma_dl,sigma_sl,source,pot
  real (kind=dp), allocatable :: pre(:),grad(:,:,:)


  real (kind=dp), allocatable, dimension (:,:) :: sigma_sl_MT_B, sigma_dl_MT_B, &
       & pot_MT_B,pot_MT_B_P,pot_MT_B_PP

  real (kind=dp), allocatable, dimension (:,:) :: targ_bulk, pottarg_bulk_MT
  real (kind=dp), allocatable, dimension (:) ::   pretarg_bulk
  real (kind=dp), allocatable, dimension (:,:,:) :: gradtarg_bulk

  real (kind=dp), allocatable, dimension (:,:) :: pottarg_bulk_SPCOR



  real (kind=dp), allocatable, dimension (:,:) :: sigma_sl_MT,sigma_dl_MT,pot_MT,pot_MT2
  real (kind=dp), allocatable, dimension (:) :: pre_MT_T, pretarg_MT
  real (kind=dp), allocatable, dimension (:,:,:) :: grad_MT_T, gradtarg_MT

  real (kind=dp), allocatable, dimension (:) :: pre_MT_B
  real (kind=dp), allocatable, dimension (:,:,:) :: grad_MT_B

  real (kind=dp), allocatable, dimension (:) :: pre_MT
  real (kind=dp), allocatable, dimension (:,:,:) :: grad_MT
  real (kind=dp), allocatable, dimension (:) :: U_bath_MT_0


  integer, allocatable, dimension (:,:) :: ia_T, ja_T,ia_IE,ja_IE 
  integer, allocatable, dimension (:,:) ::   ia_D, ja_D
  integer, allocatable, dimension (:,:) :: ia_XT, ja_XT,ia_XT2,ja_XT2
  real (kind=dp), allocatable, dimension (:,:) :: a_T
  real (kind=dp), allocatable, dimension (:,:) :: a_XT,a_XT2,a_IE
  real (kind=dp), allocatable, dimension (:,:) :: a_D
  real( kind=dp), allocatable, dimension (:,:) :: XX,F_X

  real (kind=dp), allocatable, dimension (:,:) :: V, V2, O,O2,V_2,V2_2,F_active
  real (kind=dp), allocatable, dimension (:,:,:) :: S_2,S_1
  integer :: FLAG_T,FLAG_BI
  !******************************************************************************************!
  !******************** C++ variables needed for FMM call********************************      

  integer :: mpi_rank, mpi_size,id
  integer, parameter :: DIM0=3

  integer (C_SIZE_T) :: FLAG_MANYBODY,t_lag,FLAG_PVFMM
  integer (C_SIZE_T) :: nsrc,ntrg,nsrc_seq,ntrg_seq,nsrc2,ntrg2,ntrg_bulk
  real (C_DOUBLE), allocatable, target, dimension(:) :: src,sl_den,dl_den_nor,trg,sldl_pot_T,sldl_pot,sldl_pot2
  real (C_DOUBLE), allocatable, target, dimension(:) :: src2,src3,sl_den2,dl_den_nor2,trg2,sldl_pot3
  real (C_DOUBLE), allocatable, target, dimension(:) :: trg_bulk,pot_bulk
  real (kind=dp), allocatable, dimension (:,:) :: seq_src, seq_den_sl, seq_den_dl,seq_pot,seq_pot_2,seq_dv
  real (kind=dp), allocatable :: seq_grad(:,:,:),seq_pre(:)
  integer (C_INT), parameter ::mult_order=6,max_pts=400,max_depth=20 !mult_ordershould be even
  real (C_DOUBLE),target :: fac2
  type (C_PTR) :: sl_context, sldl_context,sldl_bulk_context
  logical (C_INT), parameter :: true=1, false=0
  logical (C_INT) :: rebuild_tree        

  !************** Spinle & Cortex Parameters *******************!

  real (kind=dp), dimension (3,1) :: V_T0, V_T, Omega_T0, Omega_T, Omega_Tn,Omega_Tp,F_MT, T_MT, V_HD, Omega_HD, &
       & Omega_HDp,Omega_HDn,V_EXT, F_EXT, &
       & V_T0_B, & 
       & V_T0_T, & 
       & Omega_T0_B, & 
       & Omega_T0_Bp, & 
       & Omega_T0_Bn, &
       & Omega_T0_Tp, Omega_T0_Tn, &
       & Omega_T0_T, Omega_EXT, Tq_EXT,V_HD_B, V_HD_T, Omega_HD_T, Omega_HD_B, &
       & F_MT_T, T_MT_T, F_MT_B, T_MT_B, V_HD_T2, V_HD_B2, U_SP,W_SP, dV, dW, &
       & V_SPCOR, Omega_SPCOR,V_SPCOR_P,Omega_SPCOR_P,V_SPCOR_PP,Omega_SPCOR_PP
  real (kind=dp), parameter, dimension (3) :: e1=(/1.0, 0.0, 0.0/), &
       & e2=(/0.0, 1.0, 0.0/), &
       & e3=(/0.0, 0.0, 1.0/) 
  real (kind=dp) :: xc0(3),xc0p(3),nn_cont(3)
  real (kind=dp), dimension (3,1) :: omega
  real (kind=dp), dimension (3,3) :: E_EXT,E_T,M1,M2, Rx, Ry, Rz, RT, S_SP_HD, S_SP_HD_B,S_SP_HD_T,S_SP_B,S_SP_T
  real (kind=dp) :: V_MOB(3,6), O_MOB(3,6)        
  real (kind=dp) :: V_grand(24), V_grand_dum(24)
  integer :: n_node_SP,n_element_SP
  integer :: n_node_cor,n_element_cor,iter,iter_n,iter1,iter0,iter2,n_iter,iter_GMRES
  !------------------------------------------------------------!

  real (kind=dp), dimension(3) :: V_SP,Omega_SP,F_SP,Tq_SP,e_SP,e_cor,F_cor,Tq_cor,V_prev,V_pp

  !------------------------------------------------------------!
  integer :: info, info_SP, info_cor
  integer, allocatable, dimension (:) :: IPVT9_SP,IPVT9_cor                

  !--------------------------------------------------------------!

  real (kind=dp),  allocatable, dimension (:) :: x_g_SP,y_g_SP,z_g_SP,h_SP
  real (kind=dp), allocatable, dimension (:) :: x_g_cor,y_g_cor,z_g_cor,h_cor

  real(kind=dp) , allocatable, dimension (:) :: QxSP_g,QySP_g,QzSP_g
  real(kind=dp), allocatable, dimension (:) :: Qxcor_g,Qycor_g,Qzcor_g
  real(kind=dp), allocatable, dimension (:,:,:) :: F 
  real(kind=dp), allocatable, dimension (:,:) :: n_N_SP,n_N_cor

  real(kind=dp), allocatable, dimension (:)  :: xm_SP,ym_SP,zm_SP,h_SP_node, &
       & nx_SP,ny_SP,nz_SP
  real(kind=dp), allocatable, dimension (:)  ::xm_cor,ym_cor,zm_cor,h_cor_node, &
       & nx_cor,ny_cor,nz_cor
  real(kind=dp), allocatable, dimension (:)  :: Qx_SP,Qy_SP,Qz_SP,Q_SP
  real(kind=dp), allocatable, dimension (:)  :: Qx_cor,Qy_cor,Qz_cor,Q_cor


  real(kind=dp), allocatable, dimension (:)  :: U_bath_SP_0,U_bath_SP_1,U_bath_SP_2
  real(kind=dp), allocatable, dimension (:)  :: U_bath_cor_0,U_bath_cor, U_bath_cor_2

  real(kind=dp), allocatable, dimension (:,:) :: T_SP,iT_SP,T_cor,iT_cor
  real(kind=dp), allocatable, dimension (:,:)  :: coor_SP,coor_cor
  integer, allocatable, dimension (:,:) :: con_SP,con_cor
  real(kind=dp), allocatable,dimension(:,:) :: gauss_p,gauss_w
  real(kind=dp), allocatable,dimension(:) :: gauss_p0,gauss_w0



  real(kind=dp) ::gr_SP=1.0001,A_S_SP,I1_SP,I2_SP,I3_SP
  real(kind=dp) ::gr_cor=1.0001,A_S_cor,I1_cor,I2_cor,I3_cor



  !############################################################!
  !##################FMM Library Variables#####################!
  !############################################################!

  integer :: ier, iprec, ifsingle, ifdouble, ifgrad, ifpot, iftarg, &
       & ifpottarg, ifgradtarg,nparts_MT,ntarg_MT,nparts_sp_l,nparts_cor_l,&
       & nparts_SP,nparts_SPCOR_l,ntargs_SP,ntargs_sp_l,&
       & nparts_cor,ntargs_cor,ntargs_cor_l,&
       & nparts_SPCOR, ntargs_SPCOR,nparts,ntarg,ntarg_bulk

  real(kind=dp), allocatable, dimension (:,:) :: sigma_dv_sp_l, sigma_dl_sp_l, source_sp_l, source_SP, sigma_sl_SP, sigma_dl_SP, & 
       sigma_dv_SP
  real(kind=dp), allocatable, dimension (:,:) :: sigma_dv_cor_l,sigma_dl_cor_l,source_cor_l,source_cor,sigma_sl_cor,sigma_dl_cor, & 
       sigma_dv_cor
  real(kind=dp), allocatable :: pot_SP(:,:), pre_SP(:), grad_SP(:,:,:)
  real(kind=dp), allocatable :: pot_cor(:,:), pre_cor(:), grad_cor(:,:,:)
  real(kind=dp), allocatable ::  targ_SP(:,:), targ_SP_l(:,:),pottarg_SP(:,:),pottarg_sp_l(:,:), &
       & pretarg_SP(:),pretarg_sp_l(:),gradtarg_SP(:,:,:),gradtarg_sp_l(:,:,:),pottarg_sp_1D(:),pottarg_sp_1D_l(:)
  real(kind=dp), allocatable :: targ_cor_l(:,:),targ_cor(:,:), pottarg_cor(:,:),pottarg_cor_l(:,:), &
       pretarg_cor(:),pretarg_cor_l(:),gradtarg_cor(:,:,:),gradtarg_cor_l(:,:,:),pottarg_cor_1D_l(:),pottarg_cor_1D(:)
  real(kind=dp), allocatable :: hh_SP(:), hh_cor(:)

  real(kind=dp), allocatable, dimension(:,:) :: source_st, sigma_sl_st, sigma_dl_st,sigma_dv_st
  real(kind=dp), allocatable :: pot_st(:,:), pre_st(:), grad_st(:,:,:)
  real(kind=dp), allocatable ::  targ_st(:,:), pottarg_st(:,:),pretarg_st(:),gradtarg_st(:,:,:)

  real(kind=dp), allocatable, dimension (:,:):: source_spcor_l,source_SPCOR,sigma_sl_SPCOR, &
       & sigma_dl_SPCOR,sigma_dv_SPCOR,sigma_dl_SPCOR_l,sigma_dv_SPCOR_l
  real(kind=dp), allocatable :: pot_SPCOR(:,:), pre_SPCOR(:), grad_SPCOR(:,:,:)
  real(kind=dp), allocatable ::  targ_SPCOR(:,:),pottarg_SPCOR(:,:),pretarg_SPCOR(:),gradtarg_SPCOR(:,:,:)
  real(kind=dp), allocatable :: sigma_dl_SPCOR_MOB(:,:,:), pottarg_SPCOR_MOB(:,:,:)
  !*************************************************************!
  !*****************Other variables*****************************!
  !*************************************************************!
  integer :: i,j,k,nf, m0, t_step, N, nf2, N2,t_init
  integer :: seed(1), put
  real (kind=dp) :: kappa, kappa_e, BCT, t1, t2, alpha, dteta_x, dteta_y,dteta_z,FDOTR
  real (kind=dp) :: a_0,a_1,a_2,a_3      
  real (kind=dp) :: fac,stab0,stab,dt2
  !***********************************************************!
  !************Cortical Pulling Forces************************!
  !***********************************************************!
  integer :: nt_site, ntt_site, n_site
  integer, allocatable, dimension(:) :: a_site
  real (kind=dp), allocatable, dimension (:) :: x_site,y_site,z_site
  !$OMP threadprivate(TID)  
end module initialize


