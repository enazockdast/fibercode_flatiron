program main 
  use, intrinsic :: iso_c_binding
  use input_file
  use initialize
  use derivatives
  use linear_algebra
  use Hydro_inter
  use Tension
  use sparse_solve
  use A_TIMES_X
!  use BoundaryIntegral
!  use  GAUSS_ARRAY
!  use BI_SP_build_array
!  use BI_cor_build_array
!  use GaussInt_Array
  use biophysics
  use fmm_interface
  implicit none


  interface 

     function OMP_get_thread_num()

       integer :: OMP_get_thread_num

     end function OMP_get_thread_num

  end interface

  real*8 :: alpha0,beta0
  alpha0=1.0d0;beta0=0.0d0

  print *, "got this far"
  call MPI_INIT(mpi_rank,mpi_size)     
  id=mpi_rank   
  print *, "ID IS ", mpi_rank
  call pvfmm_mpi_barrier()
  include 'init2.f90'
  id=mpi_rank
  !	print *, "number of fibers are", N_f
  m0=m
  call make_fmm_context(mult_order, max_pts, max_depth, true, false,false,sl_context) !sl, dl, periodic, context 
  call make_fmm_context(mult_order, max_pts, max_depth, true, true ,false ,sldl_context) !sl, dl, periodic, context
  call make_fmm_context(mult_order, max_pts, max_depth, true,true,false ,sldl_bulk_context)
  id=mpi_rank


  !	print *, "n_s is ...", n_s
  open(unit=20,file='positions.txt')
  open(unit=21,file='positions_2.txt')
  open(unit=30,file='Tension.txt')
  open(unit=31,file='Tension_2.txt')
  open(unit=40, file='dots.txt')

  open(unit=50, file='B_deriv.txt')
  open(unit=60, file='u_2.txt')
  open(unit=400, file='u.txt')
  open(unit=80, file='xc.txt')

  open(unit=90, file='vel.txt')
  open(unit=100, file='omega.txt')
  open(unit=110,file='vel2.txt')
  open(unit=120,file='omega2.txt')
  open(unit=15, file='TB.txt')
  open(unit=16,file='F_T.txt')
  open(unit=150,file="polymer.txt",status="replace")
  open(unit=160,file="u_bulk.txt",status="replace")
  open(unit=201,file="U_bath_SP_0.txt",status="replace")
  open(unit=5,file='ia.txt')
  open(unit=6,file='ja.txt')
  open(unit=7,file='a.txt')
  open(unit=202,file='N_u_bulk.txt');


  !*********************************************************************!
  !*********************************************************************!
  !*********************************************************************!
  !*********************   Simulation time loop starts *****************! 
  !*********************************************************************!
  !*********************************************************************!
  !*********************************************************************!


  V_EXT(1:3,1)=0.0;
  E_ext(1:3,1:3)=0.0d0
  Omega_EXT(1,1)=0.0d0
  F_EXT(:,1)=0.0d0;Tq_EXT(:,1)=0.0d0;
  M1(:,:)=0.0;
  M2(:,:)=0.0;
  M1(1,1)=1.0d0;M1(2,2)=1.0d0;M1(3,3)=1.0d0;
  M2(1,1)=1.0d0;M2(2,2)=1.0d0;M2(3,3)=1.0d0;

  M1=M1*8.0d0/6.0d0
  stab=-0.0
  stab0=2.0d0
  fac=40.d0
  t_init=0

  do t_step=1,300000

     call system_clock(t000,rate)
     k=0	
     if (t_step > t_init) then
        F_EXT(3,1)=0.0d0!*sin(1.0d0*(t_step-1)*dt0);
        V_EXT=matmul(M1,F_EXT)
        !V_EXT(3,1)=0.0d0 
        Tq_EXT(2,1)=0.0d0!*sin(1.0d0*(t_step-1)*dt0)
        OMEGA_EXT=matmul(M2,Tq_EXT)
     endif

     if (t_step<40) then
        dt0=1.0e-4!/mu1(1)
     else
        dt0=1.0e-4!/mu1(1)
     endif

     if (t_step>t_init .and. (t_step < (t_init+5))) then
        n_iter=1
     else
        n_iter=1
     endif

     if (id .eq. 0) then
        print *,"time step is ...", t_step        
     endif


     if ((mod(t_step,1) .eq. 0) .or. (t_step .eq. 1)) then
        call polymerize()
     endif
     beta_t(:,1)=beta_t(:,2)
     beta_t(:,2)=beta_t(:,3)
     beta_t(:,3)=beta_t(:,4)        
     beta_t(:,4)=beta_n(:)
     beta_end(:)=(1.0*beta_t(:,1)+3.0*beta_t(:,2)+3.0*beta_t(:,3)+1.0*beta_t(:,4))/8.0

     do nf=1,N_f
        if (flag_BC(N0,nf) .eq. 2) then

           beta(nf)=(beta_i(nf)-beta_end(nf))*exp(-0.10d0*(t_att(nf)*1.0d0-1.0d0))+beta_end(nf)
        else
           beta(nf)=beta_end(nf);
        endif
        !beta(nf)=(2.0d0*(nf/N_f)-1.0d0)*100.0d0
        !beta(nf)=0.0d0
        L_FF(nf)=L_FF(nf)+dt0*beta(nf)
     enddo

     call cpu_time (t1)

     FLAG_T=0

     do iter0=1,n_iter

        !$OMP PARALLEL DEFAULT(SHARED) SHARED(XT) PRIVATE(nf,N,kappa,kappa_e)
        !$OMP DO
        do nf=1,N_f,1
           N=N0
           if (iter0 .eq. 1) then
              dum(1:N0,nf)=1.0d0-exp(-1.0d0*L_F(nf)*s(1:N0))
              call sparse_build_diffusion_ON(N,N0,n_s,n_half,m,&
                   & C_T0,ia_T(1:N+1,nf),ja_T(1:N*n_s,nf),a_T(1:N*n_s,nf),L_FF(nf))
              call deriv_O_N(x0(1:N,nf),N,n_s,C_T(:,:,1:N),m,x_ns0(1:m,1:N,nf),L_F(nf));
              call deriv_O_N(y0(1:N,nf),N,n_s,C_T(:,:,1:N),m,y_ns0(1:m,1:N,nf),L_F(nf));
              call deriv_O_N(z0(1:N,nf),N,n_s,C_T(:,:,1:N),m,z_ns0(1:m,1:N,nf),L_F(nf));

              dot0(nf,1:N)=(x_ns0(1,1:N,nf)*x_ns0(1,1:N,nf)+ &
                   & y_ns0(1,1:N,nf)*y_ns0(1,1:N,nf)+ &
                   & z_ns0(1,1:N,nf)*z_ns0(1,1:N,nf));

              dot1(nf,1:N)=x_ns0(2,1:N,nf)**2+y_ns0(2,1:N,nf)**2+z_ns0(2,1:N,nf)**2;

              dot2(nf,1:N)=x_ns0(2,1:N,nf)*x_ns0(4,1:N,nf)+ &
                   & y_ns0(2,1:N,nf)*y_ns0(4,1:N,nf)+ &
                   & z_ns0(2,1:N,nf)*z_ns0(4,1:N,nf);

              dot3(nf,1:N)=x_ns0(3,1:N,nf)*x_ns0(3,1:N,nf)+ &
                   & y_ns0(3,1:N,nf)*y_ns0(3,1:N,nf)+ &
                   & z_ns0(3,1:N,nf)*z_ns0(3,1:N,nf);

              call sparse_build_implicit_flag(N,N0,n_s,n_half,m,flag_BC(1:N0,nf),flag_BC_T(1:N0,nf),C_T0,&
                   & ia_XT2(1:4*N+1,nf),ja_XT2(1:16*N*n_s,nf),a_XT2(1:16*N*n_s,nf),&
                   & fac,stab0,stab,beta(nf),s(1:N0),L_FF(nf),mu0(nf),mu(nf),&
                   & a_2,x_ns0(:,1:N,nf),y_ns0(:,1:N,nf),z_ns0(:,1:N,nf),&
                   & n_0(:,:,nf),n_1(:,:,nf),n_2(:,:,nf),dt0)

              !*********************************************************************!
              !**********External force "densities" along the fiber length *********!
              !*********************************************************************!    

              fx_MTT_ext(1,1:N,nf)=0.0d0*x_ns0(1,1:N,nf)/(1.0d0);
              fy_MTT_ext(1,1:N,nf)=0.0d0*y_ns0(1,1:N,nf)/(1.0d0);
              fz_MTT_ext(1,1:N,nf)=0.0d0*z_ns0(1,1:N,nf)/(1.0d0);

              fx_MT_ext(1:N,nf)=fx_MTT_ext(1,1:N,nf);
              fy_MT_ext(1:N,nf)=fy_MTT_ext(1,1:N,nf);
              fz_MT_ext(1:N,nf)=fz_MTT_ext(1,1:N,nf);

              m0=1
              call deriv_O_N(fx_MT_ext(1:N,nf),N,n_s,C_T(:,:,1:N),m0,fx_s(1:m0,1:N,nf),L_F(nf));
              call deriv_O_N(fy_MT_ext(1:N,nf),N,n_s,C_T(:,:,1:N),m0,fy_s(1:m0,1:N,nf),L_F(nf));
              call deriv_O_N(fz_MT_ext(1:N,nf),N,n_s,C_T(:,:,1:N),m0,fz_s(1:m0,1:N,nf),L_F(nf));

              fsxs(1,1:N,nf)=0.0d0*(fx_s(1,1:N,nf)*x_ns0(1,1:N,nf)+ &
                   & fy_s(1,1:N,nf)*y_ns0(1,1:N,nf)+ &
                   & fz_s(1,1:N,nf)*z_ns0(1,1:N,nf));

              fxss(1,1:N,nf)=fx_MTT_ext(1,1:N,nf)*x_ns0(2,1:N,nf)+ &
                   & fy_MTT_ext(1,1:N,nf)*y_ns0(2,1:N,nf)+ &
                   & fz_MTT_ext(1,1:N,nf)*z_ns0(2,1:N,nf);

              fxs(1,1:N,nf)=fx_MTT_ext(1,1:N,nf)*x_ns0(1,1:N,nf)+ &
                   & fy_MTT_ext(1,1:N,nf)*y_ns0(1,1:N,nf)+ &
                   & fz_MTT_ext(1,1:N,nf)*z_ns0(1,1:N,nf);

              F_active(1,nf)=sum(fx_MT_ext(1:N,nf)*chev_w(1:N))*L_F(nf)
              F_active(2,nf)=sum(fy_MT_ext(1:N,nf)*chev_w(1:N))*L_F(nf)
              F_active(3,nf)=sum(fz_MT_ext(1:N,nf)*chev_w(1:N))*L_F(nf)
              xsus_f(1,1:N,nf)=1.0d0*(2.0d0*fsxs(1,1:N,nf)+fxss(1,1:N,nf));

           endif

           C_T=C_T0;
           call sphere_motion(N0,N,x0(:,nf),y0(:,nf),z0(:,nf),& 
                & v_ext,omega_ext,E_ext,xc0,u_ext(1:N,1:3,nf))
           call deriv_O_N(u_ext(1:N,1,nf),N,n_s,C_T(:,:,1:N),m0,ux_s0(1:m0,1:N,nf),L_FF(nf));
           call deriv_O_N(u_ext(1:N,2,nf),N,n_s,C_T(:,:,1:N),m0,uy_s0(1:m0,1:N,nf),L_FF(nf));
           call deriv_O_N(u_ext(1:N,3,nf),N,n_s,C_T(:,:,1:N),m0,uz_s0(1:m0,1:N,nf),L_FF(nf));

           xsus_2(1,1:N,nf)=ux_s0(1,1:N,nf)*x_ns0(1,1:N,nf)+&
                & uy_s0(1,1:N,nf)*y_ns0(1,1:N,nf)+uz_s0(1,1:N,nf)*z_ns0(1,1:N,nf)

           do i=1,N0
              F_XT2(4*(i-1)+1+(nf-1)*4*N)=fac*mu0(nf) &
                   & -xsus_f(1,i,nf)/2.0d0-1.0d0*xsus_2(1,i,nf)*mu(nf)/2.0d0

              F_XT2(4*(i-1)+2+(nf-1)*4*N)=mu(nf)*((a_1*xp(i,nf)+a_0*xpp(i,nf))/dt0+&
                   & 1.0d0*U_EXT(1,i,nf)+0.0d0*beta(nf)*s(i)*x_ns0(1,i,nf))+&
                   & stab*x_ns0(4,i,nf)+&
                   & fx_MTT_ext(1,i,nf)+1.0d0*fxs(1,i,nf)*x_ns0(1,i,nf);

              F_XT2(4*(i-1)+3+(nf-1)*4*N)=mu(nf)*((a_1*yp(i,nf)+a_0*ypp(i,nf))/dt0+&
                   & 1.0d0*U_EXT(2,i,nf)+0.0d0*beta(nf)*s(i)*y_ns0(1,i,nf))+&
                   & stab*y_ns0(4,i,nf)+&
                   & fy_MTT_ext(1,i,nf)+1.0d0*fxs(1,i,nf)*y_ns0(1,i,nf);

              F_XT2(4*(i-1)+4+(nf-1)*4*N)=mu(nf)*((a_1*zp(i,nf)+a_0*zpp(i,nf))/dt0+&
                   & 1.0d0*U_EXT(3,i,nf)+0.0d0*beta(nf)*s(i)*z_ns0(1,i,nf))+&
                   & stab*z_ns0(4,i,nf)+&
                   & fz_MTT_ext(1,i,nf)+1.0d0*fxs(1,i,nf)*z_ns0(1,i,nf);

           enddo

           
           F_XT2(4*(N0-1)+1+(nf-1)*4*N)=(x_ns0(1,N,nf)*F_L(1,nf)+y_ns0(1,N,nf)*F_L(2,nf)+ & 
                z_ns0(1,N,nf)*F_L(3,nf))/(sqrt(x_ns0(1,N,nf)**2+y_ns0(1,N,nf)**2+z_ns0(1,N,nf)**2))

           F_XT2(4*(1 -1)+1+(nf-1)*4*N)=-1.0d0*fxs(1,1,nf)

           X_att0(1,nf)=x0(1,nf);X_att0(2,nf)=y0(1,nf);X_att0(3,nf)=z0(1,nf);
           X_att0_2(1,nf)=x0(2,nf);X_att0_2(2,nf)=y0(2,nf);X_att0_2(3,nf)=z0(2,nf);

           dxc1p(1:3,nf)= X_att0(1:3,nf)-xc0(1:3)

           dxc2p(1:3,nf)= X_att0_2(1:3,nf)-xc0(1:3)

           F_XT2((nf-1)*4*N0+2)=(a_1*xp(1,nf)+a_0*xpp(1,nf))/dt0+dV(1,1)+&
                & dW(2,1)*dxc1p(3,nf)-dW(3,1)*dxc1p(2,nf);

           F_XT2((nf-1)*4*N0+3)=(a_1*yp(1,nf)+a_0*ypp(1,nf))/dt0+dV(2,1) &
                & -dW(1,1)*dxc1p(3,nf)+dW(3,1)*dxc1p(1,nf)

           F_XT2((nf-1)*4*N0+4)=(a_1*zp(1,nf)+a_0*zpp(1,nf))/dt0+dV(3,1)+&
                & dW(1,1)*dxc1p(2,nf)-dW(2,1)*dxc1p(1,nf);

           F_XT2((nf-1)*4*N0+4+2)=(a_1*xp(2,nf)+a_0*xpp(2,nf))/dt0+dV(1,1)+&
                & dW(2,1)*dxc2p(3,nf)-dW(3,1)*dxc2p(2,nf)!+&

           F_XT2((nf-1)*4*N0+4+3)=(a_1*yp(2,nf)+a_0*ypp(2,nf))/dt0+dV(2,1) &
                & -dW(1,1)*dxc2p(3,nf)+dW(3,1)*dxc2p(1,nf)!+&

           F_XT2((nf-1)*4*N0+4+4)=(a_1*zp(2,nf)+a_0*zpp(2,nf))/dt0+dV(3,1) &
                & +dW(1,1)*dxc2p(2,nf)-&
                & dW(2,1)*dxc2p(1,nf)!+&

           F_XT2(4*(nf-1)*N0+4*(N0-2)+2)=0.0d0
           F_XT2(4*(nf-1)*N0+4*(N0-2)+3)=0.0d0
           F_XT2(4*(nf-1)*N0+4*(N0-2)+4)=0.0d0

           if (flag_BC(N0,nf) .eq. 3) then
              F_XT2(4*(nf-1)*N0+4*(N0-1)+2)=F_L(1,nf)
              F_XT2(4*(nf-1)*N0+4*(N0-1)+3)=F_L(2,nf)
              F_XT2(4*(nf-1)*N0+4*(N0-1)+4)=F_L(3,nf)
           elseif (flag_BC(N0,nf) .eq. 2) then
              F_XT2((nf-1)*4*N0+4*(N0-1)+1)=-1.0d0*fxs(1,N0,nf)-s(N0)*beta(nf)*mu(nf)/2.0d0
              F_XT2((nf-1)*4*N0+4*(N0-1)+2)=(a_1*xp(N0,nf)+a_0*xpp(N0,nf))/dt0
              F_XT2((nf-1)*4*N0+4*(N0-1)+3)=(a_1*yp(N0,nf)+a_0*ypp(N0,nf))/dt0
              F_XT2((nf-1)*4*N0+4*(N0-1)+4)=(a_1*zp(N0,nf)+a_0*zpp(N0,nf))/dt0
           elseif (flag_BC(N0,nf) .eq. 5) then
              F_XT2((nf-1)*4*N0+4*(N0-1)+2)=(a_1*xp(N0,nf)+a_0*xpp(N0,nf))/dt0+&
                   & 0.50d0*beta(nf)*n_1(1,N0,nf)+&
                   & 1.0d0*((V_EXT(1,1)+V_SPCOR(1,1))*x_ns0(1,N0,nf)+ & 
                   (V_EXT(2,1)+V_SPCOR(2,1))*y_ns0(1,N0,nf)+(V_EXT(3,1)+ & 
                   V_SPCOR(3,1))*z_ns0(1,N0,nf))*n_1(1,N0,nf)
              F_XT2((nf-1)*4*N0+4*(N0-1)+3)=(a_1*yp(N0,nf)+ & 
                   a_0*ypp(N0,nf))/dt0+0.50d0*beta(nf)*n_1(2,N0,nf)+&
                   & 1.0d0*((V_EXT(1,1)+V_SPCOR(1,1))*x_ns0(1,N0,nf)+ & 
                   (V_EXT(2,1)+V_SPCOR(2,1))*y_ns0(1,N0,nf)+(V_EXT(3,1)+ & 
                   V_SPCOR(3,1))*z_ns0(1,N0,nf))*n_1(2,N0,nf)
              F_XT2((nf-1)*4*N0+4*(N0-1)+4)=(a_1*zp(N0,nf)+ & 
                   a_0*zpp(N0,nf))/dt0+0.50d0*beta(nf)*n_1(3,N0,nf)+&
                   & 1.0d0*((V_EXT(1,1)+V_SPCOR(1,1))*x_ns0(1,N0,nf)+ & 
                   (V_EXT(2,1)+V_SPCOR(2,1))*y_ns0(1,N0,nf)+(V_EXT(3,1)+ & 
                   V_SPCOR(3,1))*z_ns0(1,N0,nf))*n_1(3,N0,nf)

           endif

        enddo

        !$OMP END DO
        !$OMP end parallel

        call  pvfmm_mpi_gather( F_XT2,4*N0*N_f,F_XT,4*N0*N_f,0) !recv_count is per proc
        call  pvfmm_mpi_bcast(F_XT,4*N0*N_f_T,0)

        if (t_step .eq. 1) then
           do nf=1,N_f_T
              do i=1,N0
                 XT((nf-1)*4*N0+(i-1)*4+2)=x_T(i,nf)
                 XT((nf-1)*4*N0+(i-1)*4+3)=y_T(i,nf)
                 XT((nf-1)*4*N0+(i-1)*4+4)=z_T(i,nf)
                 XT((nf-1)*4*N0+(i-1)*4+1)=0.0d0
              enddo
           enddo
        endif


        iter_GMRES=0
        call pvfmm_mpi_barrier()
        call system_clock(t00)
        call implicit_GMRES()
        call system_clock(t07)
        if (id .eq. 0) then
           print *, "GMRES time", real(t07-t00)/real(rate)
           print *,"-----------------------------"
           print *,"PASSED GMRES"
           print *,"-----------------------------"        
        endif


        call A_DOT_X_implicit(XT)

        print *,"Force is ", F_MT_T+F_MT_B

        if (flag_pvfmm .eq. 1) then                 
           call pvfmm_mpi_gather(sldl_pot,3*nsrc,sldl_pot_T,nsrc*3,0)
           call pvfmm_mpi_bcast(sldl_pot_T,3*nsrc*nprocs,0)

           sldl_pot_T=2.0d0*sldl_pot_T!/(8.0d0*mu0(1)/6.0d0)

        else
           seq_pot=seq_pot*2.0d0!/(8.0d0*mu0(1)/6.0d0)
        endif

        call pvfmm_mpi_barrier()

        call pvfmm_mpi_barrier()
        print *, "REACHED pvfmm_sldl"


     enddo


     !*****************************************!
     !*********** BDF time-stepping************!


     !a_0=-0.5;a_1=2.0;a_2=1.50
     a_0=-0.0d0;a_1=1.0d0;a_2=1.0d0;
     xpp=xp;ypp=yp;zpp=zp;
     xp=x;yp=y;zp=z;
     !x0=2.0*xp-xpp;y0=2.0*yp-ypp;z0=2.0*zp-zpp;
     x0=x;y0=y;z0=z;
     Omega_Tp=Omega_T;
     Omega_T0_Tp=Omega_T0;
     Omega_T0_Bp=Omega_T0_B;
     Omega_HDp=Omega_HD;
     !***************************************!
     !***************************************! 

     L_F(1:N_f)=L_FF(1:N_f)
     xc0p=xc0
     e_SP(:)=xc0(:)


     !***************************************************************************! 
     !***************************************************************************! 

     if (mod(t_step,1) .eq. 0 ) then
        write(40,50) maxval(abs(dot0_2(:,:)))
        if (mod(t_step,10) .eq. 0 .or. t_step .eq. 1) then
           do i=1,N0*N_f_T
              if (id .eq. 0) then
                 write(20,50) XT((i-1)*4+2),XT((i-1)*4+3),XT((i-1)*4+4)
                 write(30,50) XT((i-1)*4+1)
              elseif(id .eq. 1) then
                 write(21,50) XT((i-1)*4+2),XT((i-1)*4+3),XT((i-1)*4+4)
                 write(31,50) XT((i-1)*4+1)                
              endif
           enddo
           do i=1,N0
              do nf=1,N_f
                 write(400,50)x(i,nf),y(i,nf),z(i,nf),ux2_ext(i,nf)+u_ext(1,i,nf), & 
                      uy2_ext(i,nf)+u_ext(2,i,nf),uz2_ext(i,nf)+u_ext(3,i,nf)
              enddo
           enddo
        endif
        if (id .eq. 0) then
           write(80,50) xc0(1),xc0(2),xc0(3)
           write(90,50) U_SP(1,1),U_SP(2,1),U_SP(3,1),V_SP(1),V_SP(2),V_SP(3)
           write (100,50) W_SP(1,1),W_SP(2,1),W_SP(3,1),Omega_SPCOR(1,1),Omega_SPCOR(2,1),Omega_SPCOR(3,1)
           write(110,50) F_MT_B(1,1)+F_MT_T(1,1),F_MT_B(2,1)+F_MT_T(2,1),& 
                & F_MT_B(3,1)+F_MT_T(3,1),W_SP(1,1),W_SP(2,1),W_SP(3,1)
        endif


     endif

     call pvfmm_mpi_barrier()

  enddo
  call MPI_finalize

	close(150);close(20);close(30);close(40);close(50);close(60);close(80)

3000 format(9F12.2)
30 format(10F10.3)
40 format(10F15.3)
50 format(6F25.12)
60 format(6F25.12)
70 format(6F25.12,2X,1I14)
80 format(12F20.12)
end program main
