program fmm_interface_tes5t

use, intrinsic :: iso_c_binding
use fmm_interface

implicit none

integer :: i
integer (C_SIZE_T), parameter :: DIM=3, nsrc=3, ntrg=5
integer (C_INT), parameter :: mult_order=2, max_pts=400, max_depth=20
integer (C_INT) :: size, rank
logical (C_INT), parameter :: true=1, false=0
real (C_DOUBLE), allocatable, target, dimension(:) :: src, den, den_nor, trg, pot
type (C_PTR) :: sl_context, sldl_context

allocate(src(DIM*nsrc), den(DIM*nsrc), den_nor(2*DIM*nsrc), trg(DIM*ntrg), pot(DIM*ntrg))

do i=1,DIM*nsrc
   src(i)=i/(nsrc+1)/DIM;
   den(i)=2*i
end do

do i=1,2*DIM*nsrc
   den_nor(i)=3*i
end do

do i=1,DIM*ntrg
   trg(i)=i/(ntrg+1)/DIM/2;
   pot(i)=4*i
end do

call mpi_init(size, rank)

call make_fmm_context(mult_order, max_pts, max_depth, true, false, false, sl_context) !sl, dl, periodic, context 
call make_fmm_context(mult_order, max_pts, max_depth, true, true , true , sldl_context) !sl, dl, periodic, context 

! sl
call stokes_sl_fmm(nsrc, src, den, ntrg, trg, pot, false, sl_context)
call stokes_sl_fmm(nsrc, src, den, ntrg, trg, pot, false, sl_context)

! sldl
call stokes_sldl_fmm(nsrc, src, den, nsrc, src, den_nor, ntrg, trg, pot, false, sldl_context)
call stokes_sldl_fmm(nsrc, src, den, nsrc, src, den_nor, ntrg, trg, pot, false, sldl_context)

call clear_fmm_context(sl_context);
call clear_fmm_context(sldl_context);
call mpi_finalize()

end program
