#ifndef _PVFMMINTERFACE_
#define _PVFMMINTERFACE_

#include <iostream>  //for size_t

extern "C"
{
  void pvfmm_mpi_init_(int &rank,int &size);
  void pvfmm_mpi_finalize_();

  void pvfmm_mpi_gather_(
        void* send_data, int &send_count,
        void* recv_data, int &recv_count,
        int &root);

  void pvfmm_mpi_scatter_(
      void* send_data, int &send_count,
      void* recv_data, int &recv_count,
      int &root);


  void pvfmm_mpi_reduce_(
      void* send_data, void* recv_data,
      int &send_count, int &root);


  void pvfmm_mpi_barrier_();

  void pvfmm_mpi_bcast_(
       void* send_data, 
       int &send_count,int &root);


  void pvfmmstokes_sl_setorder_(int &mult_order);
  void pvfmmstokes_sldl_setorder_(int &mult_order);

  void pvfmmstokes_sl_(
      double *src, double *den, size_t &nsrc,
      double *trg, double *pot, size_t &ntrg,
      int &max_pts, int &rebuild_tree);

  void pvfmmstokes_sldl_(
      double *sl_src, double *sl_den, size_t &sl_nsrc,
      double *dl_src, double *dl_den_nor, size_t &dl_nsrc,
      double *trg, double *pot, size_t &ntrg,
      int &max_pts, int &rebuild_tree);
}

#endif //_PVFMMINTERFACE_
