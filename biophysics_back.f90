	module biophysics
	use input_file
	use  initialize
        use derivatives
	implicit none
	contains 

	subroutine polymerize()
        implicit none

        real*8 :: alpha, fac(2*N_f+1),dr_min,r_min,dr_site,n_cont(3),dot_cont,fp,fd
        real*8 :: dr_cor(N0)
        real*8 :: r_pol,r_dep,k_cat,k_res,dot_cat
        integer :: seed(2*N_f+1),flag_att(nf)
        integer :: m00,clock
        integer, parameter :: nteta_att=15, nphi_att=7,n_att=nteta_att*nphi_att
        real*8 :: teta_att, phi_att,dz_att,dteta_att
        integer :: flag_att_2(n_att),nphi0,nteta0

        real*8, parameter :: V_g0=0.50d0, V_s0=-1.0d0 ! micron / sec
        real*8, parameter :: f_cat0=0.014d0, f_res0=0.044d0 ! frequency 1/sec
        real*8 :: V_g, V_s, f_cat, f_res
        real*8 :: p_att
!        if (t_step .le. t_init) then
         V_g=1.0d0*(V_g0/a_SP0)*tau_s
         V_s=1.0d0*(V_s0/a_SP0)*tau_s
!        endif 

        f_cat=f_cat0*tau_s*dt0
        f_res=f_res0*tau_s*dt0

        m00=2
        
        dz_att=2.0d0/(nphi_att)*1.0d0
        dteta_att=2.0d0*pi/(nteta_att*1.0d0)

        do nf=1,2*N_f+1
        call system_clock(count=clock)
        seed(nf)=(t_step-1)*N_f+2*nf+1+clock
        enddo
        call random_seed(put=seed(1:N_f+1))
        call random_number(fac)
        
        if (t_step .eq. 0) then
         flag_att(:)=0
        endif
        flag_att_2(:)=0        



!                seed(1)=3


        print *, "number of fibers is", N_f 

!        call random_seed(put=seed)
!        call ellips_min(xc0(1),xc0(2),xc0(3),a_cor-1.0d0,b_cor-1.0d0,dr_min,n_cont)
         dr_min=a_cor-abs(xc0(3))

        do nf=1,N_f


             do i=1,N0
                call ellips_min(x(i,nf),y(i,nf),z(i,nf),a_cor-0.250d0,b_cor-0.250d0,dr_cor(i),n_cont)
             enddo;

!                call sites(x_site,y_site,z_site,x(N0,nf),y(N0,nf),z(N0,nf),a_site,n_site,nf)
!                dr_site=sqrt(minval((x_site-x(N0,nf))**2+(y_site-y(N0,nf))**2+(z_site-z(N0,nf))**2));
!                print *, "distance to force generators is", dr_site

                n_0(1:3,N0,nf)=n_cont(1:3)

                if (abs(n_cont(1))<0.9) then
                 n_1(1,N0,nf)=n_cont(2)
                 n_1(2,N0,nf)=-n_cont(1)
                 n_1(3,N0,nf)=0.0d0
                 n_1(1:3,N0,nf)=n_1(1:3,N0,nf)/sqrt(sum(n_1(1:3,N0,nf)**2))
                 n_2(1,N0,nf)=n_cont(2)*n_1(3,N0,nf)-n_cont(3)*n_1(2,N0,nf)
                 n_2(1,N0,nf)=n_cont(3)*n_1(1,N0,nf)-n_cont(1)*n_1(3,N0,nf)
                 n_2(1,N0,nf)=n_cont(1)*n_1(2,N0,nf)-n_cont(2)*n_1(1,N0,nf)
                 n_2(1:3,N0,nf)=n_2(1:3,N0,nf)/sqrt(sum(n_2(1:3,N0,nf)**2))
                else
                 n_1(1,N0,nf)=-n_cont(3)
                 n_1(2,N0,nf)=0.0d0
                 n_1(3,N0,nf)=n_cont(1)
                 n_1(1:3,N0,nf)=n_1(1:3,N0,nf)/sqrt(sum(n_1(1:3,N0,nf)**2))
                 n_2(1,N0,nf)=n_cont(2)*n_1(3,N0,nf)-n_cont(3)*n_1(2,N0,nf)
                 n_2(1,N0,nf)=n_cont(3)*n_1(1,N0,nf)-n_cont(1)*n_1(3,N0,nf)
                 n_2(1,N0,nf)=n_cont(1)*n_1(2,N0,nf)-n_cont(2)*n_1(1,N0,nf) 
                 n_2(1:3,N0,nf)=n_2(1:3,N0,nf)/sqrt(sum(n_2(1:3,N0,nf)**2))
                endif
                 
                 n_1(1,N0,nf)=n_cont(2)*(x_ns0(1,N0,nf)*n_cont(2)-y_ns0(1,N0,nf)*n_cont(1))-&
                 & n_cont(3)*(-x_ns0(1,N0,nf)*n_cont(3)+z_ns0(1,N0,nf)*n_cont(1))
                 n_1(2,N0,nf)=-n_cont(1)*(x_ns0(1,N0,nf)*n_cont(2)-y_ns0(1,N0,nf)*n_cont(1))+&
                 & n_cont(3)*(y_ns0(1,N0,nf)*n_cont(3)-z_ns0(1,N0,nf)*n_cont(2))
                 n_1(3,N0,nf)=n_cont(1)*(-x_ns0(1,N0,nf)*n_cont(3)+z_ns0(1,N0,nf)*n_cont(1))-&
                 & n_cont(2)*(y_ns0(1,N0,nf)*n_cont(3)-z_ns0(1,N0,nf)*n_cont(2))


                if (n_1(1,N0,nf)*x_ns0(1,N0,nf)+n_1(2,N0,nf)*y_ns0(1,N0,nf)+n_1(3,N0,nf)*z_ns0(1,N0,nf)<0) then
                  n_1(1:3,N0,nf)=-n_1(1:3,N0,nf)/sqrt(sum(n_1(1:3,N0,nf)**2))
                endif 

                if (z(N0,nf)<0) then
                 p_att=fac(nf+1)*(0.5+1.5d0*abs(z(N0,nf)/(a_cor-1.0d0)))
                else
                 p_att=fac(nf+1)
                endif 
!                 p_att= fac(nf+1)*max(0.0d0,(dr_min-1.5d0)/(a_cor-1.5d0))
!                else
!                 p_att=fac(nf+1)
!                endif 


              
                r_min=dr_cor(N0)
                
               if (flag_BC(N0,nf) .eq. 3) then
!                if (a_site(nf) .ne. 0) then
!                 print *, "A_SITE is ....", a_site(nf),nf
!                endif 
                if (r_min .le. 0.5d0) then
                
                  nphi0=floor((z(N0,nf)/(a_cor-1.0d0)+1.0d0)/dz_att)+1
                  nteta0=floor(atan2(y(N0,nf),x(N0,nf))/dteta_att)+1
                  if (nteta0<0) then
                        nteta0=nteta0+floor(2.0d0*pi/dteta_att)
                  endif       
                  if (nphi0>nphi_att) then
                      nphi0=nphi_att
                      print *, "somthing is wrong"
                  elseif (nphi0<1) then
                       nphi0=1
                       print *, "again something is wrong"
                  endif 

                  if (flag_att_2(nphi0*nteta_att+nteta0) .eq. 0 .and. p_att>0.25d0) then
                     flag_att_2(nphi0*nteta_att+nteta0)=1
                     flag_att(nf)=1
                  endif 
                  if (flag_att(nf) .eq. 1) then                              
                   if (flagp_p(nf) .ne. 1 .or. L_F(nf)<0.60d0 ) then
                     flag_att(nf)=0
                   endif 
                  endif 
                 if (flag_att(nf) .eq. 1) then      
                   F_L(1,nf)=0.0d0*mu1(nf)*x_ns0(1,N0,nf);F_L(2,nf)=0.0d0*mu1(nf)*y_ns0(1,N0,nf);F_L(3,nf)=0.0d0*mu1(nf)*z_ns0(1,N0,nf)
                   print *, "PULLING FORCE ....",sum(abs(F_L(1:3,nf))),nphi0*nteta_att+nteta0
                 elseif (t_step>t_init) then
                  fp=0.0d0*min(pi**2/L_F(nf)**2,13.0d0*mu1(1))
!                  fd=fp;  
                  fd=0.0d0             
                  dot_cont=abs(n_cont(1)*x_ns0(1,N0,nf)+n_cont(2)*y_ns0(1,N0,nf)+n_cont(3)*z_ns0(1,N0,nf))
                  F_L(1:3,nf)=-fp*n_cont(1:3)*exp(-(r_min)*5.0d0)*dot_cont-fd*exp(-(r_min)*5.0d0)*n_1(1:3,N0,nf)*(1.0d0-dot_cont); 
                  print *, "PUSHING FORCE ....", sum(abs(F_L(1:3,nf))),nf
                 else  
                  F_L(1:3,nf)=0.0d0
                 endif                   
                
!                endif 
               else
                F_L(1:3,nf)=0.0d0
               endif

             else 
                F_L(1:3,nf)=0.0d0               
             endif 


            dot_cat=abs(F_L(1,nf)*x_ns0(1,N0,nf)+F_L(2,nf)*y_ns0(1,N0,nf)+F_L(3,nf)*z_ns0(1,N0,nf))/mu0(nf)
            k_cat=f_cat!*exp(dot_cat/(3.0*mu1(1)))
            k_res=f_res
            r_pol=V_g*exp(-dot_cat/(3.0*mu1(1)));r_dep=V_s
            k_cat=f_cat
            if (flag_att(nf) .eq. 1) then
             r_pol=V_g*(1.0d0-2.0d0*exp(-8.0*r_min));
             k_cat=0.010d0*dt0*tau_s
            endif
            if (r_min .le. 0.25 .and. flag_att(nf) .ne. 1) then
                k_cat=0.10d0*dt0*tau_s
            endif 
                


           if (flagp_p(nf) .eq.  1) then 
                
                if (fac(nf+1) <k_cat) then
                flag_p(nf)=-1
                beta_n(nf)=r_dep
                else
                flag_p(nf)=1
                beta_n(nf)=r_pol
                endif

            elseif (flagp_p(nf) .eq. (-1) ) then

                if (fac(nf+1) <k_res) then
                 flag_p(nf)=1
                 beta_n(nf)=r_pol
                else
                 flag_p(nf)=-1
                 beta_n(nf)=r_dep
                endif 
           
           endif

                    
           
        
           if (r_min <0.03d0  .and. (minval(dr_cor)-r_min < -0.05)) then
                flag_p(nf)=-1
                beta_n(nf)=r_dep
                flag_BC(N0,nf)=3
           endif
           if (minval(dr_cor) .le. 0.04d0) then
               flag_p(nf)=-1
               beta_n(nf)=r_dep
               flag_BC(N0,nf)=3
               flag_att(nf)=0
           endif

           if (flag_BC(N0,nf) .eq. 2 ) then
                beta_n(nf)=r_pol/1.0d0 
           endif                  

           if ((L_F(nf) .le. 0.40))  then
               flag_p(nf)=1
               beta_n(nf)=r_pol
           endif

           
           flagp_p(nf)=flag_p(nf);

        enddo
        

        end subroutine polymerize

!************************************************************************!
!************************************************************************!
!************************************************************************!

        subroutine ellips_min(x_end,y_end,z_end,a_cor,b_cor,r_min,n_cor)

        implicit none

        real*8,intent(IN) :: x_end, y_end, z_end
        real*8, intent (in)  :: a_cor,b_cor
        real*8 :: r_min,n_cor(:)
        real*8 :: r_end,teta_end,f_end,fp_end,teta_2
        integer :: i

        r_end=sqrt(x_end**2+y_end**2);
        teta_end=atan2(a_cor*r_end,b_cor*z_end);

        do i=1,4;
          f_end=(a_cor**2-b_cor**2)*sin(2.0*teta_end)/2.0-z_end*a_cor*sin(teta_end)+r_end*b_cor*cos(teta_end);
          fp_end=(a_cor**2-b_cor**2)*cos(2.0*teta_end)-z_end*a_cor*cos(teta_end)-r_end*b_cor*sin(teta_end);    
          teta_end=teta_end-f_end/fp_end;    
        enddo;
              
        r_min=sqrt((r_end-b_cor*sin(teta_end))**2+(z_end-a_cor*cos(teta_end))**2)
        teta_2=atan2(y_end,x_end)
        n_cor(1)=cos(teta_2)*sin(teta_end);
        n_cor(2)=sin(teta_2)*sin(teta_end);
        n_cor(3)=cos(teta_end);

        end subroutine ellips_min
!*******************************************************************!
        subroutine sites(x,y,z,x0,y0,z0,a,n,nf)

        integer, intent(in) :: nf,n
        integer :: a(:)
        real*8, intent (in), dimension (:) :: x,y,z
        real*8, intent (in) :: x0,y0,z0
        real*8 :: r
        integer :: dum
        dum=0
         do i=1,n
             r=sqrt((x(i)-x0)**2+(y(i)-y0)**2+(z(i)-z0)**2)
             if (r .le. 0.50d0 .and. dum .eq. 0) then
                do j=1,nf
                if (j .ne. nf) then
                if (a(j) .eq. i) then
                   a(nf)=0
                else
                   a(nf)=i; dum=1               
                endif 
                endif 
                enddo                           
             endif
          enddo
        
          if (dum .eq. 0) then
                a(nf)=0
          endif
           
          end subroutine sites
        
!*******************************************************************!
!*******************************************************************!
!*******************************************************************!

        end module biophysics 
