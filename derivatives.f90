module derivatives
  use linear_algebra
  implicit none 

contains
  !******************************************************************
  !*****************************************************************!
  !*****************************************************************!
  !-This subroutine formes the stencil weights for all the fibers based
  ! on their length and position using subroutine weights1 
  !*****************************************************************!
  subroutine weights2(N0,n_s,n_half,m,s,C_T0)
    implicit none

    INTEGER, PARAMETER :: dp = KIND(1.0D0)
    integer :: i,j,k,n_half,n_s,m,N0
    real (kind=dp)  :: s(:),C_T0(:,:,:)
    real (kind=dp)  :: c(n_s,m)

    do i=1,N0;
       !   print *,i

       if ((i> n_half)  .and. (i<N0-n_half)) then
          call weights1(s(i),s(i-n_half:i+n_half),n_s,m,c);
       elseif (i< n_half+1) then
          call weights1(s(i),s(1:n_s),n_s,m,c);
       else
          call weights1(s(i),s(N0-n_s+1:N0),n_s,m,c);    
       endif;

       do j=1,n_s
          do k=1,m
             C_T0(k,j,i)=c(j,k);
          enddo
       enddo

    enddo;

  end subroutine weights2

  !******************************************************************
  !*****************************************************************!
  !*****************************************************************!
  !*******************************************************************
  !-This subroutine produces "n_s" stencil weights for "m" derivative 
  ! of point xi on the grids of x
  !*****************************************************************!

  subroutine weights1(xi,x,n_s,m,c)

    implicit none 
    INTEGER, PARAMETER :: dp = KIND(1.0D0)
    integer :: m,n_s,i,j,k,mn
    real (kind=dp) :: c(:,:),c1,c2,c3,c4,c5
    real (kind=dp) :: xi,x(:)

    c(:,:)=0.0; c(1,1)=1.0;
    c1=1.0
    c4=x(1)-xi

    do i=2,n_s
       mn=min(i,m)
       c2=1.0
       c5=c4
       c4=x(i)-xi

       do j=1,i-1;
          c3=x(i)-x(j);
          c2=c3*c2;
          do k=mn,2,-1;
             c(i,k)=c1*((k-1)*c(i-1,k-1)-c5*c(i-1,k))/c2;
          enddo;
          c(i,1)=-c1*c5*c(i-1,1)/c2;
          do k=mn,2,-1;
             c(j,k)=(c4*c(j,k)-(k-1)*c(j,k-1))/c3;
          enddo;
          c(j,1)=c4*c(j,1)/c3;
       enddo;
       c1=c2;
    enddo;

  end subroutine weights1


  !*************************************************************
  !*************************************************************
  !*************************************************************
  !*************************************************************
  ! This subroutine computes the numerical derivatives of "N" points"
  ! on "x" up to order m based on the "n_s" stencil points obtained from 
  !subroutines weights1 and weights 2 and stores it in x_ns 
  !which has the size of m*N. Note that x_ns(1,:) contains the first derivative
  ! (f') results but the weights points include the trivial zeroth derivative too.
  ! and has the size (m+1)*n_s*N. Notice c is always taken from the second row 

  subroutine deriv_O_N(x,N,n_s,C_T,m,xx_ns,lf)
    implicit none
    INTEGER, PARAMETER :: dp = KIND(1.0D0)
    integer, intent (IN) :: m,N,n_s
    integer :: n_half,i
    real (kind=dp), intent (IN) :: x(N),C_T(:,:,:)
    real (kind=dp), intent (OUT) :: xx_ns(:,:)
    real (kind=dp) :: c(m,n_s),lf
    n_half=nint((n_s*1.0-1.0)/2.0);

    do i=1,n_half;

       ! print *, " i is ...",  i,N
       c=C_T(2:m+1,1:n_s,i);    
       xx_ns(1:m,i)=matmul(c(1:m,1:n_s),x(1:n_s));  
    enddo;

    do i=n_half+1,N-n_half;  
       ! print *, " i is ...",  i,N  
       c=C_T(2:m+1,1:n_s,i);
       xx_ns(1:m,i)=matmul(c(1:m,1:n_s),x(i-n_half:i+n_half));    
    enddo;

    do i=N-n_half+1,N;
       ! print *, " i is ...",  i,N
       c=C_T(2:m+1,1:n_s,i);
       xx_ns(1:m,i)=matmul(c(1:m,1:n_s),x(N-n_s+1:N));   
    enddo;

    !        xx_ns(1:m,2)=2.0*xx_ns(1:m,3)-xx_ns(1:m,4);

    !********* Equally spaced nodes *********************!
    !        xx_ns(2:m,1)=2.0*xx_ns(2:m,2)-xx_ns(2:m,3);
    !!        xx_ns(2:m,N-1)=2.0*xx_ns(2:m,N-2)-xx_ns(2:m,N-3)
    !        xx_ns(2:m,N)=2.0*xx_ns(2:m,N-1)-xx_ns(2:m,N-2)
    !!  xx_ns(1:m,2)=xx_ns(1:m,3);
    !!  xx_ns(1:m,1)=xx_ns(1:m,2);

    !*****************************************************!
    do i=1,m
       xx_ns(i,1:N)=xx_ns(i,1:N)/lf**i
    enddo

  end subroutine deriv_O_N

  !******************************************************************!
  !******************************************************************!
  !******************************************************************!
  ! This subroutine computes the derivatives of the end point c=c_T(2:m+1,end,nf)

  subroutine deriv_O_N_1point(x,n_s,c,m, x_ns)
    implicit none
    INTEGER, PARAMETER :: dp = KIND(1.0D0)
    integer :: m, n_s  
    real (kind=dp)  :: x(:), c(:,:), x_ns(:)

    x_ns(:)=matmul(c(1:m,1:n_s), x(1:n_s))

  end subroutine deriv_O_N_1point



  !******************************************************************!
  !******************************************************************!
  !******************************************************************!
  ! This subroutine computes the third derivative of x ($x_{sss}$)
  ! and fixes the possible errors that may occur for cases of 
  ! very straight fibers to accurately impose the boundary conditiosn

  subroutine BC1(kappa, xns_1, yns_1,zns_1, xb, yb, zb, n_s, m, c, s, BC_T)
    implicit none 

    INTEGER, PARAMETER :: dp = KIND(1.0D0)
    integer, intent (IN) :: n_s
    real (kind=dp), intent (IN) :: kappa
    real (kind=dp) :: xns_1(:), yns_1(:), zns_1(:)
    real (kind=dp), intent (IN) :: xb(:), yb(:), zb(:)
    real (kind=dp), intent (IN) :: c(:,:),s(:)
    real (kind=dp) ::  dd, BC_T, dd2
    real (kind=dp) :: e1(3), e2(3), e3(3), ROT(3,3), IROT(3,3) 
    real (kind=dp) :: dumi(3,1), dumi_2(3,1)
    real (kind=dp), dimension (n_s) :: x_prime, y_prime, z_prime
    integer :: info, i, m
    real (kind=dp) :: x_s, x_ss, x_sss
    real (kind=dp) :: x2ns_1(m), y2ns_1(m), z2ns_1(m)




    if (kappa>1e-5) then;


       dd=sqrt(xns_1(1)**2+yns_1(1)**2+zns_1(1)**2);
       e1(1)=xns_1(1)/dd;
       e1(2)=yns_1(1)/dd;
       e1(3)=zns_1(1)/dd;


       if ((abs(e1(1))> abs(e1(2))) .and. (abs(e1(1))> abs(e1(3)))) then

          if (max(abs(yns_1(2)),abs(zns_1(2)))>1e-6) then
             e2(2)=yns_1(2);
             e2(3)=zns_1(2);
             e2(1)=-(e1(2)*e2(2)+e1(3)*e2(3))/e1(1);
             e2=e2/sqrt(e2(1)**2+e2(2)**2+e2(3)**2);
          else 
             e2(1)=0.0;e2(2)=1.0;e2(3)=0.0;
          endif;

       elseif ((abs(e1(2))> abs(e1(1))) .and. (abs(e1(2))> abs(e1(3)))) then
          if (max(abs(xns_1(2)),abs(zns_1(2)))>1e-6) then
             e2(1)=xns_1(2);
             e2(3)=zns_1(2);
             e2(2)=-(e1(1)*e2(1)+e1(3)*e2(3))/e1(2);
             e2=e2/sqrt(e2(1)**2+e2(2)**2+e2(3)**2); 

          else
             e2(1)=0.0; e2(2)=1.0; e2(3)=0.0;
          endif;

       else
          if (max(abs(yns_1(2)),abs(xns_1(2)))>1e-6) then

             e2(2)=yns_1(2);
             e2(1)=xns_1(2);
             e2(3)=-(e1(2)*e2(2)+e1(1)*e2(1))/e1(3);
             e2=e2/sqrt(e2(1)**2+e2(2)**2+e2(3)**2); 
          else
             e2(1)=0.0;e2(1)=1.0;e2(3)=0.0; 
          endif;
       endif;    


       call cross(e1,e2,e3)
       !  e3=cross(e1,e2);
       ROT(1:3,1)=e1(1:3)
       ROT(1:3,2)=e2(1:3)
       ROT(1:3,3)=e3(1:3)

       !        M=[e1 e2 e3];
       call inv(3,ROT,IROT,info)
       ! print *,"!!!!!!!!!!!!!!!!!!!!!!1"
       ! print *, "inside BCT subroutine"
       ! print *, matmul(ROT,IROT)
       ! print *,"!!!!!!!!!!!!!!!!!!!!!!1"

       !        M_in=inv(M);
       do i=1,n_s;

          dumi_2(1,1)=xb(i);
          dumi_2(2,1)=yb(i);
          dumi_2(3,1)=zb(i)
          dumi=matmul(IROT,dumi_2)

          !        dumi=M_in*[x(qq,nf);y(qq,nf);z(qq,nf)];


          x_prime(i)=dumi(1,1);
          y_prime(i)=dumi(2,1);
          z_prime(i)=dumi(3,1);

       enddo;

       call deriv_O_N_1point(x_prime,n_s,c,m, x2ns_1(1:m))
       call deriv_O_N_1point(y_prime,n_s,c,m, y2ns_1(1:m))
       call deriv_O_N_1point(z_prime,n_s,c,m, z2ns_1(1:m))

       ! print *, "++++++++++++++++++++++++++"
       ! print *, y_prime(1:n_s)
       ! print *, "--------------------------"
       ! print *, z_prime(1:n_s)
       ! print *, "++++++++++++++++++++++++++"

       ! print *, "##########################"
       ! print *, y2ns_1(1:m)
       ! print *, z2ns_1(1:m)
       ! print *, "##########################"


       !        y2_ns0=deriv_O_N_(s(1:n_s),y_prime,n_s,C_T(:,1:n_s,:),m);
       !       z2ns_1=deriv_O_N(s(1:n_s),z_prime,n_s,C_T(:,1:n_s,:),m);


       dd2=sqrt(x2ns_1(1)**2+y2ns_1(1)**2+z2ns_1(1)**2)


       y2ns_1(1)=y2ns_1(1)/dd2
       z2ns_1(1)=z2ns_1(1)/dd2

       x_s=sqrt(1.0-(y2ns_1(1)**2+z2ns_1(1)**2));
       x_ss=(1.0/x_s)*(-y2ns_1(1)*y2ns_1(2)-z2ns_1(1)*z2ns_1(2));
       x_sss=(1.0/x_s)*(-y2ns_1(3)*y2ns_1(1)-y2ns_1(2)**2 - &
            z2ns_1(3)*z2ns_1(1)-z2ns_1(2)**2-x_ss**2);

       ! print *, "************************"
       ! print *, x_s, x_ss, x_sss
       ! print *, "************************"


       BC_T=-3.0*(x_ss*x_sss+y2ns_1(2)*y2ns_1(3)+z2ns_1(2)*z2ns_1(3));

       !        dumi_2=M*[x_ss;y2ns_1(2,1);z2ns_1(2,1)];
       dumi_2(1,1)=x_ss;
       dumi_2(2,1)=y2ns_1(2);
       dumi_2(3,1)=z2ns_1(2);
       dumi =matmul( ROT, dumi_2)

       xns_1(2)=dumi(1,1);
       yns_1(2)=dumi(2,1);
       zns_1(2)=dumi(3,1);

       dumi_2(1,1)=x_sss;
       dumi_2(2,1)=y2ns_1(3);
       dumi_2(3,1)=z2ns_1(3);

       !!        dumi=M*[x_sss;y2ns_1(3,1);z2ns_1(3,1)];
       dumi =matmul( ROT, dumi_2)
       xns_1(3)=dumi(1,1);
       yns_1(3)=dumi(2,1);
       zns_1(3)=dumi(3,1);

    else

       BC_T=0.0;

    endif;

  end subroutine BC1



  !******************************************************************!
  !******************************************************************!
  !******************************************************************!
  ! This subroutine computes the third derivative of x (up to $x_{sss}$)
  ! and fixes the possible errors that may occur for cases of 
  ! very straight fibers to accurately impose the boundary conditions at the end of fiber 

  subroutine BC2(kappa, xns_1, yns_1,zns_1, xb, yb, zb, n_s, m, c)
    implicit none 

    INTEGER, PARAMETER :: dp = KIND(1.0D0)
    integer, intent (IN) :: n_s
    real (kind=dp), intent (IN) :: kappa
    real (kind=dp) :: xns_1(:), yns_1(:), zns_1(:)
    real (kind=dp), intent (IN) :: xb(:), yb(:), zb(:)
    real (kind=dp), intent (IN) :: c(:,:)
    real (kind=dp) ::  dd, dd2
    real (kind=dp) :: e1(3), e2(3), e3(3), ROT(3,3), IROT(3,3) 
    real (kind=dp) :: dumi(3,1), dumi_2(3,1)
    real (kind=dp), dimension (n_s) :: x_prime, y_prime, z_prime
    integer :: info, i, m
    real (kind=dp) :: x_s, x_ss, x_sss
    real (kind=dp) :: x2ns_1(m), y2ns_1(m), z2ns_1(m)




    if (kappa>1.0e-12) then;


       dd=sqrt(xns_1(1)**2+yns_1(1)**2+zns_1(1)**2);
       e1(1)=xns_1(1)/dd;
       e1(2)=yns_1(1)/dd;
       e1(3)=zns_1(1)/dd;


       if ((abs(e1(1))> abs(e1(2))) .and. (abs(e1(1))> abs(e1(3)))) then

          if (max(abs(yns_1(2)),abs(zns_1(2)))>1e-6) then
             e2(2)=yns_1(2);
             e2(3)=zns_1(2);
             e2(1)=-(e1(2)*e2(2)+e1(3)*e2(3))/e1(1);
             e2=e2/sqrt(e2(1)**2+e2(2)**2+e2(3)**2);
          else 
             e2(1)=0.0;e2(2)=1.0;e2(3)=0.0;
          endif;

       elseif ((abs(e1(2))> abs(e1(1))) .and. (abs(e1(2))> abs(e1(3)))) then
          if (max(abs(xns_1(2)),abs(zns_1(2)))>1e-6) then
             e2(1)=xns_1(2);
             e2(3)=zns_1(2);
             e2(2)=-(e1(1)*e2(1)+e1(3)*e2(3))/e1(2);
             e2=e2/sqrt(e2(1)**2+e2(2)**2+e2(3)**2); 

          else
             e2(1)=0.0; e2(2)=1.0; e2(3)=0.0;
          endif;

       else
          if (max(abs(yns_1(2)),abs(xns_1(2)))>1e-6) then

             e2(2)=yns_1(2);
             e2(1)=xns_1(2);
             e2(3)=-(e1(2)*e2(2)+e1(1)*e2(1))/e1(3);
             e2=e2/sqrt(e2(1)**2+e2(2)**2+e2(3)**2); 
          else
             e2(1)=0.0;e2(1)=1.0;e2(3)=0.0; 
          endif;
       endif;    


       call cross(e1,e2,e3)

       ROT(1:3,1)=e1(1:3)
       ROT(1:3,2)=e2(1:3)
       ROT(1:3,3)=e3(1:3)

       call inv(3,ROT,IROT,info)

       do i=1,n_s;

          dumi_2(1,1)=xb(i);
          dumi_2(2,1)=yb(i);
          dumi_2(3,1)=zb(i)
          dumi=matmul(IROT,dumi_2)


          x_prime(i)=dumi(1,1);
          y_prime(i)=dumi(2,1);
          z_prime(i)=dumi(3,1);

       enddo;

       call deriv_O_N_1point(x_prime,n_s,c,m, x2ns_1(1:m))
       call deriv_O_N_1point(y_prime,n_s,c,m, y2ns_1(1:m))
       call deriv_O_N_1point(z_prime,n_s,c,m, z2ns_1(1:m))


       dd2=sqrt(x2ns_1(1)**2+y2ns_1(1)**2+z2ns_1(1)**2)

       !        y2ns_1(1)=y2ns_1(1)/dd2
       !        z2ns_1(1)=z2ns_1(1)/dd2

       x_s=sqrt(1.0-(y2ns_1(1)**2+z2ns_1(1)**2));
       x_ss=(1.0/x_s)*(-y2ns_1(1)*y2ns_1(2)-z2ns_1(1)*z2ns_1(2));
       x_sss=(1.0/x_s)*(-y2ns_1(3)*y2ns_1(1)-y2ns_1(2)**2 - &
            z2ns_1(3)*z2ns_1(1)-z2ns_1(2)**2-x_ss**2);

       ! dumi_2(1,1)=x_s;
       ! dumi_2(2,1)=y2ns_1(1);
       ! dumi_2(3,1)=z2ns_1(1);
       ! dumi =matmul( ROT, dumi_2)

       !        xns_1(1)=dumi(1,1);
       !        yns_1(1)=dumi(2,1);
       !        zns_1(1)=dumi(3,1);


       dumi_2(1,1)=x_ss;
       dumi_2(2,1)=y2ns_1(2);
       dumi_2(3,1)=z2ns_1(2);
       dumi =matmul( ROT, dumi_2)

       xns_1(2)=dumi(1,1);
       yns_1(2)=dumi(2,1);
       zns_1(2)=dumi(3,1);

       dumi_2(1,1)=x_sss;
       dumi_2(2,1)=y2ns_1(3);
       dumi_2(3,1)=z2ns_1(3);

       !!        dumi=M*[x_sss;y2ns_1(3,1);z2ns_1(3,1)];
       dumi =matmul( ROT, dumi_2)
       xns_1(3)=dumi(1,1);
       yns_1(3)=dumi(2,1);
       zns_1(3)=dumi(3,1);

    endif;

  end subroutine BC2

  !********************************************************!
  !********************************************************!
  !********************************************************!

  !********************************************************!
  !********************************************************!
  !********************************************************!
  subroutine  swap(f_t0,f,n_t,N)
    implicit none
    INTEGER, PARAMETER :: dp = KIND(1.0D0)
    integer :: n_t, N
    integer :: i
    real (kind=dp) :: f_t0(:), f(:,:)



    do i=2,n_t; 
       f(i-1,1:N)=f(i,1:N);
    enddo;

    f(n_t,1:N)=f_t0(1:N);

  end subroutine swap

  !********************************************************!
  !********************************************************!
  !********************************************************!

  subroutine extrap2(f,n_t,c_1,N,f_t)
    implicit none
    INTEGER, PARAMETER :: dp = KIND(1.0D0)
    integer, intent (IN) :: n_t,N
    integer :: i
    real (kind=dp), intent (IN)  :: c_1(:), f(:,:)
    real (kind=dp), intent (OUT) :: f_t(N)

    f_t(1:N)=0.0;

    do i=1,n_t;
       f_t(1:N)=f_t(1:N)+f(i,1:N)*c_1(i);
    enddo;

  end subroutine extrap2

  !********************************************************!
  !********************************************************!
  !********************************************************!

end module derivatives

