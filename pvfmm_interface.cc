#include "pvfmm_interface.h"
#include "pvfmm.hpp"
#include "pvfmm_stks_kernel.tpp"

#include <cassert>
#include <iostream>
#include <mpi.h>
#include <omp.h>
#include <vector>

#define COUT(str) (std::cout<<str<<std::endl)
#define CERR(str,action) (std::cerr<<"[ERROR]["<< __FUNCTION__ <<"] "<<str<<std::endl, action)
#define ASSERT(expr, msg) ( (expr) ? assert(true) : CERR(msg,abort()))
#define COUTDEBUG(str) (std::cout<<"[DEBUG] "<<str<<std::endl)

typedef std::vector<real_t> vec;

extern pvfmm::PeriodicType pvfmm::periodicType;

//PeriodicType periodicType = PeriodicType::NONE;

class PVFMMContext{
  public:
  typedef pvfmm::FMM_Node<pvfmm::MPI_Node<real_t> > Node_t;
  typedef pvfmm::FMM_Pts<Node_t> Mat_t;
  typedef pvfmm::FMM_Tree<Mat_t> Tree_t;

  PVFMMContext() :
    tree(NULL),
    mat(NULL)
  {}

  ~PVFMMContext(){
    COUTDEBUG("destructing context object");
    delete this->tree;
    delete this->mat;
    COUTDEBUG("deleted all pointers");
  };

  int mult_order;
  int max_pts;
  int max_depth;
  int sl;
  int dl;
  int periodic;
  MPI_Comm comm;

  pvfmm::BoundaryType boundary;
  const pvfmm::Kernel<real_t>* ker;

  Tree_t* tree;
  Mat_t* mat;
};

void mpi_init_pvfmm(int &rank, int &size){
  int argc(0);
  char **argv;
  MPI_Init(&argc, &argv);
  MPI_Comm comm=MPI_COMM_WORLD;

  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &size);
  COUTDEBUG("mpi_init (rank/size="<<rank<<"/"<<size<<")");
}

void mpi_finalize_pvfmm(){
  int rank,size;
  MPI_Comm comm=MPI_COMM_WORLD;
  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &size);
  COUTDEBUG("finalizing MPI (rank/size="<<rank<<"/"<<size<<")");
  MPI_Finalize();
}

void pvfmm_mpi_gather_(
    void* send_data, int &send_count,
    void* recv_data, int &recv_count,
    int &root){

    MPI_Comm comm=MPI_COMM_WORLD;
    std::cout<<MPI_DOUBLE<<"\t"<<MPI_INT<<"\t"<<root<<send_count<<"\t"<<recv_count<<std::endl;
    MPI_Gather(
        send_data, send_count, MPI_DOUBLE,
        recv_data, recv_count, MPI_DOUBLE, root, comm);
}

void pvfmm_mpi_scatter_(
    void* send_data, int &send_count,
    void* recv_data, int &recv_count,
    int &root){

    MPI_Comm comm=MPI_COMM_WORLD;
    MPI_Scatter(
        send_data, send_count, MPI_DOUBLE,
        recv_data, recv_count, MPI_DOUBLE, root, comm);
}

void pvfmm_mpi_reduce_(
      void* send_data, void* recv_data,
      int &send_count, int &root){
      MPI_Comm comm=MPI_COMM_WORLD;
      MPI_Reduce(send_data,recv_data,send_count,MPI_DOUBLE,
      MPI_SUM,root,comm);
}

void pvfmm_mpi_barrier_(){
        MPI_Comm comm=MPI_COMM_WORLD;
        MPI_Barrier(comm);
}

void pvfmm_mpi_bcast_(
      void* send_data, int &send_count,int &root){
      MPI_Comm comm=MPI_COMM_WORLD;
      MPI_Bcast(send_data,send_count, MPI_DOUBLE,root,comm);
}

void make_pvfmm_context(const int &mult_order, const int &max_pts,
			const int &max_depth, const int &sl,  const int &dl,
			const int &periodic, void **context)
{
  COUTDEBUG("Initializing a pvfmm context object"
	    <<" mult_order="<<mult_order
	    <<", max_pts="<<max_pts
	    <<", max_depth="<<max_depth
	    <<", sl="<<sl
	    <<", dl="<<dl
	    <<", periodic="<<periodic
	    );

  pvfmm::BoundaryType bndry(periodic ? pvfmm::Periodic : pvfmm::FreeSpace);

  // Create new context.
  PVFMMContext *ctx = new PVFMMContext;

  ctx->mult_order = mult_order;
  ctx->max_pts	  = max_pts;
  ctx->max_depth  = max_depth;
  ctx->sl	  = sl;
  ctx->dl	  = dl;
  ctx->periodic	  = periodic;
  ctx->comm	  = MPI_COMM_WORLD;

  ctx->boundary   = bndry;
  ctx->ker        = dl ? &ker_stokes_dl : &ker_stokes_sl;
  ctx->mat        = new PVFMMContext::Mat_t;

  ctx->mat->Initialize(ctx->mult_order, ctx->comm, ctx->ker);

  *context = (void*) ctx;
}

void clear_pvfmm_context(void **context){

  if(*context == NULL) return;
  COUTDEBUG("deleting context");
  PVFMMContext *ctx = (PVFMMContext*) *context;
  delete ctx;
  *context = NULL;
}

void stokes_sl_pvfmm(
    const size_t &nsrc, const real_t *src, const real_t *den,
    const size_t &ntrg, const real_t *trg, real_t *pot,
    const int &rebuild_tree, void **context)
{
  ASSERT(*context != NULL, "was called with null context. "
	 "Generate a context by calling make_pvfmm_context.");

  // context object
  PVFMMContext *ctx = (PVFMMContext*) *context;
  COUTDEBUG("pvfmm_stokes_sl with nsrc="<<nsrc<<", ntrg="<<ntrg
	    <<", rebuild_tree="<<rebuild_tree);

  ASSERT(ctx->sl, "context doesn't support single layer");
  ASSERT(!ctx->dl, "context is build for combined sl and dl kernel");

  COUTDEBUG("Using context with"
	    <<" mult_order="<<ctx->mult_order
	    <<", max_pts="<<ctx->max_pts
	    <<", max_depth="<<ctx->max_depth
	    <<", sl="<<ctx->sl
	    <<", dl="<<ctx->dl
	    <<", periodic="<<ctx->periodic
	    );

  // copy data
  vec srcv(src,src+DIM*nsrc);
  vec denv(den,den+DIM*nsrc);
  vec trgv(trg,trg+DIM*ntrg);
  vec potv(DIM*ntrg);

  for(size_t i=0;i<DIM*nsrc;++i)
    if (isnan(srcv[i]))
      CERR("src is NaN",abort());

  for(size_t i=0;i<DIM*nsrc;++i)
    if (isnan(denv[i]))
      CERR("den is NaN",abort());

  for(size_t i=0;i<DIM*ntrg;++i)
    if (isnan(trgv[i]))
      CERR("trg is NaN",abort());


  // rebuild tree
  if(rebuild_tree || ctx->tree == NULL){
    COUTDEBUG("(Re)building pvfmm tree");
    ASSERT(ctx->mat!=NULL, "badly initialized context");

    // FMM Setup
    delete ctx->tree;
    ctx->tree = PtFMM_CreateTree(srcv, denv, trgv, ctx->comm, ctx->max_pts, ctx->boundary);
    ctx->tree->SetupFMM(ctx->mat);

    // Run FMM
    COUT("Evaluating point FMM");
    PtFMM_Evaluate(ctx->tree,potv,ntrg);
  } else {
    // only Run FMM
    ctx->tree->ClearFMMData();
    COUT("Evaluating point FMM");
    PtFMM_Evaluate(ctx->tree,potv,ntrg,&denv);
  }

  // copy to pot
  for(size_t i=0;i<DIM*ntrg;++i){
    pot[i]=potv[i];
    if (isnan(pot[i])){
      CERR("fmm potential is NaN",abort());
    }
  }
}

void stokes_sldl_pvfmm(
		       const size_t &sl_nsrc, const real_t *sl_src, const real_t *sl_den,
		       const size_t &dl_nsrc, const real_t *dl_src, const real_t *dl_den_nor,
		       const size_t &ntrg, const real_t *trg, real_t *pot,
		       const int &rebuild_tree, void **context)
{
  ASSERT(*context != NULL, "was called with null context. "
	 "Generate a context by calling make_pvfmm_context.");

  // context object
  PVFMMContext *ctx = (PVFMMContext*) *context;
  COUTDEBUG("pvfmm_stokes_sldl with sl_nsrc="<<sl_nsrc<<", dl_nsrc="<<dl_nsrc
	    <<", ntrg="<<ntrg<<", rebuild_tree="<<rebuild_tree);

  ASSERT(ctx->sl, "context doesn't support single layer");
  ASSERT(ctx->dl, "context doesn't support double layer");

  COUTDEBUG("Using context with"
	    <<" mult_order="<<ctx->mult_order
	    <<", max_pts="<<ctx->max_pts
	    <<", max_depth="<<ctx->max_depth
	    <<", sl="<<ctx->sl
	    <<", dl="<<ctx->dl
	    <<", periodic="<<ctx->periodic
	    );

  // copy data
  vec sl_srcv(sl_src,sl_src+DIM*sl_nsrc);
  vec sl_denv(sl_den,sl_den+DIM*sl_nsrc);

  vec dl_srcv(dl_src    ,dl_src    +  DIM*dl_nsrc);
  vec dl_denv(dl_den_nor,dl_den_nor+2*DIM*dl_nsrc);

  vec trgv(trg,trg+DIM*ntrg);
  vec potv(DIM*ntrg);

  for(size_t i=0;i<DIM*sl_nsrc;++i)
    if (isnan(sl_srcv[i]))
      CERR("sl_src is NaN",abort());

  for(size_t i=0;i<DIM*sl_nsrc;++i)
    if (isnan(sl_denv[i]))
      CERR("sl_den is NaN",abort());

  for(size_t i=0;i<DIM*dl_nsrc;++i)
    if (isnan(dl_srcv[i]))
      CERR("dl_src is NaN",abort());

  for(size_t i=0;i<2*DIM*dl_nsrc;++i)
    if (isnan(dl_denv[i]))
      CERR("dl_den is NaN",abort());

  for(size_t i=0;i<DIM*ntrg;++i)
    if (isnan(trgv[i]))
      CERR("trg is NaN",abort());

  // rebuild tree
  if(rebuild_tree || ctx->tree == NULL){
    COUTDEBUG("(Re)building sldl pvfmm tree");
    ASSERT(ctx->mat != NULL, "badly initialized context");

    // FMM Setup
    delete ctx->tree;

    ctx->tree = PtFMM_CreateTree(sl_srcv, sl_denv, dl_srcv, dl_denv,
				 trgv,ctx->comm, ctx->max_pts, ctx->boundary);

    ctx->tree->SetupFMM(ctx->mat);

    // Run FMM
    COUT("Evaluating point FMM");
    PtFMM_Evaluate(ctx->tree, potv, ntrg);

  } else {
    // only Run FMM
    ctx->tree->ClearFMMData();
    COUT("Evaluating point FMM");
    PtFMM_Evaluate(ctx->tree, potv, ntrg, &sl_denv, &dl_denv);
  }

  //copy
  for(size_t iT=0;iT<potv.size();++iT){
    pot[iT]=potv[iT];
    if (isnan(pot[iT])){
      CERR("fmm potential is NaN",abort());
    }
  }
}
