	program test
	use input_file
	use derivatives 
	use initialize 
	implicit none
!	integer :: N0,n_s,n_half,m
!	integer :: nteta
!	integer :: i,j
!	real*8, allocatable  :: s(:),x(:),y(:),x_ns(:,:),y_ns(:,:)
!	real*8, allocatable :: C_T0(:,:,:)
!	real*8, allocatable :: teta(:)
!	real*8, parameter :: pi=4.0*atan(1.0)
!	real*8 :: DT
!	m=4;
!	n_s=6
!	n_half=nint((n_s*1.0-1.0)/2.0);
!	N0=100

	
!	allocate(s(N0),x(N0),teta(N0),y(N0),C_T0(m+1,n_s,N0))
!	allocate(x_ns(m,N0),y_ns(m,N0))


	include 'init2.f90'

	print *, " m & N0 are is ", m, N0

!	stop 

!	DT=1.0*2*pi/(N0*1.0-1.0)
!	do i=1,N0
	
!	teta(i)=(i-1)*DT
!	s(i)=(i-1)*DT
!	x(i)=cos(4.0*teta(i))
!	y(i)=sin(4.0*teta(i))
!	enddo

	

	call weights2(N0,n_s,n_half,m+1,s,C_T0)
	call deriv_O_n(x(:,1),N0,n_s,C_T0,m,x_ns0(:,:,1))
	call deriv_O_n(y(:,1),N0,n_s,C_T0,m,y_ns0(:,:,1))


30	format(7F7.3)


	do i=1,N0
	write(*,40) x_ns0(1:4,i,1),y_ns0(1:4,i,1)
	enddo
40	format(10F15.3)
	end program test 
