VENDOR   = intel
FMM      = pvfmm

ifeq (${FMM}, pvfmm)
  PVFMM_DIR = /mnt/home/ehssan/software/pvfmm-lib-intel/share/pvfmm
  include $(PVFMM_DIR)/MakeVariables
endif

CXX      = ${CXX_PVFMM}
CXXFLAGS = ${CXXFLAGS_PVFMM} ${INCDIR}
LDLIBS  := ${LDFLAGS_PVFMM}

OBJS     = input_file.o linear_algebra.o fmm_interface.o derivatives.o initialize.o      \
           Hydro_inter.o A_TIMES_X.o                 \
           sparse_solve.o Tension.o biophysics.o
#OBJS	  = fmm_interface.o
COBJS    =
INCDIR   =
BIN      = main_original.x
LDLIBS   += -L/mnt/home/ehssan/software/codes/stfmmlib3d-1.2/src/
#LDLIBS  += -lstfmm3d

ifeq (${FMM}, pvfmm)
  COBJS  += pvfmm_interface.o
endif

ifeq (${VENDOR},intel)
  FC       = mpiifort
	FFLAGS   = -lmkl -lstatic-intel -lopenmp -O3
	LDFLAGS  = -fopenmp -mkl
else
  FC       = mpif90
  FFLAGS   = -fopenmp -Wall -Wcast-align -lopenmp
	LDFLAGS  = -lmpi_cxx -fopenmp
endif
LDLIBS   +=  -lgfortran -lstdc++

all: ${BIN}

%.x: %.o ${OBJS} ${COBJS}
	${FC} -O3  ${LDFLAGS} $^ -L ${CURDIR} ${LDLIBS} -o $@

fmm_interface_test.x: fmm_interface_test.o fmm_interface.o ${COBJS}
	${FC} ${LDFLAGS} $^ -L ${CURDIR} ${LDLIBS} -o $@

%.o: %.f90
	${FC} -c ${FFLAGS} ${CPPFLAGS} $<

%.mod: %.f90
	${FC} -c ${FFLAGS} ${CPPFLAGS} $<

clean:
	${RM} -r *.o *.mod *.x  ${BIN}

.SECONDARY:

# expressing dependencies (rule is implied above)
main_original.o : ${OBJS} ${COBJS}

initialize.o: initialize.f90 input_file.mod fmm_interface.mod

A_TIMES_X.o: A_TIMES_X.f90 derivatives.mod

derivatives.o: derivatives.f90 linear_algebra.mod

biophysics.o: biophysics.f90 input_file.mod initialize.mod derivatives.mod

sparse_solve.o: sparse_solve.f90 initialize.mod

Tension.o: Tension.f90 A_TIMES_X.mod derivatives.mod linear_algebra.mod Hydro_inter.mod sparse_solve.mod fmm_interface.mod

#BoundaryIntegral.o: BoundaryIntegral.f90 GAUSS_ARRAY.mod BI_SP_build_array.mod GaussInt_Array.mod BI_cor_build_array.mod

fmm_interface_test.o: fmm_interface_test.f90 fmm_interface.mod

