  !******************************************************************************************!
  n_node_SP=(nt_SP-1)*ntt_SP;n_element_SP=(nt_SP-1)*(ntt_SP-1)
  n_node_cor=(nt_cor-1)*ntt_cor;n_element_cor=(nt_cor-1)*(ntt_cor-1)
  ntarg_MT=n_node_SP+n_node_cor
  nparts_MT=N0*N_f

  ntargs_SP=n_node_SP
  nparts_SP=n_node_SP
  !        nparts_SP=n_element_SP*gauss_n**2
  !        nparts_cor=n_element_cor*gauss_n**2
  nparts_cor=n_node_cor
  ntargs_cor=n_node_cor

  !        nparts_SPCOR=(n_element_SP+n_element_cor)*gauss_n**2
  nparts_SPCOR=n_node_SP+n_node_cor
  ntargs_SPCOR=nparts_MT
  nparts_sp_l=nparts_sp/nprocs
  ntargs_sp_l=ntargs_sp/nprocs
  nparts_cor_l=nparts_cor/nprocs
  ntargs_cor_l=ntargs_cor/nprocs
  nparts_SPCOR_l=nparts_SPCOR/nprocs
  nsrc=nparts_MT+nparts_SPCOR_l
  ntrg=nparts_MT+nparts_SPCOR_l    

  nt_site=21
  ntt_site=12
  n_site=(nt_site-1)*ntt_site

  ! print *, "Number of fibers here are now .. ", N_f
  allocate(N_var(N_f), FLAG_P(N_f),N_VAR_P(N_f),FLAGP_P(N_f),flag_BC(N0,N_f),flag_BC_T(N0,N_f),counter(N_f),tp(N_f),t_att(N_f))
  allocate(f_pin(N_f),x_pin(N_f),y_pin(N_f),z_pin(N_f))
  allocate(mu0(N_f), mu(N_f), mu1(N_f), mu2(N_f),  imu2(N_F), &
       & L_F_T(N_f_T),L_F(N_f), L_FF(N_f),C(N_f), C0(N_f), BC_T(N_f), teta(N_f_T),phi(N_f_T),L_max(N_f),& 
       & beta(N_f),beta_n(N_f),beta_t(N_f,4),beta_i(N_f),beta_end(N_f))
  allocate(dxc1(3,N_f), dxc1p(3,N_f),dxc2p(3,N_f),dxc1_2(3,N_f), &
       & X_att0(3,N_f), X_att_T(3,N_f_T),X_rot(3,N_f),X_att(3,N_f), & 
       & DX_at(3,N_f),DX_at2(3,N_f),DR_at(N_f),dxc2(3,N_f),dxc2_2(3,N_f),X_att0_2(3,N_f),X_att_2(3,N_f),& 
       & Vatt0(3,N_f),Vatt(3,N_f),Vatt0_2(3,N_f),Vatt_2(3,N_f), Ten_B(3,N_f),Ten_T(3,N_f), &
       & F_MT_TN(3,N_f),F_MT_BN(3,N_f))
  allocate(s(N0),dum(N0,N_f),chev_w(N0))
  allocate(n_0(3,N0,N_f),n_1(3,N0,N_f),n_2(3,N0,N_f))
  allocate(dot0(N_f,N0),dot0_2(N_f,N0),dot1(N_f,N0),dot2(N_f,N0),dot3(N_f,N0))

  allocate(x(N0,N_f),y(N0,N_f),z(N0,N_f),&
       & x0(N0,N_f),y0(N0,N_f),z0(N0,N_f), r0(N0,N_f), &
       x_T(N0,N_f_T),y_T(N0,N_f_T),z_T(N0,N_f_T),&
       & xp(N0,N_f),yp(N0,N_f),zp(N0,N_f),&
       & xpp(N0,N_f),ypp(N0,N_f),zpp(N0,N_f),&
       & dxdt(N0,N_f), dydt(N0,N_f), dzdt(N0,N_f), &
       & ux_ext(N0,N_f), uy_ext(N0,N_f), uz_ext(N0,N_f), &
       & ux2_ext(N0,N_f), uy2_ext(N0,N_f), uz2_ext(N0,N_f), &
       & ux_ext_tr(1,N0,N_f), uy_ext_tr(1,N0,N_f), uz_ext_tr(1,N0,N_f), &
       & ux_SP(N0,N_f), uy_SP(N0,N_f), uz_SP(N0,N_f), &
       & ux_SP_B(N0,N_f), uy_SP_B(N0,N_f), uz_SP_B(N0,N_f), &
       & ux_SP_T(N0,N_f), uy_SP_T(N0,N_f), uz_SP_T(N0,N_f), &
       & ux_SP_HD(N0,N_f), uy_SP_HD(N0,N_f), uz_SP_HD(N0,N_f), &
       & ux_SP_HD_B(N0,N_f), uy_SP_HD_B(N0,N_f), uz_SP_HD_B(N0,N_f), &
       & ux_SP_HD_T(N0,N_f), uy_SP_HD_T(N0,N_f), uz_SP_HD_T(N0,N_f), &
       & ux_SP_HD_B2(N0,N_f), uy_SP_HD_B2(N0,N_f), uz_SP_HD_B2(N0,N_f), &
       & ux_SP_HD_T2(N0,N_f), uy_SP_HD_T2(N0,N_f), uz_SP_HD_T2(N0,N_f), &
       & ux_MT(N0,N_f), uy_MT(N0,N_f), uz_MT(N0,N_f), &
       & ux_MT_T(N0,N_f), uy_MT_T(N0,N_f), uz_MT_T(N0,N_f), &
       & ux_MT_T_P(N0,N_f), uy_MT_T_P(N0,N_f), uz_MT_T_P(N0,N_f),&
       & ux_MT_T_PP(N0,N_f), uy_MT_T_PP(N0,N_f), uz_MT_T_PP(N0,N_f), &
       & ux_self(N0,N_f), uy_self(N0,N_f), uz_self(N0,N_f), &
!       & ux_SPCOR(N0,N_f),uy_SPCOR(N0,N_f),uz_SPCOR(N0,N_f), &
!       & ux_SPCOR_P(N0,N_f),uy_SPCOR_P(N0,N_f),uz_SPCOR_P(N0,N_f), &
!       & ux_SPCOR_PP(N0,N_f),uy_SPCOR_PP(N0,N_f),uz_SPCOR_PP(N0,N_f), &
       & fx_MT_B(N0,N_f), fy_MT_B(N0,N_f), fz_MT_B(N0,N_f), &
       & fx_MT_T(N0,N_f), fy_MT_T(N0,N_f), fz_MT_T(N0,N_f), &
       & fx_MT_ext(N0,N_f), fy_MT_ext(N0,N_f), fz_MT_ext(N0,N_f), &
       & fx_2(N0,N_f), fy_2(N0,N_f), fz_2(N0,N_f), &
       & ux_2(N0,N_f), uy_2(N0,N_f), uz_2(N0,N_f), &
       & ux_3(N0,N_f), uy_3(N0,N_f), uz_3(N0,N_f), &
       & ux_2_P(N0,N_f), uy_2_P(N0,N_f), uz_2_P(N0,N_f), &
       & ux_2_PP(N0,N_f), uy_2_PP(N0,N_f), uz_2_PP(N0,N_f), &
       & uxs_2(1,N0,N_f), uys_2(1,N0,N_f), uzs_2(1,N0,N_f), xsus_2(1,N0,N_f), &
       & T(N0,N_f), T0(N0,N_f),T_tr(1,N0,N_f))

  allocate(ia_IE(N0+1,N_f),ia_T(N0+1,N_f),ja_T(n_s*N0,N_f), ja_IE(n_s*N0,N_f),ia_D(3*N0+1,N_f), ja_D(3*3*n_s*N0,N_f), &
       & a_T(n_s*N0,N_f),a_D(3*3*n_s*N0,N_f),a_IE(N0*n_s,N_f), &
       & ia_XT(4*N0+1,N_f),ja_XT(n_s*10*N0,N_f),a_XT(n_s*10*N0,N_f),&
       & ia_XT2(4*N0+1,N_f),ja_XT2(n_s*16*N0,N_f),a_XT2(n_s*16*N0,N_f))


  allocate(xs0(1,N0,N_f),ys0(1,N0,N_f),zs0(1,N0,N_f), &
       & xs(1,N0,N_f),ys(1,N0,N_f),zs(1,N0,N_f), &
       & ux_s(1,N0,N_f), uy_s(1,N0,N_f), uz_s(1,N0,N_f), &
       & ux_s0(1,N0,N_f), uy_s0(1,N0,N_f), uz_s0(1,N0,N_f), &
       & xsus(1,N0,N_f), xsuTs(1,N0,N_f), xsus_f(1,N0,N_f), xsus_T(1,N0,N_f), & 
       & fx_s(1,N0,N_f), fy_s(1,N0,N_f), fz_s(1,N0,N_f), &
       & fx_MTT_T(1,N0,N_f), fy_MTT_T(1,N0,N_f), fz_MTT_T(1,N0,N_f), & 
       & fx_MTT_B(1,N0,N_f), fy_MTT_B(1,N0,N_f), fz_MTT_B(1,N0,N_f), &
       & uxs_MT_T(1,N0,N_f), uys_MT_T(1,N0,N_f), uzs_MT_T(1,N0,N_f), &
       & uxs_MT(1,N0,N_f), uys_MT(1,N0,N_f), uzs_MT(1,N0,N_f), &
       & uxs_SP(1,N0,N_f), uys_SP(1,N0,N_f), uzs_SP(1,N0,N_f), &
       & uxs_SP_B(1,N0,N_f), uys_SP_B(1,N0,N_f), uzs_SP_B(1,N0,N_f), &
       & uxs_SP_T(1,N0,N_f), uys_SP_T(1,N0,N_f), uzs_SP_T(1,N0,N_f), &
       & uxs_SP_HD(1,N0,N_f), uys_SP_HD(1,N0,N_f), uzs_SP_HD(1,N0,N_f), &
       & uxs_SP_HD_T(1,N0,N_f), uys_SP_HD_T(1,N0,N_f), uzs_SP_HD_T(1,N0,N_f), &
       & uxs_SP_HD_B(1,N0,N_f), uys_SP_HD_B(1,N0,N_f), uzs_SP_HD_B(1,N0,N_f), &
       & uxs_SP_HD_T2(1,N0,N_f), uys_SP_HD_T2(1,N0,N_f), uzs_SP_HD_T2(1,N0,N_f), &
       & uxs_SP_HD_B2(1,N0,N_f), uys_SP_HD_B2(1,N0,N_f), uzs_SP_HD_B2(1,N0,N_f), &
!       & uxs_SPCOR(1,N0,N_f),uys_SPCOR(1,N0,N_f),uzs_SPCOR(1,N0,N_f),&
!       & uxs_SPCOR_P(1,N0,N_f),uys_SPCOR_P(1,N0,N_f),uzs_SPCOR_P(1,N0,N_f),&
       & fx_MTT_ext(1,N0,N_f), fy_MTT_ext(1,N0,N_f), fz_MTT_ext(1,N0,N_f), & 
       & fxs(1,N0,N_f),fxss(1,N0,N_f), fsxs(1,N0,N_f), & 
       & QX0(1,N0,N_f), QY0(1,N0,N_f), QZ0(1,N0,N_f), & 
       & QX(1,N0,N_f), QY(1,N0,N_f), QZ(1,N0,N_f), & 
       & x_ns0(m,N0,N_f), y_ns0(m,N0,N_f), z_ns0(m,N0,N_f), &
       & x_ns(m,N0,N_f), y_ns(m,N0,N_f), z_ns(m,N0,N_f), &
       & U_EXT(3,N0,N_f),&
       & Ts(1,N0,N_f), T_ns(m,N0,N_f))   

  ! allocate(xt(n_t,N0,N_f),yt(n_t,N0,N_f),zt(n_t,N0,N_f), &
  ! & xst(n_t,N0,N_f),yst(n_t,N0,N_f),zst(n_t,N0,N_f), &
  ! & QXt(n_t,N0,N_f),QYt(n_t,N0,N_f),QZt(n_t,N0,N_f), &
  ! & ux_t(n_t,N0,N_f),uy_t(n_t,N0,N_f),uz_t(n_t,N0,N_f), &
  ! & Tst(n_t,N0,N_f),Tt(n_t,N0,N_f))

  allocate(T_T(N0*N_f),F_T(N0*N_f), FT(3*N0,N_f),F_T0(N_f*N0),XT(4*N0*N_f_T),XT_2(4*N0*N_f),F_XT(4*N_f_T*N0),F_XT2(4*N_f*N0),&
       & RHS(N0*N_f), RHS_im(4*N0*N_f_T),A_DOT_T(N0*N_f), A_DOT_X(4*N0*N_f),A_DOT_XT(4*N0*N_f_T),B3_T(4*N0*N_f_T), &
       & computed_solution(N0*N_f),computed_solution_im(4*N0*N_f_T))
  allocate(F_X(4*N0,N_f),XX(3*N0,N_f))

  allocate(source_MT(3, N0*N_f), targ_MT(3,ntarg_MT), &
       & sigma_dl_MT_T(3,N0*N_f), sigma_sl_MT_T(3,N0*N_f), sigma_dv_MT(3,N0*N_f), &
       & pottarg_MT(3,ntarg_MT),pot_MT2(3,N0*N_f),pot_MT_T(3,N0*N_f),pot_MT_T_P(3,N0*N_f),pot_MT_T_PP(3,N0*N_f), &
       & grad_MT_T(3,3,N0*N_f),gradtarg_MT(3,3,ntarg_MT), &
       & pre_MT_T(N0*N_f), pretarg_MT(ntarg_MT))
  allocate(sigma_dl_MT_B(3,N0*N_f), sigma_sl_MT_B(3,N0*N_f), &
       & pot_MT_B(3,N0*N_f),pot_MT_B_P(3,N0*N_f), pot_MT_B_PP(3,N0*N_f), &
       & grad_MT_B(3,3,N0*N_f), &
       & pre_MT_B(N0*N_f), &
       & pot_MT(3,N0*N_f),pre_MT(N0*N_f),grad_MT(3,3,N0*N_f), &
       & sigma_dl_MT(3,N0*N_f), sigma_sl_MT(3,N0*N_f),&
       & U_bath_MT_0(3*nparts_MT))


  allocate(source(3,nsrc),pot(3,nsrc),pre(nsrc),sigma_sl(3,nsrc),sigma_dl(3,nsrc),sigma_dv(3,nsrc),grad(3,3,nsrc))

  !************************************************************************************!
  !***************************C++ PVFMM allocations************************************ 

  allocate(src(nsrc*DIM0),trg(ntrg*DIM0),sldl_pot2(ntrg*DIM0),sldl_pot(ntrg*DIM0), & 
       & sldl_pot_T(ntrg*DIM0*nprocs),sl_den(nsrc*DIM0),dl_den_nor(6*nsrc),&
       & seq_den_sl(DIM0,nsrc),seq_den_dl(DIM0,nsrc),seq_src(DIM0,nsrc), & 
       & seq_pot(DIM0,nsrc),seq_pot_2(DIM0,nsrc),seq_pre(nsrc), & 
       & seq_grad(DIM0,DIM0,nsrc),seq_dv(DIM0,nsrc))
  allocate(src2(nsrc*DIM0),src3(nsrc*DIM0),trg2(ntrg*DIM0),sldl_pot3(ntrg*DIM0),sl_den2(nsrc*DIM0),dl_den_nor2(2*DIM0*nsrc))

  !************************************************************************************!


  allocate(Vt(n_t,3),xct(n_t,3))
  allocate(TNt(n_t,N_f),Vatt_t(n_t,3,N_f),Vatt_t_2(n_t,3,N_f)) 
  allocate(ES(3,3,N_f), F_L(3,N_f))

  allocate(C_T0(m+1,n_s,N0),C_T(m+1,n_s,N0), &
       & C_T_B1(m+1,n_s,n_half), C_T_B2(m+1,n_s,n_half))

  allocate(c_1(n_t),c_1T(n_t,1), c_2T(n_t+1,2),c_2(n_t+1), c_3(n_t), time_1(n_t), time_2(n_t+1))

  allocate (xns_end(m), yns_end(m), zns_end(m))

  allocate (V(3,N_F), V2(3,N_F), O(3,N_f), O2(3,N_f), V_2(3,N_F), V2_2(3,N_F),F_active(3,N_f),S_2(3,3,N_f),S_1(3,3,N_f))

  !************************************************************************************************!
  !************************************************************************************************!
  !************************************************************************************************!

  allocate(F(gauss_n,gauss_n,4),gauss_p(gauss_n,1),gauss_w(gauss_n,1), &
       gauss_p0(gauss_n),gauss_w0(gauss_n))

  !-------------------------------------------------------!
  allocate(x_g_SP(n_element_SP*gauss_n*gauss_n),&
       & y_g_SP(n_element_SP*gauss_n*gauss_n), &
       & z_g_SP(n_element_SP*gauss_n*gauss_n),&
       & h_SP(n_element_SP*gauss_n*gauss_n), &
       & h_SP_node(n_node_SP), &
       & QxSP_g(n_element_SP*gauss_n*gauss_n),&
       & QySP_g(n_element_SP*gauss_n*gauss_n),&
       & QzSP_g(n_element_SP*gauss_n*gauss_n))

  allocate(n_N_SP(3,n_element_SP*gauss_n*gauss_n),&
       & xm_SP(n_node_SP),ym_SP(n_node_SP),zm_SP(n_node_SP),&
       & nx_SP(n_node_SP),ny_SP(n_node_SP), nz_SP(n_node_SP), &
       & Qx_SP(n_node_SP),Qy_SP(n_node_SP),Qz_SP(n_node_SP),Q_SP(3*n_node_SP))


  allocate(U_bath_SP_0(3*n_node_SP),U_bath_SP_1(3*n_node_SP),U_bath_SP_2(3*n_node_SP))

  allocate(coor_SP(n_node_SP,3),con_SP(n_element_SP,4))
  allocate(T_SP(3*n_node_SP,3*n_node_SP), iT_SP(3*n_node_SP,3*n_node_SP))
  allocate (IPVT9_SP(3*n_node_SP))
  !-----------------------------------------------------!


  allocate(x_g_cor(n_element_cor*gauss_n*gauss_n),&
       & y_g_cor(n_element_cor*gauss_n*gauss_n), &
       & z_g_cor(n_element_cor*gauss_n*gauss_n),&
       & h_cor(n_element_cor*gauss_n*gauss_n), &
       & h_cor_node(n_node_cor), &
       & Qxcor_g(n_element_cor*gauss_n*gauss_n), &     
       & Qycor_g(n_element_cor*gauss_n*gauss_n),&
       & Qzcor_g(n_element_cor*gauss_n*gauss_n),Q_cor(3*n_node_cor))

  allocate(n_N_cor(3,n_element_cor*gauss_n*gauss_n),&
       & xm_cor(n_node_cor),ym_cor(n_node_cor),zm_cor(n_node_cor),&
       & nx_cor(n_node_cor),ny_cor(n_node_cor), nz_cor(n_node_cor), &
       & Qx_cor(n_node_cor),Qy_cor(n_node_cor),Qz_cor(n_node_cor))


  allocate(U_bath_cor_0(3*n_node_cor),U_bath_cor(3*n_node_cor),U_bath_cor_2(3*n_node_cor))

  allocate(coor_cor(n_node_cor,3),con_cor(n_element_cor,4))
  allocate(T_cor(3*n_node_cor,3*n_node_cor),iT_cor(3*n_node_cor,3*n_node_cor))
  allocate (IPVT9_cor(3*n_node_cor))
  !----------------------------------------------------!

  !*********************************************************************************!
  !*********************************************************************************!
  !*********************************************************************************!



  allocate(source_SP(3,nparts_SP), source_sp_l(3,nparts_sp_l),sigma_sl_SP(3,nparts_SP), &
       & sigma_dl_SP(3,nparts_SP), sigma_dv_SP(3,nparts_SP),&
       & sigma_dl_sp_l(3,nparts_sp_l), sigma_dv_sp_l(3,nparts_sp_l))

  allocate(source_cor(3,nparts_cor), source_cor_l(3,nparts_cor_l),sigma_sl_cor(3,nparts_cor), &
       & sigma_dl_cor(3,nparts_cor), sigma_dv_cor(3,nparts_cor), &
       & sigma_dl_cor_l(3,nparts_cor_l), sigma_dv_cor_l(3,nparts_cor_l))

  allocate(pot_SP(3, nparts_SP), pre_SP(nparts_SP), grad_SP(3,3,nparts_SP))
  allocate(pot_cor(3, nparts_cor), pre_cor(nparts_cor), grad_cor(3,3,nparts_cor))
  allocate(pottarg_cor_1D(3*ntargs_cor),pottarg_cor_1D_l(3*ntargs_cor_l))
  allocate(targ_SP(3,ntargs_SP), targ_sp_l(3,ntargs_sp_l), pottarg_SP(3, ntargs_SP),pottarg_sp_l(3,ntargs_sp_l), &
       & pretarg_SP(ntargs_SP),pretarg_sp_l(ntargs_sp_l),gradtarg_SP(3,3,ntargs_SP),gradtarg_sp_l(3,3,ntargs_sp_l))

  allocate(targ_cor(3,ntargs_cor), pottarg_cor(3,ntargs_cor),pretarg_cor(ntargs_cor), gradtarg_cor(3,3,ntargs_cor), &
       targ_cor_l(3,ntargs_cor_l),pottarg_cor_l(3,ntargs_cor_l),pretarg_cor_l(ntargs_cor_l), gradtarg_cor_l(3,3,ntargs_cor_l))
  allocate(pottarg_sp_1D(3*ntargs_sp),pottarg_sp_1D_l(3*ntargs_sp_l))
  allocate(source_SPCOR(3,nparts_SPCOR),source_spcor_l(3,nparts_spcor_l),&
       & sigma_sl_SPCOR(3,nparts_SPCOR), sigma_dl_SPCOR(3,nparts_SPCOR), sigma_dv_SPCOR(3,nparts_SPCOR), &
       sigma_dl_SPCOR_l(3,nparts_spcor_l),sigma_dv_spcor_l(3,nparts_spcor_l))
  allocate(pot_SPCOR(3, nparts_SPCOR), pre_SPCOR(nparts_SPCOR),grad_SPCOR(3,3,nparts_SPCOR))
  allocate(targ_SPCOR(3,ntargs_SPCOR), pottarg_SPCOR(3, ntargs_SPCOR),& 
       & pretarg_SPCOR(ntargs_SPCOR),gradtarg_SPCOR(3,3,ntargs_SPCOR))
  allocate(sigma_dl_SPCOR_MOB(3,nparts_SPCOR,6),pottarg_SPCOR_MOB(3,ntargs_SPCOR,6))
  !*********************************************************************************!
  !*********************************************************************************!

  allocate(a_site(N_f),x_site(n_site),y_site(n_site),z_site(n_site))
  !*********************************************************************************!
  a_site(:)=0

  do nf=1,N_f-1,2
     FLAG_P(nf)=1
     flag_p(nf+1)=1
  enddo
  ! FLAG_P(:)=1
  FLAGP_P(:)=FLAG_P(:)
  beta(:)=0.0
  beta_t(:,:)=0.0
  tp(:)=0
  dum(:,:)=0.0;
  ! FLAG_P(1)=-1
  ! FLAG_P(2)=1
  xc0=(/-0.0d0, -0.0d0, -0.0d0/)
  xc0p=xc0;
  e_SP=xc0


  n_0(1:3,:,:)=0.0d0;n_0(1,:,:)=1.0d0;
  n_1(1:3,:,:)=0.0d0;n_1(2,:,:)=1.0d0;
  n_2(1:3,:,:)=0.0d0;n_2(3,:,:)=1.0d0;

  V_SPCOR(:,:)=0.0;V_SPCOR_P(:,:)=0.0;V_SPCOR_PP(:,:)=0.0;
  x(:,:)=0.0; y(:,:)=0.0; z(:,:)=0.0; xi=0.0;
  x0(:,:)=0.0; y0(:,:)=0.0; z0(:,:)=0.0; r0(:,:)=0.0;

  xs0(:,:,:)= 0.0; ys0(:,:,:)= 0.0; zs0(:,:,:)= 0.0;
  xs(:,:,:) = 0.0; ys(:,:,:) = 0.0; zs(:,:,:) = 0.0;
  x_ns0(:,:,:)=0.0;y_ns0(:,:,:)=0.0;z_ns0(:,:,:)=0.0;

  t_att(:)=0;flag_BC(:,:)=0;
  flag_BC(1,:)=1;flag_BC(2,:)=1;flag_BC(N0-1,:)=4;flag_BC(N0,:)=3
  flag_BC_T(:,:)=0;
  flag_BC_T(N0,:)=1;flag_BC_T(1,:)=1;
  ux_ext(:,:)=0.0; uy_ext(:,:)=0.0; uz_ext(:,:)=0.0;
  ux_s(:,:,:)=0.0; uy_s(:,:,:)=0.0; uz_s(:,:,:)=0.0;   
  ux_SP(:,:)=0.0; uy_SP(:,:)=0.0; uz_SP(:,:)=0.0;  
  ux_SP_T(:,:)=0.0; uy_SP_T(:,:)=0.0; uz_SP_T(:,:)=0.0;  
  ux_SP_B(:,:)=0.0; uy_SP_B(:,:)=0.0; uz_SP_B(:,:)=0.0; 
  ux_SP_HD_T(:,:)=0.0; uy_SP_HD_T(:,:)=0.0; uz_SP_HD_T(:,:)=0.0;  
  ux_SP_HD_B(:,:)=0.0; uy_SP_HD_B(:,:)=0.0; uz_SP_HD_B(:,:)=0.0;  
  ux_MT_T(:,:)=0.0; uy_MT_T(:,:)=0.0; uz_MT_T(:,:)=0.0; 
  ux_MT_T_P(:,:)=0.0; uy_MT_T_P(:,:)=0.0; uz_MT_T_P(:,:)=0.0;
  ux_MT_T_PP(:,:)=0.0; uy_MT_T_PP(:,:)=0.0; uz_MT_T_PP(:,:)=0.0;
  pot_MT_B_P(:,:)=0.0;pot_MT_B_PP(:,:)=0.0;
  pot_MT_T_P(:,:)=0.0;pot_MT_T_PP(:,:)=0.0;

  ux_self(:,:)=0.0; uy_self(:,:)=0.0; uz_self(:,:)=0.0; 

  fx_MT_ext(:,:)=0.0; fy_MT_ext(:,:)=0.0; fz_MT_ext(:,:)=0.0; 
  fx_MTT_ext(:,:,:)=0.0; fy_MTT_ext(:,:,:)=0.0; fz_MTT_ext(:,:,:)=0.0; 
  fx_s(1,:,:)=0.0; fy_s(1,:,:)=0.0; fz_s(1,:,:)=0.0; 


  fx_MT_B(:,:)=0.0; fy_MT_B(:,:)=0.0; fz_MT_B(:,:)=0.0; 
  fx_MT_T(:,:)=0.0; fy_MT_T(:,:)=0.0; fz_MT_T(:,:)=0.0; 
  fx_MTT_B(:,:,:)=0.0; fy_MTT_B(:,:,:)=0.0; fz_MTT_B(:,:,:)=0.0; 
  fx_MTT_T(:,:,:)=0.0; fy_MTT_T(:,:,:)=0.0; fz_MTT_T(:,:,:)=0.0; 

  fx_2(:,:)=0.0;fy_2(:,:)=0.0;fz_2(:,:)=0.0;
  ux_2(:,:)=0.0;uy_2(:,:)=0.0;uz_2(:,:)=0.0;
  ux_2_p(:,:)=0.0;uy_2_p(:,:)=0.0;uz_2_p(:,:)=0.0;
  ux_2_pp(:,:)=0.0;uy_2_pp(:,:)=0.0;uz_2_pp(:,:)=0.0;
  fsxs(:,:,:)=0.0;fxss(:,:,:)=0.0;


!  ux_SPCOR(:,:)=0.0;uy_SPCOR(:,:)=0.0;uz_SPCOR(:,:)=0.0;
!  ux_SPCOR_P(:,:)=0.0;uy_SPCOR_P(:,:)=0.0;uz_SPCOR_P(:,:)=0.0;
!  ux_SPCOR_PP(:,:)=0.0;uy_SPCOR_PP(:,:)=0.0;uz_SPCOR_PP(:,:)=0.0;

  QX(:,:,:)=0.0; QY(:,:,:)=0.0; QZ(:,:,:)=0.0; U_EXT(:,:,:)=0.0;
  QX0(:,:,:)=0.0; QY0(:,:,:)=0.0; QZ0(:,:,:)=0.0; 

  XT(:)=0.0;A_DOT_xT(:)=0.0
  T(:,:)=0.0;T0(:,:)=0.0;T_ns(:,:,:)=0.0; Ts(:,:,:)=0.0;
  ! Tt(:,:,:)=0.0; Tst(:,:,:)=0.0; TNt(:,:)=0.0;

  T_T(:)=0.0;F_T(:)=0.0;

  ! xt(:,:,:)=0.0; yt(:,:,:)=0.0; zt(:,:,:)=0.0; 
  ! xst(:,:,:)=0.0; yst(:,:,:)=0.0; zst(:,:,:)=0.0;
  ! dxdt(:,:)=0.0; dydt(:,:)=0.0; dzdt(:,:)=0.0;
  ! QXt(:,:,:)=0.0; QYt(:,:,:)=0.0; QZt(:,:,:)=0.0; 
  ! ux_t(:,:,:)=0.0; uy_t(:,:,:)=0.0; uz_t(:,:,:)=0.0; 

  xsus(:,:,:)=0.0; xsuTs(:,:,:)=0.0;xsus_f(:,:,:)=0.0; xsus_T(:,:,:)=0.0;

  Vt(:,:)=0.0; xct(:,:)=0.0; 
  ! Vatt_t(:,:,:)=0.0; Vatt_t_2(:,:,:)=0.0;
  Vatt0(:,:)=0.0;Vatt0_2(:,:)=0.0;Vatt(:,:)=0.0;Vatt_2(:,:)=0.0;
  ES(1:3,1:3,N_f)=0.0;

  time_1(:)=0.0; time_2(:)=0.0;

  c_1(:)=0.0;c_2(:)=0.0;c_3(:)=0.0;
  c_1T(:,:)=0.0;c_2T(:,:)=0.0;

  N_teta=nint(N_f_T/10.0d0);
  N_phi=nint(1.0*N_f_T/(1.0*N_teta));
  print *, "N_teta is .. ", N_teta
  print *, " N_Phi is ", N_phi
  ! do i=1, N_f
  ! L_F(i)=L+1.0*(i-1)*(1.50*L)/(N_f*1.0)
  ! enddo

  seed(1)=3
  ! Floren commented this line
  ! call random_seed(put=seed)
  call random_seed()

  do i=1,N_F
     call random_number (L_F(i))
     L_F(i)=2.350d0+L_F(i)*1.0d0
  enddo

  !        L_F_T(:)=1.0d0
  ! L_F(1:N_f_h)=1.0d0;
  !        L_F(N_f_h+1:N_f)=1.0d0
  L_FF(1:N_f)=L_F(1:N_f)
  L_F_T(1:N_f_T)=L_F(:)
  L_max(1:N_f)=1.0
  ! N_var(1:N_f)=nint(L_F(1:N_f)/ds)+1;
  N_var(1:N_f)=N0
  C(1:N_f)=1.0d0;
  C0(1:N_f)=log((L_F/a_MT)**2*exp(-1.0));
  !C0(1:N_f)=40.0d0

  !C0(1:N_f)=8.0/6.0;
  mu0(1:N_f)=1.0d0;!*** $mu_{0} = \frac  { F^{ST} a_{SP}^2}  { E_{MT} } == cte ***!
  mu1=mu0;
  mu2(1:N_f)=8.0/(8.0*C0);
  imu2=1.0/mu2;
  mu(1:N_f)=mu0(1:N_f)*mu2(1:N_f);
  mu1(1:N_f)=F_stall/(E_MT/a_SP0**2);
  !          mu1(:)=1.0d0
  print *, "MU is ....", mu1(1)
  !%mu2(1:N_f)=6.0*(1.0*L).*C0./(8.0*L_F);

  !***********************************************!
  !************Equally spaced nodes***************!
  !***********************************************!
  !        print *, "ds is ...", ds
  ! do i=1,N0
  ! s(i)=(i*1.0d0-1.0d0)*ds
  !        chev_w(i)=ds
  !        enddo
  !***********************************************!
  !*************Chebychev points******************!
  call clencurt(N0-1,s(:),chev_w(:))
  chev_w=chev_w/2.0d0
  s=(1.0d0-s)/2.0d0

  do i=1,N0

     print *, "chebychev is ", s(i),chev_w(i)

  enddo
  !         s(i)=-cos((i*1.0d0-1.0d0)*pi/(N0*1.0d0-1.0d0));
  !         chev_w(i)=sqrt(abs(1.0d0-s(i)**2+1.0e-12))*pi/(N0*2.0d0)
  !!        chev_w(i)=0.0 
  !        enddo 
  !       a_1=1.0d0;
  !       s=(s+a_1)/(2.0d0*a_1)
  !       s(1)=0.0;s(N0)=a_1;
  !***********************************************!
  !***********************************************!

  !******************************************************!
  !**** RANDOM DISTRIBUTION OF FIBERS ON THE SPHERE *****!
  !******************************************************!

  do i=1,N_f_T/2

     call random_number (teta_i)
     teta(i)=2.0*pi*teta_i

  enddo

  call random_seed()

  do i=1,N_f_T/2

     call random_number(phi_i)

     !        Omega_T(1:2,1)=0.0d0;Omega_T0_T(1:2,1)=0.0d0;Omega_T0_B(1:2,1)=0.0d0;
     phi(i)=acos(phi_i)

  enddo

  !        L_F_T(1:N_f_T/2)=L_F_T(1:N_f_T/2)+((teta/pi)-1.0d0)*0.1d0
  !        L_F_T(N_f_T/2+1:N_f_T)=L_F(1:N_f_T/2);
  !        L_F(1:N_f)=L_F_T(
  !        L_FF(1:N_f)=L_F(1:N_f)

  N_teta_bulk=N_teta*2
  N_phi_bulk=N_phi*2
  N_r_bulk=N0*2
  ntarg_bulk=N_phi_bulk*N_teta_bulk*N_r_bulk
  ntrg_bulk=ntarg_bulk
  k=0


!  allocate(teta_bulk(N_teta_bulk),phi_bulk(N_phi_bulk),s_bulk(N_r_bulk),&
!       & x_bulk(ntarg_bulk),y_bulk(ntarg_bulk),z_bulk(ntarg_bulk),dum_bulk(ntarg_bulk), &
!       & ux_bulk(ntarg_bulk),uy_bulk(ntarg_bulk),uz_bulk(ntarg_bulk),&
!       & ux_bulk_SP(ntarg_bulk),uy_bulk_SP(ntarg_bulk),uz_bulk_SP(ntarg_bulk),&
!       & ux_bulk_SP2(ntarg_bulk),uy_bulk_SP2(ntarg_bulk),uz_bulk_SP2(ntarg_bulk),&
!       & targ_bulk(3,ntarg_bulk),&
!       & pretarg_bulk(ntarg_bulk),gradtarg_bulk(3,3,ntarg_bulk),&
!       & pottarg_bulk_MT(3,ntarg_bulk),pottarg_bulk_SPCOR(3,ntarg_bulk),W_bulk(ntarg_bulk))

!  allocate(trg_bulk(ntrg_bulk*DIM0),pot_bulk(ntrg_bulk*DIM0))        



  !*******************************************************!
  !*******************************************************!
  teta_i=-pi/1.0; teta_f=pi/1.0
  !       phi_i=-1.0*pi/3.0;phi_f=1.0*pi/3.0\
  phi_i=pi/8.0d0;phi_f=pi/2.0d0+1.0*pi/12.0d0;
  dteta=(teta_f-teta_i)/(N_teta*1.0)
  dphi=2.0*(phi_f-phi_i)/(N_phi*1.0+1.0e-12)

  k=0;

  do j=1,N_phi
     do i=1,N_teta



        k=k+1
        if (k .le. N_f_T/2) then
           teta(k)=teta_i+dteta*(i*1.0-1.0)
           phi(k)=phi_i+dphi*(j*1.0-1.0)

        else

           teta(k)=teta_i+dteta*(i*1.0-1.0)
           phi(k)=pi-phi(N_f_T-k+1)

        endif

     enddo
  enddo

  !        phi(:)=pi/2.0d0
  k=0       

  !        teta(N_f_h+1:N_f)=teta(1:N_f_h)
  !        phi(N_f_h+1:N_f)=pi-phi(1:N_f_h)
  do i=1,N_f_T
     if (i .le. N_f_T/2) then
        X_att_T(1,i)=1.20d0*b_sp*cos(teta(i))*sin(phi(i)/1.0d0)!+xc0(1)
        X_att_T(2,i)=1.20d0*b_sp*sin(teta(i))*sin(phi(i)/1.0d0)!+xc0(2)
        X_att_T(3,i)=1.20d0*a_sp*cos(phi(i)/1.0)!+xc0(3)
     else 
        X_att_T(3,i)=-1.20d0*a_sp*cos(phi(N_f_T-i+1)/2.50d0)!+xc0(3)
        X_att_T(1,i)=1.20d0*b_sp*cos(teta(i))*sin(phi(N_f_T-i+1)/2.50d0)!+xc0(1)
        X_att_T(2,i)=1.20d0*b_sp*sin(teta(i))*sin(phi(N_f_T-i+1)/2.50d0)!+xc0(2)
     endif
  enddo
  !        X_att(1,N_f_h+1:N_f)=X_att(1,1:N_f_h)
  !        X_att(2,N_f_h+1:N_f)=X_att(2,1:N_f_h)
  !        X_att(3,N_f_h+1:N_f)=-X_att(3,1:N_f_h)  


  !!         call ellips_min(xc0(1),xc0(2),xc0(3),a_cor,b_cor,L_F(1),nn_cont)
  !!         print *, "LEngth from center is ", L_F(1)

  !        do nf=1,N_f_T
  !         call ellips_min(X_att_T(1,nf)+xc0(1),X_att_T(2,nf)+xc0(2),X_att_T(3,nf)+xc0(3),b_cor-1.0d0,a_cor-1.0d0,L_F_T(nf),nn_cont)
  !        if (L_F_T(nf)<1.0d0) then
  !             L_F_T(nf)=1.0d0
  !         endif 
  !         print *, "LEngth is ", L_F_T(nf)
  !        enddo

  !        L_FF=L_F_T;L_F=L_F_T;
  do i=1,N_f_T        
     !        x(1:N0,i)=(s(1:N0)+r_MTOC)*cos(teta(i))*sin(phi(i))
     !        y(1:N0,i)=(s(1:N0)+r_MTOC)*sin(teta(i))*sin(phi(i))
     !        z(1:N0,i)=a_SP+r_MTOC+(s(1:N0)+r_MTOC)*cos(phi(i))
     x_T(1:N0,i)=s(1:N0)*cos(teta(i))*sin(phi(i))*L_F_T(i)+X_att_T(1,i)
     y_T(1:N0,i)=s(1:N0)*sin(teta(i))*sin(phi(i))*L_F_T(i)+X_att_T(2,i)
     z_T(1:N0,i)=s(1:N0)*cos(phi(i))*L_F_T(i)+X_att_T(3,i)

     !        X_rot(1,i)=X_att(1,i)-0.25d0*cos(teta(i))*sin(phi(i))
     !        X_rot(2,i)=X_att(2,i)-0.25d0*sin(teta(i))*sin(phi(i))
     !        X_rot(3,i)=X_att(3,i)-0.25d0*cos(phi(i))
  enddo

  x(1:N0,1:N_f)=x_T(1:N0,id*N_f+1:(id+1)*N_f);
  y(1:N0,1:N_f)=y_T(1:N0,id*N_f+1:(id+1)*N_f);
  z(1:N0,1:N_f)=z_T(1:N0,id*N_f+1:(id+1)*N_f);

  X_att(1:3,1:N_f)=X_att_T(1:3,id*N_f+1:(id+1)*N_f)

  print *," PASSED LOCAL ALLOCATION" 
  print *, " ID is .......", ID
  x0=x;y0=y;z0=z;        
  !        x=-z0;z=x0;y=y0;
  x=x+xc0(1);y=y+xc0(2);z=z+xc0(3)
  x0=x;y0=y;z0=z;




  !        do nf=1,N_f
  !            print *, "att is ", X_att(1:3,nf),sqrt(sum(X_att(1:3,nf)**2))
  !        enddo


  !        x(1:N0,N_f_h+1:N_f)=x(1:N0,1:N_f_h)
  !        y(1:N0,N_f_h+1:N_f)=y(1:N0,1:N_f_h)
  !        z(1:N0,N_f_h+1:N_f)=-z(1:N0,1:N_f_h)    






  ! teta_i=-pi/1.05; teta_f=pi/1.05
  ! phi_i=-1.0*pi/3.0;phi_f=1.0*pi/3.0\
  !        phi_i=pi/6.0;phi_f=pi/2.0;
  ! dteta=(teta_f-teta_i)/(N_teta*1.0-1.0)
  ! dphi=(phi_f-phi_i)/(N_phi*1.0-1.0+1.0e-12)
  !dphi=0.0;
  ! do j=1,N_phi
  !     do i=1,N_teta;

  ! teta0=teta_i+dteta*(i*1.0-1.0)
  ! phi0=phi_i+ +dphi*(j*1.0-1.0)
  !
  ! x(1:N0,(j-1)*N_teta+i)=s(1:N0)*cos(teta0)*cos(phi0);
  !        y(1:N0,(j-1)*N_teta+i)=s(1:N0)*sin(teta0)*cos(phi0);
  ! z(1:N0,(j-1)*N_teta+i)=s(1:N0)*sin(phi0);


  !     enddo;
  ! enddo;
  !        do i=1,N_f
  !        print *,x(1,i),y(1,i),z(1,i),dteta,dphi

  !        enddo

  ! do i=1,N_f

  ! x(1:N0,i)=s(1:N0)
  ! y(1:N0,i)=(i-1)*0.50
  ! z(1:N0,i)=0.0
  ! enddo

  !---------------------------------------------------------------------------!
  !------------- positions of evaluating the bulk velocity -------------------!
!  do i=1,N_teta_bulk
!     teta_bulk(i)=2.0d0*pi*(i*1.0d0-1.0d0)/(N_teta_bulk*1.0d0)+2.0d0*pi/(N_teta*1.0d0*2.0d0)
!  enddo

!  do i=1,N_phi_bulk
!     phi_bulk(i)=acos(2.0d0/(N_phi_bulk*2.0d0)+(i*1.0d0-1.0d0)*2.0d0/(N_phi_bulk*1.0d0)-1.0d0)
!  enddo

!  do i=1,N_r_bulk
     ! s_bulk(i)=((0.90d0)/(N_r_bulk*1.0d0))*i
!     s_bulk(i)=((1.750*b_cor-1.0d0)/(N_r_bulk*1.0d0))*i+1.10
!  enddo
!  print *, "MAXIMUM s_bulk is ...", maxval(s_bulk)

  k=0

!  do i=1,N_r_bulk
!     do j=1,N_teta_bulk
!        do nf=1,N_phi_bulk
!           k=k+1
           !           x_bulk(k)=b_cor*s_bulk(i)*cos(teta_bulk(j))*sin(phi_bulk(nf))
           !           y_bulk(k)=b_cor*s_bulk(i)*sin(teta_bulk(j))*sin(phi_bulk(nf))
           !           z_bulk(k)=a_cor*s_bulk(i)*cos(phi_bulk(nf))

!           x_bulk(k)=b_sp*s_bulk(i)*cos(teta_bulk(j))*sin(phi_bulk(nf))
!           y_bulk(k)=b_sp*s_bulk(i)*sin(teta_bulk(j))*sin(phi_bulk(nf))
!           z_bulk(k)=-a_sp*s_bulk(i)*cos(phi_bulk(nf))


!        enddo
!     enddo
!  enddo

  call weights2(N0,n_s,n_half,m+1,s,C_T0)

  C_T_B1=C_T0(:,:,1:n_half);
  C_T_B2=C_T0(:,:,N0-n_half+1:N0); 
  C_T=C_T0


  do i=1,n_t
     time_1(i)=(i-1)*1.0-n_t*1.0
     time_2(i)=time_1(i);
  enddo
  time_2(n_t+1)=0.0
  if (n_t .eq. 2) then
     c_3=(/1.5, -0.5/)
  elseif (n_t .eq. 3) then
     c_3(1)=2.25;c_3(2)=-2.0;c_3(3)=0.75
  else
     print *, "Please check the input file. n_t should only take values of 1 or 2"
     stop
  endif

  call weights1(xi,time_1,n_t,1,c_1T)
  c_1(:)=c_1T(:,1)
  call weights1(xi,time_2,n_t+1,2,c_2T)
  c_2(:)=c_2T(:,2)

  do nf=1,N_f;

     call deriv_O_n(x(1:N0,nf),N0, n_s,C_T0,m, x_ns0(1:m,1:N0,nf),L_F(nf));
     call deriv_O_n(y(1:N0,nf),N0, n_s,C_T0,m, y_ns0(1:m,1:N0,nf),L_F(nf));
     call deriv_O_n(z(1:N0,nf),N0, n_s,C_T0,m, z_ns0(1:m,1:N0,nf),L_F(nf));

     xs0(1,:,nf)=x_ns0(1,:,nf);
     ys0(1,:,nf)=y_ns0(1,:,nf);
     zs0(1,:,nf)=z_ns0(1,:,nf);

     !   do i=1,n_t;
     !       xt(i,1:N0,nf)=x(1:N0,nf);
     !       yt(i,1:N0,nf)=y(1:N0,nf);
     !       zt(i,1:N0,nf)=z(1:N0,nf);

     !       xst(i,1:N0,nf)=xs0(1,1:N0,nf);
     !       yst(i,1:N0,nf)=ys0(1,1:N0,nf);
     !       zst(i,1:N0,nf)=zs0(1,1:N0,nf);

     !   enddo ;


     !***************************************************!
     !***************Rotating the fibers*****************!


     X_att(1,nf)=x(1,nf)
     X_att(2,nf)=y(1,nf)
     X_att(3,nf)=z(1,nf)

     X_att_2(1,nf)=x(2,nf)
     X_att_2(2,nf)=y(2,nf)
     X_att_2(3,nf)=z(2,nf)
     dxc1(1:3,nf)=X_att(1:3,nf)-xc0(1:3)
     dxc2(1:3,nf)=X_att_2(1:3,nf)-xc0(1:3)

  enddo;

  X_att0=X_att;X_att0_2=X_att_2
  x0=x;y0=y;z0=z;
  dxc1p=dxc1;dxc2p=dxc2;

  !************Forward/backward Euler first step ********!
  xp=x;yp=y;zp=z;
  xpp=x;ypp=y;zpp=z;


  Omega_Tp(:,1)=0.0d0;
  Omega_T0_Tp(:,1)=0.0d0;
  Omega_T0_Bp(:,1)=0.0d0; 
  Omega_HDp(:,1)=0.0d0
  a_0=0.0;a_1=1.0;a_2=1.0

  !******************************************************!
  !**************Cortical Pulling sites******************!
  !******************************************************!
  k=0
  do j=1,nt_site-1
     do i=1,ntt_site
        k=k+1
        x_site(k)=(b_cor-1.0d0)*cos(2.0d0*pi*(1.0d0*j-1.0d0+0.5d0)/(nt_site*1.0d0))*sin(pi*(1.0d0*i-1.0d0+0.5d0)/(ntt_site+1));
        y_site(k)=(b_cor-1.0d0)*sin(2.0d0*pi*(1.0d0*j-1.0d0+0.5d0)/(nt_site*1.0d0))*sin(pi*(1.0d0*i-1.0d0+0.5d0)/(ntt_site+1));
        z_site(k)=(a_cor-1.0d0)*cos(pi*(1.0d0*i-1.0d0+0.5d0)/(ntt_site+1));
     enddo
  enddo



20 format(4F10.3)


