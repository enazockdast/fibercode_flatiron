	program test_pvFmm
        use, intrinsic :: iso_c_binding

        implicit none

        !! variable
        integer :: i,mpi_rank,mpi_size
	integer (C_SIZE_T), parameter :: nprocs=2,nsrc=4,nsrc_seq=nsrc*nprocs
        real (C_DOUBLE), allocatable, target, dimension(:,:) :: src,src2,src_seq

        !! allocate/init
        allocate(src(nsrc,1),src2(nsrc,1))

        call pvFMM_MPI_INIT(mpi_rank,mpi_size)

        allocate(src_seq(nsrc_seq,1))

        do i=1,nsrc
           src(i,1)=10*mpi_rank+i
        end do

        if( mpi_rank .eq. 0) then
           print *, "src:",src
        end if

        call  pvfmm_mpi_gather( src(:,1),nsrc,src_seq(:,1),nsrc,0) !recv_count is per proc
        if (mpi_rank .eq. 0) then
        src_seq=src_seq*2
        endif

        call  pvfmm_mpi_scatter(src_seq(:,1),nsrc,src(:,1),nsrc,0)
        
       ! if( mpi_rank .eq. 1) then
           print *, "src is now:",src
        !end if
	print *,"proc id is", mpi_rank
	call pvfmm_mpi_barrier()

	call pvfmm_mpi_reduce(src(:,1),src2(:,1),nsrc,0)
	if (mpi_rank .eq. 0) then
	print *, "src2 is ", src2 
	endif

        call pvfmm_mpi_bcast(src_seq,nsrc_seq,0)


        print *, "src_seq is ", src_seq
        call pvfmm_mpi_barrier()
        print *, "---------------------------------"

        call pvFMM_MPI_FINALIZE()
	end program test_pvFmm

