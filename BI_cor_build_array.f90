	module BI_cor_build_array

	implicit none
	
	contains 
	
	subroutine cor_T(T,nt,ntt,gauss_n,xm,ym,zm,x_g,y_g,z_g,h,n_N, con, &
	 I1,I2,I3,A_S,gauss_w,F)
	
	real*8, dimension (:) :: xm,ym,zm
	real*8, dimension (:) :: x_g,y_g,z_g,h
	real*8, dimension (:,:) :: n_N
	real*8 ::gauss_w(:),F(:,:,:)
	integer, dimension (:,:) :: con
	real*8, allocatable, dimension(:,:,:,:) ::  A
	real*8, dimension(:,:) :: T
	real*8, dimension (3,3) :: k_ij,b_ij,k2_ij
	
	integer :: i,j,k,l,m,n,k2
	integer :: n_node,n_element,node,jj,nt,ntt,gauss_n
	real*8 :: dr,dr5,dum,rr,nx,ny,nz,dot,dot2
	real*8 :: I1,I2,I3,A_S,pi
	
	
	n_node=(nt-1)*ntt;n_element=(nt-1)*(ntt-1)
	
	pi=4.0*atan(1.0)
	
	allocate (A(n_node,n_node,3,3))
	

	print *,I1,I2,I3,A_S
	print *,gauss_w
	T(:,:)=0.0;
	do i=1,n_node;
		
	!print *,i
		
		do j=1,n_element;
			
			do node=1,4;
				jj=con(j,node);
				
				
		   
				do k=1,gauss_n;
					do l=1,gauss_n;
					
					k2=(j-1)*gauss_n**2+(k-1)*gauss_n+l
					
				    nx=n_N(1,k2);
					ny=n_N(2,k2);
					nz=n_N(3,k2);
						
						
					dr=sqrt((xm(i)-x_g(k2))**2+(ym(i)-y_g(k2))**2+&
					& (zm(i)-z_g(k2))**2)+1e-8;
					
					dr5=dr**5
				   
					dot=(xm(i)-x_g(k2))*nx+&
					& (ym(i)-y_g(k2))*ny+(zm(i)-z_g(k2))*nz;

				   rr=sqrt(x_g(k2)**2+y_g(k2)**2+z_g(k2)**2);
				   
				   k_ij(1,1)=(xm(i)-x_g(k2))*(xm(i)-x_g(k2))*dot/dr5;
				   k_ij(2,1)=(ym(i)-y_g(k2))*(xm(i)-x_g(k2))*dot/dr5;
				   k_ij(3,1)=(zm(i)-z_g(k2))*(xm(i)-x_g(k2))*dot/dr5;
				   k_ij(1,2)=k_ij(2,1);
				   k_ij(2,2)=(ym(i)-y_g(k2))*(ym(i)-y_g(k2))*dot/dr5;
				   k_ij(3,2)=(zm(i)-z_g(k2))*(ym(i)-y_g(k2))*dot/dr5;
				   k_ij(1,3)=k_ij(3,1);
				   k_ij(2,3)=k_ij(3,2);
				   k_ij(3,3)=(zm(i)-z_g(k2))*(zm(i)-z_g(k2))*dot/dr5;
				   


				   k2_ij=k_ij*h(k2)*F(k,l,node);
				   
				   A(i,jj,1,1)=gauss_w(k)*k2_ij(1,1)*gauss_w(l)+A(i,jj,1,1);
				   A(i,jj,1,2)=gauss_w(k)*k2_ij(1,2)*gauss_w(l)+A(i,jj,1,2);
				   A(i,jj,1,3)=gauss_w(k)*k2_ij(1,3)*gauss_w(l)+A(i,jj,1,3);
				   A(i,jj,2,2)=gauss_w(k)*k2_ij(2,2)*gauss_w(l)+A(i,jj,2,2);
				   A(i,jj,2,3)=gauss_w(k)*k2_ij(2,3)*gauss_w(l)+A(i,jj,2,3);
				   A(i,jj,3,3)=gauss_w(k)*k2_ij(3,3)*gauss_w(l)+A(i,jj,3,3);
				   A(i,jj,3,2)=A(i,jj,2,3);
				   A(i,jj,3,1)=A(i,jj,1,3);
				   A(i,jj,2,1)=A(i,jj,1,2);
				   
				   
				   
					enddo;
				enddo;
			enddo;
		
			 
		enddo;




		 do  m=1,3;
			 do n=1,3;
				 A(i,i,m,n)=0.0;
				 dum=0.0;
				 do l=1,n_node;
					 dum=dum+A(i,l,m,n);
				 enddo;
				 A(i,i,m,n)=-dum;
			 enddo;
		 enddo;

	enddo;





	A=-(3.0/(4.0*pi))*A;

	do i=1,n_node;
		
		
		do j=1,n_node;
			
			T((i-1)*3+1,(j-1)*3+1)=A(i,j,1,1);
			T((i-1)*3+1,(j-1)*3+2)=A(i,j,1,2);
			T((i-1)*3+1,(j-1)*3+3)=A(i,j,1,3);
			
			T((i-1)*3+2,(j-1)*3+1)=A(i,j,2,1);
			T((i-1)*3+2,(j-1)*3+2)=A(i,j,2,2);
			T((i-1)*3+2,(j-1)*3+3)=A(i,j,2,3);
			
			T((i-1)*3+3,(j-1)*3+1)=A(i,j,3,1);
			T((i-1)*3+3,(j-1)*3+2)=A(i,j,3,2);
			T((i-1)*3+3,(j-1)*3+3)=A(i,j,3,3);
		enddo;
	enddo;
	
	do i=1,n_node;
    T((i-1)*3+1,(i-1)*3+1)=1.0+T((i-1)*3+1,(i-1)*3+1);
    T((i-1)*3+2,(i-1)*3+2)=1.0+T((i-1)*3+2,(i-1)*3+2);
    T((i-1)*3+3,(i-1)*3+3)=1.0+T((i-1)*3+3,(i-1)*3+3);
	enddo;
	
	end subroutine cor_T
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	  subroutine SPcor_T(T_SPcor,nt_1,ntt_1,nt_2,ntt_2, &
	  gauss_n,xm,ym,zm,x_g,y_g,z_g,h,n_N, con,gauss_w,F)
	 
	
	real*8, dimension (:) :: xm,ym,zm
	real*8, dimension (:) :: x_g,y_g,z_g,h
	real*8, dimension (:,:) :: n_N
	real*8 ::gauss_w(:),F(:,:,:)
	integer, dimension (:,:) :: con
	real*8, allocatable, dimension(:,:,:,:) ::  A
	real*8, dimension(:,:) :: T_SPcor 
	real*8, dimension (3,3) :: k_ij,k2_ij
	
	integer :: i,j,k,l,m,n,k2
	integer :: n_node_1,n_node_2,n_element_1,n_element_2
	integer :: node,jj,nt_1,ntt_1,nt_2,ntt_2,gauss_n
	real*8 :: dr,dr5,dum,rr,nx,ny,nz,dot,dot2
	real*8 :: I1,I2,I3,A_S,pi
	
	
	n_node_2=(nt_2-1)*ntt_2;n_element_1=(nt_1-1)*(ntt_1-1)
	n_node_1=(nt_1-1)*ntt_1;n_element_2=(nt_2-1)*(ntt_2-1)
	
	pi=4.0*atan(1.0)
	
	allocate (A(n_node_2,n_node_1,3,3))
	
	A(:,:,:,:)=0.0;

	T_SPcor(:,:)=0.0;
	do i=1,n_node_1;
		
	!print *,i
		
		do j=1,n_element_2;
			
			do node=1,4;
				jj=con(j,node);
				
				
		   
				do k=1,gauss_n;
					do l=1,gauss_n;
					k2=(j-1)*gauss_n**2+(k-1)*gauss_n+l
				    nx=n_N(k2,1);
					ny=n_N(k2,2);
					nz=n_N(k2,3);
						
						
					dr=sqrt((xm(i)-x_g(k2))**2+(ym(i)-y_g(k2))**2+&
					& (zm(i)-z_g(k2))**2)+1e-8;
					
					dr5=dr**5
				   
					dot=(xm(i)-x_g(k2))*nx+&
					& (ym(i)-y_g(k2))*ny+(zm(i)-z_g(k2))*nz;

				   rr=sqrt(x_g(k2)**2+y_g(k2)**2+z_g(k2)**2);
				   
				   k_ij(1,1)=(xm(i)-x_g(k2))*(xm(i)-x_g(k2))*dot/dr5;
				   k_ij(2,1)=(ym(i)-y_g(k2))*(xm(i)-x_g(k2))*dot/dr5;
				   k_ij(3,1)=(zm(i)-z_g(k2))*(xm(i)-x_g(k2))*dot/dr5;
				   k_ij(1,2)=k_ij(2,1);
				   k_ij(2,2)=(ym(i)-y_g(k2))*(ym(i)-y_g(k2))*dot/dr5;
				   k_ij(3,2)=(zm(i)-z_g(k2))*(ym(i)-y_g(k2))*dot/dr5;
				   k_ij(1,3)=k_ij(3,1);
				   k_ij(2,3)=k_ij(3,2);
				   k_ij(3,3)=(zm(i)-z_g(k2))*(zm(i)-z_g(k2))*dot/dr5;
				   


				   k2_ij=k_ij*h(k2)*F(k,l,node);
				   
				   A(i,jj,1,1)=gauss_w(k)*k2_ij(1,1)*gauss_w(l)+A(i,jj,1,1);
				   A(i,jj,1,2)=gauss_w(k)*k2_ij(1,2)*gauss_w(l)+A(i,jj,1,2);
				   A(i,jj,1,3)=gauss_w(k)*k2_ij(1,3)*gauss_w(l)+A(i,jj,1,3);
				   A(i,jj,2,2)=gauss_w(k)*k2_ij(2,2)*gauss_w(l)+A(i,jj,2,2);
				   A(i,jj,2,3)=gauss_w(k)*k2_ij(2,3)*gauss_w(l)+A(i,jj,2,3);
				   A(i,jj,3,3)=gauss_w(k)*k2_ij(3,3)*gauss_w(l)+A(i,jj,3,3);
				   A(i,jj,3,2)=A(i,jj,2,3);
				   A(i,jj,3,1)=A(i,jj,1,3);
				   A(i,jj,2,1)=A(i,jj,1,2);
				   
				   
				   
					enddo;
				enddo;
			enddo;
		
			 
		enddo;
	enddo

	do i=1,n_node_1;

		do j=1,n_node_2;
		

			
			T_SPcor((i-1)*3+1,(j-1)*3+1)=A(i,j,1,1);
			T_SPcor((i-1)*3+1,(j-1)*3+2)=A(i,j,1,2);
			T_SPcor((i-1)*3+1,(j-1)*3+3)=A(i,j,1,3);
			
			T_SPcor((i-1)*3+2,(j-1)*3+1)=A(i,j,2,1);
			T_SPcor((i-1)*3+2,(j-1)*3+2)=A(i,j,2,2);
			T_SPcor((i-1)*3+2,(j-1)*3+3)=A(i,j,2,3);
			
			T_SPcor((i-1)*3+3,(j-1)*3+1)=A(i,j,3,1);
			T_SPcor((i-1)*3+3,(j-1)*3+2)=A(i,j,3,2);
			T_SPcor((i-1)*3+3,(j-1)*3+3)=A(i,j,3,3);
		enddo;
	enddo;
	
	
	end subroutine SPcor_T
	
	
	
	
	end module BI_cor_build_array


