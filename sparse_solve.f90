  
  !----------------------------------------------------------------------
  ! This program takes matrix  in a CSS sparse format (A,IA,JA) and the right-
  ! hand side (B) and gives the solution (X) to A * X = B 
  !---------------------------------------------------------------------
  
  INCLUDE 'mkl_pardiso.f90'
  Module sparse_solve
    ! use initialize
    implicit none 

  contains 
    subroutine sparse_solve_1(n,ia,ja,a,b,x1)
      USE mkl_pardiso
      IMPLICIT NONE
      INTEGER, PARAMETER :: dp = KIND(1.0D0)
      !.. Internal solver memory pointer 
      TYPE(MKL_PARDISO_HANDLE), ALLOCATABLE  :: pt(:)
      !.. All other variables
      INTEGER maxfct, mnum, mtype, phase, n, nrhs, error, msglvl
      INTEGER error1
      INTEGER, ALLOCATABLE :: iparm( : )
      INTEGER :: ia( : )
      INTEGER :: ja( : )
      REAL(KIND=DP) :: a( : )
      REAL(KIND=DP) :: b( : )
      REAL(KIND=DP) :: x1( : )
      INTEGER i, idum(1)
      REAL(KIND=DP) ddum(1)
      real (kind=dp) :: DS

      nrhs = 1 
      maxfct = 1 
      mnum = 1


      ALLOCATE( iparm ( 64 ) )

      do i = 1, 64
         iparm(i) = 0
      end do

      iparm(1) = 1 ! no solver default
      iparm(2) = 2 ! fill-in reordering from METIS
      iparm(4) = 0 ! no iterative-direct algorithm
      iparm(5) = 0 ! no user fill-in reducing permutation
      iparm(6) = 0 ! =0 solution on the first n compoments of x
      iparm(8) = 2 ! numbers of iterative refinement steps
      iparm(10) = 13 ! perturbe the pivot elements with 1E-13
      iparm(11) = 1 ! use nonsymmetric permutation and scaling MPS
      iparm(13) = 1 ! maximum weighted matching algorithm is switched-off (default for symmetric). Try iparm(13) = 1 in case of inappropriate accuracy
      iparm(14) = 1 ! NO Output: number of perturbed pivots
      iparm(18) = 0 ! NO Output: number of nonzeros in the factor LU
      iparm(19) = 0 ! NO Output: Mflops for LU factorization
      iparm(20) = 0 ! Output: Numbers of CG Iterations

      iparm(28)=0


      error  = 0 ! initialize error flag
      msglvl = 0 ! doesn't print statistical information. change the value to "1" or printing
      mtype  = 11 ! real unsymmetric, indefinite

      !.. Initiliaze the internal solver memory pointer. This is only
      ! necessary for the FIRST call of the PARDISO solver.

      ALLOCATE ( pt ( 64 ) )
      do i = 1, 64
         pt( i )%DUMMY =  0 
      end do

      !.. Reordering and Symbolic Factorization, This step also allocates
      ! all memory that is necessary for the factorization

      phase = 11 ! only reordering and symbolic factorization

      CALL pardiso (pt, maxfct, mnum, mtype, phase, n, a, ia, ja, &
           idum, nrhs, iparm, msglvl, ddum, ddum, error)

      !WRITE(*,*) 'Reordering completed ... '
      IF (error /= 0) THEN
         WRITE(*,*) 'The following ERROR was detected: ', error
         GOTO 1000
      END IF
      !WRITE(*,*) 'Number of nonzeros in factors = ',iparm(18)
      !WRITE(*,*) 'Number of factorization MFLOPS = ',iparm(19)

      !.. Factorization.
      phase = 22 ! only factorization
      CALL pardiso (pt, maxfct, mnum, mtype, phase, n, a, ia, ja, &
           idum, nrhs, iparm, msglvl, ddum, ddum, error)
      !WRITE(*,*) 'Factorization completed ... '
      IF (error /= 0) THEN
         WRITE(*,*) 'The following ERROR was detected: ', error
         GOTO 1000
      ENDIF

      !.. Back substitution and iterative refinement
      iparm(8) = 2 ! max numbers of iterative refinement steps
      phase = 33 ! only solving

      CALL pardiso (pt, maxfct, mnum, mtype, phase, n, a, ia, ja, &
           idum, nrhs, iparm, msglvl, b, x1, error)
      !WRITE(*,*) 'Solve completed ... '
      IF (error /= 0) THEN
         WRITE(*,*) 'The following ERROR was detected: ', error
         GOTO 1000
      ENDIF
      !WRITE(*,*) 'The solution of the system is '

1000  CONTINUE
      !.. Termination and release of memory
      phase = -1 ! release internal memory
      CALL pardiso (pt, maxfct, mnum, mtype, phase, n, ddum, idum, idum, &
           idum, nrhs, iparm, msglvl, ddum, ddum, error1)

      IF ( ALLOCATED( iparm ) )   DEALLOCATE( iparm )

    END subroutine sparse_solve_1
    !**************************************************************************!
    !**************************************************************************!
    !**************************************************************************!

    subroutine MT_SPARSE(nf00,TID0)

      use initialize
      
      implicit none


      integer :: j0, j1, j2, j3, j4, k2,kk,ii,nf00,TID0


      XX(:,nf00)=0.0;
      F_X(:,nf00)=0.0

      a_D(:,nf00)=0.0;
      ia_D(:,nf00)=0;
      ja_D(:,nf00)=0;
      C_T=C_T0;

      ! C_T(:,:,N-n_half+1:N)=C_T_B2(:,:,1:n_half);
      N=N0

      do kk=1,3*N+1
         ia_D(kk,nf00)=(kk-1)*3*n_s+1  
      enddo

      do kk=3,N-2;

         do k2=1,3;

            do ii=1,n_s;

               j0=k2+(kk-1)*3;
               if (kk<n_half+1) then;
                  j2=1+(ii-1)*3;

               elseif ( (kk>n_half) .and. (kk<N-n_half+1)) then          
                  j2=j0+(ii-n_half-1)*3-k2+1;

               else
                  j2=3*(N-n_s+ii-1)+1;

               endif;
               j3=(kk-1)*3*n_s*3+(k2-1)*3*n_s+1+(ii-1)*3;

               if ( k2 .eq. 1) then 

                  ja_D(j3,nf00)=j2;  
                  a_D(j3,nf00)=1.0*C_T(5,ii,kk)*(1.0+x_ns(1,kk,nf00)*x_ns(1,kk,nf00))/L_F(nf00)**4!-beta(nf)*mu(nf)*C_T(2,i,k)*s(k)/L_FF(nf);
                  a_D(j3+1,nf00)=1.0*C_T(5,ii,kk)*(0.0+y_ns(1,kk,nf00)*x_ns(1,kk,nf00))/L_F(nf00)**4;
                  a_D(j3+2,nf00)=1.0*C_T(5,ii,kk)*(0.0+z_ns(1,kk,nf00)*x_ns(1,kk,nf00))/L_F(nf00)**4;
                  ja_D(j3+1,nf00)=j2+1;
                  ja_D(j3+2,nf00)=j2+2;



               elseif (k2 .eq. 2) then
                  a_D(j3+0,nf00)=1.0*C_T(5,ii,kk)*(0.0+y_ns(1,kk,nf00)*x_ns(1,kk,nf00))/L_F(nf00)**4;
                  a_D(j3+1,nf00)=1.0*C_T(5,ii,kk)*(1.0+y_ns(1,kk,nf00)*y_ns(1,kk,nf00))/L_F(nf00)**4!-beta(nf)*mu(nf)*C_T(2,i,k)*s(k)/L_FF(nf);
                  a_D(j3+2,nf00)=1.0*C_T(5,ii,kk)*(0.0+y_ns(1,kk,nf00)*z_ns(1,kk,nf00))/L_F(nf00)**4;
                  ja_D(j3,nf00)=j2;
                  ja_D(j3+1,nf00)=j2+1;
                  ja_D(j3+2,nf00)=j2+2; 

               else
                  a_D(j3+0,nf00)=1.0*C_T(5,ii,kk)*(0.0+x_ns(1,kk,nf00)*z_ns(1,kk,nf00))/L_F(nf00)**4;
                  a_D(j3+1,nf00)=1.0*C_T(5,ii,kk)*(0.0+y_ns(1,kk,nf00)*z_ns(1,kk,nf00))/L_F(nf00)**4;
                  a_D(j3+2,nf00)=1.0*C_T(5,ii,kk)*(1.0+z_ns(1,kk,nf00)*z_ns(1,kk,nf00))/L_F(nf00)**4!-beta(nf)*mu(nf)*C_T(2,i,k)*s(k)/L_FF(nf);
                  ja_D(j3+1,nf00)=j2+1;
                  ja_D(j3+2,nf00)=j2+2;
                  ja_D(j3,nf00)=j2;

               endif;


            enddo;
         enddo;



         if (kk<n_half+1) then;

            j4=(kk-1)*3*3*n_s+(kk-1)*3+1;
         elseif ( (kk>n_half) .and. (kk<N-n_half+1))  then           

            j4=(kk-1)*3*3*n_s+n_half*3+1;
         else     
            j4=(kk-1)*3*3*n_s+(kk-N+n_s-1)*3+1;             
         endif;

         a_D(j4,nf00)=1.0*a_D(j4,nf00)+alpha*mu(nf00)/dt0;

         a_D(j4+n_s*3+1,nf00)=1.0*a_D(j4+n_s*3+1,nf00)+alpha*mu(nf00)/dt0;

         a_D(j4+n_s*3*2+2,nf00)=1.0*a_D(j4+n_s*3*2+2,nf00)+alpha*mu(nf00)/dt0;

         F_X(3*(kk-1)+1,nf00)=-dxdt(kk,nf00)*mu(nf00)/dt0+1.0*mu(nf00)*U_EXT(1,kk,nf00)+mu(nf00)*beta(nf00)*s(kk)*x_ns(1,kk,nf00);
         F_X(3*(kk-1)+2,nf00)=-dydt(kk,nf00)*mu(nf00)/dt0+1.0*mu(nf00)*U_EXT(2,kk,nf00)+mu(nf00)*beta(nf00)*s(kk)*y_ns(1,kk,nf00); 
         F_X(3*(kk-1)+3,nf00)=-dzdt(kk,nf00)*mu(nf00)/dt0+1.0*mu(nf00)*U_EXT(3,kk,nf00)+mu(nf00)*beta(nf00)*s(kk)*z_ns(1,kk,nf00);

      enddo;





      ! *********** imposing direction of MT rotates with angular rotation of the Spindle ******* !

      kk=1;
      do ii=1,3*n_s
         ja_D(ii,nf00)=ii
         ja_D(ii+3*n_s,nf00)=ii
         ja_D(ii+3*2*n_s,nf00)=ii


      enddo

      a_D(1:3*n_s*3,nf00)=0.0;
      a_D(1,nf00) = 1.0/dt0;
      ja_D(1,nf00)=1;
      a_D(3*n_s+2,nf00)=1.0/dt0;
      ja_D(3*n_s+2,nf00)=2;
      a_D(3*n_s*2+3,nf00)=1.0/dt0;
      ja_D(3*n_s*2+3,nf00)=3;

      F_X(1,nf00)=X_att(1,nf00)/dt0+Vatt(1,nf00);
      F_X(2,nf00)=X_att(2,nf00)/dt0+Vatt(2,nf00);
      F_X(3,nf00)=X_att(3,nf00)/dt0+Vatt(3,nf00);



      ! *********** imposing direction of MT rotates with angular rotation of the Spindle ******* !

      do ii=1,3*n_s
         ja_D(ii+3*3*n_s,nf00)=ii
         ja_D(ii+3*3*n_s+3*n_s,nf00)=ii
         ja_D(ii+3*3*n_s+3*2*n_s,nf00)=ii


      enddo
      a_D(3*n_s*3+1:2*3*n_s*3,nf00)=0.0;
      kk=2;
      a_D(3*n_s*3+3+1,nf00) = 1.0/dt0;
      ja_D(3*n_s*3+3+1,nf00)=4;
      a_D(3*n_s*3+3*n_s+3+2,nf00)=1.0/dt0;
      ja_D(3*n_s*3+3*n_s+3+2,nf00)=5;
      a_D(3*n_s*3+3*n_s*2+3+3,nf00)=1.0/dt0;
      ja_D(3*n_s*3+3*n_s*2+3+3,nf00)=6;

      F_X(3*(kk-1)+1,nf00)=X_att_2(1,nf00)/dt0+Vatt_2(1,nf00)+beta(nf00)*s(kk)*x_ns(1,kk,nf00);
      F_X(3*(kk-1)+2,nf00)=X_att_2(2,nf00)/dt0+Vatt_2(2,nf00)+beta(nf00)*s(kk)*y_ns(1,kk,nf00);
      F_X(3*(kk-1)+3,nf00)=X_att_2(3,nf00)/dt0+Vatt_2(3,nf00)+beta(nf00)*s(kk)*z_ns(1,kk,nf00);


      !********************BC3**********************!
      !*********************************************!
      ! ******** Torque is zero at the end ******** !

      kk = N-1;
      F_X(3*(kk-1)+1,nf00)=0.0;
      F_X(3*(kk-1)+2,nf00)=0.0;
      F_X(3*(kk-1)+3,nf00)=0.0;
      do ii=1,n_s;
         j1=(kk-1)*3*n_s*3+1+(ii-1)*3;
         a_D(j1,nf00)=C_T(3,ii,kk)/L_F(nf00)**2;
         ja_D(j1,nf00)=(N-n_s)*3+(ii-1)*3+1;
         ja_D(j1+1,nf00)=(N-n_s)*3+(ii-1)*3+2;
         ja_D(j1+2,nf00)=(N-n_s)*3+(ii-1)*3+3;

         j1=(kk-1)*3*n_s*3+2+(ii-1)*3+3*n_s;
         a_D(j1,nf00)=C_T(3,ii,kk)/L_F(nf00)**2;
         ja_D(j1-1,nf00)=(N-n_s)*3+(ii-1)*3+1;
         ja_D(j1+0,nf00)=(N-n_s)*3+(ii-1)*3+2;
         ja_D(j1+1,nf00)=(N-n_s)*3+(ii-1)*3+3;


         j1=(kk-1)*3*n_s*3+3+(ii-1)*3+3*n_s*2;
         a_D(j1,nf00)=C_T(3,ii,kk)/L_F(nf00)**2;
         ja_D(j1-2,nf00)=(N-n_s)*3+(ii-1)*3+1;  
         ja_D(j1-1,nf00)=(N-n_s)*3+(ii-1)*3+2;  
         ja_D(j1+0,nf00)=(N-n_s)*3+(ii-1)*3+3;     


      enddo;


      !********************BC4**********************!
      !*********************************************!
      ! ******** Constant Force  ******** !


      kk=N  
      F_X(3*(kk-1)+1,nf00)=F_L(1,nf00);
      F_X(3*(kk-1)+2,nf00)=F_L(2,nf00);
      F_X(3*(kk-1)+3,nf00)=F_L(3,nf00);


      do ii=1,n_s;  
         j1=(kk-1)*3*n_s*3+1+(ii-1)*3+0*3*n_s;
         a_D(j1,nf00)=-C_T(4,ii,kk)/L_F(nf00)**3+T(N,nf00)*C_T(2,ii,kk)/L_F(nf00);
         ja_D(j1+0,nf00)=(N-n_s)*3+(ii-1)*3+1;
         ja_D(j1+1,nf00)=(N-n_s)*3+(ii-1)*3+2;
         ja_D(j1+2,nf00)=(N-n_s)*3+(ii-1)*3+3;

         j1=(kk-1)*3*n_s*3+2+(ii-1)*3+1*3*n_s;
         a_D(j1,nf00)=-C_T(4,ii,kk)/L_F(nf00)**3+T(N,nf00)*C_T(2,ii,kk)/L_F(nf00);
         ja_D(j1-1,nf00)=(N-n_s)*3+(ii-1)*3+1;
         ja_D(j1+0,nf00)=(N-n_s)*3+(ii-1)*3+2;
         ja_D(j1+1,nf00)=(N-n_s)*3+(ii-1)*3+3;

         j1=(kk-1)*3*n_s*3+3+(ii-1)*3+2*3*n_s;
         a_D(j1,nf00)=-C_T(4,ii,kk)/L_F(nf00)**3+T(N,nf00)*C_T(2,ii,kk)/L_F(nf00);
         ja_D(j1-2,nf00)=(N-n_s)*3+(ii-1)*3+1; 
         ja_D(j1-1,nf00)=(N-n_s)*3+(ii-1)*3+2; 
         ja_D(j1+0,nf00)=(N-n_s)*3+(ii-1)*3+3;   

      enddo;




      call  sparse_solve_1(3*N0,ia_D(1:N0*3+1,nf00),ja_D(1:3*n_s*N0*3,nf00),a_D(1:3*n_s*N0*3,nf00),F_X(1:N0*3,nf00),XX(1:N0*3,nf00))

      do ii=1,N


         x(ii,nf00)=XX((ii-1)*3+1,nf00)
         y(ii,nf00)=XX((ii-1)*3+2,nf00)
         z(ii,nf00)=XX((ii-1)*3+3,nf00)
      enddo


20    format(2F20.5)
30    format(6F25.8)
    end subroutine MT_SPARSE
    !**************************************************************************!
    !**************************************************************************!
    !**************************************************************************!

  end module sparse_solve
