!***************************************************************!
! This module cntains the subroutines used to compute           !
! the hydrodynamic interactions between bodies presented as     !
! Stokes equation singularities (Stokeslet, Doublet, Stresslet).!
!***************************************************************! 
module Hydro_Inter
  implicit none 
contains 
  ! This subroutine computes the velocity disturbance field induced
  ! by a distribution of stokeslets from all fibers on each fiber:
  ! through direct summation. This can be replaced with Fast Multiple Method
  ! and calling stfmm library and subroutines within. Here x1, y1, z1 are the
  ! coordinate of the target point and x2, y2 and z2 are the source points. 

  subroutine MT_uinf_single(fx, fy, fz, &
       & x1, y1, z1, x2, y2, z2, N0, N1, ds0 ,u, o, u2, S)
    implicit none 
    INTEGER, PARAMETER :: dp = KIND(1.0D0)
    integer, intent (IN) :: N0, N1
    real (kind=dp), intent (IN) :: ds0(:)
    real (kind=dp) :: u(3), o(3), u2(3),S(3,3)
    real (kind=dp), intent (IN) ::  fx(:), fy(:), fz(:)
    real (kind=dp), dimension (N0) :: dx, dy, dz, dr, idr, idr3, idr5, &
         & dot, W,idr2,W2,A,B
    real (kind=dp), intent (IN), dimension (:) :: x1, y1, z1
    real (kind=dp), intent (IN) :: x2, y2, z2


    u(:)=0.0;
    o(:)=0.0;
    u2(:)=0.0;
    W(1:N0)=ds0(1:N0);
    S(:,:)=0.0;
    ! W(1)=0.50;W(N1)=0.50;


    W2(1:N0)=ds0(1:N0)
    !    W2(1:2)=0.0;W2(N1-1:N1)=0.0;    
    dx(1:N1)=x1(1:N1)-x2;
    dy(1:N1)=y1(1:N1)-y2;
    dz(1:N1)=z1(1:N1)-z2;

    dr(1:N1)=sqrt(dx(1:N1)**2+dy(1:N1)**2+dz(1:N1)**2);

    idr(1:N1)=1.0d0/dr(1:N1);
    idr2(1:N1)=1.0d0/dr(1:N1)**2
    idr3(1:N1)=1.0d0/(dr(1:N1)**3);
    idr5(1:N1)=1.0d0/(dr(1:N1)**5);
    dot(1:N1)=dx(1:N1)*fx(1:N1)+dy(1:N1)*fy(1:N1)+dz(1:N1)*fz(1:N1);
    A(1:N1)=(-5.0d0/2.0d0)*idr5(1:N1)+(3.0d0/2.0d0)*idr2(1:N1)**2
    B(1:N1)=(-1.0d0/2.0d0)*idr2(1:N1)**2

    u(1)=sum(W(1:N1)*(fx(1:N1)*(idr(1:N1)+idr3(1:N1)/3.0d0)+dx(1:N1)*dot(1:N1)*(idr3(1:N1)-idr5(1:N1))))

    u(2)=sum(W(1:N1)*(fy(1:N1)*(idr(1:N1)+idr3(1:N1)/3.0d0)+dy(1:N1)*dot(1:N1)*(idr3(1:N1)-idr5(1:N1))))

    u(3)=sum(W(1:N1)*(fz(1:N1)*(idr(1:N1)+idr3(1:N1)/3.0d0)+dz(1:N1)*dot(1:N1)*(idr3(1:N1)-idr5(1:N1))))


    u2(1)=sum(W(1:N1)*(fx(1:N1)*(0.0*idr(1:N1)+idr3(1:N1)/3.0d0)+dx(1:N1)*dot(1:N1)*(0.0*idr3(1:N1)-idr5(1:N1))))

    u2(2)=sum(W(1:N1)*(fy(1:N1)*(0.0*idr(1:N1)+idr3(1:N1)/3.0d0)+dy(1:N1)*dot(1:N1)*(0.0*idr3(1:N1)-idr5(1:N1))))

    u2(3)=sum(W(1:N1)*(fz(1:N1)*(0.0*idr(1:N1)+idr3(1:N1)/3.0d0)+dz(1:N1)*dot(1:N1)*(0.0*idr3(1:N1)-idr5(1:N1))))



    o(1)=0.75d0*sum(W2(1:N1)*(fy(1:N1)*dz(1:N1)-fz(1:N1)*dy(1:N1))*idr3(1:N1))
    o(2)=0.75d0*sum(W2(1:N1)*(-fx(1:N1)*dz(1:N1)+fz(1:N1)*dx(1:N1))*idr3(1:N1))
    o(3)=0.75d0*sum(W2(1:N1)*(fx(1:N1)*dy(1:N1)-fy(1:N1)*dx(1:N1))*idr3(1:N1))
    !     uy=sum(W(1:N1)*(fy(1:N1)/dr(1:N1)+dy(1:N1)*dot(1:N1)/dr3(1:N1))) 


    S(2,1)=sum(W(1:N1)*(A*dy(1:N1)*dx(1:N1)*idr2(1:N1)*dot(1:N1)*idr(1:N1)+& 
         & B*((fy(1:N1)*dx(1:N1)+fx(1:N1)*dy(1:N1))*idr(1:N1)-2.0d0*dy(1:N1)*dx(1:N1)*dot(1:N1)*idr3(1:N1))))

    S(3,1)=sum(W(1:N1)*(A*dz(1:N1)*dx(1:N1)*idr2(1:N1)*dot(1:N1)*idr(1:N1)+& 
         & B*((fz(1:N1)*dx(1:N1)+fx(1:N1)*dz(1:N1))*idr(1:N1)-2.0d0*dz(1:N1)*dx(1:N1)*dot(1:N1)*idr3(1:N1)))) 

    S(2,3)=sum(W(1:N1)*(A*dy(1:N1)*dz(1:N1)*idr2(1:N1)*dot(1:N1)*idr(1:N1)+& 
         & B*((fy(1:N1)*dz(1:N1)+fz(1:N1)*dy(1:N1))*idr(1:N1)-2.0d0*dy(1:N1)*dz(1:N1)*dot(1:N1)*idr3(1:N1))))         

    S(1,1)=sum(W(1:N1)*(A*(dx(1:N1)*dx(1:N1)*idr2(1:N1)-1.0/3.0)*dot(1:N1)*idr(1:N1)+&
         & B*((fx(1:N1)*dx(1:N1)+fx(1:N1)*dx(1:N1))*idr(1:N1)-2.0d0*dx(1:N1)*dx(1:N1)*dot(1:N1)*idr3(1:N1))))

    S(2,2)=sum(W(1:N1)*(A*(dy(1:N1)*dy(1:N1)*idr2(1:N1)-1.0/3.0)*dot(1:N1)*idr(1:N1)+&
         & B*((fy(1:N1)*dy(1:N1)+fy(1:N1)*dy(1:N1))*idr(1:N1)-2.0d0*dy(1:N1)*dy(1:N1)*dot(1:N1)*idr3(1:N1))))

    S(3,3)=sum(W(1:N1)*(A*(dz(1:N1)*dz(1:N1)*idr2(1:N1)-1.0/3.0)*dot(1:N1)*idr(1:N1)+&
         & B*((fz(1:N1)*dz(1:N1)+fz(1:N1)*dz(1:N1))*idr(1:N1)-2.0d0*dz(1:N1)*dz(1:N1)*dot(1:N1)*idr3(1:N1))))
    S(1,2)=S(2,1);S(1,3)=S(3,1);S(3,2)=S(2,3);
    !     uz=sum(W(1:N1)*(fz(1:N1)/dr(1:N1)+dz(1:N1)*dot(1:N1)/dr3(1:N1))) 

    !           u=u*ds;
    !           o=o*ds
    !           u2=u2*ds
    !           S=S*ds

  end subroutine MT_uinf_single
  !************************************************!
  !************************************************!
  !************************************************!

  subroutine MT_uinf_self(fx,fy,fz,&
       & x,y,z,N1,N0,ds, fac,eps,ux,uy,uz)

    implicit none 
    INTEGER, PARAMETER :: dp = KIND(1.0D0)
    integer, intent (IN) :: N1, N0
    integer :: i
    real (kind=dp), intent (IN), dimension (:) :: x, y, z 
    real (kind=dp), intent (IN), dimension (:) :: fx, fy, fz
    real (kind=dp), dimension (:) :: ux, uy, uz
    real (kind=dp), intent (IN) :: ds(:),fac,eps
    real (kind=dp), dimension (N0) :: W0, W1, dx, dy, dz, dr, dr3, dot

    dr(:)=1.0;dx(:)=0.0;dy(:)=0.0;dz(:)=0.0;
    dr3(:)=1.0;dot(:)=0.0;

    W0(:)=ds(:);!W0(N1)=0.50;W0(1)=0.50;

    do i=1,N1;

       W1=W0;
       W1(i)=0.0;

       dx(1:N1)=x(1:N1)-x(i);
       dy(1:N1)=y(1:N1)-y(i);
       dz(1:N1)=z(1:N1)-z(i);

       dr=sqrt(dx**2+dy**2+dz**2+fac**2*eps);
       dr3=dr**3;
       !     dxdx=dx*dx/dr3+1.0/dr;
       !     dxdy=dx*dy/dr3;
       !     dxdz=dx*dz/dr3;
       !     dydy=dy*dy/dr3+1.0/dr;
       !     dydz=dy*dz/dr3;
       !     dzdz=dz*dz/dr3+1.0/dr;

       dot(1:N1)=(fx(1:N1)*dx(1:N1)+fy(1:N1)*dy(1:N1)+fz(1:N1)*dz(1:N1))

       ux(i)=sum(W1(1:N1)*(fx(1:N1)/dr(1:N1)+ dx(1:N1)*dot(1:N1)/dr3(1:N1)))
       uy(i)=sum(W1(1:N1)*(fy(1:N1)/dr(1:N1)+ dy(1:N1)*dot(1:N1)/dr3(1:N1)))
       uz(i)=sum(W1(1:N1)*(fz(1:N1)/dr(1:N1)+ dz(1:N1)*dot(1:N1)/dr3(1:N1)))

    enddo

    !ux=ux*ds;
    !uy=uy*ds;
    !uz=uz*ds;

  end subroutine MT_uinf_self
  !*********************************************!    
  !*********************************************!    
  !*********************************************!    
  subroutine stokeslet_single(F,Tq,r0,ntrg,x,y,z,ux,uy,uz)

    implicit none
    INTEGER, PARAMETER :: dp = KIND(1.0D0)
    integer, intent (in) :: ntrg
    real (kind=dp), intent(in) :: x(ntrg),y(ntrg),z(ntrg),F(3),Tq(3),r0(3)
    real (kind=dp)             :: ux(ntrg),uy(ntrg),uz(ntrg)
    real (kind=dp),dimension(ntrg) :: dx,dy,dz,dr,dot

    dx(1:ntrg)=x(1:ntrg)-r0(1)
    dy(1:ntrg)=y(1:ntrg)-r0(2)
    dz(1:ntrg)=z(1:ntrg)-r0(3)
    dr(1:ntrg)=sqrt(dx(1:ntrg)**2+dy(1:ntrg)**2+dz(1:ntrg)**2)
    dot(1:ntrg)=F(1)*dx(1:ntrg)+F(2)*dy(1:ntrg)+F(3)*dz(1:ntrg)

    ux(1:ntrg)=F(1)/dr+dot*dx/dr**3+(Tq(2)*dz-Tq(3)*dy)/dr**3
    uy(1:ntrg)=F(2)/dr+dot*dy/dr**3+(Tq(3)*dx-Tq(1)*dz)/dr**3
    uz(1:ntrg)=F(3)/dr+dot*dz/dr**3+(Tq(1)*dy-Tq(2)*dx)/dr**3

  end subroutine stokeslet_single
  !*****************************************************!
  !*****************************************************!
  !*****************************************************!
  subroutine sphere_motion(N0,N,x0,y0,z0,V0,Omega,E,xc0,u_ext)
    implicit none
    INTEGER, PARAMETER :: dp = KIND(1.0D0)
    integer, intent (IN) :: N0, N
    real (kind=dp), intent (IN) :: x0(:), y0(:), z0(:)
    real (kind=dp), intent (IN)  :: E(:,:), V0(:,:), omega(:,:), xc0(:)
    real (kind=dp), dimension (N0) :: r0, x, y, z, A, B, C, D, dot, dot2
    real (kind=dp), dimension (N0,3) ::  u_ext

    x(1:N)=x0(1:N)-xc0(1);
    y(1:N)=y0(1:N)-xc0(2);
    z(1:N)=z0(1:N)-xc0(3);

    r0(1:N0)=1.0d0;

    r0(1:N)=sqrt(x(1:N)**2+y(1:N)**2+z(1:N)**2)+0.0d-2
    A(1:N)=(3.0d0/4.0d0)*(1.0d0/r0(1:N)-1.0d0/r0(1:N)**3);
    B(1:N)=(1.0d0/4.0d0)*(3.0d0/r0(1:N)+1.0d0/r0(1:N)**3);

    C(1:N)=-(5.0d0/2.0d0)*(1.0d0/r0(1:N)**5-1.0d0/r0(1:N)**7);
    D(1:N)=-1.0d0/r0(1:N)**5;

    dot(1:N)=(V0(1,1)*x(1:N)+V0(2,1)*y(1:N)+V0(3,1)*z(1:N))/r0(1:N)**2;
    dot2(1:N)=x(1:N)*x(1:N)*E(1,1)+2.0d0*x(1:N)*y(1:N)*E(1,2)+2.0d0*x(1:N)*z(1:N)*E(1,3)+ &
         & y(1:N)*y(1:N)*E(2,2)+2.0d0*y(1:N)*z(1:N)*E(2,3)+z(1:N)*z(1:N)*E(3,3);

    u_ext(1:N,1)=V0(1,1)*B(1:N)+dot(1:N)*(x(1:N)*A(1:N)) - &
         & (y(1:N)*Omega(3,1)-z(1:N)*Omega(2,1))/r0(1:N)**3+ &
         & C(1:N)*dot2(1:N)*x(1:N)+(E(1,1)*x(1:N)+E(1,2)*y(1:N)+E(1,3)*z(1:N))*(1.0d0+D(1:N));

    u_ext(1:N,2)=V0(2,1)*B(1:N)+dot(1:N)*y(1:N)*A(1:N)-(-x(1:N)*Omega(3,1)+ &
         & z(1:N)*Omega(1,1))/r0(1:N)**3+ &
         & C(1:N)*dot2(1:N)*y(1:N)+(E(2,1)*x(1:N)+E(2,2)*y(1:N)+E(2,3)*z(1:N))*(1.0d0+D(1:N));

    u_ext(1:N,3)=V0(3,1)*B(1:N)+dot*z(1:N)*A(1:N) - &
         & (x(1:N)*Omega(2,1)-y(1:N)*Omega(1,1))/r0(1:N)**3+ &
         & C(1:N)*dot2(1:N)*z(1:N)+(E(3,1)*x(1:N)+E(3,2)*y(1:N)+E(3,3)*z(1:N))*(1.0d0+D(1:N));

  end subroutine sphere_motion


  !*******************************************************!

  subroutine rigid_body_motion(N,x,y,z,r0,U,W,ux,uy,uz)

    implicit none 
    integer, parameter :: dp = kind(1.0d0)
    integer, intent(in) :: N
    real (kind=dp), intent(in) :: x(N),y(N),z(N),r0(3),U(3),W(3) 
    real (kind=dp) :: ux(N),uy(N),uz(N)        
    real (kind=dp) :: dx(N),dy(N),dz(N),dr(N)

    dx=x-r0(1);dy=y-r0(2);dz=z-r0(3);
    dr=sqrt(dx**2+dy**2+dz**2)
    ux(1:N)=U(1)/(dr/dr(1))**0.25+(W(2)*dz(1)-W(3)*dy(1))/(dr/dr(1))**0.25
    uy(1:N)=U(2)/(dr/dr(1))**0.25+(W(3)*dx(1)-W(1)*dz(1))/(dr/dr(1))**0.25
    uz(1:N)=U(3)/(dr/dr(1))**0.25+(W(1)*dy(1)-W(2)*dx(1))/(dr/dr(1))**0.25

  end subroutine rigid_body_motion

  !*******************************************************!
  !*******************************************************!

  ! subroutine manybody_stokeslet (fx,fy,fz,x,y,z,ds,N_T,N_f, N_var, N0, mu, &
  ! ux_self, uy_self, uz_self, & 
  ! & ier, iprec,nparts,source, ifsingle,sigma_sl,ifdouble,& 
  ! & sigma_dl,sigma_dv, ifpot,pot,pre,ifgrad,grad,&
  ! ux,uy,uz)

  ! implicit none 
  ! INTEGER, PARAMETER :: dp = KIND(1.0D0)
  ! integer , intent (IN) :: N_var(:), N_T, N_f, N0
  ! real (kind=dp), intent (IN) :: ds(:), mu(:)
  ! real (kind=dp), intent (IN), dimension (:,:) :: fx, fy, fz, x, y, z
  ! real (kind=dp), intent (OUT), dimension (:,:) :: ux, uy, uz, ux_self, uy_self, uz_self
  ! integer :: ier, iprec, nparts, ifsingle, ifdouble, ifpot, ifgrad
  ! real (kind=dp), dimension (:,:) :: sigma_sl, sigma_dv, sigma_dl, pot, source 
  ! real (kind=dp), dimension (:,:,:) :: grad
  ! real (kind=dp), dimension (:) :: pre
  ! integer :: nf, k, N
  ! iprec=3;
  ! nparts=N_T;
  ! ifsingle=1;
  ! ifdouble=0;
  ! ifpot=1;
  ! ifgrad=0;
  ! k=0;
  ! do nf=1,N_f
  ! N=N_var(nf);
  ! k=k+N;

  ! sigma_sl(1,k-N+1:k)=fx(1:N,nf)*ds(1:N);
  ! sigma_sl(2,k-N+1:k)=fy(1:N,nf)*ds(1:N);
  ! sigma_sl(3,k-N+1:k)=fz(1:N,nf)*ds(1:N);

  ! source(1,k-N+1:k)=x(1:N,nf);
  ! source(2,k-N+1:k)=y(1:N,nf);
  ! source(3,k-N+1:k)=z(1:N,nf);
  ! enddo

  ! call stfmm3Dpartself(ier,iprec,nparts,source(1:3,1:N_T), ifsingle,sigma_sl(1:3,1:N_T),ifdouble,& 
  ! & sigma_dl(1:3,1:N_T),sigma_dv(1:3,1:N_T), ifpot,pot(1:3,1:N_T),pre(1:N_T),ifgrad,grad(1:3,1:3,1:N_T))
  ! k=0  
  ! do nf=1,N_f;
  !    
  ! N=N_var(nf);
  ! k=k+N;
  ! call MT_uinf_self(fx(1:N,nf),fy(1:N,nf),fz(1:N,nf),&
  ! & x(1:N,nf),y(1:N,nf),z(1:N,nf),N,N0, ds, &
  ! & ux_self(1:N,nf),uy_self(1:N,nf),uz_self(1:N,nf))
  !
  ! ux(1:N,nf)=(2.0*pot(1,k-N+1:k)-ux_self(1:N,nf))/mu(nf);
  ! uy(1:N,nf)=(2.0*pot(2,k-N+1:k)-uy_self(1:N,nf))/mu(nf);
  ! uz(1:N,nf)=(2.0*pot(3,k-N+1:k)-uz_self(1:N,nf))/mu(nf);
  !
  ! enddo;
  !
  ! end subroutine manybody_stokeslet

  !***************************************************************************!
  !***************************************************************************!
  !***************************************************************************!


  subroutine direct_manybody(fx,fy,fz,x,y,z,N_var,N_f,N0,ds,ux,uy,uz)

    implicit none
    INTEGER, PARAMETER :: dp = KIND(1.0D0)
    real (kind=dp), intent (IN), dimension (:,:) :: fx,fy,fz,x,y,z
    real (kind=dp), intent (IN) :: ds(:)
    integer, intent (IN) :: N_var(:), N_f, N0
    real (kind=dp), dimension (:,:) :: ux, uy, uz
    real (kind=dp), dimension (N0) :: dr,dr3, dx,dy,dz,dot, dxdx,dxdy, dxdz, dydz, dzdz, w,w0, fr

    integer :: nf, nf2, N1, N2, i

    ux(:,:)=0.0;
    uy(:,:)=0.0;
    uz(:,:)=0.0;

    dx(:)=0.0;dy(:)=0.0;dz(:)=0.0;
    dxdx(:)=0.0;dxdy(:)=0.0;dxdz(:)=0.0;dydz(:)=0.0;dzdz(:)=0.0;
    dr(:)=1.0;dr3(:)=1.0;dot(:)=0.0;fr(:)=0.0;

    w(:)=ds(:);
    w0(:)=ds(:);

    do nf=1, N_f

       N1=N_var(nf)

       do nf2=1, N_f


          N2=N_var(nf2)

          if (nf .ne. nf2) then



             do i=1,N1




                dx(1:N2)=x(1:N2,nf2)-x(i,nf);
                dy(1:N2)=y(1:N2,nf2)-y(i,nf);
                dz(1:N2)=z(1:N2,nf2)-z(i,nf);


                dot(1:N2)=dx(1:N2)*fx(1:N2,nf2)+dy(1:N2)*fy(1:N2,nf2)+dz(1:N2)*fz(1:N2,nf2)

                dr=sqrt(dx**2+dy**2+dz**2)+2.0e-6;

                dr3=dr**3;

                fr(1:N2)=-0.0*exp(-10.0*(dr(1:N2)-2.0e-2))/(dr(1:N2)-2.0e-2+1.0e-4)
                !     dxdx=0.0!1.0/dr+dx*dx/dr3;
                !     dxdy=dx*dy/dr3;
                !     dxdz=dx*dz/dr3;
                !     dydy=0.0!1.0/dr+dy*dy/dr3;
                !     dydz=dy*dz/dr3;
                !     dzdz=0.0!1.0/dr+dz*dz/dr3;


                w(1:N2)=1.0;!w(1)=0.50;w(N2)=0.50;

                ux(i,nf)=ux(i,nf)+sum(w(1:N2)*(1.0*fx(1:N2,nf2)/dr(1:N2)+ & 
                     (1.0*dx(1:N2)*dot(1:N2))/dr3(1:N2))+fr(1:N2)*dx(1:N2)/dr(1:N2));!0.0*dx(1:N2)*
                

                uy(i,nf)=uy(i,nf)+sum(w(1:N2)*(1.0*fy(1:N2,nf2)/dr(1:N2)+ & 
                     (1.0*dy(1:N2)*dot(1:N2))/dr3(1:N2))+fr(1:N2)*dy(1:N2)/dr(1:N2));

                uz(i,nf)=uz(i,nf)+sum(w(1:N2)*(1.0*fz(1:N2,nf2)/dr(1:N2)+ & 
                     (1.0*dz(1:N2)*dot(1:N2))/dr3(1:N2))+fr(1:N2)*dz(1:N2)/dr(1:N2));



             enddo


          endif


       enddo

       ! print *, "maximum velocity is ...", maxval(abs(uy(1:N1,nf2)))*ds
    enddo

    ! ux=ux*ds;
    ! uy=uy*ds;
    ! uz=uz*ds;

    ! open(unit=70, file='u_1.txt')

    ! do nf=1,N_F
    ! N1=N_var(nf)
    ! do i=1,N1
    ! write(70,100) ux(i,nf), uy(i,nf), fx(i,nf), fy(i,nf)  
    ! enddo

    ! enddo



  end subroutine direct_manybody


  !***************************************************************************!
  !***************************************************************************!
  !***************************************************************************!

  subroutine stresslet(N0,N,x0,y0,z0,xc0,ds,S,ux,uy,uz)

    implicit none
    INTEGER, PARAMETER :: dp = KIND(1.0D0)
    INTEGER, INTENT(IN) :: N,N0
    real (kind=dp), intent (IN), dimension (:) :: x0,y0,z0
    real (kind=dp), intent (IN) :: S(:,:)
    real (kind=dp), intent (IN) :: ds(:),xc0(:)
    real (kind=dp), dimension (:) :: ux, uy, uz
    real (kind=dp), dimension (N0) :: dot,x,y,z,r,dot2

    x(1:N)=x0(1:N)-xc0(1)
    y(1:N)=y0(1:N)-xc0(2)
    z(1:N)=z0(1:N)-xc0(3)

    r(1:N)=sqrt(x(1:N)**2+y(1:N)**2+z(1:N)**2)

    dot(1:N)=2.0*(x(1:N)*y(1:N)*S(1,2)+x(1:N)*z(1:N)*S(1,3)+z(1:N)*y(1:N)*S(2,3))+&
         x(1:N)*x(1:N)*S(1,1)+y(1:N)*y(1:N)*S(2,2)+z(1:N)*z(1:N)*S(3,3)
    dot2(1:N)=dot(1:N)*(-3.0)/r(1:N)**5
    ux(1:N)=dot2(1:N)*x(1:N)
    uy(1:N)=dot2(1:N)*y(1:N)
    uz(1:N)=dot2(1:N)*z(1:N)

  end subroutine stresslet





end module Hydro_Inter
