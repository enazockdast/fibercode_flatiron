module Tension
  !USE input_file
  use, intrinsic :: iso_c_binding
  USE sparse_solve
  USE A_TIMES_X 
  USE derivatives 
  USE Hydro_Inter 
  USE linear_algebra
  use initialize
  use fmm_interface
  !use fmm_mod
  implicit none 

contains


  !**************************************************************************!

  subroutine implicit_GMRES()
    !       USE input_file
    use initialize


    IMPLICIT NONE
    ! INCLUDE "mkl_rci.fi"


    INTEGER, PARAMETER :: SIZE0=128
    real (kind=dp), allocatable ::  TMP(:), DPAR(:), B(:), B2(:), B3(:), RESIDUAL(:)
    integer, allocatable :: IPAR(:)
    INTEGER :: k2

    !---------------------------------------------------------------------------
    ! Some additional variables to use with the RCI (P)FGMRES solver
    !---------------------------------------------------------------------------
    INTEGER :: ITERCOUNT

    INTEGER :: RCI_REQUEST!, I
    DOUBLE PRECISION :: DVAR
    !     real (kind=dp), intent (IN)  :: DS
    !---------------------------------------------------------------------------
    ! An external BLAS function is taken from MKL BLAS to use
    ! with the RCI (P)FGMRES solver
    !---------------------------------------------------------------------------
    DOUBLE PRECISION :: DNRM2
    EXTERNAL DNRM2

    !---------------------------------------------------------------------------
    ! Initialize the initial guess
    !---------------------------------------------------------------------------



    N_total=4*N0*N_f_T
    N=N0

    allocate (TMP(N_total*(2*(30)+1)+(30*(30+9))/2+1))
    allocate(DPAR(SIZE0), IPAR(SIZE0))

    allocate(B(4*N0*N_f), B2(4*N0*N_f), B3(4*N0*N_f), RESIDUAL(N_total))
    COMPUTED_SOLUTION_IM(1:N_total)=XT(1:N_total)
    RHS_im(1:N_total)=F_XT(1:N_total)


    !---------------------------------------------------------------------------
    ! Initialize the solver
    !---------------------------------------------------------------------------
    CALL DFGMRES_INIT(N_total, COMPUTED_SOLUTION_IM, RHS_im, RCI_REQUEST, IPAR, &
         & DPAR, TMP)
    IF (RCI_REQUEST.NE.0) GOTO 999
    !---------------------------------------------------------------------------
    ! Set the desired parameters:
    ! do the restart after 2 iterations
    ! LOGICAL parameters:
    ! do not do the stopping test for the maximal number of iterations
    ! do the Preconditioned iterations of FGMRES method
    ! DOUBLE PRECISION parameters
    ! set the relative tolerance to 1.0D-3 instead of default value 1.0D-6
    !---------------------------------------------------------------------------

    IPAR(5)=16
    IPAR(15)=16
    IPAR(8)=1
    IPAR(9)=0
    IPAR(10)=0
    IPAR(11)=1
    IPAR(12)=1
    DPAR(1)=1.0D-9

    !---------------------------------------------------------------------------
    ! Check the correctness and consistency of the newly set parameters
    !---------------------------------------------------------------------------
    CALL DFGMRES_CHECK(N_total, COMPUTED_SOLUTION_IM, RHS_im, RCI_REQUEST, &
         & IPAR, DPAR, TMP)
    IF (RCI_REQUEST.NE.0) GOTO 999


    !---------------------------------------------------------------------------
    ! Compute the solution by RCI (P)FGMRES solver with preconditioning
    ! Reverse Communication starts here
    !---------------------------------------------------------------------------
1   CALL DFGMRES(N_total, COMPUTED_SOLUTION_IM, RHS_im, RCI_REQUEST, IPAR, &
         & DPAR, TMP)

    !---------------------------------------------------------------------------
    ! If RCI_REQUEST=0, then the solution was found with the required precision
    !---------------------------------------------------------------------------
    IF (RCI_REQUEST.EQ.0) GOTO 3
    !---------------------------------------------------------------------------
    ! If RCI_REQUEST=1, then compute the vector A*TMP(IPAR(22))
    ! and put the result in vector TMP(IPAR(23))
    !---------------------------------------------------------------------------    


    IF (RCI_REQUEST.EQ.1) THEN

       call system_clock(t01)      

       ! if (id .eq. 0) then
       ! B3_T=TMP(IPAR(22):IPAR(22)+N_total-1) 
       ! endif 
       ! call pvfmm_mpi_bcast(B3_T,N0*4*N_f_T,0)
       ! TMP(IPAR(22):IPAR(22)+N_total-1)=B3_T
       CALL A_DOT_X_implicit(TMP(IPAR(22):IPAR(22)+N_total-1))
       TMP(IPAR(23):IPAR(23)+N_total-1)=A_DOT_XT(1:N_total)
       call system_clock(t02)
       print *, "A times x timing", real(t02-t01)/real(rate)
       GOTO 1
    ENDIF


    !---------------------------------------------------------------------------
    ! If RCI_REQUEST=3, then apply the preconditioner on the vector
    ! TMP(IPAR(22)) and put the result in vector TMP(IPAR(23))
    !---------------------------------------------------------------------------
    IF (RCI_REQUEST.EQ.3) THEN
       call system_clock(t03)
       !$OMP parallel default(shared) private(nf,k2)
       N=N0
       k2=0
       !$OMP do
       do nf=1,N_f
          N=N0
          k2=(nf-1)*4*N
          !N=N_var(nf)
          B2(1+k2:4*N+k2)=TMP(id*4*N0*N_f+IPAR(22)+k2:id*4*N0*N_f+IPAR(22)+4*N+k2-1)

          call sparse_solve_1(4*N,IA_XT2(1:4*N+1,nf),JA_XT2(1:16*n_s*N,nf),A_XT2(1:16*n_s*N,nf),B2(1+k2:4*N+k2),B3(1+k2:4*N+k2))

          !        TMP(IPAR(23)+k2:IPAR(23)+k2+4*N-1)=B3(1+k2:4*N+k2)
          !        k2=k2+N*4

       enddo
       !$OMP end do
       !$OMP end parallel        

       !        B3_T(:)=0.0d0
       call  pvfmm_mpi_gather( B3,4*N0*N_f,B3_T,4*N0*N_f,0) !recv_count is perproc

       call pvfmm_mpi_bcast(B3_T,N0*4*N_f_T,0)


       TMP(IPAR(23):IPAR(23)+N_total-1)=B3_T(:)

       call system_clock(t04)
       if (id .eq. 0) then
          print *, "precon solve", real(t04-t03)/real(rate)
       endif
       GOTO 1

    ENDIF



    !---------------------------------------------------------------------------
    ! If RCI_REQUEST=4, then check if the norm of the next generated vector is
    ! not zero up to rounding and computational errors. The norm is contained
    ! in DPAR(7) parameter
    !---------------------------------------------------------------------------
    IF (RCI_REQUEST.EQ.4) THEN

       IF (DPAR(7).LT.1.0D-12) THEN
          GOTO 3
       ELSE
          GOTO 1
       ENDIF
       !---------------------------------------------------------------------------
       ! If RCI_REQUEST=anything else, then DFGMRES subroutine failed
       ! to compute the solution vector: COMPUTED_SOLUTION(N)
       !---------------------------------------------------------------------------
    ELSE
       GOTO 999
    ENDIF
    !---------------------------------------------------------------------------
    ! Reverse Communication ends here
    ! Get the current iteration number and the FGMRES solution. (DO NOT FORGET to
    ! call DFGMRES_GET routine as computed_solution is still containing
    ! the initial guess!). Request to DFGMRES_GET to put the solution into
    ! vector COMPUTED_SOLUTION(N) via IPAR(13)
    !---------------------------------------------------------------------------
3   IPAR(13)=0
    CALL DFGMRES_GET(N_total, COMPUTED_SOLUTION_IM, RHS_im, RCI_REQUEST, IPAR, &
         & DPAR, TMP, ITERCOUNT)
    !---------------------------------------------------------------------------
    ! Print solution vector: COMPUTED_SOLUTION(N) and
    ! the number of iterations: ITERCOUNT
    !---------------------------------------------------------------------------
    !      PRINT *, ''
    !      PRINT *,' The system has been solved'
    !      PRINT *, ''
    !      PRINT *,' The following solution has been obtained:'
    !      DO I=1,N_total

    !          write(*,*) computed_solution(I)
    !      ENDDO
    !        k=0;

    call pvfmm_mpi_bcast(computed_solution,N_total,0)
    XT=computed_solution_im
    !      PRINT *, ''


    !---------------------------------------------------------------------------
    ! Release internal MKL memory that might be used for computations
    ! NOTE: It is important to call the routine below to avoid memory leaks
    ! unless you disable MKL Memory Manager
    !---------------------------------------------------------------------------
    CALL MKL_FREE_BUFFERS
    goto 1000




999 WRITE( *,'(A,A,I5)') 'This example FAILED as the solver has', &
         & ' returned the ERROR code', RCI_REQUEST
    CALL MKL_FREE_BUFFERS
    STOP 1

1000 continue
  END subroutine implicit_GMRES


  !**************************************************************************!
  !**************************************************************************!
  !****************************************************************!
  !****** IMPLICIT FORMULATION FOR TENSION*************************!
  !****************************************************************!
  !****************************************************************!


  subroutine A_DOT_X_implicit(XT2)
    ! USE input_file
    use initialize
    use fmm_interface
    !        use fmm_mod
    use, intrinsic :: iso_c_binding
    implicit none 
    real(kind=dp) :: XT2(:),dum_2,eps,alpha0,beta0
    !*******************************!
    eps=1.0e-12
    iter_GMRES=iter_GMRES+1

    N=N0
    m0=2


    print *, "ID IS .....", id, mpi_rank
    !$OMP parallel default(shared) private(nf)
    !$OMP do
    do nf=1,N_f
       do i=1,N0
          T(i,nf)=XT2(id*4*N_f*N0+4*(nf-1)*N0+(i-1)*4+1)
          x(i,nf)=XT2(id*4*N_f*N0+4*(nf-1)*N0+(i-1)*4+2)
          y(i,nf)=XT2(id*4*N_f*N0+4*(nf-1)*N0+(i-1)*4+3)
          z(i,nf)=XT2(id*4*N_f*N0+4*(nf-1)*N0+(i-1)*4+4)


       enddo
       source_MT(1,(nf-1)*N+1:nf*N)=x0(1:N,nf)
       source_MT(2,(nf-1)*N+1:nf*N)=y0(1:N,nf)
       source_MT(3,(nf-1)*N+1:nf*N)=z0(1:N,nf)

       call deriv_O_N(x(1:N,nf),N,n_s,C_T(:,:,1:N),m,x_ns(1:m,1:N,nf),L_FF(nf));
       call deriv_O_N(y(1:N,nf),N,n_s,C_T(:,:,1:N),m,y_ns(1:m,1:N,nf),L_FF(nf));
       call deriv_O_N(z(1:N,nf),N,n_s,C_T(:,:,1:N),m,z_ns(1:m,1:N,nf),L_FF(nf));
       call deriv_O_N(T(1:N,nf),N,n_s,C_T(:,:,1:N),m0,T_ns(1:m0,1:N,nf),L_FF(nf))
    enddo
    !$OMP end do
    N=N0
    !$OMP do
    do nf=1,N_f

       fx_MTT_T(1,1:N,nf)=1.0d0*(T_ns(1,1:N,nf)*x_ns0(1,1:N,nf)+ &
            & T_tr(1,1:N,nf)*x_ns0(2,1:N,nf));

       fy_MTT_T(1,1:N,nf)=1.0d0*(T_ns(1,1:N,nf)*y_ns0(1,1:N,nf)+ &
            & T_tr(1,1:N,nf)*y_ns0(2,1:N,nf));

       fz_MTT_T(1,1:N,nf)=1.0d0*(T_ns(1,1:N,nf)*z_ns0(1,1:N,nf)+ &
            & T_tr(1,1:N,nf)*z_ns0(2,1:N,nf));

       fx_MT_T(1:N,nf)=fx_MTT_T(1,1:N,nf);
       fy_MT_T(1:N,nf)=fy_MTT_T(1,1:N,nf);
       fz_MT_T(1:N,nf)=fz_MTT_T(1,1:N,nf);

       fx_MTT_B(1,1:N,nf)=1.0d0*(-1.0d0*x_ns(4,1:N,nf));
       fy_MTT_B(1,1:N,nf)=1.0d0*(-1.0d0*y_ns(4,1:N,nf));
       fz_MTT_B(1,1:N,nf)=1.0d0*(-1.0d0*z_ns(4,1:N,nf));

       fx_MT_B(1:N,nf)=fx_MTT_B(1,1:N,nf);
       fy_MT_B(1:N,nf)=fy_MTT_B(1,1:N,nf);
       fz_MT_B(1:N,nf)=fz_MTT_B(1,1:N,nf);



       sigma_sl_MT(1,(nf-1)*N0+1:nf*N0)=8.0d0*pi*(fx_MT_T(1:N0,nf)+fx_MT_B(1:N0,nf))*chev_w(1:N0)*L_FF(nf)
       sigma_sl_MT(2,(nf-1)*N0+1:nf*N0)=8.0d0*pi*(fy_MT_T(1:N0,nf)+fy_MT_B(1:N0,nf))*chev_w(1:N0)*L_FF(nf)
       sigma_sl_MT(3,(nf-1)*N0+1:nf*N0)=8.0d0*pi*(fz_MT_T(1:N0,nf)+fz_MT_B(1:N0,nf))*chev_w(1:N0)*L_FF(nf)


    enddo
    !$OMP enddo
    !$OMP end parallel

    pot_MT(1:3,:)=0.0;
    grad_MT(1:3,1:3,:)=0.0;
    seq_src(1:3,1:nparts_MT)=source_MT(1:3,1:nparts_MT)
    seq_den_sl(1:3,1:nparts_MT)=sigma_sl_MT
    seq_den_dl(:,:)=0.0d0        
    seq_dv(:,:)=0.0d0;seq_dv(3,:)=1.0d0

    fac2=2.10d0*a_cor+4.0d-3
    sl_den(:)=0.0d0

    do i=1,nsrc

          src((i-1)*3+1)=(source_MT(1,i)+fac2/2.0d0)/fac2
          src((i-1)*3+2)=(source_MT(2,i)+fac2/2.0d0)/fac2
          src((i-1)*3+3)=(source_MT(3,i)+fac2/2.0d0)/fac2

          sl_den((i-1)*3+1)=seq_den_sl(1,i)/fac2
          sl_den((i-1)*3+2)=seq_den_sl(2,i)/fac2
          sl_den((i-1)*3+3)=seq_den_sl(3,i)/fac2

    end do
    trg=src

    flag_pvfmm=0
    if (iter_GMRES .eq. 1) then
       rebuild_tree=true
    else
       rebuild_tree=false
    endif

    call system_clock(t05)
    if (t_step>t_init+0e5) then
       call stokes_sl_fmm(nsrc, src, sl_den, nsrc, trg, sldl_pot,rebuild_tree,sl_context)
    else
       sldl_pot(:)=0.0d0
    endif

    call system_clock(t06)

    seq_pot=1.0d0*seq_pot_2

    do i=1,nparts_MT
       pot_MT(1,i)=1.0d0*sldl_pot((i-1)*3+1)
       pot_MT(2,i)=1.0d0*sldl_pot((i-1)*3+2)
       pot_MT(3,i)=1.0d0*sldl_pot((i-1)*3+3)
    end do


    print *, "PASSED FMM"       

    V_T0_T(1:3,1)=0.0d0;Omega_T0_T(1:3,1)=0.0d0;
    F_MT_T(:,:)=0.0d0;T_MT_T(:,1)=0.0d0;
    V_T0_B(1:3,1)=0.0d0;Omega_T0_B(1:3,1)=0.0d0;
    F_MT_B(:,:)=0.0d0;T_MT_B(:,1)=0.0d0;
    k=0;V_HD(:,1)=0.0d0;V(:,:)=0.0d0;
    V_2(:,:)=0.0d0;
    Omega_HD(:,:)=0.0d0;O(:,:)=0.0d0;
    S_SP_HD(:,:)=0.0d0;

    !$OMP parallel default(shared) private(nf,N,C_T,dum_2)
    !$OMP do 
    do nf=1,N_f;
       N=N0
       C_T=C_T0;
       m0=2
       dot0(nf,1:N)=(x_ns0(1,1:N,nf)*x_ns(1,1:N,nf)+ &
            & y_ns0(1,1:N,nf)*y_ns(1,1:N,nf)+ &
            & z_ns0(1,1:N,nf)*z_ns(1,1:N,nf));  
       dot1(nf,1:N)=x_ns0(2,1:N,nf)**2+y_ns0(2,1:N,nf)**2+z_ns0(2,1:N,nf)**2;

       dot2(nf,1:N)=x_ns0(2,1:N,nf)*x_ns(4,1:N,nf)+ &
            & y_ns0(2,1:N,nf)*y_ns(4,1:N,nf)+ &
            & z_ns0(2,1:N,nf)*z_ns(4,1:N,nf);

       dot3(nf,1:N)=x_ns0(3,1:N,nf)*x_ns(3,1:N,nf)+ &
            & y_ns0(3,1:N,nf)*y_ns(3,1:N,nf)+ &
            & z_ns0(3,1:N,nf)*z_ns(3,1:N,nf);   

       F_MT_TN(1,nf)=T(1,nf)*x_ns0(1,1,nf)/mu0(nf)
       F_MT_TN(2,nf)=T(1,nf)*y_ns0(1,1,nf)/mu0(nf)
       F_MT_TN(3,nf)=T(1,nf)*z_ns0(1,1,nf)/mu0(nf)

       Ten_T(1,nf)=1.0d0*(dxc1p(2,nf)*F_MT_TN(3,nf)-dxc1p(3,nf)*F_MT_TN(2,nf))/mu0(nf);        
       Ten_T(2,nf)=1.0d0*(-dxc1p(1,nf)*F_MT_TN(3,nf)+dxc1p(3,nf)*F_MT_TN(1,nf))/mu0(nf);       
       Ten_T(3,nf)=1.0d0*(dxc1p(1,nf)*F_MT_TN(2,nf)-dxc1p(2,nf)*F_MT_TN(1,nf))/mu0(nf);


       F_MT_BN(1,nf)=-x_ns(3,1,nf)/mu0(nf)
       F_MT_BN(2,nf)=-y_ns(3,1,nf)/mu0(nf)
       F_MT_BN(3,nf)=-z_ns(3,1,nf)/mu0(nf)

       Ten_B(1,nf)=-1.0d0*(y_ns(2,1,nf)*z_ns0(1,1,nf)-z_ns(2,1,nf)*y_ns0(1,1,nf))/mu0(nf)+&
            & 1.0d0*(1.0*dxc1p(2,nf)*(-z_ns(3,1,nf))-dxc1p(3,nf)*(-y_ns(3,1,nf)))/mu0(nf);

       Ten_B(2,nf)=-1.0d0*(-x_ns(2,1,nf)*z_ns0(1,1,nf)+z_ns(2,1,nf)*x_ns0(1,1,nf))/mu0(nf)+&
            & 1.0d0*(-dxc1p(1,nf)*(-z_ns(3,1,nf))+dxc1p(3,nf)*(-x_ns(3,1,nf)))/mu0(nf);

       Ten_B(3,nf)=-1.0d0*(x_ns(2,1,nf)*y_ns0(1,1,nf)-y_ns(2,1,nf)*x_ns0(1,1,nf))/mu0(nf)+ &
            & 1.0d0*(dxc1p(1,nf)*(-y_ns(3,1,nf))-dxc1p(2,nf)*(-x_ns(3,1,nf)))/mu0(nf) ;

       call MT_uinf_single(fx_MT_T(:,nf)+1.0d0*fx_MT_B(:,nf),&
            & fy_MT_T(:,nf)+1.0d0*fy_MT_B(:,nf), fz_MT_T(:,nf)+1.0d0*fz_MT_B(:,nf), &
            & x0(:,nf), y0(:,nf), z0(:,nf), xc0(1), xc0(2), xc0(3), N0, N0, chev_w(1:N0)*L_FF(nf),&
            & V(1:3,nf), O(1:3,nf),V_2(1:3,nf),S_1(1:3,1:3,nf))

    enddo;

    !$OMP   end do
    !$OMP   single

    T_MT_T(1,1)=sum(Ten_T(1,1:N_f))
    T_MT_T(2,1)=sum(Ten_T(2,1:N_f))
    T_MT_T(3,1)=sum(Ten_T(3,1:N_f))

    F_MT_T(1,1)=sum(F_MT_TN(1,1:N_f))
    F_MT_T(2,1)=sum(F_MT_TN(2,1:N_f))
    F_MT_T(3,1)=sum(F_MT_TN(3,1:N_f))

    T_MT_B(1,1)=sum(Ten_B(1,1:N_f))
    T_MT_B(2,1)=sum(Ten_B(2,1:N_f))
    T_MT_B(3,1)=sum(Ten_B(3,1:N_f))

    F_MT_B(1,1)=sum(F_MT_BN(1,1:N_f))
    F_MT_B(2,1)=sum(F_MT_BN(2,1:N_f))
    F_MT_B(3,1)=sum(F_MT_BN(3,1:N_f))

    V_T0_B=matmul(M1,F_MT_B)
    Omega_T0_B=1.0d0*matmul(M2,T_MT_B)
    V_T0_T=matmul(M1,F_MT_T)
    Omega_T0_T=1.0d0*matmul(M2,T_MT_T)

    Omega_HD(1,1)=1.0d0*sum(O(1,1:N_f))
    Omega_HD(2,1)=1.0d0*sum(O(2,1:N_f))
    Omega_HD(3,1)=1.0d0*sum(O(3,1:N_f))

    V_HD(1,1)=1.0d0*sum(V(1,1:N_f))
    V_HD(2,1)=1.0d0*sum(V(2,1:N_f))
    V_HD(3,1)=1.0d0*sum(V(3,1:N_f))
    print *, "V_HD is ...", V_HD(3,1)

    S_SP_HD(1,1)=sum(S_1(1,1,1:N_f));
    S_SP_HD(1,2)=sum(S_1(1,2,1:N_f));
    S_SP_HD(1,3)=sum(S_1(1,3,1:N_f));
    S_SP_HD(2,2)=sum(S_1(2,2,1:N_f));
    S_SP_HD(2,3)=sum(S_1(2,3,1:N_f));
    S_SP_HD(3,3)=sum(S_1(3,3,1:N_f));
    S_SP_HD=0.0*S_SP_HD
    S_SP_HD(2,1)=S_SP_HD(1,2);
    S_SP_HD(3,2)=S_SP_HD(2,3);
    S_SP_HD(3,1)=S_SP_HD(1,3);

    V_T(:,1)=V_SP(:);
    Omega_T(:,1)=Omega_SP(:)

    !-------------------------------------------------------------------------------!
    !----------------------MPI reduction and broadcasting of velocities-------------!
    !-------------------------------------------------------------------------------!
    V_grand(1:3)=V_T0_T(1:3,1);V_grand(4:6)=V_T0_B(1:3,1);
    V_grand(7:9)=V_HD(1:3,1);V_grand(10:12)=V_T(1:3,1);
    V_grand(13:15)=Omega_T0_T(1:3,1);V_grand(16:18)=Omega_T0_B(1:3,1);
    V_grand(19:21)=Omega_HD(1:3,1);V_grand(22:24)=Omega_T(1:3,1);

    V_grand_dum(:)=0.0d0

    call pvfmm_mpi_reduce(V_grand,V_grand_dum,24,0)
    V_grand=V_grand_dum;
    call pvfmm_mpi_bcast(V_grand,24,0)

    V_T0_T(1:3,1)=V_grand(1:3);V_T0_B(1:3,1)=V_grand(4:6);
    V_HD(1:3,1)=V_grand(7:9);V_T(1:3,1)=V_grand(10:12);
    Omega_T0_T(1:3,1)=V_grand(13:15);Omega_T0_B(1:3,1)=V_grand(16:18);
    Omega_HD(1:3,1)=V_grand(19:21);Omega_T(1:3,1)=V_grand(22:24);
    !----------------------------------------------------------------------------!
    !----------------------------------------------------------------------------!
    Omega_Tn=Omega_T
    Omega_T0_Tn=Omega_T0_T
    Omega_T0_Bn=Omega_T0_B
    Omega_HDn=Omega_HD

    !$OMP end single 
    !$OMP do
    do nf=1,N_f
       Vatt(1:3,nf)=0.0d0;      
       Vatt(1,nf)=(Omega_Tn(2,1)*dxc1p(3,nf)-Omega_Tn(3,1)*dxc1p(2,nf))
       Vatt(2,nf)=-Omega_Tn(1,1)*dxc1p(3,nf)+1.0*Omega_Tn(3,1)*dxc1p(1,nf)
       Vatt(3,nf)=Omega_Tn(1,1)*dxc1p(2,nf)-1.0*Omega_Tn(2,1)*dxc1p(1,nf)
       Vatt(1:3,nf)=Vatt(1:3,nf)+V_T(1:3,1)

       Vatt_2(1:3,nf)=0.0d0;
       Vatt_2(1 ,nf)=Omega_Tn(2,1)*dxc2p(3,nf)-Omega_Tn(3,1)*dxc2p(2,nf)
       Vatt_2(2 ,nf)=-Omega_Tn(1,1)*dxc2p(3,nf)+1.0*Omega_Tn(3,1)*dxc2p(1,nf)
       Vatt_2(3 ,nf)=Omega_Tn(1,1)*dxc2p(2,nf)-1.0*Omega_Tn(2,1)*dxc2p(1,nf)
       Vatt_2(1:3,nf)=Vatt_2(1:3,nf)+V_T(1:3,1)
    enddo
    !$OMP end do
    !$OMP   do      
    do nf=1,N_f;
       N=N0
       C_T=C_T0;

       ! call sphere_motion(N0, N, x0(:,nf),y0(:,nf),z0(:,nf),V_T0_T+V_T0_B,Omega_T0_Tn+Omega_T0_Bn,0.0*E_T,xc0, &
       ! & ux_SP(:,nf),uy_SP(:,nf),uz_SP(:,nf));

       call stokeslet_single( 0.0d0*(F_MT_T(:,1)+F_MT_B(:,1)),& 
            & 0.0d0*(T_MT_T(:,1)+T_MT_B(:,1)),xc0,N0, &
            & x0(:,nf),y0(:,nf),z0(:,nf),ux_SP(:,nf),uy_SP(:,nf),uz_SP(:,nf))

       ! call sphere_motion(N0, N, x0(:,nf),y0(:,nf),z0(:,nf),V_T,&
       !        & Omega_T,0.0*(S_SP_HD)/(20.0*mu0(nf)/18.0),xc0, &
       ! & ux_SP_HD(:,nf),uy_SP_HD(:,nf),uz_SP_HD(:,nf)); 

       call rigid_body_motion(N0,x0(:,nf),y0(:,nf),z0(:,nf),xc0,&
            & V_T(:,1),Omega_T(:,1),&
            & ux_SP_HD(:,nf),uy_SP_HD(:,nf),uz_SP_HD(:,nf))

       call MT_uinf_self(fx_MT_T(:,nf)+fx_MT_B(:,nf),&
            & fy_MT_T(:,nf)+fy_MT_B(:,nf),fz_MT_T(:,nf)+fz_MT_B(:,nf),&
            & x0(:,nf),y0(:,nf),z0(:,nf),N,N0,chev_w(1:N0)*L_FF(nf), &
            & fac2,eps,ux_self(:,nf),uy_self(:,nf),uz_self(:,nf))

       if (t_step>t_init+0e5) then
          ux_MT(1:N,nf)=(pot_MT(1,(nf-1)*N+1:nf*N)-ux_self(1:N,nf))
          uy_MT(1:N,nf)=(pot_MT(2,(nf-1)*N+1:nf*N)-uy_self(1:N,nf))
          uz_MT(1:N,nf)=(pot_MT(3,(nf-1)*N+1:nf*N)-uz_self(1:N,nf))
       else
          ux_MT(1:N,nf)=0.0d0;uy_MT(1:N,nf)=0.0d0;uz_MT(1:N,nf)=0.0d0;
       endif

       uxs_SP_HD(:,:,:)=0.0d0;uys_SP_HD(:,:,:)=0.0d0;uzs_SP_HD(:,:,:)=0.0d0;
       uxs_SP(:,:,:)=0.0d0;uxs_SP(:,:,:)=0.0d0;uzs_SP(:,:,:)=0.0d0;
       uxs_MT(:,:,:)=0.0d0;uxs_MT(:,:,:)=0.0d0;uzs_MT(:,:,:)=0.0d0;

       if (t_step .ge. t_init+0e5) then
          ux2_ext(1:N,nf)=1.0d0*ux_MT(1:N,nf)*dum(1:N,nf)+ &
               & dum(1:N,nf)*ux_SP(1:N,nf)+& 
               & 1.0d0*ux_SP_HD(1:N,nf)*(1.0d0-dum(1:N,nf))
          ux_ext(1:N,nf)=ux2_ext(1:N,nf)+&
               & beta(nf)*x_ns(1,1:N,nf)*s(1:N) !&

          uy2_ext(1:N,nf)=1.0d0*uy_MT(1:N,nf)*dum(1:N,nf)+ &
               & dum(1:N,nf)*uy_SP(1:N,nf)+& 
               & 1.0d0*uy_SP_HD(1:N,nf)*(1.0d0-dum(1:N,nf))
          uy_ext(1:N,nf)=uy2_ext(1:N,nf)+&
               & beta(nf)*y_ns(1,1:N,nf)*s(1:N) !&

          uz2_ext(1:N,nf)=1.0d0*uz_MT(1:N,nf)*dum(1:N,nf)+ &
               & dum(1:N,nf)*uz_SP(1:N,nf)+& 
               & 1.0d0*uz_SP_HD(1:N,nf)*(1.0d0-dum(1:N,nf))
          uz_ext(1:N,nf)=uz2_ext(1:N,nf)+&
               & beta(nf)*z_ns(1,1:N,nf)*s(1:N) !&

          call deriv_O_N(ux2_ext(1:N,nf),N,n_s,C_T(:,:,1:N),m0,ux_s(1:m0,1:N,nf),L_FF(nf));
          call deriv_O_N(uy2_ext(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uy_s(1:m0,1:N,nf),L_FF(nf));
          call deriv_O_N(uz2_ext(1:N,nf),N,n_s,C_T(:,:,1:N),m0,uz_s(1:m0,1:N,nf),L_FF(nf));

          xsus(1,1:N,nf)=ux_s(1,1:N,nf)*x_ns0(1,1:N,nf)+ &
               &  uy_s(1,1:N,nf)*y_ns0(1,1:N,nf)+uz_s(1,1:N,nf)*z_ns0(1,1:N,nf);

       else
          xsus(1,1:N,nf)=0.0d0;
          ux2_ext(1:N,nf)=0.0d0;uy2_ext(1:N,nf)=0.0d0;uz2_ext(1:N,nf)=0.0d0;
          
          ux_ext(1:N,nf)=ux2_ext(1:N,nf)+&
               & beta(nf)*x_ns(1,1:N,nf)*s(1:N)

          uy_ext(1:N,nf)=uy2_ext(1:N,nf)+&
               & beta(nf)*y_ns(1,1:N,nf)*s(1:N)

          uz_ext(1:N,nf)=uz2_ext(1:N,nf)+&
               & beta(nf)*z_ns(1,1:N,nf)*s(1:N) 
       endif


    enddo;
    !$OMP   end do 
    !$OMP  end parallel
    print *, "PASSED HERER 0"

    !!$OMP  do
    do nf=1,N_f

       do i=1,N0

          A_DOT_X(4*(nf-1)*N0+(i-1)*4+1)=T_ns(2,i,nf)-dot1(nf,i)*T(i,nf)/2.0d0+&
               & +(6.0d0*dot3(nf,i)+7.0d0*dot2(nf,i))/2.0d0+1.0d0*mu(nf)*xsus(1,i,nf)/2.0d0+&
               & fac*mu0(nf)*dot0(nf,i);

          dum_2=x_ns(4,i,nf)*x_ns0(1,i,nf)+y_ns(4,i,nf)*y_ns0(1,i,nf)+z_ns(4,i,nf)*z_ns0(1,i,nf);

          A_DOT_X(4*(nf-1)*N0+(i-1)*4+2)=mu(nf)*x(i,nf)/dt0+x_ns(4,i,nf)+dum_2*x_ns0(1,i,nf)-&
               & T(i,nf)*x_ns0(2,i,nf)-2.0d0*T_ns(1,i,nf)*x_ns0(1,i,nf)-mu(nf)*ux_ext(i,nf)

          A_DOT_X(4*(nf-1)*N0+(i-1)*4+3)=mu(nf)*y(i,nf)/dt0+y_ns(4,i,nf)+dum_2*y_ns0(1,i,nf)-&
               & T(i,nf)*y_ns0(2,i,nf)-2.0d0*T_ns(1,i,nf)*y_ns0(1,i,nf)-mu(nf)*uy_ext(i,nf)

          A_DOT_X(4*(nf-1)*N0+(i-1)*4+4)=mu(nf)*z(i,nf)/dt0+z_ns(4,i,nf)+dum_2*z_ns0(1,i,nf)-&
               & T(i,nf)*z_ns0(2,i,nf)-2.0d0*T_ns(1,i,nf)*z_ns0(1,i,nf)-mu(nf)*uz_ext(i,nf)

       enddo

       A_DOT_X(1+4*(nf-1)*N0)=T_ns(1,1,nf)+& 
            & 3.0d0*(x_ns0(2,1,nf)*x_ns(3,1,nf)+y_ns0(2,1,nf)*y_ns(3,1,nf)+z_ns0(2,1,nf)*z_ns(3,1,nf))!&
!            & -0.0d0*mu(nf)*(V(1,nf)*x_ns0(1,1,nf)+V(2,nf)*y_ns0(1,1,nf)+V(3,nf)*z_ns0(1,1,nf))&
!            & /(2.0*sqrt(x_ns0(1,1,nf)**2+y_ns0(1,1,nf)**2+z_ns0(1,1,nf)**2))

       if (flag_BC(N0,nf) .eq. 3) then
          A_DOT_X(4*(nf-1)*N0+4*(N0-1)+1)=T(N0,nf)
       elseif(flag_BC(N0,nf) .eq. 2) then
          A_DOT_X(4*(nf-1)*N0+4*(N0-1)+1)=T_ns(1,N0,nf)
       endif

       A_DOT_X(4*(nf-1)*N0+2)=x(1,nf)/dt0-Vatt(1,nf)-beta(nf)*x_ns(1,1,nf)*s(1)
       A_DOT_X(4*(nf-1)*N0+3)=y(1,nf)/dt0-Vatt(2,nf)-beta(nf)*y_ns(1,1,nf)*s(1)
       A_DOT_X(4*(nf-1)*N0+4)=z(1,nf)/dt0-Vatt(3,nf)-beta(nf)*z_ns(1,1,nf)*s(1)

       A_DOT_X(4*(nf-1)*N0+6)=x(2,nf)/dt0-Vatt_2(1,nf)-beta(nf)*x_ns(1,2,nf)*s(2)!-&
       A_DOT_X(4*(nf-1)*N0+7)=y(2,nf)/dt0-Vatt_2(2,nf)-beta(nf)*y_ns(1,2,nf)*s(2)!-&
       A_DOT_X(4*(nf-1)*N0+8)=z(2,nf)/dt0-Vatt_2(3,nf)-beta(nf)*z_ns(1,2,nf)*s(2)!-&

       A_DOT_X(4*(N0-1-1)+2+(nf-1)*4*N0)=x_ns(2,N0-1,nf)
       A_DOT_X(4*(N0-1-1)+3+(nf-1)*4*N0)=y_ns(2,N0-1,nf)
       A_DOT_X(4*(N0-1-1)+4+(nf-1)*4*N0)=z_ns(2,N0-1,nf)
       if (flag_BC(N0,nf) .eq. 3) then
          A_DOT_X(4*(N0-1)+2+(nf-1)*4*N0)=-x_ns(3,N0,nf)+T(N0,nf)*x_ns0(1,N0,nf)
          A_DOT_X(4*(N0-1)+3+(nf-1)*4*N0)=-y_ns(3,N0,nf)+T(N0,nf)*y_ns0(1,N0,nf)
          A_DOT_X(4*(N0-1)+4+(nf-1)*4*N0)=-z_ns(3,N0,nf)+T(N0,nf)*z_ns0(1,N0,nf)
       elseif(flag_BC(N0,nf) .eq. 2) then
          A_DOT_X(4*(N0-1)+2+(nf-1)*4*N0)=x(N0,nf)/dt0
          A_DOT_X(4*(N0-1)+3+(nf-1)*4*N0)=y(N0,nf)/dt0
          A_DOT_X(4*(N0-1)+4+(nf-1)*4*N0)=z(N0,nf)/dt0
       endif
    enddo
!!$OMP  end do
!!$OMP  end parallel        

    print *,"PASSED HERE"

    call pvfmm_mpi_gather(A_DOT_X,4*N0*N_f,A_DOT_XT,4*N0*N_f,0)
    call pvfmm_mpi_bcast(A_DOT_XT,4*N0*N_f_T,0)


  end subroutine A_DOT_X_implicit

  !************************************************************************************!
  !************************************************************************************!
  !************************************************************************************!

end module Tension




