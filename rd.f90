        program rd

        implicit none
        
        integer :: t_end, i, j, nf, N, N_F, t_step
        real*8 :: x, y, z, T(2), vx, vy, vz, ox, oy, oz
        real*8 :: B(6), dot(4), u(6)


        open(unit =1, file='positions.txt')
        open(unit =2, file='Tension.txt')
!        open(unit =3, file='dots.txt')
!        open(unit =4, file='B_deriv.txt')

        open(unit =5, file='positions_2.txt')
        open(unit =6, file='Tension_2.txt')
!        open(unit =7, file='dots_2.txt')
!        open(unit =8, file='B_deriv_2.txt')

        open(unit=10,file='pos_restart.txt')
        open(unit=11,file='xc_final.txt')
        open(unit=12,file='xc.txt')
        N_F=40
        N=134
        
        do t_step=1,4505
        print *, t_step        
        do nf=1,N_F

        do i=1,N

        read(1,20) x, y, z
        write(5,20) x, y, z

        read(2,*) T(1), T(2)
        write(6,10) T(1), T(2)

!        read(3,*) dot(1), dot(2), dot(3), dot(4)
!        write(7,10) dot(1), dot(2), dot(3), dot(4)

!        read(4,*) B(1), B(2), B(3), B(4), B(5), B(6) 
!        write(5,10)  B(1), B(2), B(3), B(4), B(5), B(6)

        if ( t_step .eq. 4505) then

        write(10,20) x, y, z
        endif 
        enddo
        enddo

        enddo

        do t_step=1,180160

        read(12,*,end=100) x,y,z
        enddo;
100        write(11,*) x,y,z


10      FORMAT(6F15.6)
20      FORMAT(3F25.12)
        end program rd
