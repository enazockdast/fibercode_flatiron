#include <pvFmmInterface.h>

int main(int argc, char **argv){
  MPI_Init(&argc, &argv);
  MPI_Comm comm=MPI_COMM_WORLD;

  // Read command line options.
  size_t N(100000);
  int m(10);

  // Run FMM with above options.
  pvFmm_test(N, m, comm);

  // Shut down MPI
  MPI_Finalize();
  return 0;
}
