clc;clear;close all;
E=10*1e-12;
a_SP0=5.0;
a_cor=5.250;
b_cor=3.250;
eta=0.330;
tau_s=1.0d0/((E/a_SP0^2)/(6.0d0*pi*eta*a_SP0^2*1.0e-12));
mu1=1e-12/(E/a_SP0^2);
dt=40.0*2e-6*tau_s/mu1;
N_F=100;
%colormap(jet(256));
cmap=colormap(cbrewer('div','RdYlBu',N_F));
%colormap default;
%cmap=autumn(N_F);
%cmap2=gray(288*2);
teta=0:pi/100:2*pi;
xi=cos(teta)*1.0*cos(pi/12);
yi=sin(teta)*1.0*cos(pi/12);
zi=sin(pi/12);
nj=89;ni=89;nk=89;
xm2=linspace(-6,6,ni);
ym2=linspace(-6,6,nj);
zm2=linspace(-10,10,nk);
[XM,YM,ZM]=meshgrid(xm2,ym2,zm2);
%pos_chrom=load('pos_chrom.txt');
%pos=load('positions_bak.txt');
%T1=load('Tension.txt');
%dot=load('dots_bak.txt');
%U=load('u_2.txt');
%xc=load('xc_bak.txt');
%pos=load('positions.txt');
%T=load('Tension.txt');
%dot=load('dots.txt');
%U=load('u_2.txt');
xc=load('xc.txt');
xc1=xc(1:10:end,:);
%B1=load('B_deriv.txt');
%pos=load('pos_restart.txt');
%xc=[  0 ,        0    ,    0];
N_var(1:N_F)=31;
[xx,yy,zz]=sphere(100);
s=-cos((0:N_var(1)-1)*pi./(N_var(1)-1));
s=(s+1)/2;
fileID_1 = fopen('u.txt');
fileID_2 = fopen('positions.txt');
%NN=load('N_u_bulk.txt');
fileID_3 = fopen('Tension.txt');
N_1=N_var(1)*N_F;
N_2=N_1;

%fspc_1='%f %f %f';
fspc_2='%f %f %f';
fspc_3='%f %f %f %f';

k=0;
k2=0;
t_end=size(xc1,1);

T_max=5.0;
T_min=-5.0;
F_max=5.0;
F_min=-5.0;
%F_L=load('F_L.txt');
k4=0;
%t_end=500
x=zeros(N_F*31,1);
for i=1:t_end
    i
 pos=textscan(fileID_2,fspc_2,N_1);
 T=textscan(fileID_3,fspc_2,N_2);
 u=textscan(fileID_1,fspc_3,N_1);
 
%T2=T{1}-2*T3{1};
%T2=3*T3{1};
T2=T{1};
xp=x;
x=pos{1,1};
y=pos{1,2};
z=pos{1,3};
%dot=u{1,1};
ux=u{1,2};
uy=u{1,3};
uz=u{1,4};
size(x);
   for nf=1:N_F
    k4=k4+1;
    k5=(nf*1-1)*N_var(1);
%    FL2(i,nf)=F_L(k4,1);
    Te(i,nf)=T2(k5+N_var(nf));
%    Te(i,nf)=F_L(k4,2);
   end
%T_min=min(T);
%T_max=max(T);
          if (mod(i,1) ==0) 
           for j=1:32
         %     surf(zz*0.1+pos_chrom(j,3),0.1*xx+1.0*pos_chrom(j,1),0.1*yy+1.0*pos_chrom(j,2),40*ones(size(xx))); hold on;shading 'interp';axis equal;

           end
          end

if (i>0)
   
   for nf=1:1:N_F
    k(nf)=(nf*1-1)*N_var(1);
    
    N=N_var(nf);
       %pos_at(1:3,nf)=pos(k+1,1:3);
       %xc2=xc;
        if (mod(i,1) == 0)

            if (nf ==1) 
                
                
             
               
      %s1=surf(zz+xc1(i,3),xx+xc1(i,1),yy+xc1(i,2),-10*ones(size(xx))); hold on; shading 'interp';
      % s1=surf(xx+1.0*xc1(i,1),yy+1.0*xc1(i,2),zz+1.0*xc1(i,3),-200*ones(size(xx))); hold on;shading 'interp';

%light('Position',[0 -20 -20],'Style','infinite');
%lighting phong;
       %alpha(s1,0.9);grid off;
       %  s2=surf(zz*a_cor,xx*b_cor,yy*b_cor,10*ones(size(xx))); shading 'interp';hold on;
       %  alpha(s2,0.1)
        %alpha(S2,0.1)
        % axis equal;
       %axis([-6+xc1(i,1),6+xc1(i,1),-6+xc1(i,2),6+xc1(i,2),-6+xc1(i,3),6+xc1(i,3)]);axis equal;
       %axis([-6,-2,-6,-2,-6,-2]); axis equal;hold on;
    %   alpha(s1,0.3)
   
       %s2=surf(zz*3.0,xx*3.2,yy*3.0,-30*ones(size(xx))); axis equal;shading 'interp';hold on;

    %   s2=surf(xx*5.75,yy*5.75,zz*11.75,0.20*ones(size(xx))); axis equal;shading 'interp';hold on;

  %     set(s2,'FaceLighting','phong','FaceColor','interp',...
  %    'AmbientStrength',0.05)
 % lighting phong;

%light('Position',[-10 -10 -10],'Style','infinite');
     
      % plot3(xx*6,yy*6,zz*6,'o');
       
      % set(gca,'Color',[1.0 1.0 1.00]);
       %set(s2,'FaceColor','Gray');
%       alpha(s2,0.15)
            end
       
        
        %k=(nf-1)*N;  
        
%plot(s,T2(k+1:k+N,1),'Color',cmap(nf,:)); hold on;
%plot(s,T2(k+1:k+N,1),'Color','r'); hold on;

T_ave=mean(T2(k(nf)+1:k(nf)+N,1));
if (T_ave>T_max)
    T_ave=T_max;
elseif (T_ave<T_min)
    T_ave=T_min;
end
%Fc=FL2(i,nf);
%if (Fc>F_max)
%    Fc=F_max;
%elseif (Fc<F_min)
%    Fc=F_min;
%end;
    
%ncolor=floor ((N_F-1)*(Fc-F_min)/(F_max-F_min))+1;
    
%ncolor=floor ((N_F-1)*(T_ave-T_min)/(T_max-T_min))+1;
%if (nf ==15)
  %plot3(x(k(nf)+1:k(nf)+N),y(k(nf)+1:k(nf)+N),z(k(nf)+1:k(nf)+N),'-','LineWidth',1.5,'Color',cmap(ncolor,:));hold on;axis equal;
   %plot3(z(k(nf)+1:k(nf)+N),x(k(nf)+1:k(nf)+N),y(k(nf)+1:k(nf)+N),'-','LineWidth',1.5);hold on;axis equal;

  %end;
  if ( mod(nf,1)==0 )%&& z(k(nf)+N)>-1.750)
   %tubeplot(z(k(nf)+1:k(nf)+N),x(k(nf)+1:k(nf)+N),y(k(nf)+1:k(nf)+N),0.03*ones(N,1),(T2(k(nf)+1:k(nf)+N,1)),10);
   %h2=quiver3(z(k(nf)+1:k(nf)+N),x(k(nf)+1:k(nf)+N),y(k(nf)+1:k(nf)+N),uz(k+1:k+N),ux(k+1:k+N),uy(k+1:k+N)); hold on;
   %set(h2,'LineWidth',2);
   plot3(z(k(nf)+1:k(nf)+N),x(k(nf)+1:k(nf)+N),y(k(nf)+1:k(nf)+N),'LineWidth',2,'color','k')
   hold on;shading 'interp';grid off;caxis([-10,10]);
  end
 %axis([-3.5,3.5,-3.5,3.5,-3.5,3.5]);axis equal;
  %tubeplot(x(k+1:k+N),y(k+1:k+N),z(k+1:k+N),0.03*ones(N,1),(T2(k+1:k+N,1)),10);hold on;shading 'interp';grid off;caxis([min(T2(k+1:k+N)),max(T2(k+1:k+N))]);


  
  %plot3([xc1(i,1) pos(k+1,1)],[xc1(i,2) pos(k+1,2)],[xc1(i,3) pos(k+1,3)],'LineWidth',2); hold on;
 %axis([-6+xc1(i,1),6+xc1(i,1),-6+xc1(i,2),6+xc1(i,2),-6+xc1(i,3),6+xc1(i,3)]);axis equal;
  %camdolly(0.1,0.1,0.1);
  %camproj perspective 
%camva(1)
  %campos([xc1(3)+3,xc1(2)-2-0.2*cos(k2),xc1(1)-2]);
%  set(gca,'XTick',[],'YTick',[],'ZTick',[]);
%  set(gca,'visible','off');
  %camtarget([xc1(3),xc1(2),xc1(1)]);
  %camzoom(1.006);
  %cbfreeze(cmap);

  %camorbit(k2*0.01,0);

%    freezeColors;
 
        end
  %axis([-2.2+xc1(i,1),2.2+xc1(i,1),-2.2+xc1(i,3),2.2+xc1(i,3),-2.2+xc1(i,2),2.2+xc1(i,2)]);hold on;

    
  

 % figure(1);plot(1:N,T1(k+1:k+N,1),'Color',cmap(nf,:)); hold on; 

   
    k=(nf-1)*N;
    %k=k+N;
 %  if (nf <101 && i == 140)

 %  end;
    
   end


if (mod(i,1) == 0 )
      ti = get(gca,'TightInset')-0.0;
set(gca,'Position',[ti(1) ti(2) 1-ti(3)-ti(1) 1-ti(4)-ti(2)]);%view(-0,90);
%set(gca,'visible','off');
   %alpha(0.6);
   %alpha(s2,0.1);
   %alpha(s1,0.4);
   light('Position',[-0.0 0.0 -20]);
   light('Position',[-0.0 0.0 20]);
   light('Position',[20.0 0.0 -0]);
   light('Position',[-20.0 0.0 -0]);
   material([0.35 0.3 0.90]);
%view(0,-90);
axis equal;axis([-5,5,-5,5,-5,5]);
   drawnow;

       
        k2=k2+1;
     

  %  saveas(h,sprintf('FIG%d.png',k2));
    
    %pause(0.01)
    aa='FIG_';bb=num2str(k2);
    cc='.png';
    cc1=strcat(aa,bb);
    cc2=strcat(cc1,cc);
   %   export_fig 'cc2'
   %eval(['print -dpng -r300 pol_' num2str(k2) '.png']);
   %whitebg('k');
   %eval(['print -dtiff Slice_' num2str(k2) '.tiff']);
   figure(1); hold off;
   
%    figure(4); hold off;
  % figure(6); hold off;

end

end
end
       
       
% k=0;
% for i=1:489;
% for nf=1:4;
%     N=N_var(nf);
%     k=k+N;
%  end;
% end;