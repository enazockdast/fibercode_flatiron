module  initialize_2
  use input_file
  implicit none

  !*************************************************************!
  !************** Simulation Parameters ************************!
  !*************************************************************! 
  integer :: nt_final

  !*************************************************************!
  !************** Fiber Parameters *****************************!
  !*************************************************************! 

  integer, parameter :: n_half=nint((n_s*1.0-1.0)/2.0), m=4
  real (kind=dp),  parameter ::  a_MT=0.002, ds=L0/(1.0*N0-1.0), L=1.0d0
  integer :: N_teta,N_phi, N_total

  real (kind=dp) :: teta0, phi0, teta_i, teta_f, phi_i, phi_f,dteta,dphi
  real (kind=dp) :: xi

  integer, allocatable :: N_var(:), FLAG_P(:)
  real (kind=dp), allocatable, dimension (:) :: L_F,mu0,mu,mu1,mu2,C,C0, imu2,BC_T
  real (kind=dp), allocatable, dimension (:,:) :: dot0, dot2, dot3
  real (kind=dp), allocatable, dimension (:) :: s, dum, teta, phi
  real (kind=dp), allocatable, dimension (:,:) :: F_L
  real (kind=dp), allocatable, dimension (:,:) :: x,y,z,x0,y0,z0, r0, & 
       & dxdt, dydt, dzdt, &
       & ux_ext,uy_ext,uz_ext, &
       & ux_SP,uy_SP,uz_SP,&
       & ux_SP_T, uy_SP_T, uz_SP_T, &
       & ux_SP_B, uy_SP_B, uz_SP_B, &
       & ux_SP_HD_B, uy_SP_HD_B, uz_SP_HD_B, &
       & ux_SP_HD_T, uy_SP_HD_T, uz_SP_HD_T, &
       & ux_SP_HD_B2, uy_SP_HD_B2, uz_SP_HD_B2, &
       & ux_SP_HD_T2, uy_SP_HD_T2, uz_SP_HD_T2, &
       & ux_MT_T,uy_MT_T,uz_MT_T, &
       & ux_self, uy_self, uz_self, &
       & fx_MT_B,fy_MT_B,fz_MT_B,fx_MT_T,fy_MT_T,fz_MT_T, &
       & fx_MT_ext, fy_MT_ext, fz_MT_ext, &
       & fx_2, fy_2, fz_2, &
       & ux_2, uy_2, uz_2, &
       & T 


  real (kind=dp), allocatable, dimension (:,:,:) :: xs0,ys0,zs0,xs,ys,zs, &
       & x_ns0,y_ns0,z_ns0, &
       & x_ns, y_ns, z_ns, & 
       & ux_s,uy_s,uz_s, &
       & xsus,xsuTs,xsus_f, xsus_T, &
       & fx_s, fy_s, fz_s, &
       & fx_MTT_B,fy_MTT_B,fz_MTT_B,fx_MTT_T,fy_MTT_T,fz_MTT_T, &
       & fx_MTT_ext, fy_MTT_ext, fz_MTT_ext, &
       & uxs_MT_T,uys_MT_T,uzs_MT_T, &
       & uxs_SP,uys_SP,uzs_SP, &
       & uxs_SP_B,uys_SP_B,uzs_SP_B, &
       & uxs_SP_T,uys_SP_T,uzs_SP_T, &
       & uxs_SP_HD_B,uys_SP_HD_B,uzs_SP_HD_B, &
       & uxs_SP_HD_T,uys_SP_HD_T,uzs_SP_HD_T, &
       & uxs_SP_HD_B2,uys_SP_HD_B2,uzs_SP_HD_B2, &
       & uxs_SP_HD_T2,uys_SP_HD_T2,uzs_SP_HD_T2, &
       & ux_ext_tr, uy_ext_tr, uz_ext_tr, &
       & uxs_2, uys_2, uzs_2, xsus_2, &  
       & fxss, fsxs, &
       & QX,QY,QZ,QX0,QY0,QZ0, U_EXT,&
       & T_tr, Ts,T_ns    

  real (kind=dp), allocatable, dimension (:,:,:) :: Tt,Tst,&
       & xt, yt, zt, &
       & xst, yst, zst, &
       & QXt, QYt, QZt, &
       & ux_t, uy_t, uz_t

  real (kind=dp), allocatable, dimension (:,:,:) :: Vatt_t, Vatt_t_2
  real (kind=dp), allocatable, dimension (:,:) :: dxc1, dxc1_2,X_att, dxc2, dxc2_2,X_att_2, & 
       & Vatt0,Vatt,Vatt0_2,Vatt_2, Ten_B, Ten_T
  real (kind=dp), allocatable, dimension (:,:) :: Vt,xct
  real (kind=dp), allocatable, dimension (:,:) :: TNt

  real (kind=dp), allocatable, dimension (:) :: F_T,T_T, RHS, computed_solution
  real (kind=dp), allocatable, dimension (:)   :: A_DOT_T
  real (kind=dp), allocatable, dimension (:,:,:) ::ES

  real (kind=dp), allocatable :: C_T0(:,:,:), C_T(:,:,:), C_T_B1(:,:,:),C_T_B2(:,:,:)
  real (kind=dp), allocatable :: c_1T(:,:), c_2T(:,:), c_1(:), c_2(:), c_3(:),time_1(:),time_2(:)


  real (kind=dp), allocatable, dimension (:) :: xns_end, yns_end, zns_end
  real (kind=dp) :: rns_end

  real (kind=dp), allocatable, dimension (:,:) :: sigma_sl, sigma_dl, sigma_dv, &
       & source, targ, pot, pottarg

  real (kind=dp), allocatable, dimension (:,:) :: sigma_sl_2, sigma_dl_2, sigma_dv_2, &
       & source_2, targ_2, pot_2, pottarg_2

  real (kind=dp), allocatable, dimension (:) :: pre, pretarg
  real (kind=dp), allocatable, dimension (:,:,:) :: grad, gradtarg

  real (kind=dp), allocatable, dimension (:) :: pre_2, pretarg_2
  real (kind=dp), allocatable, dimension (:,:,:) :: grad_2, gradtarg_2



  integer, allocatable, dimension (:,:) :: ia_T, ja_T 
  integer, allocatable, dimension (:) ::   ia_D, ja_D
  real (kind=dp), allocatable, dimension (:,:) :: a_T
  real (kind=dp), allocatable, dimension (:) :: a_D

  real (kind=dp), allocatable, dimension (:,:) :: V, V2, O, O2, V_2,V2_2

  integer :: FLAG_T

  !************** Spinle & Cortex Parameters *******************!

  real (kind=dp), dimension (3,1) :: V_T0, V_T, Omega_T0, Omega_T, F_MT, T_MT, V_HD, V_EXT, F_EXT, &
       & V_T0_B, V_T0_T, Omega_T0_B, Omega_T0_T, Omega_EXT, V_HD_B, V_HD_T, Omega_HD_T, Omega_HD_B, &
       & F_MT_T, T_MT_T, F_MT_B, T_MT_B, V_HD_T2, V_HD_B2
  real (kind=dp), parameter, dimension (3) :: e1=(/1.0, 0.0, 0.0/), &
       & e2=(/0.0, 1.0, 0.0/), &
       & e3=(/0.0, 0.0, 1.0/) 
  real (kind=dp) :: xc0(3)
  real (kind=dp), dimension (3,1) :: omega
  real (kind=dp), dimension (3,3) :: E_T,M1,M2, Rx, Ry, Rz, RT
  

  !*************************************************************!
  !*****************Other variables*****************************!
  !*************************************************************!
  integer :: i,j,k,nf, m0, t_step, N, nf2, N2
  integer :: seed(1), put
  real (kind=dp) :: kappa, kappa_e, BCT, t1, t2, alpha, dteta_x, dteta_y,dteta_z
  integer :: ier, iprec, ifsingle, ifdouble, ifgrad, ifpot, iftarg, &
       & ifpottarg, ifgradtarg, nparts, ntarg



end module initialize_2


